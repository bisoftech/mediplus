<?php
/**
 * Created by PhpStorm.
 * User: ss11469
 * Date: 7/2/2015
 * Time: 11:11 PM
 */



use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;



//include all the models required here
use App\User;
use App\Shop;
use App\Branch;
use App\BranchBankDetails;
use App\UserBranchDetails;
use App\Tax;
use App\Distributor;
use App\Product;
use App\PurchaseOrder;
use App\PurchaseOrderItems;
use App\Invoice;
use App\InvoiceItems;
use App\OrderReturn;
use App\Customer;
use App\Billing;
use App\BillingItems;
use App\OperationLog;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        foreach(range(1,30) as $index){

            User::create([
                'first_name' => $faker->firstName,
                'email_id'=>$faker->email,
		'active'=>$faker->boolean(),
		'last_name'=> $faker->lastName,
		'middle_name'=> $faker->firstName,
		'city'=> $faker->city,
		'district'=> $faker->city,
		'state'=> $faker->state,
		'street_address'=> $faker->streetAddress,
		'mobile_number'=> $faker->phoneNumber,
		'pin_code'=> $faker->postcode,
		//'aadhar_number'=> $faker->regon,
		//'pan_number'=> $faker->personalIdentityNumber,
		'user_name'=> $faker->firstName,
		'role'=> $faker->randomElement($array = array('super_admin','admin','employee','pharmacist')),
                'password'=>Hash::make('secret')

		]);
	}
	$user_ids = DB::table('users')->lists('id');
	foreach(range(1,30) as $index)
	{
	    Shop::create([
		    'brand_name' => $faker->firstName,
            
            
		    'super_admin_id' => $faker->randomElement($user_ids)
		]);
	}
	$shop_ids = DB::table('shop')->lists('id');
	foreach(range(1,30) as $index)
	{
	    Branch::create([
		    'branch_name' => $faker->name,
           // 'branch_name'=>'asdf',
		    'registration_number' => $faker->randomNumber,
		    'registration_validity' => $faker->dateTime,
		    'shop_act_number' => $faker->randomNumber,
		    'shop_act_validity' => $faker->dateTime,
		    'vat_number' => $faker->randomNumber,
		    'cst_number' => $faker->randomNumber,
		    'lbt_number' => $faker->randomNumber,
		    'pan_card' => $faker->randomNumber,
		    'district' => $faker->city,
		    'city' => $faker->city,
		    'state' => $faker->state,
		    'pin_code' => $faker->postcode,
		    'address' => $faker->streetAddress,
		    'user_drug_license_number' => $faker->randomNumber,
		    'pharmacist_id' => $faker->randomElement($user_ids),
		    'tin_number' => $faker->randomNumber,
		    'contact_number' => $faker->phoneNumber,
		    'alternate_contact' => $faker->phoneNumber,
		    'fax_number' => $faker->phoneNumber,
		    'shop_id' => $faker->randomElement($shop_ids)
		]);
	}
	$branch_ids = DB::table('branch')->lists('id');
	foreach(range(1,30) as $index)
	{
	    BranchBankDetails::create([
		    'name' => $faker->firstName,
		    'account_number' => $faker->randomNumber,
		    'ifsc_code' => $faker->randomNumber,
		    'account_holder_name' => $faker->name,
		    'contact_number' => $faker->phoneNumber,
		    'email_id' => $faker->email,
		    'branch_id' => $faker->randomElement($branch_ids)
		]);
	}
	foreach(range(1,30) as $index)
	{
	    UserBranchDetails::create([
		    'branch_id' => $faker->randomElement($branch_ids),
		    'user_id' => $faker->randomElement($user_ids)
		]);
	}
	foreach(range(1,30) as $index)
	{
	    Tax::create([
		    'vat_tax' => $faker->randomDigit,
		    'cst_tax' => $faker->randomDigit,
		    'lbt_tax' => $faker->randomDigit,
		    'service_tax' => $faker->randomDigit,
		    'branch_id' => $faker->randomElement($branch_ids)
		]);
	}
	foreach(range(1,30) as $index)
	{
	    Product::create([
		    'name' => $faker->name,
		    'manufacturer' => $faker->name,
		    'formula' => $faker->name,
		    'dosage' => $faker->randomDigit
		]);
	}
	$product_ids = DB::table('product')->lists('id');
	foreach(range(1,30) as $index)
	{
	    Distributor::create([
		    'agency_number' => $faker->randomNumber,
		    'owner_name' => $faker->name,
		    'authorised_distributor' => $faker->name,
		    'tin_number' => $faker->randomNumber,
		    'cst_number' => $faker->randomNumber,
		    'dl_number' => $faker->randomNumber,
		    'credit_period' => $faker->randomDigit,
		    'contact_number' => $faker->phoneNumber,
		]);
	}
	$distributor_ids = DB::table('distributor')->lists('id');
	foreach(range(1,30) as $index)
	{
	    PurchaseOrder::create([
		    'order_date' => $faker->dateTime,
		    'user_id' => $faker->randomElement($user_ids),
		    'distributor_id' => $faker->randomElement($distributor_ids),
		    'branch_id' => $faker->randomElement($branch_ids)
		]);
	}
	$purchase_order_ids = DB::table('purchase_order')->lists('id');
	foreach(range(1,30) as $index)
	{
	    PurchaseOrderItems::create([
		    'quantity' => $faker->randomNumber,
		    'packaging_number' => $faker->randomNumber,
		    'purchase_order_id' => $faker->randomElement($purchase_order_ids),
		    'product_id' => $faker->randomElement($product_ids)
		]);
	}
	foreach(range(1,30) as $index)
	{
	    Invoice::create([
		    'parcel_number' => $faker->randomNumber,
		    'invoice_date' => $faker->dateTime,
		    'user_id' => $faker->randomElement($user_ids),
		    'purchase_order_id' => $faker->randomElement($purchase_order_ids),
            'amount_paid' => $faker->randomNumber,
		    'total_amount' => $faker->randomNumber,
		    'payment_type' => $faker->randomElement($array = array('online','cheque','debit','credit')),
		    'cheque_number' => $faker->randomNumber,
		    'free_items' => $faker->name
		]);
	}
	$invoice_ids = DB::table('invoice')->lists('id');
	foreach(range(1,30) as $index)
	{
	    InvoiceItems::create([
		    'batch_number' => $faker->randomNumber,
		    'quantity' => $faker->randomNumber,
		    'expiry_date' => $faker->dateTime,
		    'mfg_date' => $faker->dateTime,
		    'purchase_price' => $faker->numberBetween($min=100,$max=10000),
		    'selling_price' => $faker->numberBetween($min=300, $max=20000),
		    'mrp' => $faker->numberBetween($min=600, $max=15000),
		    'sell_discount' => $faker->randomDigit,
		    'purchase_discount' => $faker->randomDigit,
		    'margin' => $faker->numberBetween($min=100, $max=10000),
		    'product_id' => $faker->randomElement($product_ids),
		    'invoice_id' => $faker->randomElement($invoice_ids)
		]);
	}
	$invoice_item_ids = DB::table('invoice_items')->lists('id');
	foreach(range(1,30) as $index)
	{
	    OrderReturn::create([
		    'extra_notes' => $faker->name,
		    'return_reason' => $faker->randomElement($array = array('expire','defected')),
		    'quantity' => $faker->randomDigit,
		    'invoice_item_id' => $faker->randomElement($invoice_item_ids)
		]);
	}
	foreach(range(1,30) as $index)
	{
	    Customer::create([
		    'first_name' => $faker->firstName,
		    'email_id'=>$faker->email,
		    'last_name'=> $faker->lastName,
		    'middle_name'=> $faker->firstName,
		    'city'=> $faker->city,
		    'district'=> $faker->city,
		    'state'=> $faker->state,
		    'country'=> $faker->country,
		    'street_address'=> $faker->streetAddress,
		    'contact_number'=> $faker->phoneNumber,
		    'pin_code'=> $faker->postcode,
		    'customer_type'=> $faker->randomElement($array = array('individual','hospital')),
		    'credit_limit' => $faker->numberBetween($min=1000,$max=3000)
		]);
	}
	$customer_ids = DB::table('customer')->lists('id');
	foreach(range(1,30) as $index)
	{
	    Billing::create([
		    'status' => $faker->randomElement($array = array('paid','unpaid','partial_paid')),
		    'is_quotation' => $faker->boolean($chanceOfGettingTrue = 10),
		    'total_amount' => $faker->numberBetween($min=100,$max=20000),
		    'amount_paid' => $faker->numberBetween($min=100,$max=20000),
		    'payment_type' => $faker->randomElement($array = array('online','cheque','debit','credit')),
		    'customer_id' => $faker->randomElement($customer_ids),
		    'user_id' => $faker->randomElement($user_ids)
		]);
	}
	$billing_ids = DB::table('billing')->lists('id');
	foreach(range(1,30) as $index)
	{
	    BillingItems::create([
		    'product_id' => $faker->randomElement($product_ids),
		    'billing_id' => $faker->randomElement($billing_ids),
            'quantity'=> 15
		]);
	}

    foreach(range(1,30) as $index)
	{
	    OperationLog::create([
		    'operation_status' => $faker->randomElement($array = array('success','fail')),
		    'custom_message' => $faker->name,
		    'operation' => $faker->name,
		    'reference_table_id' => $faker->numberBetween($min=1,$max=100),
            'user_id' => $faker->randomElement($user_ids)
		]);
	}
        
    }
}
<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


//include all the models required here
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        Model::unguard();

        //truncate all the tables
        //User::truncate();


        $this->call('UserTableSeeder');
        $this->command->info('User table seeded!');

        //$this->call('UserTableSeeder');
        //$this->command->info('User table seeded!');

        Model::reguard();
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSalesReturn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_return', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
	    $table->softDeletes();
	    //$table->foreign('bill_id')->references('id')->on('billing')->onDelete('cascade')->onUpdate('cascade');
	    //$table->foreign('bill_item_id')->references('id')->on('billing_items')->onDelete('cascade')->onUpdate('cascade');
	    $table->enum('reason',['defected','expired','not-required']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales_return');
    }
}

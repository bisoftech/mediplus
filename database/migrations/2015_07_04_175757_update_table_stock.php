<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock', function (Blueprint $table) {
            //
	    $table->integer('product_id')->unsigned();
        $table->foreign('product_id')->references('id')->on('product');
        $table->integer('quantity');
        $table->integer('invoice_id')->unsigned();
        $table->foreign('invoice_id')->references('id')->on('invoice');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stock', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMapping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mapping', function (Blueprint $table) 
        {
            $table->increments('id');
    	    $table->string('primary_table_name');
	        $table->string('secondary_table_name');
	        $table->string('foreign_key');
	        $table->string('primary_key');
	        $table->string('third_table_name');
	   });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mapping');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediplusRegistrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mediplus_registration', function (Blueprint $table) {
        $table->increments('id');
        $table->timestamps();
	    $table->string('registration_key');
	    $table->dateTime('registration_date');
	    $table->integer('duration');
	    $table->string('extend_on');
	    $table->string('marketing_member');
	    $table->string('plan');
	    $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mediplus_registration');
    }
}

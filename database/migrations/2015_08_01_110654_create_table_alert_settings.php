<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAlertSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alert_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
	    $table->softDeletes();
	    $table->integer('user_id')->unsigned();
	    $table->foreign('user_id')->references('id')->on('users');
	    $table->integer('branch_id')->unsigned();
	    $table->foreign('branch_id')->references('id')->on('branch');
	    $table->boolean('alert_billing_payment_status_unpaid')->nullable;
	    $table->boolean('alert_billing_payment_status_partial_paid')->nullable;
	    $table->boolean('alert_branch_registration_validity')->nullable;
	    $table->boolean('alert_branch_shop_act_validity')->nullable;
	    $table->boolean('alert_customer_credit_limit_exceed')->nullable;
	    $table->boolean('alert_customer_birthday')->nullable;
	    $table->boolean('alert_distributor_credit_period_exceed')->nullable;
	    $table->boolean('alert_invoice_payment_status_unpaid')->nullable;
	    $table->boolean('alert_invoice_payment_status_partial_paid')->nullable;
	    $table->boolean('alert_order_return')->nullable;
	    $table->boolean('alert_sales_return')->nullable;
	    $table->boolean('alert_stock_product_expires')->nullable;
	    $table->boolean('alert_stock_product_out_of_stock')->nullable;
	    $table->boolean('alert_stock_product_low_quantity')->nullable;
	    $table->boolean('alert_user_birthday')->nullable;
	    $table->integer('alert_billing_payment_status_unpaid_period_days')->nullable;
	    $table->integer('alert_billing_payment_status_partial_paid_period_days')->nullable;
	    $table->integer('alert_branch_registration_validity_period_days')->nullable;
	    $table->integer('alert_branch_shop_act_validity_period_days')->nullable;
	    $table->integer('alert_customer_credit_limit_exceed_period_days')->nullable;
	    $table->integer('alert_customer_birthday_period_days')->nullable;
	    $table->integer('alert_distributor_credit_period_exceed_period_days')->nullable;
	    $table->integer('alert_invoice_payment_status_unpaid_period_days')->nullable;
	    $table->integer('alert_invoice_payment_status_partial_paid_period_days')->nullable;
	    $table->integer('alert_order_return_period_days')->nullable;
	    $table->integer('alert_sales_return_period_days')->nullable;
	    $table->integer('alert_stock_product_expires_period_days')->nullable;
	    $table->integer('alert_stock_product_out_of_stock_period_days')->nullable;
	    $table->integer('alert_stock_product_low_quantity_period_days')->nullable;
	    $table->integer('alert_user_birthday_period_days')->nullable;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alert_settings');
    }
}

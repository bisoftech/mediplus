<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBranchBankDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_bank_details', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
	    $table->string('name');
	    $table->string('account_number');
	    $table->string('ifsc_code');
	    $table->string('account_holder_name');
	    $table->string('contact_number');
	    $table->string('email_id');
	    $table->softDeletes();
	    //$table->foreign('branch_id')->references('id')->on('branch')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('branch_bank_details');
    }
}

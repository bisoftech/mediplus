<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableDistributor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('distributor', function (Blueprint $table) {
        $table->string('agency_name')->after('agency_number');   
	    $table->integer('vat_number')->after('cst_number');
        $table->string('expiry_duration')->after('credit_period');
        $table->string('other_details')->after('contact_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('distributor', function (Blueprint $table) {
            //
        });
    }
}

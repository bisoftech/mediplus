<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePurchaseOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
	    $table->integer('quantity');
	    $table->string('packaging_number');
	    $table->softDeletes();
	    //$table->foreign('purchase_order_id')->references('id')->on('purchase_order')->onDelete('cascade')->onUpdate('cascade');
	    //$table->foreign('product_id')->references('id')->on('product')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchase_order_items');
    }
}

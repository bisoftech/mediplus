<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDistributor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributor', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
	    $table->string('agency_number');
	    $table->string('owner_name');
	    $table->string('authorised_distributor');
	    $table->string('tin_number');
	    $table->string('cst_number');
	    $table->string('dl_number');
	    $table->integer('credit_period');
	    $table->string('contact_number');
	    $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('distributor');
    }
}

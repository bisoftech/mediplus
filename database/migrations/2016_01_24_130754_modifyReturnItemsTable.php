<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ModifyReturnItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
         Schema::table('purchase_order_return_items', function (Blueprint $table) {
            $table->integer('purchase_order_return_id')->unsigned();
            $table->foreign('purchase_order_return_id')->references('id')->on('purchase_order_return');
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_order_return_items', function (Blueprint $table) {
            $table->dropColumn('purchase_order_return_id');
        });
    }
}

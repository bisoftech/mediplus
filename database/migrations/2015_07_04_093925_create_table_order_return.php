<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrderReturn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_return', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
	    $table->string('extra_notes');
	    $table->enum('return_reason',['expire','defected']);
	    $table->integer('quantity');
	    $table->softDeletes();
	    //$table->foreign('invoice_item_id')->references('id')->on('invoice_items')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_return');
    }
}

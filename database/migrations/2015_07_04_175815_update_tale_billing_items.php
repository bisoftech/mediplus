<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTaleBillingItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billing_items', function (Blueprint $table) {
            //
	    $table->integer('product_id')->unsigned();
	    $table->integer('billing_id')->unsigned();
        $table->integer('quantity')->unsigned();
	    $table->foreign('billing_id')->references('id')->on('billing');
	    $table->foreign('product_id')->references('id')->on('product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billing_items', function (Blueprint $table) {
            //
        });
    }
}

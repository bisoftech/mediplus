<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInvoice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->timestamps();
	        $table->string('parcel_number');
	        $table->softDeletes();
	        $table->dateTime('invoice_date');
            $table->integer('total_amount');
	        $table->integer('amount_paid');
	        $table->enum('payment_type',['online','cheque','debit','credit']);
	        $table->string('cheque_number');
	        $table->string('free_items');
            //$table->foreign('purchase_order_id')->references('id')->on('purchase_order')->onDelete('cascade')->onUpdate('cascade');
	    //$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoice');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTaleSalesReturn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_return', function (Blueprint $table) {
            //
	    $table->integer('bill_item_id')->unsigned();
	    $table->integer('bill_id')->unsigned();
	    $table->foreign('bill_id')->references('id')->on('billing');
	    $table->foreign('bill_item_id')->references('id')->on('billing_items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_return', function (Blueprint $table) {
            //
        });
    }
}

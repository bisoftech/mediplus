<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBranch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
	    $table->string('branch_name');
	    $table->string('registration_number');
	    $table->dateTime('registration_validity');
	    $table->string('shop_act_number');
	    $table->dateTime('shop_act_validity');
	    $table->string('vat_number');
	    $table->string('cst_number');
	    $table->string('lbt_number');
	    $table->string('pan_card');
	    $table->string('district');
	    $table->string('city');
	    $table->string('pin_code');
	    $table->string('state');
	    $table->string('address');
	    $table->string('user_drug_license_number');
	    $table->string('pharmacist_id');
	    $table->string('tin_number');
	    $table->string('contact_number');
	    $table->string('alernate_contact');
	    $table->string('fax_number');
	    $table->softDeletes();
	    //$table->foreign('shop_id')->references('id')->on('shop')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('branch');
    }
}

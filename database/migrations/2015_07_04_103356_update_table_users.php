<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
	    //
	    //$table->renameColumn('name','first_name');
	    $table->dropColumn('email');
	    $table->dropColumn('name');
	    $table->string('first_name');
	    $table->string('middle_name');
	    $table->string('last_name');
	    $table->string('email_id');
	    $table->string('city');
	    $table->string('district');
	    $table->string('state');
	    $table->string('street_address');
	    $table->integer('mobile_number');
	    $table->integer('pin_code');
	    $table->string('aadhar_number');
	    $table->string('pan_number');
	    //$table->string('user_name');
	    $table->softDeletes();
	    $table->enum('role',['super_admin','admin','employee','pharmacist']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}

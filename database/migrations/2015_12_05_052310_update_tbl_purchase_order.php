<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblPurchaseOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_order', function (Blueprint $table) {
	    $table->enum('status',['Active','Inactive']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_order', function (Blueprint $table) {
            //
	    $table->integer('user_id')->unsigned();
	    $table->integer('distributor_id')->unsigned();
	    $table->foreign('distributor_id')->references('id')->on('distributor');
	    $table->foreign('user_id')->references('id')->on('users');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBilling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
	    $table->integer('total_amount');
	    $table->enum('status',['paid','unpaid','partial_paid']);
	    $table->boolean('is_quotation');
	    $table->integer('amount_paid');
	    $table->enum('payment_type',['online','cheque','debit','credit']);
	    $table->softDeletes();
	    //$table->foreign('customer_id')->references('id')->on('customer')->onDelete('cascade')->onUpdate('cascade');
	    //$table->foreign('billing_item_id')->references('id')->on('billing_items')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('billing');
    }
}

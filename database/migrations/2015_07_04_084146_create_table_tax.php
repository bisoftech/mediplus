<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTax extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
	    $table->decimal('vat_tax',5,2);
	    $table->decimal('cst_tax',5,2);
	    $table->decimal('lbt_tax',5,2);
	    $table->decimal('service_tax',5,2);
	    $table->softDeletes();
	    //$table->foreign('branch_id')->references('id')->on('branch')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tax');
    }
}

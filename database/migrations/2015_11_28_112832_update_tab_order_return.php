<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTabOrderReturn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_return', function (Blueprint $table) {
	    $table->integer('product_id')->unsigned();
	    //$table->foreign('purchase_order_id')->references('id')->on('purchase_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_return', function ($table) {
        $table->dropColumn('purchase_order_id');
        });
    }
}

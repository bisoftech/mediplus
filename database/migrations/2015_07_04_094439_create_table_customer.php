<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
	    $table->string('first_name');
	    $table->string('middle_name');
	    $table->string('last_name');
	    $table->string('city');
	    $table->string('district');
	    $table->string('state');
	    $table->string('country');
	    $table->string('street_address');
	    $table->integer('pin_code');
	    $table->string('email_id');
	    $table->dateTime('dob');
	    $table->string('contact_number');
	    $table->integer('credit_limit');
	    $table->enum('customer_type',['individual','hospital']);
	    $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer');
    }
}

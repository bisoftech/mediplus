<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePurchaseOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
	    $table->softDeletes();
	    //$table->foreign('distributor_id')->references('id')->on('distributor')->onDelete('cascade')->onUpdate('cascade');
	    //$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
	    $table->dateTime('order_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchase_order');
    }
}

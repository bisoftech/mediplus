<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOperationLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operation_log', function (Blueprint $table) 
        {
           $table->increments('id');
           $table->timestamps();
	       $table->integer('user_id');
           $table->string('operation');
           $table->integer('reference_table_id');
	       $table->enum('operation_status',['success','fail']);
           $table->string('custom_message');    
	       $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

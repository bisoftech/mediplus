<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInvoiceItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_items', function (Blueprint $table) 
        {
            $table->increments('id');
            $table->timestamps();
	        $table->string('batch_number');
	        $table->integer('quantity');
	        $table->dateTime('expiry_date');
	        $table->dateTime('mfg_date');
	        $table->integer('purchase_price');
	        $table->integer('selling_price');
	        $table->integer('mrp');
	        $table->decimal('sell_discount',3,2);
	        $table->decimal('purchase_discount',3,2);
	        $table->integer('margin');
	        $table->softDeletes();
	    //$table->foreign('product_id')->references('id')->on('product')->onDelete('cascade')->onUpdate('cascade');
	    //$table->foreign('invoice_id')->references('id')->on('invoice')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoice_items');
    }
}

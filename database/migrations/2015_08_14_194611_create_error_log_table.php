<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateErrorLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

         Schema::create('DAW_Error_Log', function (Blueprint $table) 
        {
           $table->increments('id');
           $table->integer('user_id')->nullable();
           $table->longText('stack_tace');
           $table->integer('line_no');
           $table->text('file');
           $table->text('short_message');
           $table->enum('error_type',['php','browser','different']);
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('DAW_Error_Log');
    }
}

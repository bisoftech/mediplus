<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserBranchDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_branch_details', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
	    $table->softDeletes();
	    $table->integer('branch_id')->unsigned();
	    $table->foreign('branch_id')->references('id')->on('branch');
	    $table->integer('user_id')->unsigned();
	    $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_branch_details');
    }
}

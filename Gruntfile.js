'use strict';
var path = require('path');
function absolutePath(file) {
  return path.join(__dirname, file);
}
module.exports = function (grunt) {
  grunt.initConfig({
    jshint: {
      all: [
        'Gruntfile.js',
        'tasks/*.js',
        '<%= nodeunit.tests %>'
      ]
    },
    clean: {
      test: ['tmp']
    },
    cssmin: {
      options: {
        sourceMap: true
      },
      absolute: {
        files: [{
          src: [
            '/public/styles/thirdParty/metisMenu.min.css',
            '/public/styles/thirdParty/bootstrap.min.css',
            '/public/styles/thirdParty/xeditable.min.css',
            '/public/styles/thirdParty/angucomplete.min.css',
            '/public/styles/thirdParty/angular-toastr.min.css',
            '/public/styles/thirdParty/ngDialog.min.css',
            '/public/styles/thirdParty/ngDialog-theme-default.css',
            '/public/styles/thirdParty/angular-datepicker.min.css',
            '/public/styles/thirdParty/search.css',
            '/public/styles/thirdParty/bootstrap-multiselect.css',
            '/public/styles/thirdParty/bootstrap-select.min.css',
            '/public/styles/thirdParty/jquery.selectBox.css',
            '/public/styles/thirdParty/bootstrap-switch.min.css',
            '/public/styles/thirdParty/main.css',
            '/public/styles/thirdParty/stickyheader.css',
            '/public/styles/thirdParty/jquery-ui.css',
            '/public/styles/main.css',
            '/public/styles/thirdParty/multiple-select.css',
            '/public/styles/thirdParty/bootstrap-multiselect.css',
            '/public/styles/thirdParty/select.min.css',
            '/public/styles/thirdParty/selectize.default.css',
            '/public/styles/thirdParty/nv.d3..min.css'
            
          ].map(absolutePath),
          dest: absolutePath('public/tmp/absolute.min.css')
        }]
      }
    }
  });

  grunt.loadTasks('tasks');
 grunt.loadNpmTasks('grunt-contrib-cssmin');
};
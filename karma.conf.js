// Karma configuration
// Generated on Sat Jan 09 2016 12:01:46 GMT+0530 (India Standard Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
    'node_modules/angular/angular.js',
    'node_modules/angular-mocks/angular-mocks.js',

    /*
    'public/scripts/app/module.app.js',
    'public/scripts/app/services/apiUtility.js',
    'public/scripts/app/services/userService.js',
    'public/scripts/app/features/home/*.js'
    */

    'public/scripts/third-party/jquery-1.11.3.min.js',
    'public/scripts/third-party/angular-ui-router.js',
    'public/scripts/third-party/xeditable.js',
    'public/scripts/third-party/xeditable.min.js',
    'public/scripts/third-party/spin.min.js',
    'public/scripts/third-party/angular-spinner.js',
    'public/scripts/third-party/ui-bootstrap-tpls-0.13.3.min.js',
    'public/scripts/third-party/angular-animate.min.js',
    'public/scripts/third-party/bootstrap-switch.min.js',
    'public/scripts/third-party/jquery-ui.js',
    'public/scripts/third-party/select.min.js',
    'public/scripts/third-party/focusif.js',
    'public/scripts/third-party/Chart.min.js',
    'public/scripts/third-party/angular-chart.js',
    'public/scripts/third-party/hotkeys.min.js',
    'public/scripts/third-party/bootstrap.min.js',
    'public/scripts/third-party/ng-csv.min.js',
    'public/scripts/third-party/angular-sanitize.min.js',
    'public/scripts/third-party/isteven-multi-select.js',

    'public/scripts/app/module.app.js',
    'public/scripts/app/services/apiUtility.js',
    'public/scripts/app/services/userService.js',
    'public/scripts/third-party/angucomplete.js',
    'public/scripts/third-party/angular-toastr.tpls.min.js',
    'public/scripts/third-party/multiple.js',
    'public/scripts/third-party/nprogress.js',
    'public/scripts/third-party/toastr.js',
    'public/scripts/third-party/toastr.tpl.js',
    'public/scripts/third-party/angular-permission.js',

    'public/scripts/third-party/ngDialog.min.js',

    'public/scripts/third-party/date.js',
    'public/scripts/third-party/bootstrap-select.min.js',
    'public/scripts/third-party/jquery.selectBox.js',
    'public/scripts/third-party/angular-growl-notifications.min.js',
    'public/scripts/third-party/jquery.stickyheader.js',
    /*CTRL*/
    'public/scripts/app/features/user/userCtrl.js',
    'public/scripts/app/features/customer/customerCtrl.js',
    //'public/scripts/app/features/home/homeCtrl.js',
    'public/scripts/app/features/home/*.js',
    'public/scripts/app/features/admin/adminCtrl.js',
    'public/scripts/app/features/products/prodCtrl.js',
    'public/scripts/app/features/distributors/distCtrl.js',
    'public/scripts/app/features/PurchaseOrder/poCtrl.js',
    'public/scripts/app/features/report/reportCtrl.js',
    'public/scripts/app/features/PurchaseInvoice/piCtrl.js',
    'public/scripts/app/features/stock/stockCtrl.js',
    'public/scripts/app/features/alert/alertCtrl.js',
    'public/scripts/app/features/bank/bankCtrl.js',
    'public/scripts/app/features/Return/returnCtrl.js',
    'public/scripts/app/directives/header.js',
    'public/scripts/app/features/userBranch/userBranchController.js',
    'public/scripts/app/features/MainCtrl.js',

    'public/scripts/third-party/bootstrap-multiselect.js',
    'public/scripts/third-party/bootstrap-multiselect-collapsible-groups.js',
    'public/scripts/third-party/angularjs-dropdown-multiselect.js',
    'public/scripts/third-party/d3.min.js',
    'public/scripts/third-party/nv.d3.min.js',
    'public/scripts/third-party/angular-nvd3.js'

    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
})
}

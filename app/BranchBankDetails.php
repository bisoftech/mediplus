<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\MediPlus\Transformers\BranchBankDetailsTransformer as BranchBankDetailsTransformer;
use App\MediPlus\Transformers\BranchTransformer as BranchTransformer;
use App\MediPlus\Transformers\ShopTransformer as ShopTransformer;
use App\Http\Requests;
use App\MediPlus\Pagination\Paginations;
use Illuminate\Database\Eloquent\SoftDeletes;
use Input;
use DB;
use Auth;
use App\Branch;
use App\Shop;
use App\User;
use App\OperationLog;
use App\UserBranchDetails;
use Session;

class BranchBankDetails extends Model
{
    use SoftDeletes;
    protected $table = 'branch_bank_details';
    
    //This function is use to assign heading for standard reports.  
    public $reportColumnTitle = array('Sr.No', 'Name', 'Account Number', 'IFSC Code', 'Account Holder Name', 'Contact Number', 'Email Id', 'Branch Name');
    
    //This function is use to get all data.
    public function getAll()
    {
        $resp = array();
        try 
        {
            $limit     = Input::get('limit')?:10;
            $branchId  = Session::get('branch_id');
            $results   = BranchBankDetails::where('branch_id', '=' , $branchId)->orderBy('id', 'DESC')->paginate($limit);
            $branchBankDetailsTransformer = new BranchBankDetailsTransformer;
            $branchTransformer  = new BranchTransformer;
            $branchbankdetails  = $branchBankDetailsTransformer->transformCollection($results);
            $resp['success']    = true;
            $resp['data']       = $branchbankdetails; 
            $resp['pagination'] = (new Paginations)->results($results);
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Bank Branch Details Not Available";
            $resp['ex']      = $e->getMessage();
        }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all branch bank details from table branch_bank_details successfully');
            (new OperationLog)->add($operationLog);
        return $resp;
    }
    
    //This function is use to get details by Id.
    public function getById($id)
    {    
       $resp = array();
       try
       {
            $branchId                     = Session::get('branch_id');
            $branchbankdetails            = BranchBankDetails::Where('branch_id', '=', $branchId)->findOrFail($id);
            $branchBankDetailsTransformer = new BranchBankDetailsTransformer;
            $branchTransformer            = new BranchTransformer;
            $resp['success']              = true;
            $resp['data']                 = $branchBankDetailsTransformer->transform($branchbankdetails);
            $operationLog=array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'success',
                                "customMessage"    => 'user id '.Auth::User()->id.' view branch bank details in table branch_bank_details with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Branch Bank Details Not Available";
            $resp['ex']      = $e->getMessage();
            $operationLog=array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'fail',
                                "customMessage"    => 'user id '.Auth::User()->id.' view branch bank details in table branch_bank_details with id '.$id.' failure');
            (new OperationLog)->add($operationLog);
       }
            
        return $resp;
    }
    
    //This function is use to add details.
    public function add()
    {
       $resp = array();
       try
       {
            $arr             = (new BranchBankDetailsTransformer)->reverseTransform(Input::all());
            $branchBankId    = BranchBankDetails::insertGetId($arr); 
            $resp['success'] = true;
            $resp['data']    = (new BranchBankDetailsTransformer)->transform(BranchBankDetails::find($branchBankId));
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => $branchBankId,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table branch_bank_details with id '.$branchBankId.' successfully');
            (new OperationLog)->add($operationLog);
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Branch Bank Details Added Failure";
            $resp['ex']      = $e->getMessage();
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => 0,
                                  "operationStatus"  => 'fail',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table branch_bank_details failure');
            (new OperationLog)->add($operationLog);
       }
            
       return $resp;
    }
    
    //This function is use to update details by Id.
    public function updateById($id)
    {
       $resp = array();
       try
       {
            $arr = (new BranchBankDetailsTransformer)->reverseTransform(Input::all());
            BranchBankDetails::where('id',$id)->update($arr);
            BranchBankDetails::findOrFail($id)->save();
            $resp['success'] = true;
            $resp['data']    = (new BranchBankDetailsTransformer)->transform(BranchBankDetails::findOrFail($id));
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Branch Bank Details Not Available";
            $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }

    //This function is use to delete details by Id.
    public function deleteById($id)
    {
       $resp = array();
       try
       {
            $branchBankDetails = BranchBankDetails::findOrFail($id);
            $branchBankDetails->delete();
            $limit   = Input::get('limit')?:10;
            $results = $branchBankDetails::paginate($limit);
            $branchBankDetailsTransformer = new BranchBankDetailsTransformer;
            $resp['success']    = true;
            $resp['data']       = $branchBankDetailsTransformer->transform($branchBankDetails);
            $resp['pagination'] = (new Paginations)->results($results);      
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Branch Bank Details Not Available";
            $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }

    //This function is use to show details by branch Id.
    public function getByBranchId($branchId)
    {
        $resp = array();
        try 
        {
            $limit   = Input::get('limit')?:10;
            $results = BranchBankDetails::where('branch_id', '=' , $branchId)->paginate($limit);
            $branchBankDetailsTransformer = new BranchBankDetailsTransformer;
            $branchTransformer  = new BranchTransformer;
            $branchbankdetails  = $branchBankDetailsTransformer->transformCollection($results);
            $resp['success']    = true;
            $resp['data']       = $branchbankdetails; 
            $resp['pagination'] = (new Paginations)->results($results);
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Bank Branch Details Not Available";
            $resp['ex']      = $e->getMessage();
        }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all branch bank details from table branch_bank_details successfully');
            (new OperationLog)->add($operationLog);
        return $resp;
    }
    
    //This function is use to search details for any keyword.
    public function autocomplete($data)
    {
       $resp = array();
       try
       {
            $limit           = Input::get('limit')?:10;
            $resp['success'] = true;
            $dataBranchBankDetails = BranchBankDetails::where('name', 'like', '%'.$data.'%')
                                                        ->orWhere('account_number', 'like', '%'.$data.'%')
                                                        ->orWhere('ifsc_code', 'like', '%'.$data.'%')
                                                        ->orWhere('account_holder_name', 'like', '%'.$data.'%')
                                                        ->orWhere('contact_number', 'like', '%'.$data.'%')
                                                        ->orWhere('email_id', 'like', '%'.$data.'%')
                                                        ->paginate($limit);
            $resp['data']       = (new BranchBankDetailsTransformer)->transformCollection($dataBranchBankDetails);
            $resp['pagination'] = (new Paginations)->results($dataBranchBankDetails);
            $resp['msg']        = "Branch Bank Details Information";     
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Branch Bank Details Not Available"; 
            $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }
    
    //This function is use to get all details in-beetween of two dates.
    public function history()
    {
        $resp        = array();
        $limit       = Input::get('limit')?:10;
        $fromDate    = Input::get('fromDate');
        $toDate      = Input::get('toDate');
        $from        = date("Y-m-d H:i:s", strtotime($fromDate));
        $to          = date("Y-m-d H:i:s", strtotime($toDate)); 
        $branchBankDeails = BranchBankDetails::whereBetween('created_at',array($from, $to))->paginate($limit);
        if($fromDate!='' && $toDate!='')
        {
            $branchBankDetailsTransformer = new BranchBankDetailsTransformer;
            $resp['success']        = "true";
            $resp['msg']            = "Branch Bank Details Available From search date";
            $resp['data']           = $branchBankDetailsTransformer->transformCollection($branchBankDeails); 
            $resp['pagination']     = (new Paginations)->results($branchBankDeails);
        }
        else
        {
            $resp['success'] = "false";
            $resp['msg']     = "Please Select date";
        }
        return $resp;
    }
    
    //This function is use to get all details.
    //This function is use for standard reports.
    public function getDataForReports($fromDate, $toDate, $myRequestObject)
    {
        $table = BranchBankDetails::join('branch', 'branch_bank_details.branch_id', '=', 'branch.id')
                                    ->select('branch_bank_details.*', 'branch.branch_name')
                                    ->whereBetween('branch_bank_details.created_at',array($fromDate, $toDate))->get();
       $reportName  = new BranchBankDetails;
       $branchBankDetailsData = array();
       foreach($table as $row) 
       {            
           array_push($branchBankDetailsData, array($row['id'], $row['name'], $row['account_number'], $row['ifsc_code'], $row['account_holder_name'], $row['contact_number'], $row['email_id'],                                                                     $row['branch_name']));
       }
       return $branchBankDetailsData;
    }
}
<?php


namespace App\MediPlus\Pagination;

class Paginations
{
   public function results($results)
   {
       return array('currentPage'=>$results->currentPage(),
            'hasMorePages'=>$results->hasMorePages(),
            'lastPage'=>$results->lastPage(),
            'nextPageUrl'=>$results->nextPageUrl(),
            'perPage'=>$results->perPage(),
            'total'=>$results->total());
   }
}

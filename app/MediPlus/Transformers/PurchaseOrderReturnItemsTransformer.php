<?php
namespace App\MediPlus\Transformers;

use App\Invoice;
use App\InvoiceItems;
use App\MediPlus\Transformers\InvoiceTransformer;
use App\MediPlus\Transformers\InvoiceItemsTransformer;
use App\MediPlus\Transformers\PurchaseOrderTransformer;
use App\MediPlus\Transformers\ProductTransformer;
use App\MediPlus\Transformers\BranchTransformer;
use App\PurchaseOrder;
use App\Product;
use App\Branch;
use DB;

class PurchaseOrderReturnItemsTransformer extends Transformer
{
    //This function is use for show functionality.
    public function transform($data,$show=true)
    {
        if($show==true)
        {
            return[
                'id'                => $data['id'],
                'createdAt'         => $data['created_at'],
                'updatedAt'         => $data['updated_at'],
                'deletedAt'         => $data['deleted_at'],
                'returnDate'        => date('d-m-Y H:i:s', strtotime($data['return_date'])),
                'extraNotes'        => $data['extra_notes'],
                'returnReason'      => $data['return_reason'],
                'quantity'          => $data['quantity'],
                'productId'         => $data['product_id'],
                //Get product name by product Id.
                'productName'       => (new Product)->getProductName($data['product_id']),
                //Get product information (deleted product information also show)         
                'product'           => (new ProductTransformer)->Transform(Product::withTrashed()->find($data['product_id'])),
                'invoiceItemId'     => $data['invoice_item_id'],
                //Get invoice information.
                'InvoiceDetails'    => (new InvoiceItemsTransformer)->transform(InvoiceItems::find($data['invoice_item_id'])),
                //Get branch information (deleted branch information also show)
                'branch'            => (new BranchTransformer)->transform(Branch::withTrashed()->find($data['branch_id'])),
                'branchId'            => $data['branch_id'],
                'status'            => $this->getStatus($data['status']),
                'replaceInvoiceId'  => $data['replace_invoice_id'],
                'purchaseOrderReturnId'=> $data['purchase_order_return_id']
                ];
        }
        else
        {
            return[
                'id'                => $data['id'],
                'createdAt'         => $data['created_at'],
                'updatedAt'         => $data['updated_at'],
                'deletedAt'         => $data['deleted_at'],
                'returnDate'        => date('d-m-Y', strtotime($data['return_date'])),
                'extraNotes'        => $data['extra_notes'],
                'returnReason'      => $data['return_reason'],
                'quantity'          => $data['quantity'],
                'productId'         => $data['product_id'],
                //Get product name by product Id.
                'productName'       => (new Product)->getProductName($data['product_id']),  
                //Get product information (deleted product information also show)       
                'product'           => (new ProductTransformer)->Transform(Product::withTrashed()->find($data['product_id'])),
                'invoiceItemId'     => $data['invoice_item_id'],
                //Get branch name by branch Id.
                'branchName'        => (new Branch)->getBranchName($data['branch_id']),
                'status'            => $data['status'],
                'replaceInvoiceId'  => $data['replace_invoice_id'],
                'purchaseOrderReturnId' => $data['purchase_order_return_id']
                       ];  
        }
    }

    //Show status for dispatched purchase order items.
    public function getStatus($status){
        switch($status){
            case 'inprogress':
                return 'In Progress';
            default:
                return $status;
            }
    }

    //This function is use for add functionality.
    public function reverseTransform($data,$obj)
    {
        $obj->id=isset($data['id'])?$data['id']:null;
        $obj->created_at=date('Y-m-d H:i:s');
        $obj->return_date=date('Y-m-d H:i:s');
        $obj->extra_notes=$data['extraNotes'];
        $obj->return_reason=$data['returnReason'];
        $obj->quantity=$data['quantity'];
        $obj->invoice_item_id=$data['invoiceItemId'];
        $obj->product_id=$data['productId'];
        $obj->branch_id=$data['branchId'];
        $obj->purchase_order_return_id=$data['purchaseOrderReturnId'];
        $obj->status=isset($data['status'])?$data['status']=='In Progress'?'Inprogress':$data['status']:'Inprogress';
        $obj->replace_invoice_id=isset($data['replaceInvoiceId'])?$data['replaceInvoiceId']:'';
        return $obj;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: ss11469
 * Date: 7/3/2015
 * Time: 12:03 AM
 */

namespace App\MediPlus\Transformers;
use App\MediPlus\Transformers\ProductTransformer;
use App\Product;

class PurchaseOrderItemsTransformer extends Transformer
{
    //This function is use for show functionality.
    public function transform($PurchaseOrderItems,$show=true)
    {
        if($show==true)
        {
            return [
            'id'                => $PurchaseOrderItems['id'],
            'createdAt'         => $PurchaseOrderItems['created_at'],
            'updatedAt'         => $PurchaseOrderItems['updated_at'],
            'quantity'          => $PurchaseOrderItems['quantity'],
            'packagingNumber'   => $PurchaseOrderItems['packaging_number'],
            'deletedAt'         => $PurchaseOrderItems['deleted_at'],
            'purchaseOrderId'   => $PurchaseOrderItems['purchase_order_id'],
            //Get product name by product Id.
            'productName'       => (new Product)->getProductName($PurchaseOrderItems['product_id']),         
            //Get product information (deleted product information also show)
            'product'           => (new ProductTransformer)->Transform(Product::withTrashed()->find($PurchaseOrderItems['product_id'])),
            ];
        }
        else
        {
            return [
            'id'                => $PurchaseOrderItems['id'],
            'createdAt'         => $PurchaseOrderItems['created_at'],
            'updatedAt'         => $PurchaseOrderItems['updated_at'],
            'quantity'          => $PurchaseOrderItems['quantity'],
            'packagingNumber'   => $PurchaseOrderItems['packaging_number'],
            'deletedAt'         => $PurchaseOrderItems['deleted_at'],
            'purchaseOrderId'   => $PurchaseOrderItems['purchase_order_id'], 
            'productId'         => $PurchaseOrderItems['product_id'],
            //Get product name by product Id.
            'productName'       => (new Product)->getProductName($PurchaseOrderItems['product_id']),     
            //Get product information (deleted product information also show)
            'product'           => (new ProductTransformer)->Transform(Product::withTrashed()->find($PurchaseOrderItems['product_id'])),
            ];
        }   
    }

    //This function is use for add functionality.
    public function reverseTransform($data,$obj,$id)
    {
            $obj->quantity            = $data['quantity'];
            $obj->purchase_order_id   = $id;
            $obj->product_id          = $data['productId'];
            $obj->save();
            return $obj;
    }
}
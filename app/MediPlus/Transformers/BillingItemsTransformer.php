<?php
/**
 * Created by PhpStorm.
 * User: ss11469
 * Date: 7/3/2015
 * Time: 12:03 AM
 */

namespace App\MediPlus\Transformers;

use App\Product;
use App\MediPlus\Transformers\StockTransformer;
use App\Stock;
use DB;
use Log;

class BillingItemsTransformer extends  Transformer
{
    //This function is use for show functionality.
    public  function transform($data,$show=true)
    {
        if($show == true)
        {
            return[
                'id'            => $data['id'],
                'createdAt'     => $data['created_at'],
                'updatedAt'     => $data['updated_at'],
                'deletedAt'     => $data['deleted_at'],
                'productId'     => $data['product_id'],
                'billingId'     => $data['billing_id'],
                'quantity'      => $data['quantity'],
                'perUnit'       => $data['perUnit'],
                //Get all product information (deleted product details information also show) 
                'product'       => (new ProductTransformer)->transform(Product::withTrashed()->find($data['product_id'])),
                ];
        }
        else
        {
            return[
                'id'            => $data['id'],
                'createdAt'     => $data['created_at'],
                'updatedAt'     => $data['updated_at'],
                'deletedAt'     => $data['deleted_at'],
                'productId'     => $data['product_id'],
                'billingId'     => $data['billing_id'],
                'quantity'      => $data['quantity'],
                'perUnit'       => $data['perUnit'],
                //Get product information (deleted product information also show) 
                'product'       => (new ProductTransformer)->transform(Product::withTrashed()->find($data['product_id'])),
                ];
        }
    }
    
    //This function is use for add functionality.
    public function reverseTransform($data,$obj,$id)
    {   
            $obj->product_id = $data['productId'];
            $obj->quantity   = $data['quantity'];
            $obj->billing_id = $id;
            $obj->stock_id   = $data['stockId'];
            $obj->perUnit    = $data['perUnit'];
            $obj->save();
            return $obj;
        }   
}
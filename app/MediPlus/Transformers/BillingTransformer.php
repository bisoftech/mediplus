<?php
/**
 * Created by PhpStorm.
 * User: ss11469
 * Date: 7/3/2015
 * Time: 12:03 AM
 */

namespace App\MediPlus\Transformers;
use App\Customer;
use App\User;
use App\MediPlus\Transformers\BillingItemsTransformer as BillingItemsTransformer;
use App\MediPlus\Transformers\CustomerTransformer as CustomerTransformer;
use App\MediPlus\Transformers\UserTransformer as UserTransformer;
use App\MediPlus\Transformers\BranchTransformer as BranchTransformer;
use App\Branch;
use Log;

class BillingTransformer extends  Transformer
{
    //This function is use for show functionality.
    public  function transform($data,$show=true)
    {
        $billingItemsTransformer    = new BillingItemsTransformer;
        $customerTransformer        = new CustomerTransformer;
        $userTransformer            = new UserTransformer;
        $branchTransformer          = new BranchTransformer;
        
        if($show== true)
        {
                $tempData=[
                'id'                => $data['id'],
                'createdAt'         => $data['created_at'],
                'updatedAt'         => $data['updated_at'],
                'totalAmount'       => $data['total_amount'],
                'status'            => $data['status'],
                'isQuotation'       => $data['is_quotation'],
                'amountPaid'        => $data['amount_paid'],
                'discount'          => $data['discount'],
                'paymentType'       => $data['payment_type'],
                'chequeNumber'      => $data['cheque_number'],
                'deletedAt'         => $data['deleted_at'],
                'customer'          => $data['customer_id'], 
                //Get customers information (deleted customers information also show) 
                'customer'          => $customerTransformer->transform(Customer::withTrashed()->find($data['customer_id'])),
                //Get user information (deleted user information also show) 
                'user'              => $userTransformer->transform(User::withTrashed()->find($data['user_id'])),
                //Get billing items information. 
                'BillingItems'      => (new BillingItemsTransformer)->transformCollection($data->BillingItems),
                //Get branch information (deleted branch information also show) 
                'branch'            => $branchTransformer->transform(Branch::withTrashed()->find($data['branch_id'])),     
                ];
           
            return $tempData;
        }
        else
        {
            return[
                'id'                => $data['id'],
                'createdAt'         => $data['created_at'],
                'updatedAt'         => $data['updated_at'],
                'totalAmount'       => $data['total_amount'],
                'status'            => $data['status'],
                'isQuotation'       => $data['is_quotation'],
                'amountPaid'        => $data['amount_paid'],
                'discount'          => $data['discount'],
                'paymentType'       => $data['payment_type'],
                'chequeNumber'      => $data['cheque_number'],
                'deletedAt'         => $data['deleted_at'],
                'customerId'        => $data['customer_id'],
                'userId'            => $data['user_id'],
                //Get customer name by customer Id.
                'customerName'      => (new Customer)->getCustomerName($data['customer_id']),
                //Get user name by user Id.
                'userName'          => (new User)->getUserName($data['user_id']),
                //Get branch name by branch Id.
                'branchName'        => (new Branch)->getBranchName($data['branch_id']),
                ];     
        }
    }
    
    //This function is use for add functionality.
    public function reverseTransform($data,$obj)
    {
            $obj->total_amount  = $data['totalAmount'];
            $obj->status        = $data['status'];
            $obj->is_quotation  = 1;
            $obj->amount_paid   = $data['amountPaid'];
            $obj->discount      = $data['discount'];
            $obj->payment_type  = $data['paymentType'];
            $obj->cheque_number = $data['chequeNumber'];
            $obj->customer_id   = $data['customerId'];
            $obj->user_id       = $data['userId'];
            $obj->branch_id     = $data['branchId'];
            $obj->save();
            return $obj;
    }
}
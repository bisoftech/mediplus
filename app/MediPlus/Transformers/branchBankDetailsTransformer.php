<?php
namespace App\MediPlus\Transformers;

use App\Branch;
use App\MediPlus\Transformers\PurchaseOrderItemsTransformer;
use App\MediPlus\Transformers\BranchTransformer;

class BranchBankDetailsTransformer extends  Transformer
{
    //This function is use for show functionality.
    public function transform($data,$show=true)
    {
        if($show==true)
        {
           return[
                'id'                => $data['id'],
                'name'              => $data['name'],
                'accountNumber'     => $data['account_number'],
                'ifscCode'          => $data['ifsc_code'],
                'accountHolderName' => $data['account_holder_name'],
                'contactNumber'     => $data['contact_number'],
                'emailId'           => $data['email_id'],
                'branchId'          => $data['branch_id'],
                //Get branch information (deleted branch information also show) 
                'BranchDetails'     => (new BranchTransformer)->transform(Branch::withTrashed()->find($data['branch_id'])),
            ];     
        }
        else
        {
            return[
                'id'                => $data['id'],
                'name'              => $data['name'],
                'accountNumber'     => $data['account_number'],
                'ifscCode'          => $data['ifsc_code'],
                'accountHolderName' => $data['account_holder_name'],
                'contactNumber'     => $data['contact_number'],
                'emailId'           => $data['email_id'],
                'branchId'          => $data['branch_id'],
                //Get branch name by branch Id.
                'BranchName'      => (new Branch)->getBranchName($data['branch_id']),
            ];      
        }
    }
    
    //This function is use for add functionality.
    public function reverseTransform($data)
    {
        return [
            'created_at'            => date('Y-m-d H:i:s'),
            'name'                  => $data['name'],
            'account_number'        => $data['accountNumber'],
            'ifsc_code'             => $data['ifscCode'],
            'account_holder_name'   => $data['accountHolderName'],
            'contact_number'        => $data['contactNumber'],
            'email_id'              => $data['emailId'],
            'branch_id'             => $data['branchId'],
            ];
    }
} 
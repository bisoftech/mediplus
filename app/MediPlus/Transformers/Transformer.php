<?php
/**
 * Created by PhpStorm.
 * User: ss11469
 * Date: 7/3/2015
 * Time: 12:01 AM
 */

namespace App\MediPlus\Transformers;
use Log;

use Illuminate\Support\Collection;

use StdClass;

abstract class Transformer {

    public  function transformCollection($items,$show=false)
    {  
        
        $object = new StdClass();
        foreach ( $items as $key => $value ){

            $id=$value->id;
            $object ->$id = $this->transform($value,$show);
            
        }        
        return $object;
    }
    public  function transformCollectionPlain($items)
    {  
        $data= array();      
        foreach($items as $value )
        {
            $data[]=$this->transform($value,true);
        }
        return $data;
    }
    public abstract  function transform($item,$show=true);
}

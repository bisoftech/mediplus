<?php
/**
 * Created by PhpStorm.
 * User: ss11469
 * Date: 7/3/2015
 * Time: 12:03 AM
 */

namespace App\MediPlus\Transformers;

use App\Branch;
use App\Distributor;
use App\user;
use App\MediPlus\Transformers\PurchaseOrderItemsTransformer;
use App\MediPlus\Transformers\BranchTransformer;
use App\MediPlus\Transformers\DistributorTransFormer;
use App\MediPlus\Transformers\UserTransformer;

class PurchaseOrderTransformer extends  Transformer
{
    //This function is use for show functionality.
    public function transform($data,$show=true)
    {
        if($show == true)
        {
           return[
                'id'            => $data['id'],
                'createdAt'     => $data['created_at'],
                'updatedAt'     => $data['updated_at'],
                'deletedAt'     => $data['deleted_at'],
                'orderDate'     => date('d-m-Y H:i:s', strtotime($data['order_date'])),
                //Get user information (deleted user information also show)
                'user'          => (new UserTransformer)->transform(User::withTrashed()->find($data['user_id'])),
                //Get distributor information (deleted distributor information also show)
                'distributor'   => (new DistributorTransFormer)->transform(Distributor::withTrashed()->find($data['distributor_id'])),
                //Get branch information (deleted branch information also show)
                'branch'        => (new BranchTransformer)->transform(Branch::withTrashed()->find($data['branch_id'])),
                //Get purchase order items information.
                'purchaseOrderItem'=> (new PurchaseOrderItemsTransformer)->transformCollectionPlain($data->PurchaseOrderItems),
            ];

        }
        else
        {
            return[
                'id'                    => $data['id'],
                'createdAt'             => $data['created_at'],
                'updatedAt'             => $data['updated_at'],
                'deletedAt'             => $data['deleted_at'],
                'orderDate'             => date('d-m-Y H:i:s', strtotime($data['order_date'])),
                'userId'                => $data['user_id'],
                'distributorId'         => $data['distributor_id'],
                'branchId'              => $data['branch_id'],
                //Get user name by user Id.
                'userName'              => (new User)->getUserName($data['user_id']),
                //Get distributor name by distributor Id.
                'distributorName'       => (new Distributor)->getDistributorName($data['distributor_id']),
                //Get branch name by branch Id.
                'branchName'            => (new Branch)->getBranchName($data['branch_id']),
            ];  
        }
    }
    
    //This function is use for add functionality.
    public function reverseTransform($data,$obj)
    {
                $obj->order_date     =   date("Y-m-d H:i:s");
                $obj->user_id        =   $data['userId'];
                $obj->distributor_id =   $data['distributorId'];
                $obj->branch_id      =   $data['branchId'];
                $obj->save();
                return $obj;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: ss11469
 * Date: 7/3/2015
 * Time: 12:03 AM
 */

namespace App\MediPlus\Transformers;

class DistributorTransformer extends  Transformer
{
    //This function is use for show functionality.
    public  function transform($distributor,$show=true)
    {
        return[
            'agencyNumber'            => $distributor['agency_number'],
            'agencyName'              => $distributor['agency_name'],
            'ownerName'               => $distributor['owner_name'],
            'authorisedDistributor'   => $distributor['authorised_distributor'],
            'TIN'                     => $distributor['tin_number'],
            'CST'                     => $distributor['cst_number'],
            'dlNumber'                => $distributor['dl_number'],
            'vatNumber'               => $distributor['vat_number'],
            'creditPeriod'            => $distributor['credit_period'],
            'expiryDuration'          => $distributor['expiry_duration'],
            'contactNumber'           => $distributor['contact_number'],
            'otherDetails'            => $distributor['other_details'],
            'branchId'                => $distributor['branch_id'],
            'shopId'                  => $distributor['shop_id'],
            'id'                      => $distributor['id'],
            ];
    }
    
    //This function is use for add functionality.
    public  function reverseTransform($distributor)
    {
        return [
            'created_at'               => date('Y-m-d H:i:s'),
            'agency_number'            => $distributor['agencyNumber'],
            'agency_name'              => $distributor['agencyName'],
            'owner_name'               => $distributor['ownerName'],
            'authorised_distributor'   => $distributor['authorisedDistributor'],
            'tin_number'               => $distributor['TIN'],
            'cst_number'               => $distributor['CST'],
            'dl_number'                => $distributor['dlNumber'],
            'vat_number'               => $distributor['vatNumber'],
            'credit_period'            => $distributor['creditPeriod'],
            'expiry_duration'          => $distributor['expiryDuration'],
            'contact_number'           => $distributor['contactNumber'],
            'other_details'            => $distributor['otherDetails'],
            'branch_id'                => $distributor['branchId'],
            'shop_id'                  => '1',
            ];
    }
}
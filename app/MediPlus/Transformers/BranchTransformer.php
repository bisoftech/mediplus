<?php
namespace App\MediPlus\Transformers;
use App\MediPlus\Transformers\ShopTransformer;
use App\Shop;
use Log;

class BranchTransformer extends  Transformer
{
    //This function is use for show functionality.
    public  function transform($data,$show=true)
    {
        if($show == true)
        {
            return[
                 'id'                    => $data['id'],
                 'createdAt'             => $data['created_at'],
                 'updatedAt'             => $data['updated_at'],
                 'branchName'            => $data['branch_name'],
                 'registrationNumber'    => $data['registration_number'],
                 'registrationValidity'  => $data['registration_validity'],
                 'shopActNumber'         => $data['shop_act_number'],
                 'shopActValidity'       => $data['shop_act_validity'],
                 'vatNumber'             => $data['vat_number'],
                 'cstNumber'             => $data['cst_number'],
                 'lbtNumber'             => $data['lbt_number'],
                 'panCard'               => $data['pan_card'],
                 'district'              => $data['district'],
                 'city'                  => $data['city'],
                 'pinCode'               => $data['pin_code'],
                 'state'                 => $data['state'],
                 'address'               => $data['address'],
                 'userDrugLicenseNumber' => $data['user_drug_license_number'],
                 'pharmacistId'          => $data['pharmacist_id'],
                 'tinNumber'             => $data['tin_numbe'],
                 'contactNumber'         => $data['contact_number'],
                 'faxNumber'             => $data['fax_number'],
                 'deletedAt'             => $data['deleted_at'],
                 'alternateContact'      => $data['alternate_contact'],
                 //Get shop information (deleted shop information also show) 
                 'ShopDetails'           => (new ShopTransformer)->transform(Shop::withTrashed()->find($data['shop_id'])),      
                ];
        }
        else
        {
            return[
                 'id'                    => $data['id'],
                 'createdAt'             => $data['created_at'],
                 'updatedAt'             => $data['updated_at'],
                 'branchName'            => $data['branch_name'],
                 'registrationNumber'    => $data['registration_number'],
                 'registrationValidity'  => $data['registration_validity'],
                 'shopActNumber'         => $data['shop_act_number'],
                 'shopActValidity'       => $data['shop_act_validity'],
                 'vatNumber'             => $data['vat_number'],
                 'cstNumber'             => $data['cst_number'],
                 'lbtNumber'             => $data['lbt_number'],
                 'panCard'               => $data['pan_card'],
                 'district'              => $data['district'],
                 'city'                  => $data['city'],
                 'pinCode'               => $data['pin_code'],
                 'state'                 => $data['state'],
                 'address'               => $data['address'],
                 'userDrugLicenseNumber' => $data['user_drug_license_number'],
                 'pharmacistId'          => $data['pharmacist_id'],
                 'tinNumber'             => $data['tin_number'],
                 'contactNumber'         => $data['contact_number'],
                 'faxNumber'             => $data['fax_number'],
                 'deletedAt'             => $data['deleted_at'],
                 'alternateContact'      => $data['alternate_contact'],
                 //Get shop name by shop Id.
                 'shopName'              => (new Shop)->getShopName($data->shop_id),
                ];
        }
    }
    
    //This function is use for add functionality.
    public function reverseTransform($data)
    {
        return [
            'created_at'                => date('Y-m-d H:i:s'),
            'branch_name'               => $data['branchName'],
            'registration_number'       => $data['registrationNumber'],
            'shop_act_number'           => $data['shopActNumber'],
            'vat_number'                => $data['vatNumber'],
            'cst_number'                => $data['cstNumber'],
            'lbt_number'                => $data['lbtNumber'],
            'address'                   => $data['address'],
            'user_drug_license_number'  => $data['userDrugLicenseNumber'],
            'tin_number'                => $data['tinNumber'],
            'contact_number'            => $data['contactNumber'],
            'shop_id'                   => $data['shopId'],
            ];
    }
}
<?php
namespace App\MediPlus\Transformers;

use App\Branch;
use App\Distributor;
use App\user;
use App\MediPlus\Transformers\PurchaseOrderItemsTransformer;
use App\MediPlus\Transformers\BranchTransformer;
use App\MediPlus\Transformers\DistributorTransFormer;
use App\MediPlus\Transformers\UserTransformer;

class TaxTransformer extends  Transformer
{
    //This function is use for show functionality.
    public function transform($data,$show=true)
    {
        if($show==true)
        {
           return[
                'id'                => $data['id'],
                'vatTax'            => $data['vat_tax'],
                'cstTax'            => $data['cst_tax'],
                'lbtTax'            => $data['lbt_tax'],
                'serviceTax'        => $data['service_tax'],
                'branchId'          => $data['branch_id'],
                //Get branch information (deleted branch information also show)
                'BranchDetails'     => (new BranchTransformer)->transform(Branch::withTrashed()->find($data['branch_id'])),    
            ];
        }
        else
        {
            return[
                'id'         => $data['id'],
                'vatTax'     => $data['vat_tax'],
                'cstTax'     => $data['cst_tax'],
                'lbtTax'     => $data['lbt_tax'],
                'serviceTax' => $data['service_tax'],
                'branchId'   => $data['branch_id'],
            ];  
        }
    }
    
    //This function is use for add functionality.
    public function reverseTransform($data)
    {
        return [
            'created_at'    => date('Y-m-d H:i:s'),
            'vat_tax'       => $data['vatTax'],
            'cst_tax'       => $data['cstTax'],
            'lbt_tax'       => $data['lbtTax'],
            'service_tax'   => $data['serviceTax'],
            'branch_id'     => $data['branchId']
            ];
    }
} 
<?php
/**
 * Created by PhpStorm.
 * User: ss11469
 * Date: 7/3/2015
 * Time: 12:03 AM
 */

namespace App\MediPlus\Transformers;
use App\MediPlus\Transformers\InvoiceItemsTransformer as InvoiceItemsTransformer;
use App\InvoiceItems;
use App\Stock;
use App\Product;

class ProductTransformer extends Transformer
{
    //This function is use for show functionality.
    public function transform($product,$show=true)
    {      
        $product['dosage']=trim($product['dosage'],']');
        $product['dosage']=trim($product['dosage'],'[');
        $product['product_location']=trim($product['product_location'],']');
        $product['product_location']=trim($product['product_location'],'[');
        
        if($show == true)
        {
        return [
            'name'             => $product['name'],
            'manufacturer'     => $product['manufacturer'],
            'formula'          => $product['formula'],
            'dosage'           => '[ '.$product['dosage'].' ]',
            'productLocation'  => '[ '.$product['product_location'].' ]',
            'lowQuantity'      => $product['low_quantity'],
            'id'               => $product['id'],
            'deleted'          => $product['deleted_at']!=null,
            //Get product price by product Id.
            'prising'          => (new Product)->prising($product['id']),
            //Get product expiry date by product Id.
            'expiry_date'      => (new Product)->expireDate($product['id']),
            ];
        }
        else
        {
            return [
            'name'             => $product['name'],
            'manufacturer'     => $product['manufacturer'],
            'formula'          => $product['formula'],
            'dosage'           => $product['dosage'],
            'productLocation'  => $product['product_location'],
            'lowQuantity'      => $product['low_quantity'],
            'id'               => $product['id'],
            'deleted'          => $product['deleted_at']!=null,
            ];
        }
    }

    //This function is use for show functionality.
    public function transformDB($product)
    {
        return [
            'name'         => $product->name,
            'manufacturer' => $product->manufacturer,
            'formula'      => $product->formula,
            'dosage'       => $product->dosage,
            'id'           => $product->id,
            ];
    }
    
    //This function is use for add functionality.
    public function reverseTransform($product)
    {
        $product['dosage']=trim($product['dosage'],']');
        $product['dosage']=trim($product['dosage'],'[');
        $product['productLocation']=trim($product['productLocation'],']');
        $product['productLocation']=trim($product['productLocation'],'[');
        
        return [
            'created_at'        => date('Y-m-d H:i:s'),
            'name'              => $product['name'],
            'manufacturer'      => $product['manufacturer'],
            'formula'           => $product['formula'],
            'dosage'            => $product['dosage'],
            'product_location'  => $product['productLocation'],
            'low_quantity'      => $product['lowQuantity']
            ];
    }
} 
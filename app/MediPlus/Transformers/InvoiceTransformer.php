<?php
namespace App\MediPlus\Transformers;
use App\User;
use App\InvoiceItems;
use App\MediPlus\Transformers\InvoiceItemsTransformer as InvoiceItemsTransformer;
use App\MediPlus\Transformers\UserTransformer;
use App\MediPlus\Transformers\PurchaseOrderTransformer;
use App\MediPlus\Transformers\BranchTransformer;
use App\PurchaseOrder;
use App\Branch;

class InvoiceTransformer extends Transformer
{
    //This function is use for show functionality.
    public function transform($data,$show=true)
    {
        if($show==true)
        {
             return[                       
                 'id'              => $data['id'],
                 'createdAt'       => $data['created_at'],
                 'updatedAt'       => $data['updated_at'],
                 'parcelNumber'    => $data['parcel_number'],
                 'deletedAt'       => $data['deleted_at'],
                 'invoiceDate'     => date("d-m-Y", strtotime($data['invoice_date'])), 
                 'alertDate'       => date("d-m-Y", strtotime($data['alert_date'])),
                 'totalAmount'     => $data['total_amount'],
                 'amountPaid'      => $data['amount_paid'],
                 'paymentType'     => $data['payment_type'],
                 'chequeNumber'    => $data['cheque_number'],
                 'freeItems'       => $data['free_items'],
                 'purchaseOrderId' => $data['purchase_order_id'], 
                 //Get user information (deleted user information also show)
                 'user'            => (new UserTransformer)->transform(User::withTrashed()->find($data['user_id'])),
                 //Get purchase order information.
                 'purchaseOrder'   => (new PurchaseOrderTransformer)->transform(PurchaseOrder::withTrashed()->find($data['purchase_order_id'])),
                 //Get invoice items information.
                 'invoiceItems'    => (new InvoiceItemsTransformer)->transformCollectionPlain($data->InvoiceItems),
                 //Get branch information (deleted branch information also show)
                 'branch'          => (new BranchTransformer)->transform(Branch::withTrashed()->find($data['branch_id'])), 
                ];
        }
        else
        {
             return[                       
                 'id'                 => $data['id'],
                 'createdAt'          => $data['created_at'],
                 'updatedAt'          => $data['updated_at'],
                 'parcelNumber'       => $data['parcel_number'],
                 'deletedAt'          => $data['deleted_at'],
                 'invoiceDate'        => date("d-m-Y", strtotime($data['invoice_date'])),    
                 'alertDate'          => date("d-m-Y", strtotime($data['alert_date'])),   
                 'totalAmount'        => $data['total_amount'],
                 'amountPaid'         => $data['amount_paid'],
                 'paymentType'        => $data['payment_type'],
                 'chequeNumber'       => $data['cheque_number'],
                 'freeItems'          => $data['free_items'],
                 'userId'             => $data['user_id'],
                 'purchaseOrderId'    => $data['purchase_order_id'],
                 //Get user name by user Id.
                 'userName'           => (new User)->getUserName($data->user_id),
                 //Get branch name by branch Id.
                 'branchName'         => (new Branch)->getBranchName($data['branch_id']),
                ];
        }
    }
    public function transformDB($data)
    {

              return[                       
                 'id'              => $data->id,
                 'createdAt'       => $data->created_at,
                 'updatedAt'       => $data->updated_at,
                 'parcelNumber'    => $data->parcel_number,
                 'deletedAt'       => $data->deleted_at,
                 'invoiceDate'     => $data->invoice_date,
                 'totalAmount'     => $data->total_amount,
                 'amountPaid'      => $data->amount_paid,
                 'paymentType'     => $data->payment_type,
                 'chequeNumber'    => $data->cheque_number,
                 'freeItems'       => $data->free_items,
                 'purchaseOrderId' => $data->purchase_order_id,
                 //Get user information (deleted user details also show).
                 'user'            => (new UserTransformer)->transform(User::withTrashed()->find($data->user_id)),
                 //Get purchase order information.
                 'purchaseOrder'   => (new PurchaseOrderTransformer)->transform(PurchaseOrder::withTrashed()->find($data->purchase_order_id)),
                ];
    }

    //This function is use for add functionality.
    public function reverseTransform($data)
    {
            //This code is use for when we are not select any date from form then today's date is select automatically.
            if(in_array("invoiceDate",$data))
            {
                $data['invoiceDate'] = date('Y-m-d H:i:s');
            }
            else
            {
                $data['invoiceDate'] = date('Y-m-d H:i:s', strtotime($data['invoiceDate']));
            }
        
            return 
            [
            "created_at"        => date('Y-m-d H:i:s'),    
            "parcel_number"     => $data['parcelNumber'],
            "invoice_date"      => date('Y-m-d H:i:s', strtotime($data['invoiceDate'])),
            "alert_date"        => $data['alertDate'],    
            "total_amount"      => $data['totalAmount'],
            "amount_paid"       => $data['amountPaid'],
            "payment_type"      => $data['paymentType'],
            "cheque_number"     => $data['chequeNumber'],
            "free_items"        => $data['freeItems'],
            "user_id"           => $data['userId'],
            "purchase_order_id" => $data['purchaseOrderId'],
            "branch_id"         => $data['branchId']
            ];
    }
}
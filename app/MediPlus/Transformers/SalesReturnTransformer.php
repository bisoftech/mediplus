<?php
namespace App\MediPlus\Transformers;

use App\MediPlus\Transformers\BillingItemsTransformer;
use App\MediPlus\Transformers\BillingTransformer;
use App\BillingItems;
use App\Billing;
use SalesReturn;
use DB;

class SalesReturnTransformer extends  Transformer
{
    //This function is use for show functionality.
    public function transform($data,$show=true)
    {
        if($show==true)
        {
            return[
                'id'               => $data['id'],
                'createdAt'        => $data['created_at'],
                'updatedAt'        => $data['updated_at'],
                'deletedAt'        => $data['deleted_at'],
                'reason'           => $data['reason'],
                'billItemId'       => $data['bill_item_id'],
                'billId'           => $data['bill_id'],
                //Get billing items detail information.
                'BillItemsDetails' => (new BillingItemsTransformer)->transform(BillingItems::find($data['bill_item_id'])),
                //Get billing detail information.
                'BillDetails'      => (new BillingTransformer)->transform(Billing::find($data['bill_id'])),
                ];
        }
        else
        {
            return[
                'id'               => $data['id'],
                'createdAt'        => $data['created_at'],
                'updatedAt'        => $data['updated_at'],
                'deletedAt'        => $data['deleted_at'],
                'reason'           => $data['reason'],
                'billItemId'       => $data['bill_item_id'],
                'billId'           => $data['bill_id'],
                ];
        }
    }
   
    //This function is use for add functionality.
    public function reverseTransform($data)
    {
        return [
            'created_at'   => date('Y-m-d H:i:s'),
            'reason'       => $data['reason'],
            'bill_item_id' => $data['billItemId'],
            'bill_id'      => $data['billId'],
            ];
    }
}
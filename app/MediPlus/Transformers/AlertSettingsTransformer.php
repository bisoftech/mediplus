<?php

namespace App\MediPlus\Transformers;

use App\AlertSettingsModel;
use DB;
use Log;
class AlertSettingsTransformer extends  Transformer
{
    public $myFields = array('bill_unpaid'=>'alert_billing_payment_status_unpaid','bill_partial_paid'=>'alert_billing_payment_status_partial_paid','branch_registration_validity'=>'alert_branch_registration_validity','branch_shop_act_validity'=>'alert_branch_shop_act_validity','customer_credit_limit_exceed'=>'alert_customer_credit_limit_exceed','customer_birthday'=>'alert_customer_birthday','distributor_credit_period_exceed'=>'alert_distributor_credit_period_exceed','invoice_unpaid'=>'alert_invoice_status_unpaid','invoice_partial_paid'=>'alert_invoice_status_partial_paid','order_return'=>'alert_order_return', 'sales_return' => 'alert_sales_return','stock_product_expires'=>'alert_stock_product_expires','stock_product_out_of_stock'=>'alert_stock_product_out_of_stock','stock_product_low_quantity'=>'alert_stock_product_low_quantity','user_birthday'=>'alert_user_birthday','bill_unpaid_period'=>'alert_billing_payment_status_unpaid_period_days','bill_partial_paid_period'=>'alert_billing_payment_status_partial_paid_period_days','branch_registration_validity_period'=>'alert_branch_registration_validity_period_days','branch_shop_act_validity_period'=>'alert_branch_shop_act_validity_period_days','customer_credit_limit_exceed_period'=>'alert_customer_credit_limit_exceed_period_days','customer_birthday_period'=>'alert_customer_birthday_period_days','distributor_credit_period_exceed_period'=>'alert_distributor_credit_period_exceed_period_days','invoice_unpaid_period'=>'alert_invoice_status_unpaid_period_days','invoice_partial_paid_period'=>'alert_invoice_status_partial_paid_period_days','order_return_period'=>'alert_order_return_period_days','sales_return_period'=>'alert_sales_return_period_days','stock_product_expires_period'=>'alert_stock_product_expires_period_days','stock_product_out_of_stock_period'=>'alert_stock_product_out_of_stock_period_days','stock_product_low_quantity_period'=>'alert_stock_product_low_quantity_period_days','user_birthday_period'=>'alert_user_birthday_period_days');
    
    
    public function reverseTransform($data)
    {

  	return 
    [
      'alertBillingPaymentStatusUnpaid'=>$data['alert_billing_payment_status_unpaid'],
      'alertBillingPaymentStatusPartialPaid'=>$data['alert_billing_payment_status_partial_paid'],
      'alertBranchRegistrationValidity'=>$data['alert_branch_registration_validity'],
      'alertBranchShopActValidity'=>$data['alert_branch_shop_act_validity'],
      'alertCustomerCreditLimitExceed'=>$data['alert_customer_credit_limit_exceed'],
      'alertCustomerBirthday'=>$data['alert_customer_birthday'],
      'alertDistributorCreditPeriodExceed'=>$data['alert_distributor_credit_period_exceed'],
      'alertInvoicePaymentStatusUnpaid'=>$data['alert_invoice_payment_status_unpaid'],
      'alertInvoicePaymentStatusPartialPaid'=>$data['alert_invoice_payment_status_partial_paid'],
      'alertOrderReturn'=>$data['alert_order_return'],
      'alertSalesReturn'=>$data['alert_sales_return'],
      'alertStockProductExpires'=>$data['alert_stock_product_expires'],
      'alertStockProductOutOfStock'=>$data['alert_stock_product_out_of_stock'],
      'alertStockProductLowQuantity'=>$data['alert_stock_product_low_quantity'],
      'alertUserBirthday'=>$data['alert_user_birthday'],
      'alertBillingPaymentStatusUnpaidPeriodDays'=>$data['alert_billing_payment_status_unpaid_period_days'],
      'alertBillingPaymentStatusPartialPaidPeriodDays'=>$data['alert_billing_payment_status_partial_paid_period_days'],
      'alertBranchRegistrationValidityPeriodDays'=>$data['alert_branch_registration_validity_period_days'],
      'alertBranchShopActValidityPeriodDays'=>$data['alert_branch_shop_act_validity_period_days'],
      'alertCustomerCreditLimitExceedPeriodDays'=>$data['alert_customer_credit_limit_exceed_period_days'],
      'alertCustomerBirthdayPeriodDays'=>$data['alert_customer_birthday_period_days'],
      'alertDistributorCreditPeriodExceedPeriodDays'=>$data['alert_distributor_credit_period_exceed_period_days'],
      'alertInvoicePaymentStatusUnpaidPeriodDays'=>$data['alert_invoice_payment_status_unpaid_period_days'],
      'alertInvoicePaymentStatusPartialPaidPeriodDays'=>$data['alert_invoice_payment_status_partial_paid_period_days'],
      'alertOrderReturnPeriodDays'=>$data['alert_order_return_period_days'],
      'alertSalesReturnPeriodDays'=>$data['alert_sales_return_period_days'],
      'alertStockProductExpiresPeriodDays'=>$data['alert_stock_product_expires_period_days'],
      'alertStockProductOutOfStockPeriodDays'=>$data['alert_stock_product_out_of_stock_period_days'],
      'alertStockProductLowQuantityPeriodDays'=>$data['alert_stock_product_low_quantity_period_days'],
      'alertUserBirthdayPeriodDays'=>$data['alert_user_birthday_period_days']
 ];

    }
    public function transform($data,$show=true)
    {
    return 
		[
		'user_id'=>$data['userId'],
		'branch_id'=>$data['branchId'],
		'alert_billing_payment_status_unpaid'=>$data['alertBillingPaymentStatusUnpaid'],
		'alert_billing_payment_status_partial_paid'=>$data['alertBillingPaymentStatusPartialPaid'],
		'alert_branch_registration_validity'=>$data['alertBranchRegistrationValidity'],
		'alert_branch_shop_act_validity'=>$data['alertBranchShopActValidity'],
		'alert_customer_credit_limit_exceed'=>$data['alertCustomerCreditLimitExceed'],
		'alert_customer_birthday'=>$data['alertCustomerBirthday'],
		'alert_distributor_credit_period_exceed'=>$data['alertDistributorCreditPeriodExceed'],
		'alert_invoice_payment_status_unpaid'=>$data['alertInvoicePaymentStatusUnpaid'],
		'alert_invoice_payment_status_partial_paid'=>$data['alertInvoicePaymentStatusPartialPaid'],
		'alert_order_return'=>$data['alertOrderReturn'],
		'alert_sales_return'=>$data['alertSalesReturn'],
		'alert_stock_product_expires'=>$data['alertStockProductExpires'],
		'alert_stock_product_out_of_stock'=>$data['alertStockProductOutOfStock'],
		'alert_stock_product_low_quantity'=>$data['alertStockProductLowQuantity'],
		'alert_user_birthday'=>$data['alertUserBirthday'],
		'alert_billing_payment_status_unpaid_period_days'=>$data['alertBillingPaymentStatusUnpaidPeriodDays'],
		'alert_billing_payment_status_partial_paid_period_days'=>$data['alertBillingPaymentStatusPartialPaidPeriodDays'],
		'alert_branch_registration_validity_period_days'=>$data['alertBranchRegistrationValidityPeriodDays'],
		'alert_branch_shop_act_validity_period_days'=>$data['alertBranchShopActValidityPeriodDays'],
		'alert_customer_credit_limit_exceed_period_days'=>$data['alertCustomerCreditLimitExceedPeriodDays'],
		'alert_customer_birthday_period_days'=>$data['alertCustomerBirthdayPeriodDays'],
		'alert_distributor_credit_period_exceed_period_days'=>$data['alertDistributorCreditPeriodExceedPeriodDays'],
		'alert_invoice_payment_status_unpaid_period_days'=>$data['alertInvoicePaymentStatusUnpaidPeriodDays'],
		'alert_invoice_payment_status_partial_paid_period_days'=>$data['alertInvoicePaymentStatusPartialPaidPeriodDays'],
		'alert_order_return_period_days'=>$data['alertOrderReturnPeriodDays'],
		'alert_sales_return_period_days'=>$data['alertSalesReturnPeriodDays'],
		'alert_stock_product_expires_period_days'=>$data['alertStockProductExpiresPeriodDays'],
		'alert_stock_product_out_of_stock_period_days'=>$data['alertStockProductOutOfStockPeriodDays'],
		'alert_stock_product_low_quantity_period_days'=>$data['alertStockProductLowQuantityPeriodDays'],
		'alert_user_birthday_period_days'=>$data['alertUserBirthdayPeriodDays'],
    ];

	}
}

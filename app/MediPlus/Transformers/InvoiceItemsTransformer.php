<?php
/**
 * Created by PhpStorm.
 * User: ss11469
 * Date: 7/3/2015
 * Time: 12:03 AM
 */
namespace App\MediPlus\Transformers;

use App\MediPlus\Transformers\ProductTransformer;
use App\MediPlus\Transformers\StockTransformer;
use App\MediPlus\Transformers\InvoiceTransformer;
use App\Stock;
use App\Product;
use Log;
use DB;

class InvoiceItemsTransformer extends Transformer
{
    //This function is use for show functionality.
    public function transform($data,$show=true)
    {
        if($show == true)
        {
           return[
               'id'                 => $data['id'],
               'createdAt'          => $data['created_at'],
               'updatedAt'          => $data['updated_at'],
               'batchNumber'        => $data['batch_number'],
               'quantity'           => $data['quantity'],
               'expiryDate'         => date("d-m-Y", strtotime($data['expiry_date'])),   
               'mfgDate'            => date("d-m-Y", strtotime($data['mfg_date'])),
               'purchasePrice'      => $data['purchase_price'],
               'sellingPrice'       => $data['selling_price'],
               'MRP'                => $data['mrp'],
               'sellDiscount'       => $data['sell_discount'],
               'purchaseDiscount'   => $data['purchase_discount'],
               'vatAmount'          => $data['vat_amount'],
               'deletedAt'          => $data['deleted_at'],
               'productId'          => $data['product_id'],
               'invoiceId'          => $data['invoice_id'],
               //Get product name by product Id.
               'productName'        => (new Product)->getProductName($data['product_id']),
               //Get product information (deleted product information also show)
               'product'            => (new ProductTransformer)->Transform(Product::withTrashed()->find($data['product_id'])),
               //Get stock id by matching stock product id and invoice id to invoice product id and id.
               'stockId'            => DB::table('stock')->select('id')->where('product_id','=',$data['product_id'])->where('invoice_id','=',$data['invoice_id'])->lists('id')
            ];
        }
        else
        {
            return[
               'id'                 => $data['id'],
               'createdAt'          => $data['created_at'],
               'updatedAt'          => $data['updated_at'],
               'batchNumber'        => $data['batch_number'],
               'quantity'           => $data['quantity'],
               'expiryDate'         => date("d-m-Y", strtotime($data['expiry_date'])),
               'mfgDate'            => date("d-m-Y", strtotime($data['mfg_date'])),
               'purchasePrice'      => $data['purchase_price'],
               'sellingPrice'       => $data['selling_price'],
               'MRP'                => $data['mrp'],
               'sellDiscount'       => $data['sell_discount'],
               'purchaseDiscount'   => $data['purchase_discount'],
               'vatAmount'          => $data['vat_amount'],
               'deletedAt'          => $data['deleted_at'],
               'productId'          => $data['product_id'],
               'invoiceId'          => $data['invoice_id'],
               //Get product name by product Id.
               'productName'        =>(new Product)->getProductName($data['product_id']),    
               //Get product information (deleted product information also show)
               'product'            => (new ProductTransformer)->Transform(Product::withTrashed()->find($data['product_id'])),
               //Get stock id by matching stock product id and invoice id to invoice product id and id.
               'stockId'            => DB::table('stock')->where('product_id','=',$data['product_id'])->orWhere('invoice_id','=',$data['invoice_id'])->lists('id')    
        ];

        }
    }

    //This function is use for show functionality.
    public function transformDB($data)
    {
               return[
               'id'                 => $data->id,
               'createdAt'          => $data->created_at,
               'updatedAt'          => $data->updated_at,
               'batchNumber'        => $data->batch_number,
               'quantity'           => $data->quantity,
               'expiryDate'         => $data->expiry_date,
               'mfgDate'            => $data->mfg_date,
               'purchasePrice'      => $data->purchase_price,
               'sellingPrice'       => $data->selling_price,
               'MRP'                => $data->mrp,
               'sellDiscount'       => $data->sell_discount,
               'purchaseDiscount'   => $data->purchase_discount,
               'margin'             => $data->margin,
               'deletedAt'          => $data->deleted_at,
               'productId'          => $data->product_id,
               'invoiceId'          => $data->invoice_id,
               //Get invoice information.
               'invoice'            => (new InvoiceTransformer)->transformDB(DB::table('invoice')->find($data->invoice_id)),
               //Get product information.
               'product'            => (new ProductTransformer)->transformDB(DB::table('product')->find($data->product_id)),
            ];
    }
    public function show()
    {
      return 123;
    }
    
    //This function is use for add functionality.
    public function reverseTransform($data,$id)
    {  
        return 
            [
            "created_at"          => date('Y-m-d H:i:s'),
            "batch_number"        => $data['batchNumber'],
            "quantity"            => $data['quantity'],
            "expiry_date"         => date('Y-m-d H:i:s', strtotime($data['expiryDate'])),
            "mfg_date"            => date('Y-m-d H:i:s', strtotime($data['mfgDate'])),
            "purchase_price"      => $data['purchasePrice'],
            "mrp"                 => $data['mrp'],
            "product_id"          => $data['productId'],
            "invoice_id"          => $id,
            ];
    }
} 
<?php
/**
 * Created by PhpStorm.
 * User: ss11469
 * Date: 7/3/2015
 * Time: 12:03 AM
 */

namespace App\MediPlus\Transformers;
use App\MediPlus\Transformers\BranchTransformer;
use App\MediPlus\Transformers\UserTransformer;
use App\Branch;

class UserBranchDetailsTransformer extends Transformer
{
    //This function is use for show functionality.
    public  function transform($userBranch,$show=true)
    {
        if($show==true)
        {
            return [
                'id'              => $userBranch['id'],
                'user_id'         => $userBranch['user_id'],
                'branch_id'       => $userBranch['branch_id'],
                //Get branch name by branch Id.
                'BranchName'      => (new Branch)->getBranchName($user['branch_id']),
                //Get branch information (deleted branch information also show)
                'BranchDetails'   => (new BranchTransformer)->Transform(Branch::withTrashed()->find($userBranch['branch_id'])),
            ] ;
        }
        else
        {
            return [
                'id'              => $userBranch['id'],
                'user_id'         => $userBranch['user_id'],
                'branch_id'       => $userBranch['branch_id'],
                //Get branch name by branch Id.
                'BranchName'      => (new Branch)->getBranchName($userBranch['branch_id']),
                ];
        }    
    }
    
    //This function is use for add functionality.
    public function reverseTransform($userBranch,$obj,$id)
    {
        $obj->branch_id            = $userBranch;
        $obj->user_id              = $id;
        $obj->save();
        return $obj;
    }
} 
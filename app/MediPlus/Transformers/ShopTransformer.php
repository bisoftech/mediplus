<?php
namespace App\MediPlus\Transformers;

use App\Branch;
use App\Distributor;
use App\User;
use App\MediPlus\Transformers\PurchaseOrderItemsTransformer;
use App\MediPlus\Transformers\BranchTransformer;
use App\MediPlus\Transformers\DistributorTransFormer;
use App\MediPlus\Transformers\UserTransformer;

class ShopTransformer extends  Transformer
{
    //This function is use for show functionality.
    public function transform($data,$show=true)
    {
        if($show==true)
        {
           return[
            'id'            => $data['id'],
            'createdAt'     => $data['created_at'],
            'updatedAt'     => $data['updated_at'],
            'brandName'     => $data['brand_name'],
            'deletedAt'     => $data['deleted_at'],
            'superAdminId'  => $data['super_admin_id'],
            //Get user information (deleted user information also show)
            'user'          => (new UserTransformer)->transform(User::withTrashed()->find($data['super_admin_id'])),
            ];
        }
        else
        {
            return[
             'id'           => $data['id'],
             'createdAt'    => $data['created_at'],
             'updatedAt'    => $data['updated_at'],
             'brandName'    => $data['brand_name'],
             'deletedAt'    => $data['deleted_at'],
             'superAdminId' => $data['super_admin_id'],
            ];  
        }
    }
    
    //This function is use for add functionality.
    public function reverseTransform($data)
    {
        return[
            'created_at'      => date('Y-m-d H:i:s'),
            'brand_name'      => $data['brandName'],
            'super_admin_id'  => $data['superAdminId']
            ];
    } 
} 
<?php
/**
 * Created by PhpStorm.
 * User: ss11469
 * Date: 7/3/2015
 * Time: 12:03 AM
 */

namespace App\MediPlus\Transformers;
use App\MediPlus\Transformers\UserBranchDetailsTransformer;
use App\UserBranchDetails;
use App\Shop;
use App\Branch;
use Hash;
use Session;

class UserTransformer extends  Transformer
{
    //This function is use for show functionality.
    public  function transform($user,$show=true)
    {
        if($show==true)
        {
        return [
            'id'                => $user['id'],
            'name'              => $user['first_name'].' '.$user['middle_name'].' '.$user['last_name'],
            'firstName'         => $user['first_name'],
            'middleName'        => $user['middle_name'],
            'lastName'          => $user['last_name'],
            'emailId'           => $user['email_id'],
            'city'              => $user['city'],
            'district'          => $user['district'],
            'state'             => $user['state'],
            'streetAddress'     => $user['street_address'],
            'mobileNumber'      => $user['mobile_number'],
            'pinCode'           => $user['pin_code'],
            'aadharNumber'      => $user['aadhar_number'],
            'panNumber'         => $user['pan_number'],
            'userName'          => $user['user_name'],
            'role'              => $user['role'],
            'isActive'          => (boolean) $user['active'],
            'ShopName'          => $user['brand_name'],
            'UserBranchDetails' => $user->userBranch($user['id']),
            'selectBranchId'    => Session::get('branch_id'),
            ] ; 
        }
        else
        {
        return [
            'id'                => $user['id'],
            'name'              => $user['first_name'].' '.$user['middle_name'].' '.$user['last_name'],
            'firstName'         => $user['first_name'],
            'middleName'        => $user['middle_name'],
            'lastName'          => $user['last_name'],
            'emailId'           => $user['email_id'],
            'city'              => $user['city'],
            'district'          => $user['district'],
            'state'             => $user['state'],
            'streetAddress'     => $user['street_address'],
            'mobileNumber'      => $user['mobile_number'],
            'pinCode'           => $user['pin_code'],
            'aadharNumber'      => $user['aadhar_number'],
            'panNumber'         => $user['pan_number'],
            'userName'          => $user['user_name'],
            'role'              => $user['role'],
            'isActive'          => (boolean) $user['active'],
            ];
        }
    }
    
    //This function is use for add functionality.
    public function reverseTransform($user,$data='z')
    {    
        if($data =='save')
        {
        return [
                "created_at"        => date('Y-m-d H:i:s'),
                "password"          => Hash::make($user['password']),
                "active"            => "1",
                "first_name"        => $user['firstName'],
                "middle_name"       => $user['middleName'],
                "last_name"         => $user['lastName'],
                "email_id"          => $user['emailId'],
                "city"              => $user['city'],
                "district"          => $user['district'],
                "state"             => $user['state'],
                "street_address"    => $user['streetAddress'],
                "mobile_number"     => $user['mobileNumber'],
                "pin_code"          => $user['pinCode'],
                "aadhar_number"     => $user['aadharNumber'],
                "pan_number"        => $user['panNumber'],
                "user_name"         => $user['userName'],
                "role"              => trim($user['role']," "),
            ];
        }
        else
        {
         return [
                "created_at"        => date('Y-m-d H:i:s'),
                "active"            => "1",
                "first_name"        => $user['firstName'],
                "middle_name"       => $user['middleName'],
                "last_name"         => $user['lastName'],
                "email_id"          => $user['emailId'],
                "city"              => $user['city'],
                "district"          => $user['district'],
                "state"             => $user['state'],
                "street_address"    => $user['streetAddress'],
                "mobile_number"     => $user['mobileNumber'],
                "pin_code"          => $user['pinCode'],
                "aadhar_number"     => $user['aadharNumber'],
                "pan_number"        => $user['panNumber'],
                "user_name"         => $user['userName'],
                "role"              => trim($user['role']," "),
            ];
        } 
    }
} 
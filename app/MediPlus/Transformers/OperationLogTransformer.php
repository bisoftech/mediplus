<?php
namespace App\MediPlus\Transformers;

use App\user;
use App\OperationLog;
use App\MediPlus\Transformers\OperationLogTransformer;
use App\MediPlus\Transformers\UserTransformer;
use DB;

class OperationLogTransformer extends Transformer
{
    //This function is use for show functionality.
    public function transform($data,$show=true)
    {
        if($show==true)
        {
            return[
                'id'                  => $data['id'],
                'createdAt'           => $data['created_at'],
                'updatedAt'           => $data['updated_at'],
                'deletedAt'           => $data['deleted_at'],
                'userId'              => $data['user_id'],
                'operation'           => $data['operation'],
                'referenceTableId'    => $data['reference_table_id'],
                'operationStatus'     => $data['operation_status'],
                'customMessage'       => $data['custom_message'],
                //Get user information.
                'User'                => (new UserTransformer)->transform(User::find($data['user_id'])),
                ];
        }
        else
        {
            return[
                'id'                  => $data['id'],
                'createdAt'           => $data['created_at'],
                'updatedAt'           => $data['updated_at'],
                'deletedAt'           => $data['deleted_at'],
                'userId'              => $data['user_id'],
                'operation'           => $data['operation'],
                'referenceTableId'    => $data['reference_table_id'],
                'operationStatus'     => $data['operation_status'],
                'customMessage'       => $data['custom_message'],
                //Get user name by user Id.
                'userName'            => (new User)->getUserName($data['user_id']),
                ];
        }
    }
    
    //This function is use for add functionality.
    public function reverseTransform($data)
    {
        return[
            'created_at'          => date('Y-m-d H:i:s'),
            'user_id'             => $data['userId'],
            'operation'           => $data['operation'],
            'reference_table_id'  => $data['referenceTableId'],
            'operation_status'    => $data['operationStatus'],
            'custom_message'      => $data['customMessage'],
            ];
    }
}
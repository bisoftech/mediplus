<?php
namespace App\MediPlus\Transformers;

use App\Invoice;
use App\MediPlus\Transformers\InvoiceTransformer;
use App\PurchaseOrderReturnItems;
use DB;
use Log;

class PurchaseOrderReturnTransformer extends Transformer
{
    //This function is use for show functionality.
    public function transform($data,$show=true)
    {
        if($show==true)
        {
            return[
                'id'                => $data['id'],
                'createdAt'         => $data['created_at'],
                'updatedAt'         => $data['updated_at'],
                'deletedAt'         => $data['deleted_at'],
                'invoiceId'        => $data['invoice_id'],
                'invoice'           => (new InvoiceTransformer)->Transform(Invoice::find($data['invoice_id'])),
                'purchaseOrderItems'    => (new PurchaseOrderReturnItemsTransformer)->transformCollectionPlain($data->purchaseOrderReturnItems)
                ];
        }
        else
        {
            $status=array();
            $totalAmount=0;
            $refundOrLossAmt=0;
            foreach($data->purchaseOrderReturnItems as $purchaseOrderReturnItem){
                if($purchaseOrderReturnItem['status']!='replace'){
                    if($purchaseOrderReturnItem['status']=='inprogress')
                        $totalAmount+=$purchaseOrderReturnItem->invoiceItems['purchase_price']*$purchaseOrderReturnItem['quantity'];
                    if($purchaseOrderReturnItem['status']=='cashback')
                        $refundOrLossAmt+=$purchaseOrderReturnItem->invoiceItems['purchase_price']*$purchaseOrderReturnItem['quantity'];
                    if($purchaseOrderReturnItem['status']=='cancel')
                        $refundOrLossAmt-=$purchaseOrderReturnItem->invoiceItems['purchase_price']*$purchaseOrderReturnItem['quantity'];
                }
                if($purchaseOrderReturnItem['status']=='inprogress')
                    $purchaseOrderReturnItem['status']='In Progress';
                in_array($purchaseOrderReturnItem['status'],$status)?:array_push($status,ucwords($purchaseOrderReturnItem['status'])) ;
            }


            return[
                'id'                => $data['id'],
                'createdAt'         => $data['created_at'],
                'updatedAt'         => $data['updated_at'],
                'deletedAt'         => $data['deleted_at'],
               'invoiceId'        => $data['invoice_id'],
                'totalAmount'   =>$totalAmount,
                'refundOrLossAmt'   =>$refundOrLossAmt,
                'status'   => implode(',',$status),
                'noOfItems' => sizeof($data->purchaseOrderReturnItems)
                ];  
        }
    }
    
    //This function is use for add functionality.
    public function reverseTransform($data)
    {   
            return [
            'created_at'        => date('Y-m-d H:i:s'),
            'invoice_id'        => $data['invoiceId']
            ];         
    }
}
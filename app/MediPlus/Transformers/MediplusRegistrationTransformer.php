<?php
namespace App\MediPlus\Transformers;
use Session;
use Crypt;

class MediplusRegistrationTransformer extends  Transformer
{
    //This function is use for show functionality.
    public  function transform($data,$show=true)
    {
        return[
            'id'                => $data['id'],
            'createdAt'         => $data['created_at'],
            'updatedAt'         => $data['updated_at'],
            'registrationKey'   => $data['registration_key'],
            'registrationDate'  => $data['registration_date'],
            'duration'          => $data['duration'],
            'extendOn'          => $data['extend_on'],
            'marketingMember'   => $data['marketing_member'],
            'plan'              => $data['plan'],
            'deletedAt'         => $data['deleted_at'],            
            ];
    }
   
   //This function is use for add functionality.
    public function reverseTransform($data)
    {
        return [
            'created_at'         => date('Y-m-d H:i:s'),
            'registration_key'   => Crypt::encrypt($data['registrationKey']), //$data['registrationKey'],
            'registration_date'  => $data['registrationDate'],
            'duration'           => $data['duration'],
            'extend_on'          => Crypt::encrypt($data['extendOn']), //$data['extendOn'],
            'marketing_member'   => $data['marketingMember'],
            'plan'               => $data['plan'],
            ];
    }
}
<?php
namespace App\MediPlus\Transformers;

use App\Branch;
use App\Distributor;
use App\user;
use App\Product;
use App\Stock;
use App\MediPlus\Transformers\ProductTransformer;
use App\MediPlus\Transformers\BranchTransformer;
use App\MediPlus\Transformers\DistributorTransFormer;
use App\MediPlus\Transformers\UserTransformer;
use DB;

class StockTransformer extends  Transformer
{
    //This function is use for show functionality.
    public function transform($data,$show=true)
    {
        if($show==true)
        {
            return[
                'id'               => $data['id'],
                'createdAt'        => $data['created_at'],
                'updatedAt'        => $data['updated_at'],
                'deletedAt'        => $data['deleted_at'],
                //Get product name by product Id.
                'productName'      => (new Product)->getProductName($data['product_id']), 
                //Get product information (deleted product information also show)
                'product'          => (new ProductTransformer)->transform(Product::withTrashed()->find($data['product_id'])),
                'quantity'         => $data['quantity'],
                'invoiceId'        => $data['invoice_id'], 
                //Get branch information (deleted branch information also show)
                'branch'           => (new BranchTransformer)->transform(Branch::withTrashed()->find($data['branch_id'])), 
                ];
        }
        else
        {
            return[
                'id'               => $data['id'],
                'createdAt'        => $data['created_at'],
                'updatedAt'        => $data['updated_at'],
                'deletedAt'        => $data['deleted_at'],
                //Get product name by product Id.
                'productName'      => (new Product)->getProductName($data['product_id']),
                'quantity'         => $data['quantity'],
                'invoiceId'        => $data['invoice_id'],
                //Get branch name by branch Id.
                'branchName'       => (new Branch)->getBranchName($data['branch_id']),
                ];  
        }
    }
  
    //This function is use for add functionality.  
    public function reverseTransform($data)
    {
        return [
            'created_at'        => date('Y-m-d H:i:s'),
            'product_id'        => $data['productId'],
            'quantity'          => $data['quantity'],
            'invoice_id'        => $data['invoiceId'],
            'branch_id'         => $data['branchId']
            ];
    }
    
    //This function is use to update product quantity in stock table.
    public function stockUpdate($data)
    {       
        DB::table('stock')
            ->where('product_id','=', $data->productId)
            ->update(['quantity' => $data->quantity]);  
    }
}
<?php
namespace App\MediPlus\Transformers;
use App\MediPlus\Transformers\BranchTransformer;
use App\Branch;
use Session;

class CustomerTransformer extends  Transformer
{
    //This function is use for show functionality.
    public  function transform($data,$show=true)
    {
        return[
            'id'                => $data['id'],
            'createdAt'         => $data['created_at'],
            'updatedAt'         => $data['updated_at'],
            'name'              => $data['first_name'].' '.$data['middle_name'].' '.$data['last_name'],
            'firstName'         => $data['first_name'],
            'middleName'        => $data['middle_name'],
            'lastName'          => $data['last_name'],
            'city'              => $data['city'],
            'district'          => $data['district'],
            'state'             => $data['state'],
            'country'           => $data['country'],
            'streetAddress'     => $data['street_address'],
            'pinCode'           => $data['pin_code'],
            'emailId'           => $data['email_id'],
            'dob'               => date("d-m-Y", strtotime($data['dob'])),
            'contactNumber'     => $data['contact_number'],
            'creditLimit'       => $data['credit_limit'],
            'creditPeriod'      => $data['credit_period'],
            'customerType'      => $data['customer_type'],
            //Get branch information (deleted branch information also show) 
            'branch'            => (new BranchTransformer)->transform(Branch::withTrashed()->find($data['branch_id'])),
            'deletedAt'         => $data['deleted_at'],            
            ];
    }
   
    //This function is use for add functionality.
    public function reverseTransform($data)
    {
        return [
            'created_at'        => date('Y-m-d H:i:s'),
            'first_name'        => $data['firstName'],
            'middle_name'       => $data['middleName'],
            'last_name'         => $data['lastName'],
            'city'              => $data['city'],
            'district'          => $data['district'],
            'state'             => $data['state'],
            'country'           => $data['country'],
            'street_address'    => $data['streetAddress'],
            'pin_code'          => $data['pinCode'],
            'email_id'          => $data['emailId'],
            'dob'               => date("Y-m-d H:i:s", strtotime($data['dob'])),
            'contact_number'    => $data['contactNumber'],
            'credit_limit'      => $data['creditLimit'],
            'credit_period'     => $data['creditPeriod'],
            'customer_type'     => $data['customerType'],
            'branch_id'         => $data['branchId']
            ];
    }
}
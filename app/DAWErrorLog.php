<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DAWErrorLog extends Model
{
    protected $table = 'DAW_Error_Log';
    
        protected $fillable = ['user_id', 'stack_tace', 'short_message','error_type','line_no','file'];

    
}
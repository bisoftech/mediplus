<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use App\MediPlus\Transformers\PurchaseOrderTransformer as PurchaseOrderTransformer;
use App\MediPlus\Transformers\UserTransformer as UserTransformer;
use App\MediPlus\Transformers\DistributorTransformer as DistributorTransformer;
use App\MediPlus\Transformers\ProductTransformer as ProductTransformer;
use App\MediPlus\Transformers\PurchaseOrderItemsTransformer as PurchaseOrderItemsTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;    
use App\MediPlus\Pagination\Paginations;
use Auth;
use Input;
use App\User;
use App\Branch;
use App\Distributor;
use App\PurchaseOrderItems;
use App\Product;
use App\OperationLog;
use App\UserBranchDetails;
use Log;
use Session;
use Illuminate\Support\Facades\Response;

class PurchaseOrder extends Model
{
    use SoftDeletes;
    protected $table = 'purchase_order';

    //This function is use to assign heading for standard reports.
    public $reportColumnTitle = array('Sr.No', 'Created Date', 'Updated Date', 'Order Date', 'Employee Name', 'Distributor Name', 'Product Name', 'Product Dosage', 'Product Location', 'Quantity', 'Packaging Number');
    
    //This function is use to assign heading for standard reports.
    public function reportheading()
    {       
        return array('Sr.No', 'Created Date', 'Updated Date', 'User Name', 'Owner Name', 'Branch Name', 'Deleted Date');
    }
    
    public function purchaseOrderItems()
    {
        return $this->hasMany('App\PurchaseOrderItems','purchase_order_id','id');
    }
    
    public function user() 
    {
        return $this->belongsTo('App\User');
    }
    
    public function distributor() 
    {
        return $this->belongsTo('App\Distributor');
    }
    
    public function branch() 
    {
        return $this->belongsTo('App\Branch');
    }
    
    //This function is use for show all purchase order details. 
    public function getAll()
    {  
        $resp=array();
        try
        {   
            $purchaseOrderTransformer      = new PurchaseOrderTransformer;
            $purchaseOrderItemsTransformer = new PurchaseOrderItemsTransformer;
            $limit    = Input::get('limit')?:10;
            $branchId = Session::get('branch_id');
            $results  = PurchaseOrder::where('branch_id', '=' , $branchId)->orderBy('id', 'DESC')->paginate($limit);
            $purchaseOrder   = $purchaseOrderTransformer->transformCollection($results, false);
            $resp['success'] = true;
            $resp['data']    = $purchaseOrder;
            $resp['pagination']  = (new Paginations)->results($results);     
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Purchase Order Details Not Available";
            $resp['ex']      = $e->getMessage();
        }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all purchase order details from table purchase_order successfully');
            (new OperationLog)->add($operationLog);
        return $resp;
    }

    //This function is use for add purchase order details in table purchase_order, purchase_order_items and relevent tables.
    public function add()
    {    
        $resp = array();
        try
        {
            $purchaseOrder     = (Input::get('purchaseOrder'));
            $purchaseOrderData = (new PurchaseOrderTransformer)->reverseTransform($purchaseOrder,(new PurchaseOrder));
            foreach(Input::get('purchaseOrderItems') as $value)
            {
                $purchaseOrderItemsData = (new PurchaseOrderItemsTransformer)->reverseTransform($value,(new purchaseOrderItems),$purchaseOrderData->id);
            }
            $resp['success'] = true;
            $resp['data']    = (new PurchaseOrderTransformer)->transform(PurchaseOrder::find($purchaseOrderData->id)); 
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => $purchaseOrderData->id,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table purchase_order with id '.$purchaseOrderData->id.' successfully');
            (new OperationLog)->add($operationLog);
        }
        catch(Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Purchase Order Details Added Failure";
            $resp['ex']      = $e->getMessage();
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => 0,
                                  "operationStatus"  => 'fail',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table purchase_order failure');
            (new OperationLog)->add($operationLog);
        }
            
        return $resp;
    }
    
    //This function is use for show purchase order details by purchase order Id.
    public function getById($id)
    {
       $resp = array();
       try
       {
           $branchId        = Session::get('branch_id');
           $purchaseorder   = PurchaseOrder::where('branch_id', '=' , $branchId)->findOrFail($id);
           $purchaseOrderTransformer = new PurchaseOrderTransformer;
           $resp['success'] = true;
           $resp['data']    = $purchaseOrderTransformer->transform($purchaseorder);
           $operationLog=array("userId"            => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'success',
                                "customMessage"    => 'user id '.Auth::User()->id.' view purchase order details in table purchase_order with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
       }
       catch(\Exception $e)
       {
           $resp['success'] = false;
           $resp['msg']     = $e->getMessage().' '."Purchase Order Detail Not Available";
           $resp['ex']      = $e->getMessage();
           $operationLog=array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'fail',
                                "customMessage"    => 'user id '.Auth::User()->id.' view purchase order details in table purchase_order with id '.$id.' failure');
            (new OperationLog)->add($operationLog); 
       }
            
       return $resp; 
    }

    //This function is use to display purchase order details for search any keyword.
    public function autocomplete($data)
    {
       $resp=array();     
       try
       {     
            $limit             = Input::get('limit')?:10;
            $branchId = Session::get('branch_id'); 
            $purchaseOrderData = PurchaseOrder::whereIn('user_id',User::Where('user_name', 'like', '%'.$data.'%')->lists('id'))
                                            ->orWhereIn('distributor_id',Distributor::Where('owner_name', 'like', '%'.$data.'%')
                                            ->lists('id'))
                                            ->orWhereIn('branch_id',Branch::Where('branch_name', 'like', '%'.$data.'%')
                                            ->lists('id'))
                                            ->orWhere('order_date', 'like', '%'.$data.'%')
                                            ->paginate($limit);
            $resp['data']       = (new PurchaseOrderTransformer)->transformCollection($purchaseOrderData);
            $resp['pagination'] = (new Paginations)->results($purchaseOrderData);  
            $resp['msg']        = "Purchase Order Details Information";
       }
       catch(\Exception $e)
       {
            $resp['success']    = false;
            $resp['msg']        = $e->getMessage().' '."Purchase Order Details Not Available";
            $resp['ex']         = $e->getMessage();
       }
       return $resp;
    }
    
    //This function is use to search purchase order details using purchase order Id
    public function autocompleteById($id)
    {  
       $resp = array();
       try
       {
            $limit           = Input::get('limit')?:10;
            $resp['success'] = true;
            $purchaseOrderData= PurchaseOrder::where('purchase_order.id', 'like', '%'.$id.'%')
                                               ->where('status', '=', 'Active')->get();
            $main_array=  array();

            foreach($purchaseOrderData as $purchaseOrder)
            {
                $var_arr = array();
                $product = array();
                $user = array();
                $distributor = array();
                $purchaseOrderItems = array();
               
               foreach($purchaseOrder->purchaseOrderItems as $purchaseOrderProduct)
               {
                  array_push($purchaseOrderItems, (new PurchaseOrderItemsTransformer)->transform($purchaseOrderProduct, false));
                  array_push($product, (new ProductTransformer)->transform($purchaseOrderProduct->product));
               }
                $var_arr = (new PurchaseOrderTransformer)->transform($purchaseOrder);
                $var_arr['distributor'] = (new DistributorTransformer)->transform($purchaseOrder->distributor);
                $var_arr['user'] = (new UserTransformer)->transform($purchaseOrder->user);
                array_push($main_array, $var_arr);
           }
           $resp['data'] = $main_array;
           $resp['msg']  = "Purchase Order Details Information";
       }
       catch(\Exception $e)
       {
               $resp['success']     = false;
               $resp['msg']         = $e->getMessage();  
       }
       return $resp;
    }
   
    //This function is use to get all purchase order details in-beetween of two dates.
    public function history()
    {
       $resp = array();
       try
       {
           $limit    = Input::get('limit')?:10;
           $fromDate = Input::get('fromDate');
           $toDate   = Input::get('toDate');
           $from     = date("Y-m-d H:i:s", strtotime($fromDate));
           $to       = date("Y-m-d H:i:s", strtotime($toDate)); 
           $productData = Product::whereBetween('created_at',array($from, $to))->paginate($limit);          
           foreach($productData as $value)
           {
                foreach(PurchaseOrderItems::where('product_id', '=', $value->id)->whereBetween('created_at',array($from, $to))->lists('purchase_order_id') as $purchaseOrderData)
                {
                    $resp['data'][]     = (new PurchaseOrderTransformer)->transform(PurchaseOrder::find($purchaseOrderData));
                    $resp['pagination'] = (new Paginations)->results($productData);
                }
           }
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Please Select Date";
            $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }
    
    //This function is use to show purchase order details for selected branch Id.
    public function getByBranchId($branchId)
    {  
        $resp=array();
        try
        {   
            $purchaseOrderTransformer      = new PurchaseOrderTransformer;
            $purchaseOrderItemsTransformer = new PurchaseOrderItemsTransformer;
            $limit   = Input::get('limit')?:10;
            $results = $this::where('branch_id', '=' , $branchId)->paginate($limit);
            $purchaseOrder   = $purchaseOrderTransformer->transformCollection($results, false);
            $resp['success'] = true;
            $resp['data']    = $purchaseOrder;
            $resp['pagination']  = (new Paginations)->results($results);     
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Purchase Order Details Not Available";
            $resp['ex']      = $e->getMessage();
        }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all purchase order details from table purchase_order successfully');
            (new OperationLog)->add($operationLog);
        return $resp;
    }
    
    //This function is use to get all purchase order details.
    //This function is use for standard reports.
    public function getDataForReports($fromDate, $toDate, $myRequestObject)
    {       
        $branchId = Session::get('branch_id');
        $table = PurchaseOrder::join('users', 'purchase_order.user_id', '=', 'users.id')
                          ->join('distributor', 'purchase_order.distributor_id', '=', 'distributor.id')
                          ->join('branch', 'purchase_order.branch_id', '=', 'branch.id')
                          ->join('purchase_order_items', 'purchase_order.id', '=', 'purchase_order_items.purchase_order_id')
                          ->join('product', 'purchase_order_items.product_id', '=', 'product.id')
                          ->select('purchase_order.*', 'users.user_name', 'distributor.owner_name', 'product.name', 'product.dosage', 'product.product_location', 'purchase_order_items.packaging_number', 'purchase_order_items.quantity')
                          ->orderBy('id', 'asc')
                          ->where('purchase_order.branch_id', '=' , $branchId)
                          ->whereBetween('purchase_order.created_at',array($fromDate, $toDate))->get();
        $reportName  = new PurchaseOrder;
        $purchaseOrderData = array();
        $i = 0;
        foreach($table as $row) 
        {      
            $i++;
            array_push($purchaseOrderData, array($i, $row['created_at']->format('d-m-Y'), $row['updated_at']->format('d-m-Y'), $row['order_date'], $row['user_name'], $row['owner_name'], $row['name'], $row['dosage'], $row['product_location'], $row['quantity'], $row['packaging_number']));
        }
        return $purchaseOrderData;
    }
}
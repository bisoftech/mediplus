<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\MediPlus\Transformers\StockTransformer;
use App\MediPlus\Pagination\Paginations;
use Input;
use Auth;
use App\OperationLog;
use App\User;
use Log;
use Session;

class Stock extends Model
{
    protected $table = 'stock';
    use SoftDeletes;
    
    //This function is use to assign heading for standard reports.
    public $reportColumnTitle = array('Sr.No', 'Product Name', 'Quantity', 'Parcel Number', 'Invoice Date', 'Total Amount', 'Amount Paid', 'Payment Type', 'Cheque Number', 'User Name', 'Order Date', 'Owner Name', 'Branch Name');
    
    //This function is use for show all stock details. 
    public function getAll()
    {
        $resp = array();
        try
        {
            $limit   = Input::get('limit')?:10;
            $branchId  = Session::get('branch_id');
            $results = Stock::where('branch_id', '=' , $branchId)->where('quantity', '>', 0)->orderBy('id', 'DESC')->paginate($limit);
            $resp['success']     = true;
            $resp['data']        = (new StockTransformer)->transformCollection($results,true);    
            $resp['pagination']  = (new Paginations)->results($results);    
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Stock Details Not Available";
            $resp['ex']      = $ex->getMessage();
        }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all stock details from table stock successfully');
            (new OperationLog)->add($operationLog);
        return $resp;
    }
    
    //This function is use for show stock details by selected stock Id.
    public function getById($id)
    {
        $resp = array();
        try
        {
            $resp['success'] = true;
            $branchId               = Session::get('branch_id');
            $stock   = Stock::where('branch_id', '=' , $branchId)->findOrFail($id);
            $stockTransformer = new StockTransformer;
            $resp['data']    = (new StockTransformer)->transform($stock);
            $operationLog=array("userId"           => Auth::User()->id,
                                 "operation"        => 'View',
                                 "referenceTableId" => $id,
                                 "operationStatus"  => 'success',
                                 "customMessage"    => 'user id '.Auth::User()->id.' view stock details in table stock with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Stock Details Not Available";
            $resp['ex']      = $ex->getMessage(); 
            $operationLog=array("userId"            => Auth::User()->id,
                                 "operation"        => 'View',
                                 "referenceTableId" => $id,
                                 "operationStatus"  => 'fail',
                                 "customMessage"    => 'user id '.Auth::User()->id.' view stock details in table stock with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
        }
        return $resp;
    }
    
    //This function is use to add stock details in stock table.
    public function add()
    {
        $resp = array();
        try
        {
            $resp['success'] = true;
            foreach(Input::all() as $value)
            {
            $value["branchId"] = Session::get('branch_id');  
            $stock= (new StockTransformer)->reverseTransform($value);
            $stockId= Stock::insertGetId($stock);
            }
            $resp['data']    = (new StockTransformer)->transform(Stock::find($stockId));
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => $stockId,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table stock with id '.$stockId.' successfully');
            (new OperationLog)->add($operationLog);
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Stock Details Added Failure";
            $resp['ex']      = $ex->getMessage();
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => 0,
                                  "operationStatus"  => 'fail',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table stock failure');
            (new OperationLog)->add($operationLog);
        }
        return $resp;
    }
    
    //This function is use to update stock details by selected stock Id.
    public function updateById($id)
    {
        $resp = array();
        try
        {
            $arr             = (new StockTransformer)->reverseTransform(Input::all(),Stock::find($id));
            Stock::where('id',$id)->update($arr);
            Stock::findOrFail($id)->save();
            $resp['success'] = true;
            $resp['data']    = (new StockTransformer)->transform(Stock::findOrFail($id));
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Stock Details Not Available";
            $resp['ex']      = $ex->getMessage();
        }
        return $resp;   
    }
    
    //This function is use to delete stock details by selected stock Id.
    public function deleteById($id)
    {    
        $resp = array();
        try
        {
            $stock = Stock::findOrFail($id);
            $stock->delete();
            $limit   = Input::get('limit')?:10;
            $results = $stock::paginate($limit);
            $stockTransformer    = new StockTransformer;
            $resp['success']     = true;
            $resp['data']        = $stockTransformer->transform($stock);
            $resp['pagination']  = (new Paginations)->results($results);      
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Stock Details Not Available";
            $resp['ex']      = $ex->getMessage();
        }
        return $resp;
    }
    
    //This function is use to display stock details for search any keyword.
    public function autocomplete($data)
    {
       $resp = array();
       try
       {
            $limit           = Input::get('limit')?:10;
            $branchId        = Session::get('branch_id');
            $resp['success'] = true;
            $dataStock = Stock::whereIn('product_id',Product::Where('name', 'like', '%'.$data.'%')->lists('id'))  
                                            ->orWhere('quantity', 'like', '%'.$data.'%')
                                            ->where('branch_id', '=' , $branchId)
                                            ->paginate($limit);  
            $resp['data']    = (new StockTransformer)->transformCollection($dataStock,true);
            $resp['pagination'] = (new Paginations)->results($dataStock);
            $resp['msg']        = "Stock Details Information";     
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Stock Details Not Available";
            $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }
    
    //This function is use to get all stock details in-beetween of two dates.
    public function history()
    {
        $resp        = array();
        $limit       = Input::get('limit')?:10;
        $fromDate    = Input::get('fromDate');
        $toDate      = Input::get('toDate');
        $from        = date("Y-m-d H:i:s", strtotime($fromDate));
        $to          = date("Y-m-d H:i:s", strtotime($toDate)); 
        $stock       = Stock::whereBetween('created_at',array($from, $to))->paginate($limit);
        if($fromDate!='' && $toDate!='')
        {
            $stockTransformer       = new StockTransformer;
            $resp['success']        = "true";
            $resp['msg']            = "Stock Details Available From search date";
            $resp['data']           = $stockTransformer->transformCollection($stock); 
            $resp['pagination']     = (new Paginations)->results($stock);
        }
        else
        {
            $resp['success'] = "false";
            $resp['msg']     = "Please Select date";
        }
        return $resp;
    }
    
    //This function is use to get all shop details.
    //This function is use for standard reports.
    public function getDataForReports($fromDate, $toDate, $myRequestObject)
    {        
        $table = Stock::join('product', 'stock.product_id', '=', 'product.id')
                        ->join('invoice', 'stock.invoice_id', '=', 'invoice.id')
                        ->join('users', 'invoice.user_id', '=', 'users.id')
                        ->join('purchase_order', 'invoice.purchase_order_id', '=', 'purchase_order.id')
                        ->join('distributor', 'purchase_order.distributor_id', '=', 'distributor.id')
                        ->join('branch', 'purchase_order.branch_id', '=', 'branch.id')
                        ->select('stock.*', 'invoice.parcel_number', 'invoice.invoice_date', 'invoice.total_amount', 'invoice.amount_paid', 'invoice.payment_type', 'invoice.cheque_number', 'purchase_order.order_date', 'users.user_name', 'distributor.owner_name', 'branch.branch_name', 'product.name')
                        ->orderBy('id', 'asc')
                        ->whereBetween('stock.created_at',array($fromDate, $toDate))->get();
        $reportName  = new Stock;
        $stockData = array();
        $i = 0;
        foreach($table as $row) 
        {      
            $i++;
            array_push($stockData, array($i, $row['name'], $row['quantity'], $row['parcel_number'], $row['invoice_date'], $row['total_amount'], $row['amount_paid'], $row['payment_type'], $row['cheque_number'],                                                    $row['user_name'], $row['order_date'], $row['owner_name'], $row['branch_name']));
        }
        return $stockData;
    }

    //This function is use to get prduct quantity from stock table for selecting product Id and Invoice Id. 
    public function getQuantityFromStock($productId,$invoiceId){
        $stockQty=0;
        $stock=Stock::select('stock.quantity')
            ->where('stock.product_id',$productId)
            ->where('stock.invoice_id',$invoiceId)
            ->get();
        foreach($stock as $stockItem)
            $stockQty+=($stockItem->quantity);
        return $stockQty;
    }
}
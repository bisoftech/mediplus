<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PurchaseOrder;
use App\PurchaseOrderItems;
use App\MediPlus\Transformers\PurchaseOrderTransformer as PurchaseOrderTransformer;
use App\MediPlus\Transformers\PurchaseOrderItemsTransformer as PurchaseOrderItemsTransformer;
use App\Product;

class PurchaseOrderItems extends Model
{
    protected $table = 'purchase_order_items';
    
    public function purchaseOrder()
    {
    return $this->belongsTo('App\PurchaseOrder','id');
    }   
    
    public function product() 
    {
        return $this->belongsTo('App\Product');
    }
}

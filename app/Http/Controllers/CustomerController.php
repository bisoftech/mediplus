<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Customer;
use App\MediPlus\Transformers\CustomerTransformer;

class CustomerController extends ApiController
{
    //Display a listing of the customer details.
    public function index()
    {
        $resp = array();
        try
        {
            $resp = (new Customer())->getAll(); //this function written in customer model file.
            if($resp['success'])
            {
                $resp['msg'] = "Customer Details Information";
                return $this->respond($resp); 
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can Not Display Customer Details');
        }
    }

    //Store a newly created customer details in storage.
    public function store()
    {
        $resp = array();
        try
        {
            $resp = (new Customer())->add(); //this function written in customer model file.
            if($resp['success'])
            {
                $resp['msg'] = "Customer Details Added Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Cannot submit Customer Details please try later');
        }
    }

    //Display the specified customer details, param int $id.
    public function show($id)
    {
        $resp = array();
        try
        {
            $resp=(new Customer())->getById($id); //this function written in customer model file.
            if($resp['success'])
            {
                $resp['msg'] = "Customer Detail Information";
                return $this->respond($resp); 
            }
            else
            {
                return  $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can Not Display Customer Details');
        }
    }

    //Show the form for updating the specified customer details, param int $id.
    public function update($id)
    {
        $resp = array();
        try
        {
            $resp = (new Customer())->updateById($id); //this function written in customer model file.
            if($resp['success'])
            {
                $resp['msg'] = "Customer Details Updated Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Customer Details Updated Failure');
        }
    }

    //Remove the specified resource from storage, param int $id.
    public function destroy($id)
    {
        $resp = array();
        try
        {
            $resp = (new Customer())->deleteById($id); //this function written in customer model file.
            if($resp['success'])
            {
                $resp['msg'] = "Customer Details Deleted Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Customer Details Deleted Failure');
        }
    }
    
    //Display a listing of the customer bill details.
    public function getCustomerBillDetails($id)
    {
        $resp = array();
        try
        {
            $resp = (new Customer())->CustomerBillDetails($id); //this function written in customer model file.
            if($resp['success'])
            {
                $resp['msg'] = "Customer Bill Details Display Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Customer Bill Details Display Failure');
        }
    }
}
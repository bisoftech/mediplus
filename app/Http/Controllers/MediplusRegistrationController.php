<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MediplusRegistration;
use App\MediPlus\Transformers\MediplusRegistrationTransformer;
use DB;
use Crypt;
use Input;

class MediplusRegistrationController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkPeriodExpired()
    {
        return (new MediplusRegistration())->checkPeriodExpired();
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $resp = array();
        try
        {
            $resp = (new MediplusRegistration())->add();
            if($resp['success'])
            {
                $resp['msg'] = "Mediplus Registration Details Added Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Cannot submit Mediplus Registration Details please try later');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $resp = array();
        try
        {
            $resp = (new MediplusRegistration())->updateById($id);
            if($resp['success'])
            {
                $resp['msg'] = "Mediplus Registration Details Updated Succesfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Mediplus Registration Details Updated Failure');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function onlineRegistration()
    {
        return 1;
    }
    public function isValid()
    {
        $resp=array();
        $mediplusRegistration=DB::table('mediplus_registration')->get();
        if(count($mediplusRegistration))
        {

        //$extendValue = DB::table('mediplus_registration')->select('extend_on')->get()[0]->extend_on;
        //$registrationKeyValue = DB::table('mediplus_registration')->select('registration_key')->get()[0]->registration_key;
        //$validTill = DB::table('mediplus_registration')->select('duration')->get()[0]->duration;
        //$plan = DB::table('mediplus_registration')->select('plan')->get()[0]->plan;
        //$registrationDate = DB::table('mediplus_registration')->select('registration_date')->get()[0]->registration_date;
        //$marketingMember = DB::table('mediplus_registration')->select('marketing_member')->get()[0]->marketing_member;
        
        $extendValue = DB::table('mediplus_registration')->select('extend_on')->get();
        $registrationKeyValue = DB::table('mediplus_registration')->select('registration_key')->get();
        $validTill = DB::table('mediplus_registration')->select('duration')->get();
        $plan = DB::table('mediplus_registration')->select('plan')->get();
        $registrationDate = DB::table('mediplus_registration')->select('registration_date')->get();
        $marketingMember = DB::table('mediplus_registration')->select('marketing_member')->get();

        $resp['is_valid'] = true;
        $resp['success'] = True;
        //$resp['registrationKey'] = $registrationValue;
        //$resp['extendOn'] = $extendOn;

        if((sizeof($extendValue) > 0) && (sizeof($registrationKeyValue) > 0) && (sizeof($validTill) > 0) && (sizeof($plan) > 0) && (sizeof($registrationDate) > 0) && (sizeof($marketingMember) > 0))
        {
            $medi = array('extendOn'=>'aa','registrationKey'=>'a','duration'=>$validTill[0]->duration,'plan'=>$plan[0]->plan,'registrationDate'=>$registrationDate[0]->registration_date,'marketingMember'=>$marketingMember[0]->marketing_member);
        } 

        //$medi = array('extendOn'=>'aa','registrationKey'=>'a','duration'=>$validTill,'plan'=>$plan,'registrationDate'=>$registrationDate,'marketingMember'=>$marketingMember);
        $resp['data'] = $medi;           

        
        }
        else
        {
        $resp['is_valid'] = False;
        $resp['success'] = True;

        }

        
 
        /*$resp['']  = $extendOn;
        $resp['']  = $registrationValue;*/
        return $resp;
    }

    public function generateRegistrationKey()
    {
        $resp = array();
        $data=Input::all();
        try
        {
            $resp = (new MediplusRegistration())->generateRegistrationKey($data);
            if($resp['success'])
            {
                $resp['msg'] = "Mediplus Registration Details Added Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Cannot submit Mediplus Registration Details please try later');
        }
    }

    public function generateKeys()
    {
        $resp = array();
        try
        {
            $resp = (new MediplusRegistration())->generateMacAddress();
            if($resp['success'])
            {
                $resp['msg'] = "Mediplus Registration Details Added Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Cannot submit Mediplus Registration Details please try later');
        }
    }

    public function completeRegistration()
    {
        $resp = array();
        $data=Input::all();
        try
        {
            $resp = (new MediplusRegistration())->completeRegistration($data);
            if($resp['success'])
            {
                $resp['msg'] = "Mediplus Registration Details Added Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Cannot submit Mediplus Registration Details please try later');
        }
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mockery\CountValidator\Exception;
use App\MediPlus\Transformers\ProductTransformer as ProductTransformer;
use App\Product;
use DB;
use Auth;
use App\InvoiceItems;
use App\Invoice;
use Log;
use App\PurchaseOrder;
use App\Branch;

class ProductController extends ApiController
{
    //Display a listing of the product details.
    public function index()
    {
        $resp = array();
        try
        {
            $resp    = (new Product())->getAll();
            if($resp['success'])
            {
                $resp['msg'] = "Product Details Information";
                return $this->respond($resp);
            }
            else
            {
                 return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Product details');
        }
    }
    
    //Store a newly created product details in storage.   
    public function store()
    {
        $resp = array();
        try
        {
            $resp = (new Product())->add();
            if($resp['success'])
            {
                $resp['msg'] = "Product Details Added Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Cannot submit Product Details please try later');
        }
    }

    //Display the specified product details, param int $id.
    public function show($id)
    {
        $resp = array();
        try
        {
            $resp = (new Product())->getById($id);
            if($resp['success'])
            {
                $resp['msg'] = "Product Detail Information";
                return $this->respond($resp); 
            }
            else
            {
                return  $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Product details');
        }
    }
    
    //Show the form for updating the specified product details, param int $id.
    public function update($id)
    {
        $resp = array();
        try
        {
            $resp = (new Product())->updateById($id);
            if($resp['success'])
            {
                $resp['msg'] = "Product Details Updated Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Product Details Updated Failure');
        }
    }
    
    //Remove the specified resource from storage, param int $id.
    public function destroy($id)
    {
        $resp = array();
        try
        {
            $resp = (new Product())->deleteById($id);
            if($resp['success'])
            {
                $resp['msg'] = "Product Details Deleted Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Product Details Deleted Failure');
        }
    }
    
    //Display the product details for search any keyword.
    public function autocomplete($data)
    {
        $resp = array();
        try
        {
            $resp = (new Product())->autocomplete($data);
            if($resp['success'])
            {
                $resp['msg'] = "Product Details Available";
                return $this->respond($resp);
            }
        }
        catch (\Exception $ex)
        {
            return $this->setStatusCode(500)->respondWithError('Product Details Not Available');
        }
    }
    
    //This function is use to get the custom report details in excel file.
    public function reporttoExcel()
    {
        $resp = array();
        $data = Input::all();
        $fromDate   = Input::get('fromDate');
        $toDate     = Input::get('toDate');
        $from       = date("Y-m-d H:i:s", strtotime($fromDate));
        $to         = date("Y-m-d H:i:s", strtotime($toDate));
        $data = Input::all();
        
        if(in_array("productDetails",$data))
        {
            $qr=DB::table('product');
            $qrSelect=array();
            $dataSelect=$data['select'][0];
            $invoiceItemJoinedFlag=false;
            foreach ($dataSelect as  $value) 
            {
                if("productName"==$value["id"])
                {
                    $qrSelect[]= "product.name";
                    $head[]= 'Product Name';       
                }
                        
                if('productManufacturar'==$value["id"])
                {
                    $qrSelect[]="product.manufacturer";
                    $head[]= 'Manufacturer'; 
                }
                        
                if('productFormula'==$value["id"])
                {
                    $qrSelect[]="product.formula";
                    $head[]= 'Formula';
                }
                        
                if((('invoiceItemsQuantity'==$value["id"] )|| ('invoiceItemsExpiryDate'==$value["id"])) && !$invoiceItemJoinedFlag)
                {
                    $invoiceItemJoinedFlag=!$invoiceItemJoinedFlag;
                    $qr->join('invoice_items', 'product.id', '=', 'invoice_items.product_id');
                }
                        
                if('productDosage'==$value["id"])
                {
                    $qrSelect[]="product.dosage";
                    $head[]= 'Dosage';
                }

                if('invoiceItemsQuantity'==$value["id"])
                {
                    $qrSelect[]="invoice_items.quantity";
                    $qr->where('invoice_items.quantity',trim($data['condition']['invoiceItemsQuantityOp']),$data['condition']['invoiceItemsQuantity']);
                }
                        
                if('invoiceItemsExpiryDate'==$value["id"])
                {
                    $qrSelect[]="invoice_items.expiry_date";
                    $from     = date("Y-m-d H:i:s", strtotime($data['condition']['invoiceItemsExpiryDate']));
                    $to     = date("Y-m-d H:i:s", strtotime($data['condition']['toDate']));
                    $qr->whereBetween('invoice_items.expiry_date',array($from, $to));
                }
                $resp['data'] = $qr->select($qrSelect)->get();
                $resp['head'] = $head;
            }
            return $resp;
        }
       
        if(in_array("customerDetails",$data))
        {   
            $qr=DB::table('customer');
            $qrSelect=array();
            $dataSelect=$data['select'][0];
            foreach ($dataSelect as  $value) 
            {
                if("customerName"==$value["id"])
                {
                    $qrSelect[]="customer.first_name"; 
                    $head[]= 'Name';    
                }

                if("customerAddress"==$value["id"])
                {
                    $qrSelect[]="customer.city"; 
                    $head[]= 'Address';
                }
                
                if("customerEmail"==$value["id"])
                {
                    $qrSelect[]="customer.email_id";
                    $head[]= 'Email ID';
                }

                if("customerContactNumber"==$value["id"])
                {
                    $qrSelect[]="customer.contact_number";
                    $head[]= 'Contact Number';
                }
                
                if("customerCreditlimit"==$value["id"])
                {
                    $qrSelect[]="customer.credit_limit";
                    $qr->where('customer.credit_limit','=',$data['condition']['customerCreditlimit']);
                    $head[]= 'Credit Limit';
                }
                
                if("customerCustomerType"==$value["id"])
                {
                    $qrSelect[]="customer.customer_type";
                    $qr->where('customer.customer_type','=',$data['condition']['customerCustomerType']);
                    $head[]= 'Customer Type';
                }
            
                if("billingTotalAmount"==$value["id"])    
                {
                    $qr->join('billing', 'customer.id', '=', 'billing.customer_id');
                }
                        
                if("billingTotalAmount"==$value["id"])
                {
                    $qrSelect[]="billing.total_amount";
                    $head[]= 'Total Amount';
                }
                        
                if("billingStatus"==$value["id"])
                {
                    $qrSelect[]="billing.status";
                    $qr->where('billing.status','=',$data['condition']['billingStatus']);
                    $head[]= 'Status';
                }
                        
                if("billingPaymentType"==$value["id"])
                {
                    $qrSelect[]="billing.payment_type";
                    $qr->where('billing.payment_type','=',$data['condition']['billingPaymentType']);
                    $head[]= 'Payment Type';
                }

                $qrData=$qr->select($qrSelect)->get();
                $resp['data']= $qrData;
                $resp['header']=$data['select'];
                $resp['head'] = $head;
            }
            return $resp;
        }
       
        if(in_array("purchaseOrderDetails",$data))
        {            
            $qr=DB::table('purchase_order');
            $qrSelect=array();
            $dataSelect=$data['select'][0];
            foreach ($dataSelect as  $value) 
            {
                if("poUser"==$value["id"])
                {
                    $qrSelect[] = 'users.first_name';
                    $qr->join('users', 'users.id', '=', 'purchase_order.user_id')->get();
                    $head[]= 'User Name';
                }
                        
                if("poBranch"==$value["id"])
                {
                    $qrSelect[]="branch.branch_name";  
                    $qr->join('branch', 'branch.id', '=', 'purchase_order.branch_id')->get();
                    $head[]= 'Branch Name';
                }
                        
                if("poDistrubutor"==$value["id"])
                {
                    $qrSelect[]="distributor.owner_name";  
                    $qr->join('distributor', 'distributor.id', '=', 'purchase_order.distributor_id')->get();
                    $head[]= 'Distributor Name';
                }
                        
                if("poPlacedDate"==$value["id"])
                {
                    $qrSelect[]="purchase_order.order_date";
                    $from     = date("Y-m-d H:i:s", strtotime($data['condition']['poPlacedDate']));
                    $to     = date("Y-m-d H:i:s", strtotime($data['condition']['toDate']));
                    $qr->whereBetween('purchase_order.order_date',array($from, $to));
                    $head[]= 'Order Date';
                }
             
                $resp['data'] = $qr->select($qrSelect)->get();;
                $resp['head'] = $head;
            }
            return $resp;   
        }
        
        if(in_array("stockDetails",$data))
        {
            $qr=DB::table('stock');
            $qrSelect=array();
            $dataSelect=$data['select'][0];
            foreach ($dataSelect as  $value) 
            {
                if("stockProduct"==$value["id"])
                {
                    $qrSelect[] = 'product.name';
                    $qr->join('product', 'product.id', '=', 'stock.product_id')->get();
                    $head[]= 'Product Name';
                }
                        
                if("stockBranch"==$value["id"])
                {
                    $qrSelect[]="branch.branch_name";  
                    $qr->join('branch', 'branch.id', '=', 'stock.branch_id')->get();
                    $head[]= 'Branch Name';
                }
                        
                if("stockInvoiceId"==$value["id"])
                {
                    $qrSelect[]="stock.invoice_id";
                    $head[]= 'Invoice Id';
                }
                
                if("stockQuantity"==$value["id"])
                {
                    $qrSelect[]="stock.quantity";
                    $head[]= 'Quantity';
                }
                
                $resp['data'] = $qr->select($qrSelect)->get();;
                $resp['head'] = $head;
            }
            return $resp;   
        }
    }
}
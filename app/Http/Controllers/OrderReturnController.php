<?php

namespace App\Http\Controllers;

use App\PurchaseOrderReturnItems;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PurchaseOrderReturn;
use App\Http\Requests;
use App\MediPlus\Transformers\OrderReturnTransformer as OrderReturnTransformer;
use App\InvoiceItems;
use DB;
use Auth;
use Input;
use App\Invoice;
use App\Stock;
use Mockery\CountValidator\Exception;

class OrderReturnController extends ApiController
{
    //Display a listing of the product order return details.
    public function index()
    {
        $resp = array();
        try
        {
            $resp = (new PurchaseOrderReturn())->getAll();
            if($resp['success'])
            {
                $resp['msg'] = "Order Return Details Information";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Order Return details');
        }
    }

    //Store a newly created product order return details in storage.
    public function store()
    {
        $resp = array();
        try
        {
            $resp = (new PurchaseOrderReturn())->add();
            if($resp['success'])
            {
                $resp['msg'] = 'Order Return Details Added Successfully';
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Cannot submit Order Return Details please try later');
        }
    }

    //Display the specified product order return details, param int $id.
    public function show($id)
    {
        $resp = array();
        try
        {
            $resp = (new PurchaseOrderReturn())->getById($id);

            if($resp['success'])
            {
                $resp['msg'] = 'Order Return Detail Information';
                return $this->respond($resp); 
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Can not Display Order Return details');
        }
    }

    //Show the form for updating the specified product order return details, param int $id.
    public function update($id)
    {
        $resp = array();
        try
        {
            $resp = (new OrderReturn())->updateById($id);
            if($resp['success'])
            {
                $resp['msg'] = 'Order Return Details Updated Successfully';
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Order Return details Updated Failure');
        }
    }

    //Show the form for updating the specified product order return details, param int $id.
    public function updateReturnItem($id){
        $resp=array();
        try
        {
            $resp=(new PurchaseOrderReturnItems())->updateById($id);
            if($resp['success'])
            {
                $resp['msg'] = 'Order Return Details Updated Successfully';
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(401)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Order Return details Updated Failure');
        }
    }

    //Remove the specified resource from storage, param int $id.
    public function destroy($id)
    {
        $resp = array();
        try
        {
            $resp = (new OrderReturn())->deleteById($id);
            if($resp['success'])
            {
                $resp['msg'] = 'Order Return Details Deleted Successfully';
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Order Return details Deleted Failure');
        }
    }

    //Display the specified updated stock invoice details, param int $id.
    public function getInvoiceDetailWithStockUpdated($id){
        $resp = array();
        try
        {
            $resp = (new Invoice())->getById($id);
            foreach ($resp['data']['invoiceItems'] as $key => $value) {
               $resp['data']['invoiceItems'][$key]['quantity']=(new Stock())->getQuantityFromStock($value['productId'],$value['invoiceId']);
                
            }
            if($resp['success'])
            {
                $resp['msg'] = "Invoice Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return  $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Invoice details');
        } 
    }
}
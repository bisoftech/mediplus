<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MediPlus\Transformers\InvoiceTransformer as InvoiceTransformer;
use App\MediPlus\Transformers\InvoiceItemsTransformer as InvoiceItemsTransformer;
use App\PurchaseOrder;
use App\PurchaseOrderItems;
use App\User;
use App\Product;
use App\Invoice;
use App\InvoiceItems;
use DB;
use Auth;
use Input;
use Illuminate\Support\Facades\Response;
use Log;
use Illuminate\Database\Eloquent\SoftDeletes;
use Session;

class InvoiceController extends ApiController
{
    use SoftDeletes;

    //Display a listing of the invoice details.
    public function index()
    {
        $resp = array();
        try
        {
            $invoiceTransformer = new InvoiceTransformer;
            $resp = (new Invoice())->getAll();
            if($resp['success'])
            {
                $resp['msg'] = "Invoice Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Invoice details');
        }         
    }

    //Store a newly created invoice details in storage.
    public function store()
    {
        $resp = array();
        try
        {
            $resp = (new Invoice())->add();
            if($resp['success'])
            {
                $resp['msg'] = "Invoice Details Added Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Cannot submit Invoice Details please try later');
        }
    }

    //Display the specified invoice details, param int $id.
    public function show($id)
    {
        $resp = array();
        try
        {
            $resp = (new Invoice())->getById($id);
            if($resp['success'])
            {
                $resp['msg'] = "Invoice Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return  $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Invoice details');
        }       
    }
    
    //Display the specified branch related invoice details, param int $branchId.
    public function getDetailsByBranchId($branchId)
    {
        $resp = array();
        try
        {
            $resp = (new Invoice())->getByBranchId($branchId);
            if($resp['success'])
            {
                $resp['msg'] = "Invoice Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Invoice details');
        }
    }
    
    //Display the specified product price details, param int $productId.
    public function productPrice($id)
    {
        $resp = array();
        try
        {
            $resp = (new Invoice())->priceByProductId($id);
            if($resp['success'])
            {
                $resp['msg'] = "Product Price Available";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Product Price Not Available');
        }
    }
    
    //Display the specified invoice details in excel format.
    public function reporttoExcel()
    {
        $fromDate = Input::get('fromDate');
        $toDate   = Input::get('toDate');
        $from     = date("Y-m-d H:i:s", strtotime($fromDate));
        $to       = date("Y-m-d H:i:s", strtotime($toDate));
        $paymentType = 'credit';

        if($paymentType=='')
        {
            $table = Invoice::join('users', 'invoice.user_id', '=', 'users.id')
                            ->join('purchase_order', 'invoice.purchase_order_id', '=', 'purchase_order.id')
                            ->join('distributor', 'purchase_order.distributor_id', '=', 'distributor.id')
                            ->select('invoice.*', 'users.user_name', 'distributor.owner_name')
                            ->orderBy('id', 'asc')
                            ->whereBetween('invoice.created_at',array($fromDate, $toDate))->get();
            $filename = "invoice.csv";
            $handle   = fopen($filename, 'w+');
            fputcsv($handle, array('Sr.No', 'Created Date', 'Updated Date', 'Parcel Number', 'Invoice Date', 'Total Amount', 'Paid Amount', 'Payment Type', 'Received By', 'Disributor Name', 'Deleted Date'));

            foreach($table as $row) {
                fputcsv($handle, array($row['id'], $row['created_at'], $row['updated_at'], $row['parcel_number'], $row['invoice_date'], $row['total_amount'], $row['amount_paid'], $row['payment_type'], $row['user_name'], $row['owner_name'], $row['deleted_at']));
            }
        }
        else
        {
            $table = Invoice::select('invoice.*')
                              ->where('payment_type', '=', $paymentType)
                              ->whereBetween('invoice.created_at',array($fromDate, $toDate))->get();
            $filename = "invoice.csv";
            $handle   = fopen($filename, 'w+');
            fputcsv($handle, array('Sr.No', 'Created Date', 'Updated Date', 'Parcel Number', 'Invoice Date', 'Total Amount', 'Paid Amount', 'Payment Type'));

            foreach($table as $row) {
                fputcsv($handle, array($row['id'], $row['created_at'], $row['updated_at'], $row['parcel_number'], $row['invoice_date'], $row['total_amount'], $row['amount_paid'], $row['payment_type']));
            }
        }

        fclose($handle);

        $headers = array('Content-Type' => 'text/csv',);

        return Response::download($filename, 'invoice.csv', $headers);
    }
}
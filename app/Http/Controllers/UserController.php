<?php

namespace App\Http\Controllers;

use App\PurchaseOrderReturn;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\MediPlus\Transformers\UserTransformer as UserTransformer;
use App\MediPlus\Transformers\UserBranchDetailsTransformer as UserBranchDetailsTransformer;
use App\MediPlus\Transformers\ShopTransformer as ShopTransformer;
use App\MediPlus\Transformers\BranchTransformer as BranchTransformer;
use Hash;
use Auth;
use Illuminate\Contracts\Encryption\DecryptException;
use Crypt;
use Mockery\CountValidator\Exception;
use App\Product;
use App\MediplusRegistration;
use App\Customer;
use App\Distributor;
use App\Billing;
use App\User;
use App\BillingItems;
use App\UserBranchDetails;
use App\Branch;
use App\PurchaseOrder;
use App\Invoice;
use App\OrderReturn;
use App\SalesReturn;
use App\BranchBankDetails;
use App\Shop;
use App\Stock;
use App\Tax;
use DB;
use Session;
use Illuminate\Http\Request;
use Mail;
use Log;
use App\OperationLog;

class UserController extends ApiController
{
    //Display a listing of the user details.
    public function index()
    {
        $resp = array();
        try
        {
            $resp = (new User())->getAll();
            if($resp['success'])
            {
                $resp['msg'] = "User Details Information";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display User details');
        }
    }

    //Store a newly created user details in storage.
    public function store()
    { 
        $resp = array();
        $resp['success'] = true;
        $user             = (Input::all());
        $user["password"] = "secret"; 
        $userData         = (new UserTransformer)->reverseTransform($user,'save');
        $id=DB::table('users')->insertGetId($userData);
        $userData['id']=$id;
        (new UserBranchDetailsTransformer)->reverseTransform($user['branchId'],(new UserBranchDetails),$id);
        $resp['msg'] = "User Details Added Successfully";
        $resp['data'] = (new UserTransformer)->transform($userData,false);
       
        $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => User::find($id),
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table users with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
       
        return $resp;
    }
   
    //Display the specified user details, param int $id.
    public function show($id)
    {
        $resp = array();
        try
        {
            $resp=(new User())->getById($id);
            if($resp['success'])
            {
                $resp['msg'] = "User Details Information";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display User details');
        }
    }
   
    //Show the form for updating the specified user details, param int $id.
    public function update($id)
    {
        $resp = array();
        try
        {
            $resp = (new User())->updateById($id);
            if($resp['success'])
            {
                $resp['msg'] = "User Details Updated Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('User Details Updated Failure');
        }
    }

    //Remove the specified resource from storage, param int $id.
    public function destroy($id)
    {
        $resp = array();
        try
        {
            $resp = (new User())->deleteById($id);
            if($resp['success'])
            {
                $resp['msg'] = "User Deleted Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('User Details Deleted Failure');
        }
    }

    //This function is use to show message.
    public function login()
    {
        return $this->setStatusCode(404)->respondWithError('Please Login');
    }

    //This function is use to get login.
    public function  loginPost()
    {
        $resp = array();
        $userTransformer   = new UserTransformer;

        if(!Auth::attempt(['user_name'=>Input::get('username'),'password'=>Input::get('password')]))
        {
            return $this->setStatusCode(401)->respondWithError('Invalid Credentials');
        }
        $validate = $this->validateLogin();
        if(!$validate)
        {
            return $this->setStatusCode(401)->respondWithError('Period Expired');   
        }
        $id = Auth::User()->id;
        $user=Auth::user();
        $role = Auth::User()->role;
        $branchId = UserBranchDetails::select('branch_id')->where('user_id','=',$id)->get();
        $branch=Branch::where('id', '=', $user['UserBranchDetails'][0]['branch_id'])->get();
            if(isset($user['UserBranchDetails'][0]['branch_id'])){
            $shop=shop::where('id', '=', $branch[0]->shop_id)->get();
            $shopName=$shop[0]->brand_name;
            $user['brand_name']=$shopName;               
            }
        if($branchId->isEmpty())
        {
            return $this->setStatusCode(500)->respondWithError('Sorry you are not assign any branch');
        }
        else
        {
            $log = (new MediplusRegistration())->loginAdd();

            $expiredData =(new MediplusRegistration)->checkPeriodExpired();
            Log::info($expiredData);
            if($expiredData)
                return array('user'=>$userTransformer->transform($user,true)); 
            else
                return $this->setStatusCode(401)->respondWithError('Period Expired');
        }
    }

    //This function is use for check login details.
    public function validateLogin()
    {
        $isValid = false;
        $lastDate = DB::table('login_track_table')->select('last')->get();
        if((sizeof($lastDate) != 0))
        {
            Log::info(Crypt::decrypt($lastDate[0]->last));
            $lastRecord = Crypt::decrypt($lastDate[0]->last);  
            $time = strtotime($lastRecord); 
            Log::info($time);
            $newformat = date('Y-m-d',$time); 

            $currentDate = date("Y-m-d");

            if($newformat <= $currentDate)
            {
                $isValid=true;
                return true;
            }
            else
            {
               return false;
            }
        }
    }

    //This function is use to set login user branch name in session.
    public function setBranchNameInSession($branchId)
    {
        $resp = array();
        Session::put('branch_id', $branchId);
      
        if (Session::has('branch_id'))
        {
            $branch = Session::get('branch_id');
            $branchDetails = Branch::where('id', '=', $branch)->get();
            $resp['success'] = true;
            $resp['data'] = $branchDetails;
            $resp['msg'] = "User login branch";
        }
        else
        {
            $resp['success'] = false;
            $resp['msg'] = "Sorry, User not selected any branch";
        }
        return $resp;
    }
   
    //This function is use to show login user details.
    public function isLogin()
    {
        $userTransformer   = new UserTransformer;
        $branchTransformer = new BranchTransformer;
        $shopTransformer   = new ShopTransformer;
        $resp['success']   = Auth::check();
        if($resp['success'])
        {
            $branchId  = Session::get('branch_id');
            $id = Auth::User()->id;
            $branchDetails = Branch::where('id', '=', $branchId)->get();

            $resp['msg']     = 'User is Login';
            $user=Auth::user();
            $branch=Branch::where('id', '=', $user['UserBranchDetails'][0]['branch_id'])->get();
            if(isset($user['UserBranchDetails'][0]['branch_id']))
            {
                $shop=shop::where('id', '=', $branch[0]->shop_id)->get();
                $shopName=$shop[0]->brand_name;
                $user['brand_name']=$shopName;               
            }
            $branchDetails = Branch::where('id', '=', $branchId)->get();
            $resp['msg']     = 'User is Login';
            session::put('branchDetails', $branchDetails);
            $resp['data']    = array('user'=>$userTransformer->transform($user,true),'selectedBranch'=>$branchTransformer->transformCollection($branchDetails));
        }
        else
        {
            $resp['msg'] = 'User Not Login';
        }
        return $resp;
    }

    //This function is use for search database details by any keyword.
    public function search($search_type,$data)
    {
        if($search_type=='distributor')
            return (new Distributor)->autocomplete($data);
        if($search_type=='billing')
            return (new Billing)->autocomplete($data);
        if($search_type=='purchaseOrder')
            return (new PurchaseOrder)->autocomplete($data);
        if($search_type=='invoice')
            return (new Invoice)->autocomplete($data);
        if($search_type=='user')
            return (new User)->autocomplete($data);
        if($search_type=='product')
            return (new Product)->autocomplete($data);
        if($search_type=='branch')
            return (new Branch)->autocomplete($data);
        if($search_type=='orderReturn')
            return (new PurchaseOrderReturn())->autocomplete($data);
        if($search_type=='salesReturn')
            return (new SalesReturn)->autocomplete($data);
        if($search_type=='branchBankDetails')
            return (new BranchBankDetails)->autocomplete($data);
        if($search_type=='customer')
            return (new Customer)->autocomplete($data);
        if($search_type=='shop')
            return (new Shop)->autocomplete($data);
        if($search_type=='stock')
            return (new Stock)->autocomplete($data);
        if($search_type=='tax')
            return (new Tax)->autocomplete($data);
    }
   
    //This function is use for search database details by any keyword.
    public function autocomplete($search_type,$id)
    {
        if($search_type == 'invoice')
            return (new Invoice)->autocompleteById($id);
        if($search_type == 'invoiceByName')
            return (new Invoice)->autocompleteByName($id);
        if($search_type == 'purchaseOrder')
            return (new PurchaseOrder)->autocompleteById($id);
        if($search_type == 'product')
            return (new Product)->autocompleteById($id);
        if($search_type == 'productByName')
            return (new Product)->autocompleteByName($id);
        if($search_type == 'distributorByName')
            return (new Distributor)->autocompleteByName($id);
        if($search_type == 'orderReturnByName')
            return (new OrderReturn)->autocompleteByName($id);
        if($search_type == 'customerName')
            return (new Customer)->autocompleteByName($id);
        if($search_type == 'userByName')
            return (new User)->autocompleteByName($id);
        if($search_type == 'branchByName')
            return (new Branch)->autocompleteByName($id);
        if($search_type == 'productName')
            return (new Product)->autocompleteByProductName($id);
        if($search_type == 'price')
            return (new Product)->prising($id);
    }
    
    //This function is use for show eaach model details.
    public function history($history_type)
    {
        if($history_type == 'product')
        {
            $resp = (new Product)->history();
        }
        if($history_type == 'billing')
        {
            $resp = (new Billing)->history();
        }
        if($history_type == 'purchaseOrder')
        {
            $resp = (new PurchaseOrder)->history();
        }
        if($history_type == 'invoice')
        {
            $resp = (new Invoice)->history();
        }
        if($history_type == 'distributor')
        {
            $resp = (new Distributor)->history();
        }
        if($history_type == 'users')
        {
            $resp = (new User)->history();
        }
        if($history_type == 'salesReturn')
        {
            $resp = (new SalesReturn)->history();
        }
        if($history_type == 'orderReturn')
        {
            $resp = (new OrderReturn)->history();
        }
        if($history_type == 'branch')
        {
            $resp = (new Branch)->history();
        }
        if($history_type == 'branchBankDetails')
        {
            $resp = (new BranchBankDetails)->history();
        }
        if($history_type == 'customer')
        {
            $resp = (new Customer)->history();
        }
        if($history_type == 'shop')
        {
            $resp = (new Shop)->history();
        }
        if($history_type == 'stock')
        {
            $resp = (new Stock)->history();
        }
        if($history_type == 'tax')
        {
            $resp = (new Tax)->history();
        }
            return $this->respond($resp);
    }
   
    //This function is use to show all user related branches.
    public function userRelatedBranches()
    {
        $id       = Auth::User()->id;
        $branchId = UserBranchDetails::where('user_id','=',$id)->lists('branch_id');
        $branch   = Branch::whereIn('id',UserBranchDetails::where('user_id','=',$id)->lists('branch_id'))->get();
        return $branch; 
    }
   
   //This function is use to show all branch names.
    public function getAllBranchName()
    {
        $branch   = Branch::select('id','branch_name')->get();
        return $branch; 
    }
   
    //This function is use for change user current password.
    public function changePassword()
    {
        $resp = array();
        $id = Auth::User()->id;
        $oldUserPassword = Auth::User()->password;
        $newUserInputPassword = Input::get('newUserInputPassword');
        $oldUserInputPassword = Input::get('oldUserInputPassword');
        if(Hash::check($oldUserInputPassword, $oldUserPassword))
        {
            $user = User::find($id);
            $user->password  = Hash::make($newUserInputPassword);
            $user->save();
            $resp['success'] = true;
            $resp['msg']     = 'User update his password Successfully';
        }
        else
        {
            $resp['success'] = false;
            $resp['msg']     = 'User password updated Failure';
        }
        return $resp;
    }
 
    //This function is use to show branch related user details. 
    public function getDetailsByBranchId($branchId)
    {
        $resp = array();
        try
        {
            $resp = (new User())->getByBranchId($branchId);
            if($resp['success'])
            {
                $resp['msg'] = "User Details Information";
                return $this->respond($resp);
            }
            else
            {
                 return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display User details');
        }
    }
   
    //This function is use for show list of tables in database.
    public function getTableNameList()
    {
        $tables = DB::select('SHOW TABLES');
        return $tables;
    }
   
    //This function is use to store branch name in session.
    public function getBranchNameInSession()
    {
        $id = Auth::User()->id;
        $branchId = UserBranchDetails::select('branch_id')->where('user_id','=',$id)->get();
        Session::put('branch_id', $branchId);
        if (Session::has('branch_id'))
        {
            $branch = Session::get('branch_id');
            return $branch;
        }
    }

    //This function is use to show branch name from session.
    public function getAllBranchNameInSession()
    {
        $branchId = Branch::select('id')->get();
        Session::put('branch_id', $branchId);
        if (Session::has('branch_id'))
        {
            $branch = Session::get('branch_id');
            return $branch;
        }
    }
   
    //This function is use for change user branch.
    public function changeUserBranch()
    {
        $resp          = array();
        $role          = Auth::User()->role;
        $userId        = Input::get('userId');
        $newUserBranch = Input::get('newUserBranch');
        $id            = UserBranchDetails::select('id')->where('user_id',$userId)->get();
        if($role == 'super_admin')
        {
            if($userId!='' && $newUserBranch!='')
            {
                $userBranchDetailsData = UserBranchDetails::where('user_id', $userId)->update(['branch_id' => $newUserBranch]);
                $resp['success'] = true;
                $resp['msg']     = 'User Branch Change Successfully';
            }
            else
            {
                $resp['success'] = false;
                $resp['msg']     = 'Please select your details properly';
            }
        }
        else
        {
            $resp['success'] = false;
            $resp['msg']     = 'You are not Authorised user to change user branch';
        }
        return $resp;
    }
   
    //This function is use to generate random password.
    function random_password( $length = 8 )
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }

    //This function is use for send password to user email.
    public function sendResetPasswordEmail()
    {
        $resp = array();
        $emailId = Input::get('email_id');
        $email = DB::table('users')->select('email_id')->where('email_id', '=', $emailId)->get();
        if($email)
        {
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
            $password = substr( str_shuffle( $chars ), 0, 8 );
   
            Mail::send('emails.test', ['password' => $password], function ($message) use ($emailId) {
                $message->to($emailId)->subject('Reset Your Password!');
            });
            $generatedPassword  = Hash::make($password);
            $updatePassword     = DB::table('users')->where('email_id', $emailId)->update(['password' => $generatedPassword]);
            $resp['success']    = true;
            $resp['msg']        = 'Your new password send to email address, Please check your email';
          }
        else
        {
            $resp['success'] = false;
            $resp['msg']     = 'Sorry, You are unauthorised user';
        }
        return $resp;
    }
    
    //This function is use for check user table is empty or not.
    public function emptyTable()
    {
        $resp = array();
        $userTable = User::get();
        $branchTable = Branch::get();
        $shopTable = Shop::get();
        
        if(count($userTable) || count($branchTable) || count($shopTable))
        {
            $resp['success'] = true;  
        }
        else
        {
            $resp['success'] = false;
        }
        return $resp;
    }
    
    //This function is use for add super admin details.
    public function addSuperAdmin()
    {
        $resp = array();
        $user   = (Input::get('user'));  
        $shop   = (Input::get('shop'));  
        $branch = (Input::get('branch'));  
        $user["password"] = "secret"; 
        
        $userData = (new UserTransformer)->reverseTransform($user,'save');
        $userId   = DB::table('users')->insertGetId($userData);
        
        $shop['superAdminId'] = $userId;
        $shopData = (new shopTransformer)->reverseTransform($shop);
        $shopId   = DB::table('shop')->insertGetId($shopData);
        
        $branch['shopId'] = $shopId;
        $branchData = (new branchTransformer)->reverseTransform($branch);
        $branchId   = DB::table('branch')->insertGetId($branchData);    
        
        (new UserBranchDetailsTransformer)->reverseTransform($branchId,(new UserBranchDetails),$userId);
        
        $resp['success'] = true;
        $resp['msg'] = "Super Admin Details Added Successfully";
        $resp['userDetails'] = $userData;
        $resp['shopDetails'] = $shopData;
        $resp['branchDetails'] = $branchData;
        return $resp;
    }
}
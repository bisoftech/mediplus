<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Shop;

class ShopController extends ApiController
{
    //Display a listing of the shop details.
    public function index()
    {
        $resp = array();
        try
        {
            $resp = (new Shop())->getAll();
            if($resp['success'])
            {
                $resp['msg'] = "Shop Details Information";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Shop details');
        }
    }

    //Store a newly created shop details in storage.
    public function store()
    {
        $resp = array();
        try
        {
            $resp = (new Shop())->add();
            if($resp['success'])
            {
                $resp['msg'] = 'Shop Details Added Successfully';
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Cannot submit Shop Details please try later');
        }
    }

    //Display the specified shop details, param int $id.
    public function show($id)
    {        
        $resp = array();
        try
        {
            $resp = (new Shop())->getById($id);
            if($resp['success'])
            {
                $resp['msg'] = 'Shop Detail Information';
                return $this->respond($resp); 
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Can not Display Shop details');
        }
    }

    //Show the form for updating the specified shop details, param int $id.
    public function update($id)
    {
        $resp = array();
        try
        {
            $resp = (new Shop())->updateById($id);
            if($resp['success'])
            {
                $resp['msg'] = 'Shop Details Updated Successfully';
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Shop details Updated Failure');
        }
    }

    //Remove the specified resource from storage, param int $id.
    public function destroy($id)
    {
        $resp = array();
        try
        {
            $resp = (new Shop())->deleteById($id);
            if($resp['success'])
            {
                $resp['msg'] = 'Shop Details Deleted Successfully';
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Shop details Deleted Failure');
        }
    }
    
    //Display the specified branch related shop details, param int $branchId.
    public function getDetailsByBranchId($branchId)
    {
        $resp = array();
        try
        {
            $resp = (new Shop())->getByBranchId($branchId);
            if($resp['success'])
            {
                $resp['msg'] = "Shop Details Information";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Shop details');
        }
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MediPlus\Transformers\TaxTransformer as TaxTransformer;
use App\MediPlus\Transformers\BranchTransformer as BranchTransformer;
use App\Tax;
use App\Branch;
use DB;
use Auth;
use Input;

class TaxController extends ApiController
{
    //Display a listing of the tax details.
    public function index()
    {
        $resp = array();
        try
        {
            $taxTransformer = new TaxTransformer;
            $resp = (new Tax())->getAll();
            if($resp['success'])
            {
                $resp['msg'] = "Branch Tax Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Branch Tax details');
        }
    }

    //Store a newly created tax details in storage.
    public function store()
    {
        $resp = array();
        try
        {
            $resp = (new Tax())->add();
            if($resp['success'])
            {
                $resp['msg'] = "Branch Tax Details Added Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Cannot submit Branch Tax Details please try later');
        }
    }

    //Display the specified tax details, param int $id.
    public function show($id)
    {
        $resp = array();
        try
        {
            $resp = (new Tax())->getById($id);
            if($resp['success'])
            {
                $resp['msg'] = "Branch Tax Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return  $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Branch Tax details');
        }
    }

    //Show the form for updating the specified tax details, param int $id.
    public function update($id)
    {
        $resp = array();
        try
        {
            $resp = (new Tax())->updateById($id);
            if($resp['success'])
            {
                $resp['msg'] = "Branch Tax Details Updated Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Branch Tax Details Updated Failure');
        }
    }

    //Remove the specified resource from storage, param int $id.
    public function destroy($id)
    {
        $resp = array();
        try
        {
            $resp = (new Tax())->deleteById($id);
            if($resp['success'])
            {
                $resp['msg'] = "Branch Tax Details Deleted Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Branch Tax Details Deleted Failure');
        }
    }
    
    //Display a listing of the branch related tax details, param int $branchId.
    public function getDetailsByBranchId($branchId)
    {
        $resp = array();
        try
        {
            $taxTransformer = new TaxTransformer;
            $resp = (new Tax())->getByBranchId($branchId);
            if($resp['success'])
            {
                $resp['msg'] = "Branch Tax Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Branch Tax details');
        }
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Distributor;
use App\MediPlus\Transformers\DistributorTransformer as DistributorTransformer;
use Auth;
use Input;
use DB;

class DistributorController extends ApiController
{    
    //Display a listing of the distributor details.
    public function index()
    {
        $resp = array();
        try
        {
            $resp = (new Distributor())->getAll(); //this function written in distributor model file.
            if($resp['success'])
            {
                $resp['msg'] = "Distributor Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Distributor details');
        }
    }
 
    //Store a newly created distributor details in storage.
    public function store()
    {
        $resp = array();
        try
        {
            $resp = (new Distributor())->add(); //this function written in distributor model file.
            if($resp['success'])
            {
                $resp['msg'] = "Distributor Details Added Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Cannot submit Distributor please try later');
        }
    }
    
    //Display the specified distributor details, param int $id.
    public function show($id)
    {
        $resp = array();
        try
        {
            $resp=(new Distributor())->getById($id); //this function written in distributor model file.
            if($resp['success'])
            {
                $resp['msg'] = "Distributor Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return  $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Distributor details');
        }
    }

    //Show the form for updating the specified distributor details, param int $id.
    public function update($id)
    {
        $resp = array();
        try
        {
            $resp = (new Distributor())->updateById($id); //this function written in distributor model file.
            if($resp['success'])
            {
                $resp['msg'] = "Distributor Updated Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Distributor Details Updated Failure');
        }
    }
    
    //Remove the specified resource from storage, param int $id.
    public function destroy($id)
    {   
        $resp = array();
        try
        {
            $resp = (new Distributor())->deleteById($id); //this function written in distributor model file.
            if($resp['success'])
            {
                $resp['msg'] = "Distributor Deleted Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Distributor Details Deleted Failure');
        }
    }

    //Display the distributor details for search any keyword.
    public function autocomplete($data)
    {
        $resp = array();
        try
        {
            $resp = (new Distributor())->autocomplete($data); //this function written in distributor model file.
            if($resp['success'])
            {
                $resp['msg'] = "Distributor Details Available";
                return $this->respond($resp);
            }
        }
        catch (\Exception $ex)
        {
            return $this->setStatusCode(500)->respondWithError('Distributor Details Not Available');
        }
    }
    
    //This function is use to get the distributor details in excel file.
    public function reporttoExcel()
    {
        $fromDate = Input::get('fromDate');
        $toDate   = Input::get('toDate');
        $from     = date("Y-m-d H:i:s", strtotime($fromDate));
        $to       = date("Y-m-d H:i:s", strtotime($toDate));
        $data = Input::all();
        $fieldName=json_decode($data['distributor']);
        array_push($fieldName,"id","created_at","updated_at");
        $table = Distributor::select($fieldName)->whereBetween('created_at',array($from, $to))->get();
        $filename = "distributor.csv";
        $handle   = fopen($filename, 'w+');
        $fields = array('Sr.No', 'Created Date', 'Updated Date', 'Agency Number', 'Owner Name', 'Authorised Distributor', 'Tin Number', 'Cst Number', 'Dl Number', 'Credit Period', 'Contact Number', 'Deleted Date');
        fputcsv($handle, $fields);

        foreach($table as $row) {
            fputcsv($handle, array($row['id'], $row['created_at'], $row['updated_at'], $row['agency_number'], $row['owner_name'], $row['authorised_distributor'], $row['tin_number'], $row['cst_number'], $row['dl_number'], $row['credit_period'], $row['contact_number'], $row['deleted_at']));
        }

        fclose($handle);

        $headers = array('Content-Type' => 'text/csv',);

        return Response::download($filename, 'distributor.csv', $headers);
   }
}
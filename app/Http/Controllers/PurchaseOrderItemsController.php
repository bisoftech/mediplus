<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PurchaseOrderItems;
use App\MediPlus\Transformers\PurchaseOrderItemsTransformer as PurchaseOrderItemsTransformer;
class PurchaseOrderItemsController extends ApiController
{
    public function index()
    {
        $data=new PurchaseOrderItemsTransformer;
        return response()->json($data->transformCollection(PurchaseOrderItems::all()));
    }
}

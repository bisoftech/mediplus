<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MediPlus\Transformers\OperationLogTransformer as OperationLogTransformer;
use App\OperationLog;
use DB;
use Auth;
use Input;

class OperationLogController extends ApiController
{
    //Display a listing of the operation log details.
    public function index()
    {
        $resp = array();
        try
        {
            $oerationLogTransformer = new OperationLogTransformer;
            $resp = (new OperationLog())->getAll();
            if($resp['success'])
            {
                $resp['msg'] = "Log Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Log details');
        }
    }
   
    //Store a newly created operation log details in storage.
    public function store()
    {
        $resp = array();
        try
        {
            $resp = (new OperationLog())->add();
            if($resp['success'])
            {
                $resp['msg'] = "Log Details Added Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Cannot submit Log Details please try later');
        }
    }

    //Display the specified operation log details, param int $id.
    public function show($id)
    {
        $resp = array();
        try
        {
            $resp = (new OperationLog())->getById($id);
            if($resp['success'])
            {
                $resp['msg'] = "Log Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return  $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Log details');
        }
    }

    //Show the form for updating the specified operation log details, param int $id.
    public function update($id)
    {
        $resp = array();
        try
        {
            $resp = (new OperationLog())->updateById($id);
            if($resp['success'])
            {
                $resp['msg'] = "Log Details Updated Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Log Details Updated Failure');
        }
    }

    //Remove the specified resource from storage, param int $id.
    public function destroy($id)
    {
        $resp = array();
        try
        {
            $resp = (new OperationLog())->deleteById($id);
            if($resp['success'])
            {
                $resp['msg'] = "Log Details Deleted Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Log Details Deleted Failure');
        }
    }
}
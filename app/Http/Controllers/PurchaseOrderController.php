<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MediPlus\Transformers\PurchaseOrderTransformer as PurchaseOrderTransformer;
use App\MediPlus\Transformers\PurchaseOrderItemsTransformer as PurchaseOrderItemsTransformer;
use App\PurchaseOrder;
use App\PurchaseOrderItems;
use App\Branch;
use App\Users;
use App\Distributor;
use DB;
use Auth;
use Input;
use Illuminate\Support\Facades\Response;

class PurchaseOrderController extends ApiController
{
    //Display a listing of the product purchase order details.
    public function index()
    {
        $resp = array();
        try
        {
            $purchaseOrderTransformer = new PurchaseOrderTransformer;
            $resp = (new PurchaseOrder())->getAll();
            if($resp['success'])
            {
                $resp['msg'] = "Purchase Order Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Purchase Order details');
        }
    }

    //Store a newly created product purchase order details in storage.
    public function store()
    {
            $purchaseOrder     = (Input::get('purchaseOrder'));
            foreach ($purchaseOrder as $key => $value) {
            if(empty($value))
            {
                return array("suuccess"=>false,"data"=>[],"msg"=>"record not sufficient");
            }
        }
        $resp = array();
        try
        {
            $resp = (new PurchaseOrder())->add();
            if($resp['success'])
            {
                $resp['msg'] = "Purchase Order Added Successfully";
                return $this->respond($resp); 
            }
            else
            {
                 return  $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not submit Purchase Order details');
        }
    }
    
    //Display the specified product purchase order details, param int $id.
    public function show($id)
    {
        $resp = array();
        try
        {
            $resp = (new PurchaseOrder())->getById($id);
            if($resp['success'])
            {
                $resp['msg'] = "Purchase Order Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return  $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Purchase Order details');
        }
    }
    
    //Display the specified product purchase order details for relevant branch, param int $id.
    public function getDetailsByBranchId($branchId)
    {
        $resp = array();
        try
        {
            $purchaseOrderTransformer = new PurchaseOrderTransformer;
            $resp = (new PurchaseOrder())->getByBranchId($branchId);
            if($resp['success'])
            {
                $resp['msg'] = "Purchase Order Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Purchase Order details');
        }
    }
    
    //This function is use to get the product purchase order details in excel file.
    public function reporttoExcel()
    {
        $fromDate  = Input::get('fromDate');
        $toDate    = Input::get('toDate');
        $from      = date("Y-m-d H:i:s", strtotime($fromDate));
        $to        = date("Y-m-d H:i:s", strtotime($toDate));
        $data      = Input::all();
        $tables=array_keys($data);
        $tableName = json_decode($data["purchase_order"],true);
        $fieldName = $tableName['field'];
        $conditionString="";
        $table = DB::table("purchase_order")->join('users', 'purchase_order.user_id', '=', 'users.id')
                          ->join('distributor', 'purchase_order.distributor_id', '=', 'distributor.id')
                          ->select($fieldName, 'users.user_name', 'distributor.owner_name')
                          ->orderBy('id', 'asc')
                          ->whereBetween('purchase_order.created_at',array($fromDate, $toDate))->get();
        
        $filename = "po.csv";
        $handle   = fopen($filename, 'w+');
       
        fputcsv($handle, $fieldName);

        foreach($tableResult as $row) 
        {
            $csvarr=[];
            foreach($fieldName as $name)
            {
                $csvarr[$name]=$row->$name;   
            }
            fputcsv($handle, $csvarr);
        }

        fclose($handle);

        $headers = array('Content-Type' => 'text/csv',);

        return Response::download($filename, 'product.csv', $headers);
    }
}
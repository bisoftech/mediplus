<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Stock;

class StockController extends ApiController
{
    //Display a listing of the stock details.
    public function index()
    {
        $resp = array();
        try
        {
            $resp = (new Stock())->getAll();
            if($resp['success'])
            {
                $resp['msg'] = "Stock Details Information";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Stock details');
        }
    }

    //Store a newly created stock details in storage.
    public function store()
    {
        $resp = array();
        try
        {
            $resp = (new Stock())->add();
            if($resp['success'])
            {
                $resp['msg'] = 'Stock Details Added Successfully';
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Cannot submit Stock Details please try later');
        }
    }

    //Display the specified stock details, param int $id.
    public function show($id)
    {
        $resp = array();
        try
        {
            $resp = (new Stock())->getById($id);
            if($resp['success'])
            {
                $resp['msg'] = 'Stock Detail Information';
                return $this->respond($resp); 
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Can not Display Stock details');
        }
    }

    //Show the form for updating the specified stock details, param int $id.
    public function update($id)
    {
        $resp = array();
        try
        {
            $resp = (new Stock())->updateById($id);
            if($resp['success'])
            {
                $resp['msg'] = 'Stock Details Updated Successfully';
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Stock details Updated Failure');
        }
    }

    //Remove the specified resource from storage, param int $id.
    public function destroy($id)
    {
        $resp = array();
        try
        {
            $resp = (new Stock())->deleteById($id);
            if($resp['success'])
            {
                $resp['msg'] = 'Stock Details Deleted Successfully';
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Stock details Deleted Failure');
        }
    }
}
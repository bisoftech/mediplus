<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MediPlus\Transformers\BillingTransformer as BillingTransformer;
use App\MediPlus\Transformers\BillingItemsTransformer as BillingItemsTransformer;
use App\Billing;
use App\BillingItems;
use Input;
use DB;
use Log;
use Session;
use App\UserBranchDetails;

class BillingController extends ApiController
{
    //Display a listing of the billing details.
    public function index()
    {
        $resp = array();
        try
        {
            $billingTransformer = new BillingTransformer;
            $resp = (new Billing())->getAll(); //this function written in billing model file.
            if($resp['success'])
            {
                $resp['msg'] = "Bill Details Information";
                return $this->respond($resp); 
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Bill details');
        }
    }

    //Display the specified billing details, param  int  $id.
    public function show($id)
    {
        $resp = array();
        try
        {
            $resp = (new Billing())->getById($id); //this function written in billing model file.
            if($resp['success'])
            {
                $resp['msg'] = "Bill Details Information";
                return $this->respond($resp); 
            }
            else
            {
                return  $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Bill details');
        }
    }
    
    //Store a newly created billing details in storage.
    public function store()
    {
        $resp = array();
        try
        {
            $resp = (new Billing())->add(); //this function written in billing model file.
            if($resp['success'])
            {
                $resp['msg'] = "Bill Details Added Successfully";
                return $this->respond($resp); 
            }
            else
            {
                return  $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Cannot submit Bill Details please try later');
        }
    }

    //Remove the specified resource from storage, param int $id.
    public function destroy($id)
    {
        $resp = array();
        try
        {
            $resp = (new Billing())->deleteById($id); //this function written in billing model file.
            if($resp['success'])
            {
                $resp['msg'] = "Billing Deleted Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Billing Details Deleted Failure');
        }
    }
    
    //Display the specified billing details, param  int  $branchId.
    public function getDetailsByBranchId($branchId)
    {
        $resp = array();
        try
        {
            $resp = (new Billing())->getByBranchId($branchId); //this function written in billing model file.
            if($resp['success'])
            {
                $resp['msg'] = "Billing Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Billing details');
        }
    }
    
    //Display the specified customer credit details, param  int  $id (Customer Id).
    public function getCustOnCreditDetails($id)
    {
        $resp = array();
        try
        {
            $resp = (new Billing())->getCustOnCreditDetailsById($id); //this function written in billing model file.
            if($resp['success'])
            {
                $resp['msg'] = "Bill Details Information";
                return $this->respond($resp); 
            }
            else
            {
                return  $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Bill details');
        }
    }
    
    //Display the specified customer unpaid amount details, param  int  $id (Customer Id).
    public function getCustUnpaidAmtDetails($id)
    {
        $resp = array();
        try
        {
            $resp = (new Billing())->getCustUnpaidAmtById($id); //this function written in billing model file.
            if($resp['success'])
            {
                $resp['msg'] = "Previous Remaining Balance Details Information";
                return $this->respond($resp); 
            }
            else
            {
                return  $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Previous Remaining Balance Details');
        }
    }

    //Display the specified customer all paid, partial paid and unpaid bill details, param  int  $id, $unpaid = false (Customer Id).
    public function showNextPrevious($id,$unpaid=false)
    {
        $resp = array();
        try
        {
            $resp = (new Billing())->nextAndPrevoius($id,$unpaid); //this function written in billing model file.
            if($resp['success'])
            {
                $resp['msg'] = "Bill Details Information";
                return $this->respond($resp); 
            }
            else
            {
                return  $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Bill details');
        }
    }
}
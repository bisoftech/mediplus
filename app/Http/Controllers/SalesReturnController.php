<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SalesReturn;
use Input;
use App\MediPlus\Transformers\SalesReturnTransformer as SalesReturnTransformer;
use App\Billing;
use App\BillingItems;
use DB;
use Auth;

class SalesReturnController extends ApiController
{
    //Display a listing of the sales return details.
    public function index()
    {
        $resp = array();
        try
        {
            $resp = (new SalesReturn())->getAll();
            if($resp['success'])
            {
                $resp['msg'] = "Sales Return Details Information";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Sales Return details');
        }
    }

    //Store a newly created sales return details in storage.
    public function store()
    {
        $resp = array();
        try
        {
            $resp = (new SalesReturn())->add();
            if($resp['success'])
            {
                $resp['msg'] = 'Sales Return Details Added Successfully';
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Cannot submit Sales Return Details please try later');
        }
    }

    //Display the specified sales return details, param int $id.
    public function show($id)
    {
        $resp = array();
        try
        {
            $resp = (new SalesReturn())->getById($id);
            if($resp['success'])
            {
                $resp['msg'] = 'Sales Return Detail Information';
                return $this->respond($resp); 
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Can not Display Sales Return details');
        }
    }

    public function edit($id)
    {
        
    }

    //Show the form for updating the specified sales return details, param int $id.
    public function update($id)
    {
        $resp = array();
        try
        {
            $resp = (new SalesReturn())->updateById($id);
            if($resp['success'])
            {
                $resp['msg'] = 'Sales Return Details Updated Successfully';
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Sales Return details Updated Failure');
        }
    }

    //Remove the specified resource from storage, param int $id.
    public function destroy($id)
    {
        $resp = array();
        try
        {
            $resp = (new SalesReturn())->deleteById($id);
            if($resp['success'])
            {
                $resp['msg'] = 'Sales Return Details Deleted Successfully';
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Sales Return details Deleted Failure');
        }
    }
}
<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Response;
use Session;

/**
 * Created by PhpStorm.
 * User: ss11469
 * Date: 7/3/2015
 * Time: 12:24 AM
 */


class ApiController extends  Controller {

    protected $statusCode=200;

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }


    public function respondNotFound($message='Not Found!'){
        return $this->setStatusCode(404)->respondWithError($message);

    }


    public function respond($data,$headers=[]){
                $headers = ['Access-Control-Allow-Origin'      => '*'];
        return Response::json($data,$this->getStatusCode(),$headers);
    }

    public function getToken(){
         $headers = ['Access-Control-Allow-Origin'      => '*'];
        return Response::json(Session::token(),$this->getStatusCode(),$headers);
    }


    public function respondWithError($message='Not Found!'){
        
        return $this->respond([
            'error'=>[
                'message'=>$message,
                'status_code'=>$this->getStatusCode()
            ]
        ]);

    }

}
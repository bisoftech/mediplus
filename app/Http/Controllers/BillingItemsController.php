<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MediPlus\Transformers\BillingItemsTransformer as BillingItemsTransformer;
use App\BillingItems;

class BillingItemsController extends Controller
{
	//Display a listing of the billing items details.
    public function index()
    {
        $billingItemsTransformer=new BillingItemsTransformer;
        
        return  response()->json($billingItemsTransformer->transformCollection(BillingItems::all()));
    }
}
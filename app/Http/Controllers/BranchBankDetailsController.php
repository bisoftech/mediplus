<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MediPlus\Transformers\BranchBankDetailsTransformer as BranchBankDetailsTransformer;
use App\MediPlus\Transformers\BranchTransformer as BranchTransformer;
use App\BranchBankDetails;
use App\Branch;
use DB;
use Auth;
use Input;
use App\UserBranchDetails;

class BranchBankDetailsController extends ApiController
{
    //Display a listing of the branch bank details.
    public function index()
    {
        $resp = array();
        try
        {
            $branchBankDetailsTransformer = new branchBankDetailsTransformer;
            $resp = (new BranchBankDetails())->getAll(); //this function written in branch bank model file.
            if($resp['success'])
            {
                $resp['msg'] = "Branch Bank Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Branch Bank details');
        } 
    }
    
    //Store a newly created branch bank details in storage.
    public function store()
    {
        $resp = array();
        try
        {
            $resp = (new BranchBankDetails())->add(); //this function written in branch bank model file.
            if($resp['success'])
            {
                $resp['msg'] = "Branch Bank Details Added Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Cannot submit Branch Bank Details please try later');
        }     
    }

    //Display the specified branch bank details, param int $id.
    public function show($id)
    {
        $resp = array();
        try
        {
            $resp = (new BranchBankDetails())->getById($id); //this function written in branch bank model file.
            if($resp['success'])
            {
                $resp['msg'] = "Branch Bank Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return  $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Branch Bank details');
        }
    }

    //Show the form for updating the specified branch bank details, param int $id.
    public function update($id)
    {
        $resp = array();
        try
        {
            $resp = (new BranchBankDetails())->updateById($id); //this function written in branch bank model file.
            if($resp['success'])
            {
                $resp['msg'] = "Branch Bank Details Updated Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Branch Bank Details Updated Failure');
        }
    }

    //Remove the specified resource from storage, param int $id.
    public function destroy($id)
    {
        $resp = array();
        try
        {
            $resp = (new BranchBankDetails())->deleteById($id); //this function written in branch bank model file.
            if($resp['success'])
            {
                $resp['msg'] = "Branch Bank Details Deleted Successfully";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Branch Bank Details Deleted Failure');
        }
    }
  
    //Display the specified branch bank details by Id, param  int  $branchId.
    public function getDetailsByBranchId($branchId)
    {
        $resp = array();
        try
        {
            $resp = (new BranchBankDetails())->getByBranchId($branchId); //this function written in branch bank model file.
            if($resp['success'])
            {
                $resp['msg'] = "Branch Bank Details Information";
                return $this->respond($resp); 
            }
            else
            {
                 return  $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch (Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Branch Bank details');
        }
    }
}
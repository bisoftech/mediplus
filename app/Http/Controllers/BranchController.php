<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Branch;

class BranchController extends ApiController 
{
    //Display a listing of the branch details.
    public function index()
    {
        $resp = array();
        try
        {
            $resp = (new Branch())->getAll(); //this function written in branch model file.
            if($resp['success'])
            {
                $resp['msg'] = "Branch Details Information";
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
            return $this->setStatusCode(401)->respondWithError('Can not Display Branch details');
        }
    }

    //Store a newly created branch details in storage.
    public function store()
    {
        $resp = array();
        try
        {
            $resp = (new Branch())->add(); //this function written in branch model file.
            if($resp['success'])
            {
                $resp['msg'] = 'Branch Details Added Successfully';
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Cannot submit Branch Details please try later');
        }    
    }

    //Display the specified branch details, param  int  $id.
    public function show($id)
    {
        $resp = array();
        try
        {
            $resp = (new Branch())->getById($id); //this function written in branch model file.
            if($resp['success'])
            {
                $resp['msg'] = 'Branch Detail Information';
                return $this->respond($resp); 
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Can not Display Branch details');
        }
    }
    
    //Show the form for updating the specified branch details, param int $id.
    public function update($id)
    {
        $resp = array();
        try
        {
            $resp = (new Branch())->updateById($id); //this function written in branch model file.
            if($resp['success'])
            {
                $resp['msg'] = 'Branch Details Updated Successfully';
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Branch details Updated Failure');
        }
    }

    //Remove the specified resource from storage, param int $id.
    public function destroy($id)
    {
        $resp = array();
        try
        {
            $resp = (new Branch())->deleteById($id); //this function written in branch model file.
            if($resp['success'])
            {
                $resp['msg'] = 'Branch Details Deleted Successfully';
                return $this->respond($resp);
            }
            else
            {
                return $this->setStatusCode(500)->respondWithError($resp['msg']);
            }
        }
        catch(Exception $ex)
        {
             return $this->setStatusCode(401)->respondWithError('Branch details Deleted Failure');
        }
    }
}
<?php

namespace App\Http\Controllers;

use App\PurchaseOrderReturn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MediPlus\Pagination\Paginations;
use App\Product;
use App\Distributor;
use App\Customer;
use App\BranchBankDetails;
use App\Tax;
use App\Shop;
use App\User;
use App\Branch;
use App\Billing;
use App\BillingItems;
use App\OrderReturn;
use App\PurchaseOrder;
use App\Stock;
use App\Invoice;
use Input;
use DB;
use Auth;
use App\MediPlus\Transformers\ProductTransformer as ProductTransformer;
use App\MediPlus\Transformers\DistributorTransformer as DistributorTransformer;
use App\MediPlus\Transformers\InvoiceTransformer;
use App\MediPlus\Transformers\InvoiceItemsTransformer;
use Session;
use App\InvoiceItems;

class ReportController extends Controller
{
    //This function is use to get the all model details in excel file.
    public function reporttoExcel()
    {
        $resp = array();
        $from = Input::get('fromDate');
        $to   = Input::get('toDate');
        $reportName = Input::get('reportName');
        $fromRDate     = date("l, d-m-Y", strtotime($from));
        $toRDate       = date("l, d-m-Y", strtotime($to));
        
        $fromDate     = date("Y-m-d H:i:s", strtotime($from));
        $toDate       = date("Y-m-d H:i:s", strtotime($to));
        if($reportName == 'product')
        {
            $reportName1 = new Product;
            $reportModelObject = new $reportName1;
        }
        if($reportName == 'distributor')
        {
            $reportName1 = new Distributor;
            $reportModelObject = new $reportName1;
        }
        if($reportName == 'PurchaseOrder')
        {
            $reportName1 = new PurchaseOrder;
            $reportModelObject = new $reportName1;
        }
        if($reportName == 'Invoice')
        {
            $reportName1 = new Invoice;
            $reportModelObject = new $reportName1;
        }
        if($reportName == 'BranchBankDetails')
        {
            $reportName1 = new BranchBankDetails;
            $reportModelObject = new $reportName1;
        }
        if($reportName == 'User')
        {
            $reportName1 = new User;
            $reportModelObject = new $reportName1;
        }
        if($reportName == 'OrderReturn')
        {
            $reportName1 = new PurchaseOrderReturn;
            $reportModelObject = new $reportName1;
        }
        if($reportName == 'Customer')
        {
            $reportName1 = new Customer;
            $reportModelObject = new $reportName1;
        }
        if($reportName == 'Billing')
        {
            $reportName1 = new Billing;
            $reportModelObject = new $reportName1;
        }
        if($reportName == 'Branch')
        {
            $reportName1 = new Branch;
            $reportModelObject = new $reportName1;
        }
        if($reportName == 'Shop')
        {
            $reportName1 = new Shop;
            $reportModelObject = new $reportName1;
        } 
        if($reportName == 'Stock')
        {
            $reportName1 = new Stock;
            $reportModelObject = new $reportName1;
        }
        if($reportName == 'Tax')
        {
            $reportName1 = new Tax;
            $reportModelObject = new $reportName1;
        } 
            $myRequestObject   = Input::all();
            $table             =$reportModelObject->getDataForReports($fromDate, $toDate, $myRequestObject);
        
            $branchId  = Session::get('branch_id');
            $branchAddress = Branch::select('address')->where('id', '=', $branchId)->get()[0]->address;
            $branchName = Branch::select('branch_name')->where('id', '=', $branchId)->get()[0]->branch_name;
            $shopId = Branch::select('shop_id')->where('id', '=', $branchId)->get()[0]->shop_id; 
            $shopName = Shop::select('brand_name')->where('id', '=', $shopId)->get()[0]->brand_name;
            
            $currentDate = date('d-M-Y'); 
            $filename = $reportName . '.csv';
            $handle   = fopen($filename, 'w+');
            fputcsv($handle,array());
            
            fputcsv($handle,array()); 
            fputcsv($handle, array($shopName ));
            fputcsv($handle, array($branchName ));
            fputcsv($handle, array($branchAddress )); 
            fputcsv($handle, array('Report :',$reportName));
            fputcsv($handle, array('Period : ',$fromRDate.' to '.$toRDate )); 
             
            fputcsv($handle,array());
            
            fputcsv($handle, $reportModelObject->reportColumnTitle);
     
            foreach($table as $row) 
            {
                fputcsv($handle, $row);
            }

            fclose($handle);

            $headers = array('Content-Type' => 'text/csv',);
            $nameOfFile = $reportName.'('.$currentDate.')';            
            return Response::download($filename, $nameOfFile.'.csv', $headers);
    }
 
    //This function is use to get the monthly total product quntity details.
    public function totalProductQuantityReport()
    {
        $resp = array(); 
        $months = array('January','February','March','April','May', 'June','July', 'August', 
                'September','October','November','December');
                  for($i=1;$i<=17;$i++)
                  {
                     $resp['data'][]=$this->productQty($i);
                     $resp['series'][]= $this->productName($i);
                  }
                  $resp['labels']=$months;
        return $resp;
    }

    //This function is use to get the monthy delivered product quantity details.
    function productQty($id) 
    {
        $months = array('January'=>'1','February'=>'2','March'=>'3','April'=>'4','May'=>'5', 'June'=>'6','July'=>'7', 'August'=>'8', 'September'=>'9','October'=>'10','November'=>'11','December'=>'12');
        foreach ($months as $key => $item) 
        {  
            $data  = BillingItems::where('product_id', '=', $id)->where( DB::raw('MONTH(created_at)'),'=',$item)->sum('quantity');
            $resp[] = $data;
        }    
        return $resp; 
    }

    //This function is use to get daily delivered product quantity details.
    public function totalProductDailyQuantityReport()
    {
         $resp = array();
        $fromDate   = Input::get('fromDate');
        $toDate     = Input::get('toDate');
        $months = array('January','February','March','April','May', 'June','July', 'August', 
                'September','October','November','December');
        $from       = date("Y-m-d H:i:s", strtotime($fromDate));
        $to         = date("Y-m-d H:i:s", strtotime($toDate));
        $productName = BillingItems::join('product', 'billing_items.product_id', '=', 'product.id')
                              ->select('product.name','billing_items.quantity')
                              ->whereBetween('billing_items.created_at',array($from, $to))
                              ->get();
        foreach($productName as $value)
        {
            $resp['series'][] = $value->name;
            $resp['value'][][] = $value->quantity;
        }
        $resp['labels']=$months;
        return $resp;
    }

    //This function is use to get all products name.
    function productName($id)
    {
        return Product::select('name')->where('id','=',$id)->withTrashed()->get()[0]->name;
    }

    //This function is use to get the monthly sales product quantity details.
    public function totalProductSalesReport()
    {
        $resp = array(); 
        $months = array('January'=>'1','February'=>'2','March'=>'3','April'=>'4','May'=>'5', 'June'=>'6','July'=>'7', 'August'=>'8', 'September'=>'9','October'=>'10','November'=>'11','December'=>'12');
        foreach ($months as $key => $item) 
        {  
            $data  = BillingItems::where( DB::raw('MONTH(created_at)'), '=',$item)->sum('quantity');
            $resp['label'][] = $key;
            $resp['value'][] = $data;
        }
        return $resp;
    }
    
    //This function is use to get the total bill details details.
    public function totalBillReport()
    {
        $resp = array();
        $fromDate   = Input::get('fromDate');
        $toDate     = Input::get('toDate');
        $from       = date("Y-m-d H:i:s", strtotime($fromDate));
        $to         = date("Y-m-d H:i:s", strtotime($toDate));   
        $billStatus = array('1'=>'Paid','2'=>'Unpaid','3'=>'Partial_Paid');
        foreach ($billStatus as $key => $item) 
        {  
            $data   = Billing::where('status','=',$item)->whereBetween('created_at',array($from, $to))->sum('total_amount');
            $count  = Billing::where('status','=',$item)->whereBetween('created_at',array($from, $to))->count('id');
            $resp['label'][] = $item;
            $resp['value'][] = $data;
            $resp['count'][] = $count;
        }
        return $resp;
    }

    //This function is use to get the monthly revenue details.
    public function totalRevenueReport()
    {
        $resp = array(); 
            
        $totalSaleAmount = DB::select("select sum(total_amount) as totalAmount, Month(created_at) as month from billing GROUP BY Month(created_at)");
       
        $totalPurchaseAmount = DB::select("select sum(total_amount) as totalAmount, Month(invoice.created_at) as month from invoice GROUP BY Month(created_at)");

        $totalProfitAmt = BillingItems::join('stock','billing_items.stock_id','=','stock.id')
                             ->join('invoice_items','stock.invoice_id','=','invoice_items.invoice_id')
                             ->select( DB::raw('sum((billing_items.perUnit*billing_items.quantity)) as bill_items_price, sum((invoice_items.mrp * billing_items.quantity)) as bill_item_mrp,  sum((billing_items.perUnit*billing_items.quantity)) - sum((invoice_items.mrp * billing_items.quantity)) as totalProfit , Month(billing_items.created_at) as month'))
                             ->whereRaw('billing_items.product_id = invoice_items.product_id')
                             ->groupBy( DB::raw('MONTH(created_at)'))
                             ->get();
       
        $graphSales=array(0,0,0,0,0,0,0,0,0,0,0,0);
        $graphPurchase=array(0,0,0,0,0,0,0,0,0,0,0,0);
        $graphProfit=array(0,0,0,0,0,0,0,0,0,0,0,0);
        
        foreach ($totalSaleAmount as $key => $value) {
            $graphSales[$value->month-1]=intval($value->totalAmount);
        }
        $totalSalesReport = array('name'=> 'Total Sales','data'=>$graphSales);

        foreach ($totalPurchaseAmount as $key => $value) {
            $graphPurchase[$value->month-1]=intval($value->totalAmount);
        }
        $totalPurchaseReport = array('name'=> 'Total Purchase Amount','data'=>$graphPurchase);

        foreach ($totalProfitAmt as $key => $value) {
            $graphProfit[$value->month-1]=intval($value->totalAmount);

        }
        $totalProfitReport = array('name'=> 'Total Profit Amount','data'=>$graphProfit);
  
        $revenueAmount = array($totalSalesReport,$totalPurchaseReport,$totalProfitReport);

        $months = array('January'=>'1','February'=>'2','March'=>'3','April'=>'4','May'=>'5', 'June'=>'6','July'=>'7', 'August'=>'8', 'September'=>'9','October'=>'10','November'=>'11','December'=>'12');    
        $resp['series'] = $revenueAmount;
        $resp['label'] = $months;
        return $resp;
    }
    
    //This function is use to get the tota product sale details.
    public function totalSalesReport()
    {
        $resp = array();
        $fromDate   = Input::get('fromDate');
        $toDate     = Input::get('toDate');
        $totalHeading = array('Total Sales','Total Quantity','Total Unpaid Amount','Distributor PayAmount');
        $from       = date("Y-m-d H:i:s", strtotime($fromDate));
        $to         = date("Y-m-d H:i:s", strtotime($toDate));
        $salesRupees    = Billing::whereBetween('created_at',array($from, $to))->sum('total_amount');
        $salesQuantity  = BillingItems::whereBetween('created_at',array($from, $to))->sum('quantity');
        $totalUnpaidAmount = Billing::where('status','=','unpaid')->whereBetween('created_at',array($from, $to))->sum('total_amount');
        $totalCustCount    = Billing::where('status','=','unpaid')->whereBetween('created_at',array($from, $to))->count('id');
        $totalDistPaidAmt  = Invoice::whereBetween('created_at',array($from, $to))->sum('amount_paid');
        $amount = array($salesRupees,$salesQuantity,$totalUnpaidAmount,$totalDistPaidAmt);
        $resp['lable'] = $totalHeading;
        $resp['value'] = $amount;
        return $resp;
    }

    //This function is use to get the total available product quantity details.
    public function totalProductDetailsReport()
    {
        $resp = array();
        $fromDate   = Input::get('fromDate');
        $toDate     = Input::get('toDate');
        $from       = date("Y-m-d H:i:s", strtotime($fromDate));
        $to         = date("Y-m-d H:i:s", strtotime($toDate));
        $productName = Stock::join('product', 'stock.product_id', '=', 'product.id')
                              ->select('product.name','stock.quantity')
                              ->distinct('product.name')
                              ->whereBetween('stock.created_at',array($from, $to))
                              ->get();
        foreach($productName as $value)
        {
            $resp['label'][] = $value->name;
            $resp['value'][] = $value->quantity;
        }
        return $resp;
    }

    //This function is use to get the total customer details.
    public function totalCustomerDetailsReport()
    {
        $resp = array();
        $fromDate   = Input::get('fromDate');
        $toDate     = Input::get('toDate');
        $from       = date("Y-m-d H:i:s", strtotime($fromDate));
        $to         = date("Y-m-d H:i:s", strtotime($toDate));
        $custStatus = array('1'=>'Hospital','2'=>'Individual');
        foreach($custStatus as $key => $item) 
        {  
            $count  = Customer::where('customer_type','=',$item)->whereBetween('created_at',array($from, $to))->count('id');
            $resp['label'][] = $item;
            $resp['value'][] = $count;
        }
        return $resp;
    }

    //This function is use to get the expired product details.
    public function expired()
    {
        $id=Auth::user()->id;
        $branchId=Session::get('branch_id');
        $fromDate   = Input::get('fromDate');
        $toDate     = Input::get('toDate');
        $from       = date("Y-m-d H:i:s", strtotime($fromDate));
        $to         = date("Y-m-d H:i:s", strtotime($toDate));
        $data = InvoiceItems::join('invoice','invoice_items.invoice_id','=','invoice.id')
                             ->join('user_branch_details', 'invoice.user_id' , '=', 'user_branch_details.user_id')
                             ->where('user_branch_details.branch_id','=',$branchId)
                             ->where('invoice_items.expiry_date', '>',$from)
                             ->where('invoice_items.expiry_date', '<',$to)
                             ->orderBy('invoice_items.expiry_date','desc')
                             ->orderBy('invoice_items.expiry_date','desc')->get();
        $dataAll=array();
        foreach ($data as $value) {
            $dataAll[]=(new InvoiceItemsTransformer)->transformDB($value);
        }
        return $dataAll;
       
        $filename = 'expired.csv';
        $handle   = fopen($filename, 'w+');

        $branchId  = Session::get('branch_id');
        $branchAddress = Branch::select('address')->where('id', '=', $branchId)->get()[0]->address;
        $shopId = Branch::select('shop_id')->where('id', '=', $branchId)->get()[0]->shop_id;
        $shopName = Shop::select('brand_name')->where('id', '=', $shopId)->get()[0]->brand_name;

        fputcsv($handle, array($shopName )); 
        fputcsv($handle, array($branchAddress )); 
        fputcsv($handle, array('Period : ',$from.' to '.$to )); 

        $head=array('productName','productManufacturer','productFormula','productDosage','invoiceExpiryDate','invoiceQuantity','invoiceMfgDate','invoicePurchasePrice','invoiceSellingPrice','invoiceParcelNumbe','invoiceInvoiceDate','invoiceTotalAmount','invoiceAmountPaid','invoicePaymentType','invoiceId','invoiceCreatedAt','invoicePurchaseOrderOrderDate','invoicePurchaseDistributor');
        fputcsv($handle,$head);
        foreach ($dataAll as  $value) {
            $data=array();
                $data['productName']=$value['product']['name'];
                $data['productManufacturer']=$value['product']['manufacturer'];
                $data['productFormula']=$value['product']['formula'];
                $data['productDosage']=$value['product']['dosage'];
                $data['invoiceExpiryDate']=$value['expiryDate'];
                $data['invoiceQuantity']=$value['quantity'];
                $data['invoiceMfgDate']=$value['mfgDate'];
                $data['invoicePurchasePrice']=$value['purchasePrice'];
                $data['invoiceSellingPrice']=$value['sellingPrice'];
                $data['invoiceParcelNumbe']=$value['invoice']['parcelNumber'];
                $data['invoiceInvoiceDate']=$value['invoice']['invoiceDate'];
                $data['invoiceTotalAmount']=$value['invoice']['totalAmount'];
                $data['invoiceAmountPaid']=$value['invoice']['amountPaid'];
                $data['invoicePaymentType']=$value['invoice']['paymentType'];
                $data['invoiceId']=$value['invoice']['id'];
                $data['invoiceCreatedAt']=$value['invoice']['createdAt'];
                $data['invoicePurchaseOrderOrderDate']=$value['invoice']['purchaseOrder']['orderDate'];
                $data['invoicePurchaseDistributor']=$value['invoice']['purchaseOrder']['distributor']['ownerName'];
                fputcsv($handle,$data);
        }
        fclose($handle);
        $headers = array('Content-Type' => 'text/csv',);        
        return Response::download($filename, $filename, $headers);
    }
}
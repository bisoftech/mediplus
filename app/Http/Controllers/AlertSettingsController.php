<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\UserBranchDetails;
use App\AlertSettingsModel;
use Log;
use DB;
use Auth;
use App\MediPlus\Transformers\AlertSettingsTransformer;
use Session;

class AlertSettingsController extends ApiController
{

    //Show the form for updating the specified resource.
    public function updateAlertSettings()
    {
    	try
    	{
    	    $myRequestObject = Input::all();     
            $user_id=Auth::user()->id;
            $branch_id=intval(Session::get('branch_id'));
            $myRequestObject["userId"]= $user_id;
            $myRequestObject["branchId"]= $branch_id;
                
            $data=(new AlertSettingsTransformer)->transform($myRequestObject);       
            
            DB::table('alert_settings')->where('user_id', '=', $user_id)
                ->where('branch_id', '=', $branch_id)->update($data);
            
    	}
    	catch (Exception $ex)
    	{
    	    return $this->setStatusCode(401)->respondWithError('Cannot update Alert Settings. Please try again later.');
    	}
    }

    //Display a listing of the alert settings.
    public function getAlerts()
    {
    	try
    	{
    	    $myAlertSettingsObject = new AlertSettingsModel;
    	    $alerts = $myAlertSettingsObject->getAlerts();
    	    if ($alerts['success'])
    	    {
    		return $alerts;
    	    }
    	    else
    	    {
    		return $this->setStatusCode(500)->respondWithError('Cannot Fetch alerts now. Please try again later.');
    	    }
    	}
    	catch(Exception $ex)
    	{
    	    return $this->setStatusCode(401)->respondWithError('Cannot retrieve Alerts. Please try again later.');
    	}
    }

    //Display a listing of the alert settings.
    public function index()
    {
        return AlertSettingsModel::all();
    }

    //Store a newly created alert details in storage.
    public function store()
    {
        $data=Input::all();
        $data1=(new AlertSettingsTransformer)->transform($data);
        AlertSettingsModel::insert($data1);
    }

    //Display the specified alert details, param  int  $id.
    public function show($id)
    {         
        $data1=(new AlertSettingsTransformer)->reverseTransform(AlertSettingsModel::find($id));
        return $data1;    
    }
    
    //Display alert details.
    public function alertDetails()
    {
        $user_id=Auth::user()->id;
        $branch_id=intval(Session::get('branch_id'));        
        $id = DB::table('alert_settings')->select('id')->where('user_id','=',$user_id)->where('branch_id','=',$branch_id)->get()[0]->id;
        if($id!='')
        {
            $data1=(new AlertSettingsTransformer)->reverseTransform(AlertSettingsModel::find($id));
        }
        return $data1;
    }
    
    //Show the form for updating the specified resource, @param  int  $id
    public function update($id)
    {
        $data=Input::all();
        $branchId  = Session::get('branch_id');
        $data1=(new AlertSettingsTransformer)->transform($data);
        DB::table('alert_settings')->where('id','=',$id)->update($data1);   
    }
}

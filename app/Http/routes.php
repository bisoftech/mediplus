<?php

Route::get('/', function () {
    return view('index');
});

Route::post('/login','UserController@loginPost');

Route::get('/isLogin','UserController@isLogin');

Route::get('/auth/login','UserController@login');

Route::group(['prefix'=>'api/v1','middleware' =>'auth'],function()
{

    Route::resource('users','UserController');
    
    Route::resource('distributor','DistributorController');
    
    Route::get('distributor/autocomplete/{data}','DistributorController@autocomplete');    
    
    Route::get('users/history/{history_type}','UserController@history');
    
    Route::resource('product','ProductController');
    
    Route::get('product/autocomplete/{data}','ProductController@autocomplete');    
    
    Route::resource('purchaseOrder','PurchaseOrderController');       
    
    Route::resource('purchaseOrderItems','PurchaseOrderItemsController');   
    
    Route::resource('invoice','InvoiceController'); 
    
    Route::resource('customer','CustomerController'); 
    
    Route::resource('billing','BillingController'); 
  
    Route::resource('billingItems','BillingItemsController');
    
    Route::resource('shop','ShopController');
    
    Route::resource('stock','StockController');
 
    Route::resource('branchBankDetails', 'BranchBankDetailsController'); 
    
    Route::resource('tax', 'TaxController'); 
    
    Route::resource('branch', 'BranchController'); 
    
    Route::resource('salesReturn', 'SalesReturnController'); 
    
    Route::resource('orderReturn', 'OrderReturnController');

    Route::post('updateReturnItem/{id}','OrderReturnController@updateReturnItem');
    
    Route::resource('operationLog', 'OperationLogController');
    
    Route::resource('alert', 'AlertSettingsController');
    
    Route::get('alertDetails', 'AlertSettingsController@alertDetails');
    
    Route::get('userRelatedBranches', 'UserController@userRelatedBranches');
    
    Route::get('getBranchBankDetailsById/{branchId}', 'BranchBankDetailsController@getDetailsByBranchId');
    
    Route::get('getBranchTaxDetailsById/{branchId}', 'TaxController@getDetailsByBranchId');
    
    Route::get('getBranchPODetailsById/{branchId}', 'PurchaseOrderController@getDetailsByBranchId');
    
    Route::get('getBranchUserDetailsById/{branchId}', 'UserController@getDetailsByBranchId');
    
    Route::get('getBranchInvoiceDetailsById/{branchId}', 'InvoiceController@getDetailsByBranchId');
    
    Route::get('getBranchBillingDetailsById/{branchId}', 'BillingController@getDetailsByBranchId');
    
    Route::get('getBranchShopDetailsById/{branchId}', 'ShopController@getDetailsByBranchId');

    Route::get('getBranchNameInSession', 'UserController@getBranchNameInSession');

    Route::get('setBranchInSession/{branch_id}', 'UserController@setBranchNameInSession');
    
    Route::post('updateAlertSettings', 'AlertSettingsController@updateAlertSettings');
    
    Route::get('getCustOnCreditDetails/{id}','billingController@getCustOnCreditDetails');
     
    Route::get('getCustUnpaidAmtDetails/{id}','billingController@getCustUnpaidAmtDetails');

    Route::get('showNextPrevious/{id}/{unpaid?}','billingController@showNextPrevious');
    
    Route::get('getInvoiceDetailWithStockUpdated/{id}','OrderReturnController@getInvoiceDetailWithStockUpdated');

});

Route::resource('mediplusRegistration','MediplusRegistrationController'); 

Route::post('addSuperAdmin/', 'UserController@addSuperAdmin');

Route::post('users/changePassword/','UserController@changePassword' );

Route::post('users/changeUserBranch/','UserController@changeUserBranch' );

Route::get('users/getTableNameList/','UserController@getTableNameList' );

Route::get('search/{search_type}/{data}','UserController@search');

Route::get('getAllBranchName', 'UserController@getAllBranchName');

Route::get('getAllBranchNameInSession', 'UserController@getAllBranchNameInSession');

Route::get('api/v1/updateAlertSettings','AlertSettingsController@updateAlertSettings');

Route::get('api/v1/getAlerts','AlertSettingsController@getAlerts');

Route::get('autocomplete/{search_type}/{id}','UserController@autocomplete');

Route::get('history/{history_type}','UserController@history');

Route::post('product/productReporttoExcel','ProductController@reporttoExcel');

Route::get('distributor/reporttoExcel','DistributorController@reporttoExcel');

Route::get('purchaseOrder/reporttoExcel','PurchaseOrderController@reporttoExcel');

Route::get('product/reporttoExcel','ReportController@reporttoExcel'); 

Route::get('expired','ReportController@expired'); 

Route::get('registration/isValid','MediplusRegistrationController@isValid'); 

Route::get('registration/onlineRegistration','MediplusRegistrationController@onlineRegistration'); //db

Route::post('totalProductDailyQuantityReport/','ReportController@totalProductDailyQuantityReport');

Route::get('totalProductQuantityReport/','ReportController@totalProductQuantityReport');

Route::get('totalProductSalesReport/','ReportController@totalProductSalesReport');

Route::post('totalBillReport/','ReportController@totalBillReport');

Route::post('totalSalesReport/','ReportController@totalSalesReport');

Route::get('totalRevenueReport/','ReportController@totalRevenueReport'); 

Route::post('totalProductDetailsReport/','ReportController@totalProductDetailsReport');

Route::post('totalCustomerDetailsReport/','ReportController@totalCustomerDetailsReport');

Route::get('invoice/reporttoExcel/','InvoiceController@reporttoExcel');

Route::get('product/productPrice/{id}','InvoiceController@productPrice');

Route::get('customer/getCustomerBillDetails/{id}','customerController@getCustomerBillDetails');

Route::get('sendResetPasswordEmail/','userController@sendResetPasswordEmail');

Route::get('user/emptyTable/','UserController@emptyTable');

Route::post('generateRegistrationKey','MediplusRegistrationController@generateRegistrationKey');

Route::get('generateKeys','MediplusRegistrationController@generateKeys');

Route::post('completeRegistration','MediplusRegistrationController@completeRegistration');

Route::get('/token','ApiController@getToken'); 
// fake route
Route::get('checkPeriodExpired','MediplusRegistrationController@checkPeriodExpired'); 

Route::get('/logout',function(){
Auth::logout();   
});

Route::resource('WebEvents','WebEventsController');

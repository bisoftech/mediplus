<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;
class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('auth/login');
            }
        }
            if(Auth::user()->role=='super_admin')
            {
                return $next($request);    
                
                
            }    
            if(Auth::user()->role=='admin')
            {
              if(str_contains($request->path(),'api/v1/setBranchInSession') && $request->method()=='GET')
                  return $next($request);
                if(str_contains($request->path(),'api/v1/branch') && $request->method()=='GET')
                  return $next($request);
                if(str_contains($request->path(),'api/v1/shop') && $request->method()=='GET')
                  return $next($request);
                if(str_contains($request->path(),'api/v1/billing'))
                    return $next($request); 
                if(str_contains($request->path(),'api/v1/customer'))
                    return $next($request);    
                if(str_contains($request->path(),'api/v1/product'))
                    return $next($request);
                if(str_contains($request->path(),'api/v1/invoice'))
                    return $next($request);
                if(str_contains($request->path(),'api/v1/purchaseOrder'))
                    return $next($request);
                if(str_contains($request->path(),'api/v1/purchaseOrder'))
                    return $next($request);
                if(str_contains($request->path(),'api/v1/distributor'))
                    return $next($request);
                
                else
                return "you are not authorized to view this page";
            }
            if(Auth::user()->role=='employee')
            {
              if(str_contains($request->path(),'api/v1/setBranchInSession') && $request->method()=='GET')
                  return $next($request);
                if(str_contains($request->path(),'api/v1/billing'))
                    return $next($request);          
                if(str_contains($request->path(),'api/v1/customer'))
                    return $next($request);    
                if(str_contains($request->path(),'api/v1/product'))
                    return $next($request);
                if(str_contains($request->path(),'api/v1/invoice')  && $request->method()=='GET')
                    return $next($request);
                if(str_contains($request->path(),'api/v1/purchaseOrder')  && $request->method()=='GET')
                    return $next($request);
                else 
                return "you are not authorized to view this page";
            }
             if(Auth::user()->role=='pharmacist')
            {
                if(str_contains($request->path(),'api/v1/billing') && $request->method()=='GET')
                    return $next($request);          
                else 
                return "you are not authorized to view this page";
            }
        return $next($request);
    }
}

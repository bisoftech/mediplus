<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Illuminate\Support\Facades\Response;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];


    public function handle($request, \Closure $next)
    {
        if ($this->isReading($request)
            || $this->tokensMatch($request))
        {
            return $this->addCookieToResponse($request, $next($request));
        }

        if(!$this->tokensMatch($request))
            return Response::json(
                [
                    'error'=>[
                        'message'=>'Invalid Token',
                        'status_code'=>498
                    ]
                ],498);

        throw new \TokenMismatchException;
    }

}

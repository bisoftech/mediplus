<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        /*
        if(Auth::user()->role=="employee")
            if($request->path()=="api/v1/invoice")
                return "NOT ALLOW";               
            */
        return array($request->path(),$request->method());
        return $next($request);
    }
}
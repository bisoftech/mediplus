<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\MediPlus\Transformers\OperationLogTransformer as OperationLogTransformer;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\MediPlus\Pagination\Paginations;
use Input;
use DB;
use Auth;
use App\Operation;

class OperationLog extends Model
{
    protected $table = 'operation_log';
    use SoftDeletes;
    
    //This function is use for show all details.
    public function getAll()
    {
        $resp = array();
        try 
        {
            $limit   = Input::get('limit')?:10;
            $results = $this::paginate($limit);
            $operationLogTransformer     = new OperationLogTransformer;
            $operationLog                = $operationLogTransformer->transformCollection($results);
            $resp['success']    = true;
            $resp['data']       = $operationLog;
            $resp['pagination'] = (new Paginations)->results($results);    
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = "Log Details Not Available";
        }
        return $resp;
    }
    
    //This function is use for show details by Id.
    public function getById($id)
    {    
       $resp = array();
       try
       {
            $operationLog                = OperationLog::findOrFail($id);
            $operationLogTransformer     = new OperationLogTransformer;
            $resp['success']    = true;
            $resp['data']       = $operationLogTransformer->transform($operationLog);
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = "Log Detail Not Available";
       }
       return $resp;
    }
    
    //This function is use for add details in table.
    public function add($data)
    {
       $resp = array();
       try
       {
            $arr             = (new OperationLogTransformer)->reverseTransform($data);
            $id              = OperationLog::insertGetId($arr); 
            $resp['success'] = true;            
            $resp['data']    = (new OperationLogTransformer)->transform(OperationLog::find($id));
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = "Log Details Added Failure";
       }
       return $resp;
    }
    
    //This function is use for update details by Id.
    public function updateById($id)
    {
       $resp = array();
       try
       {
            $arr             = (new OperationLogTransformer)->reverseTransform(Input::all());
            OperationLog::where('id',$id)->update($arr);
            OperationLog::findOrFail($id)->save();
            $resp['success'] = true;
            $resp['data']    = (new OperationLogTransformer)->transform(OperationLog::findOrFail($id));
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = "Log Details Not Available";
       }
       return $resp;
    }
    
    //This function is use for delete details by Id.
    public function deleteById($id)
    {
       $resp = array();
       try
       {
            $operationLog = OperationLog::findOrFail($id);
            $operationLog->delete();
            $operationLogTransformer  = new OperationLogTransformer;
            $resp['success'] = true;
            $resp['data']    = $operationLogTransformer->transform($operationLog);
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = "Log Details Not Available";  
       }
       return $resp;
    }
}
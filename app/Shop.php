<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MediPlus\Transformers\ShopTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\MediPlus\Pagination\Paginations;
use Input;
use Auth;
use App\OperationLog;
use App\User;
use App\UserBranchDetails;
use Session;

class Shop extends Model
{
    protected $table = 'shop';
    use SoftDeletes;
    
    //This function is use to assign heading for standard reports.
    public $reportColumnTitle = array('Sr.No', 'Brand Name', 'User Name');
    
    //This function is use for display shop name by selected shop Id.
    public function getShopName($shopId)
    {
        $shop=Shop::find($shopId);
        
        if($shop)
            return $shop->brand_name;
        else 
            return 'Shop Deleted';   
    }
    
    //This function is use for show all shop details.
    public function getAll()
    {
        $resp = array();
        try
        {
            $limit    = Input::get('limit')?:10;
            $branchId = Session::get('branch_id'); 
            $results  = Shop::whereIn('super_admin_id',UserBranchDetails::where('branch_id','=',$branchId)
                                                                          ->lists('user_id'))->paginate($limit);
            $resp['success']     = true;
            $resp['data']        = (new ShopTransformer)->transformCollection($results);    
            $resp['pagination']  = (new Paginations)->results($results);
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Shop Details Not Available";
            $resp['ex']      = $ex->getMessage();
        }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all shop details from table shop successfully');
            (new OperationLog)->add($operationLog);
        return $resp;    
    }
    
    //This function is use for show shop details by selected shop Id.
    public function getById($id)
    {
        $resp = array();
        try
        {
            $resp['success'] = true;
            $branchId = Session::get('branch_id'); 
            $resp['data']    = (new ShopTransformer)->transform(Shop::whereIn('super_admin_id', UserBranchDetails::where('branch_id','=',$branchId)->lists('user_id'))->findOrFail($id)); 
            
            $operationLog=array("userId"           => Auth::User()->id,
                                 "operation"        => 'View',
                                 "referenceTableId" => $id,
                                 "operationStatus"  => 'success',
                                 "customMessage"    => 'user id '.Auth::User()->id.' view shop details in table shop with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Shop Details Not Available";
            $resp['ex']      = $ex->getMessage();
            $operationLog=array("userId"           => Auth::User()->id,
                                 "operation"        => 'View',
                                 "referenceTableId" => $id,
                                 "operationStatus"  => 'fail',
                                 "customMessage"    => 'user id '.Auth::User()->id.' view shop details in table shop with id '.$id.' failure');
            (new OperationLog)->add($operationLog);
        }
        return $resp;
    }
    
    //This function is use to add shop details in shop table.
    public function add()
    {
        $resp = array();
        try
        {
            $resp['success'] = true;
            $shop            = (new ShopTransformer)->reverseTransform(Input::all());
            $shopId          = Shop::insertGetId($shop);
            $resp['data']    = (new ShopTransformer)->transform(Shop::find($shopId));
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => $shopId,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table shop with id '.$shopId.' successfully');
            (new OperationLog)->add($operationLog);
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Shop Details Added Failure";
            $resp['ex']      = $ex->getMessage();
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => 0,
                                  "operationStatus"  => 'fail',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table shop failure');
            (new OperationLog)->add($operationLog);
        }
        return $resp;
    }
    
    //This function is use to update shop details for selected shop Id.
    public function updateById($id)
    {
        $resp = array();
        try
        {
            $arr             = (new ShopTransformer)->reverseTransform(Input::all());
            Shop::where('id',$id)->update($arr);
            Shop::findOrFail($id)->save();
            $resp['success'] = true;
            $resp['data']    = (new ShopTransformer)->transform(Shop::findOrFail($id));
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Shop Details Not Available";
            $resp['ex']      = $ex->getMessage();
        }
        return $resp;
    }
    
    //This function is use to delete shop details for selected shop Id.
    public function deleteById($id)
    {
        $resp = array();
        try
        {
            $shop = Shop::findOrFail($id);
            $shop->delete();
            $limit   = Input::get('limit')?:10;
            $results = $shop::paginate($limit);
            $shopTransformer     = new ShopTransformer;
            $resp['success']     = true;
            $resp['data']        = $shopTransformer->transform($shop);
            $resp['pagination']  = (new Paginations)->results($results);      
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Shop Details Not Available";
            $resp['ex']      = $ex->getMessage();
        }
        return $resp;   
    }
    
    //This function is use for show shop details for selected branch Id.
    public function getByBranchId($branchId)
    {
        $resp = array();
        try
        {
            $limit   = Input::get('limit')?:10;
            $results = Shop::whereIn('super_admin_id',UserBranchDetails::where('branch_id','=',$branchId)->lists('user_id'))->paginate($limit);
            $resp['success']     = true;
            $resp['data']        = (new ShopTransformer)->transformCollection($results);    
            $resp['pagination']  = (new Paginations)->results($results);
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Shop Details Not Available";
            $resp['ex']      = $ex->getMessage();
        }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all shop details from table shop successfully');
            (new OperationLog)->add($operationLog);
        return $resp;    
    }
    
    //This function is use to display shop details for search any keyword.
    public function autocomplete($data)
    {
       $resp = array();
       try
       {
            $limit           = Input::get('limit')?:10;
            $resp['success'] = true;
            $dataShop        = Shop::where('brand_name', 'like', '%'.$data.'%')->paginate($limit);
            $resp['data']    = (new ShopTransformer)->transformCollection($dataShop);
            $resp['pagination'] = (new Paginations)->results($dataShop);
            $resp['msg']        = "Shop Details Information";     
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Shop Details Not Available";
            $resp['ex']      = $ex->getMessage();
       }
       return $resp;
    }
    
    //This function is use to get all shop details in-beetween of two dates.
    public function history()
    {
        $resp        = array();
        $limit       = Input::get('limit')?:10;
        $fromDate    = Input::get('fromDate');
        $toDate      = Input::get('toDate');
        $from        = date("Y-m-d H:i:s", strtotime($fromDate));
        $to          = date("Y-m-d H:i:s", strtotime($toDate)); 
        $shop        = Shop::whereBetween('created_at',array($from, $to))->paginate($limit);
        if($fromDate!='' && $toDate!='')
        {
            $shopTransformer        = new ShopTransformer;
            $resp['success']        = "true";
            $resp['msg']            = "Shop Details Available From search date";
            $resp['data']           = $shopTransformer->transformCollection($shop); 
            $resp['pagination']     = (new Paginations)->results($shop);
        }
        else
        {
            $resp['success'] = "false";
            $resp['msg']     = "Please Select date";
        }
        return $resp;
    }
    
    //This function is use to get all shop details.
    //This function is use for standard reports.
    public function getDataForReports($fromDate, $toDate, $myRequestObject)
    {
        $table = Shop::join('users', 'shop.super_admin_id', '=', 'users.id')
                       ->select('shop.*', 'users.user_name')
                       ->whereBetween('shop.created_at',array($fromDate, $toDate))->get();
        $reportName  = new Shop;
        $shopData = array();
        foreach($table as $row) 
        {            
            array_push($shopData, array($row['id'], $row['brand_name'], $row['user_name']));
        }
        return $shopData;
    }
}
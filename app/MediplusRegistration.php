<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MediPlus\Transformers\MediplusRegistrationTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\MediPlus\Pagination\Paginations;
use Input;
use DB;
use Auth;
use Crypt;
use DateTime;
use DateInterval;
use Log;
class MediplusRegistration extends Model
{
    protected $table = 'mediplus_registration';
    
    public function add($data)
    {
        $resp = array();
        $firstEntry = DB::table('mediplus_registration')->count();
        $firstEntry = 0;
        try
        {
            if($firstEntry == 0)
            {
                //$mediplusRegistrationData = Input::all();
                //$mediplusRegistrationData['registrationKey'] = Crypt::encrypt('registrationKey');
                //$mediplusRegistrationData = Input::get(Crypt::encrypt('extendOn'));
                //$data = (new MediplusRegistrationTransformer)->reverseTransform($mediplusRegistrationData);
                $mediplusRegistrationId = MediplusRegistration::insertGetId($data);
                $resp['success']  = true;
                //$resp['data']     = (new MediplusRegistrationTransformer)->transform(MediplusRegistration::find($mediplusRegistrationId));
            }
            else
            {
                $resp['success'] = false;
                $resp['msg']     = 'Sorry,You have already fill entry in table';
            }
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Mediplus Registration Details Added Failure";
            $resp['ex']      = $e->getMessage();
        }
        return $resp;
    }
    
    public function updateById($id)
    {
        $resp = array();
        try
        {
            $mediplusRegistrationData = Input::all();
            $extendOn = Input::get('extendOn');
            $extendValue = DB::table('mediplus_registration')->select('extend_on')->get()[0]->extend_on;
            //$extendValue = DB::table('mediplus_registration')->select('extend_on')->get;

            /*if((sizeof($extendValue) > 0))
            {
                $decryptedExtendOn = Crypt::decrypt($extendValue[0]->extend_on);
            } */
            $decryptedExtendOn = Crypt::decrypt($extendValue);
            
            
            if($extendOn == $decryptedExtendOn)
            {
                $data = (new MediplusRegistrationTransformer)->reverseTransform($mediplusRegistrationData);
                MediplusRegistration::where('id',$id)->update($data);
                $resp['success'] = true;
                $resp['data']    = (new MediplusRegistrationTransformer)->transform(MediplusRegistration::findOrFail($id));
            }
            else
            {
                $resp['success'] = false;
                $resp['msg']     = 'Sorry, Extend On value does not match with excisting value';
            }           
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Mediplus Registration Details Not Available";
            $resp['ex']      = $e->getMessage();
        }
        return $resp;
    }

    public function generateMacAddress()
    {
	$res = array();
	try
	{
	    ob_start();
	    system('ipconfig /all');
	    $mycom=ob_get_contents();
	    ob_clean();
	    $findme = "Physical";
	    $pmac = strpos($mycom, $findme);
	    $mac=substr($mycom,($pmac+36),17);
	    $tdate = date("Y-m-d");
	    $res['key1'] = $mac;
	    $res['key2'] = $tdate;
	    $res['success'] = True;
	}
	catch(Exception $ex)
	{
	    $res['success'] = False;
	}
	return $res;
    }

    public function verifyLoginDate()
    {
	//$last_date = LoginTrackTable();
	//$key_record = LoginTrackTable();
	//$tdate = date("Y-m-d");
	//$decryptedExtendOn = Crypt::decrypt($extendValue);
    }

    public function generateRegistrationKey($myRequestObject)
    {
    $res = array();
    try
    {
        $mac = $myRequestObject['key1'];
        $tdate = $myRequestObject['key2'];
        $res['key1'] = Crypt::encrypt($mac);
        $res['key2'] = Crypt::encrypt($tdate);
        $res['success'] = True;
        //save rest of data
    }
    catch(Exception $ex)
    {
        $res['success'] = False;
        Log::info('test');
    }
    return $res;
    }

    public function completeRegistration($myRequestObject)
    {
        $key1 = $myRequestObject['key1'];
        $key2 = $myRequestObject['key2'];
        $data = array();
        $data['registration_key'] = $key2;
        $data['extend_on'] = $key1;
        $resp = $this->add($data);
        if($resp['success']==true)
        {
            $loginResp = $this->loginAdd();
        }
        return $resp;
    }
    public function checkPeriodExpired()
    {
           //$mediplusRegistrationData = DB::table('mediplus_registration')->all()->get();
           $mediplusRegistrationData = DB::table('mediplus_registration')
                            ->orderBy('id', 'desc')->first();

                             $data=Crypt::decrypt($mediplusRegistrationData->registration_key);
                             //   return json_encode($data);
                             //return json_encode($data);
                            //$dateTimeObject = new DateTime($data);
                                $date = date_create_from_format('Y-m-j', $data);


                            $end_date = $date->add(new DateInterval('P365D'));
                            
                            
                    $loginTrackTableData = DB::table('login_track_table')->orderBy('id', 'desc')->first();
                    $loginTrackTableLastData=Crypt::decrypt($loginTrackTableData->last);
                    $loginTrackTableLastDataDate = date_create_from_format('Y-m-j',$loginTrackTableLastData);
                    //$loginTrackTable_date = $loginTrackTableLastDataDate->add(new DateInterval('P365D'));
                            
                            if($end_date > $loginTrackTableLastDataDate)
                            {
                                    return true;
                                    //return json_encode('true');
                            }
                            else
                            {
                                //$loginTrackTableData = DB::table('login_track_table')->orderBy('id', 'desc')->first();
                                    
                                    Log::info($loginTrackTableData->id);
                                    DB::table('login_track_table')->where('id', '=', $loginTrackTableData->id)->delete();
                                    return false;
                                //return json_encode('false');

                            }
           return json_encode(array($end_date,$loginTrackTableLastDataDate));
    }

    public function loginAdd()
    {
        $resp = array();
        $currentDate = date("Y-m-d");
        $cdat = Crypt::encrypt($currentDate);
        //$cdat = $currentDate;
        $resp['success'] = DB::table('login_track_table')->insert(['last' => $cdat]);
        return $resp;
    }
}

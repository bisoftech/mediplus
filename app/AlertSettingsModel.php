<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MediPlus\Transformers\AlertSettingsTransformer;
use Log;
use App\Billing;
use DateTime;
use DateInterval;
use App\Customer;
use App\User;
use App\UserBranchDetails;
use Auth;
use Session;
use DB;

class AlertSettingsModel extends Model
{
    protected $table = 'alert_settings';

    public $fillable = array('bill_unpaid'=>'alert_billing_payment_status_unpaid','bill_partial_paid'=>'alert_billing_payment_status_partial_paid','branch_registration_validity'=>'alert_branch_registration_validity','branch_shop_act_validity'=>'alert_branch_shop_act_validity','customer_credit_limit_exceed'=>'alert_customer_credit_limit_exceed','customer_birthday'=>'alert_customer_birthday','distributor_credit_period_exceed'=>'alert_distributor_credit_period_exceed','invoice_unpaid'=>'alert_invoice_payment_status_unpaid','invoice_partial_paid'=>'alert_invoice_payment_status_partial_paid','order_return'=>'alert_order_return', 'sales_return' => 'alert_sales_return','stock_product_expires'=>'alert_stock_product_expires','stock_product_out_of_stock'=>'alert_stock_product_out_of_stock','stock_product_low_quantity'=>'alert_stock_product_low_quantity','user_birthday'=>'alert_user_birthday','bill_unpaid_period'=>'alert_billing_payment_status_unpaid_period_days','bill_partial_paid_period'=>'alert_billing_payment_status_partial_paid_period_days','branch_registration_validity_period'=>'alert_branch_registration_validity_period_days','branch_shop_act_validity_period'=>'alert_branch_shop_act_validity_period_days','customer_credit_limit_exceed_period'=>'alert_customer_credit_limit_exceed_period_days','customer_birthday_period'=>'alert_customer_birthday_period_days','distributor_credit_period_exceed_period'=>'alert_distributor_credit_period_exceed_period_days','invoice_unpaid_period'=>'alert_invoice_status_unpaid_period_days','invoice_partial_paid_period'=>'alert_invoice_status_partial_paid_period_days','order_return_period'=>'alert_order_return_period_days','sales_return_period'=>'alert_sales_return_period_days','stock_product_expires_period'=>'alert_stock_product_expires_period_days','stock_product_out_of_stock_period'=>'alert_stock_product_out_of_stock_period_days','stock_product_low_quantity_period'=>'alert_stock_product_low_quantity_period_days','user_birthday_period'=>'alert_user_birthday_period_days');
    
    //This function is use to update alert details.
    public function updateSettings($myRequestObject)
    {
	$resp = array();
	try
	{
	    $myAlertSettingsTransformObject = new AlertSettingsTransformer;
	    $myAlertSettingsObject = new AlertSettingsModel;
	    $myupdateddata = $myAlertSettingsTransformObject->transform($myRequestObject);
	    while(list($key,$value) = each($this->fillable))
	    {
		if(isset($myupdateddata[$value]))
		{
		    $myAlertSettingsObject->$value = $myupdateddata[$value];
		}
	    }
	    $id = Auth::User()->id;
	    $branchId  = UserBranchDetails::select('branch_id')->where('user_id','=',$id)->get();
	    $branch_id = $branchId[0]->branch_id;
	    $myAlertSettingsObject->user_id = $id;
	    $myAlertSettingsObject->branch_id = $branch_id;
	    $myAlertSettingsObject->save();
	    $resp['success'] = true;
	}
	catch(Exception $ex)
	{
	    $resp['success'] = false;
	}
	
    }

    //This function is use to show all alert details.
    public function getAlerts()
    {

 
    $resp = array();
    try
    {
        $alerts = array();
        $id = Auth::User()->id;
        $branch_id=Session::get('branch_id');
        $applicableAlerts = $this->getApplicableAlerts($id, $branch_id);
        
        while(list($key, $value)=each($applicableAlerts))
        {
        $func_name = $key."_get_alerts";
        $alerts[$key] = $this->$func_name($id, $branch_id, $value);
        }
        $resp['success'] = True;
        $resp['alerts'] = $alerts;
    }
    catch(Exception $ex)
    {
        $resp['dev_msg'] = $ex->getMessage();
        $resp['success'] = False;
    }
    return $resp;
    }

    //This function is use to show alert details for selected user Id and branch Id. 
    public function getApplicableAlerts($user_id, $branch_id)
    {
    $applicableAlerts = array();

    $alertSetting = AlertSettingsModel::where('user_id',$user_id)->where('branch_id',$branch_id)->get();

    while(list($key,$value)=each($this->fillable))
    {
       
        if(sizeof($alertSetting) && ($alertSetting[0]->$value == 1 or $alertSetting[0]->value == true))
        {
        $fieldName = $value."_period_days";
        $applicableAlerts[$key] = $alertSetting[0]->$fieldName;
        }
    }
    return $applicableAlerts;
    }

    //This function is use to get unpaid bill details for selected user Id, branch Id and no of days.
    public function bill_unpaid_get_alerts($user_id, $branch_id, $period)
    {
    $alerts = array();
    $current_date = date('Y-m-d');   
    $dateTimeObject = new DateTime($current_date);
    $end_date = $dateTimeObject->sub(new DateInterval('P'. $period .'D'));
    $start_date = $end_date->format('Y-m-d');
    $end_date = $current_date;
    $branchUsers = UserBranchDetails::where('branch_id',$branch_id)->lists('user_id');
    foreach($branchUsers as $user)
    {
        $myBillings = Billing::where('user_id',$user)->where('status','unpaid')->whereBetween('created_at',array($start_date, $end_date))->get();
        foreach($myBillings as $billing)
        {
        array_push($alerts,$billing->id);
        }
    }
    $data=array();
    foreach ($alerts as $key => $value) {
        
        array_push($data,$this->bill_unpaid_get_alerts_details($value));
    }
    return($data);
    }

    //This function is use to get unpaid bill details for selected billing Id.
    function bill_unpaid_get_alerts_details($id)
    {
    return DB::table('billing')
            ->join('customer', 'billing.customer_id', '=', 'customer.id')
            ->join('billing_items', 'billing.id', '=', 'billing_items.billing_id')
            ->where('billing.id', '=',$id)
            ->select('billing.id','billing.created_at','customer.first_name','customer.middle_name','customer.last_name',DB::raw('sum(billing_items.quantity * billing_items.perUnit) AS totalBillUnpaidAmt'))
            ->get()[0];
    }

    //This function is use to get partial paid bill details for selected user Id, branch Id and no of days.
    public function bill_partial_paid_get_alerts($user_id, $branch_id, $period)
    {
    $alerts = array();
    $current_date = date('Y-m-d');   
    $dateTimeObject = new DateTime($current_date);
    $end_date = $dateTimeObject->sub(new DateInterval('P'. $period .'D'));
    $start_date = $end_date->format('Y-m-d');
    $end_date = $current_date;
    $branchUsers = UserBranchDetails::where('branch_id',$branch_id)->lists('user_id');
    foreach($branchUsers as $user)
    {
        $myBillings = Billing::where('user_id',$user)->where('status','partial_paid')->whereBetween('created_at',array($start_date, $end_date))->get();
        foreach($myBillings as $billing)
        {
        array_push($alerts,$billing->id);
        }
  
    }
    $data=array();
          foreach ($alerts as $key => $value) 
            {
                $data[]=$this->bill_unpaid_get_alerts_details($value);
            }
    return($data);
    }

    //This function is use to get branch registration validity details for selected user Id, branch Id and no of days.
    public function branch_registration_validity_get_alerts($user_id, $branch_id, $period)
    {
    $alerts = array();
    $current_date = date('Y-m-d');   
    $dateTimeObject = new DateTime($current_date);
    $end_date = $dateTimeObject->add(new DateInterval('P'. $period .'D'));
    $start_date = $current_date;
    $end_date = $end_date->format('Y-m-d');
    $userBranches = UserBranchDetails::where('user_id',$user_id)->lists('branch_id');
    foreach($userBranches as $branch)
    {
        $branch = Branch::where('id',$branch)->whereBetween('registration_validity',array($start_date, $end_date))->get();
        if(count($branch)>0)
        array_push($alerts,$branch[0]->id);
    }
        $data=array(1,2,3);
        $data1=array();
          foreach ($data as $key => $value) 
            {
                $data1[]=$this->branch_registration_validity_get_alerts_details($value);
            }

    return($data1);
    }

    //This function is use to get branch registration validity details for selected branch Id.
    function branch_registration_validity_get_alerts_details($id)
    {
    $data=DB::table('branch')->where('id','=',$id)->select('id','branch_name','registration_number','registration_validity','shop_act_number','shop_act_validity','vat_number','cst_number','lbt_number','pan_card','district','city','pin_code','contact_number')->get();
    if($data)
        return $data[0];

    }

    //This function is use to get shop validity details for selected user Id, branch Id and no of days.
    public function branch_shop_act_validity_get_alerts($user_id, $branch_id, $period)
    {
    $alerts = array();
    $current_date = date('Y-m-d');   
    $dateTimeObject = new DateTime($current_date);
    $end_date = $dateTimeObject->add(new DateInterval('P'. $period .'D'));
    $start_date = $current_date;
    $end_date = $end_date->format('Y-m-d');
    $userBranches = UserBranchDetails::where('user_id',$user_id)->lists('branch_id');
    foreach($userBranches as $branch)
    {
        $branch = Branch::where('id',$branch)->whereBetween('shop_act_validity',array($start_date, $end_date))->get();
        if(count($branch)>0)
        array_push($alerts,$branch[0]->id);
    }
            $data=array(1,2,3);
        $data1=array();
          foreach ($data as $key => $value) 
            {
                $data1[]=$this->branch_registration_validity_get_alerts_details($value);
            }

    return($data1);
    }

    //This function is use to get customer credit limit exceed details for selected user Id, branch Id and no of days.
    public function customer_credit_limit_exceed_get_alerts($user_id, $branch_id, $period)
    {
    $alerts = array();
    $current_date = date('Y-m-d');   
    $dateTimeObject = new DateTime($current_date);
    $end_date = $dateTimeObject->sub(new DateInterval('P'. $period .'D'));
    $start_date = $end_date->format('Y-m-d');
    $end_date = $current_date;
    $customers = Billing::where('user_id', $user_id)->whereBetween('created_at',array($start_date, $end_date))->where('status','partial_paid')->orWhere('status','unpaid')->lists('customer_id','created_at');

    $myCustomerObject = new Customer;
    foreach($customers as $date=>$id)
    {
        
    }
    return($alerts);
    }
    
    function customer_credit_limit_exceed_get_alerts_details($id)
    {
    
    }

    public function customer_birthday_get_alerts($user_id, $branch_id)
    {
    }

    //This function is use to get distributor credit period exceed details for selected user Id, branch Id and no of days.
    public function distributor_credit_period_exceed_get_alerts($user_id, $branch_id, $period)
    {
        $alerts = array();
        $current_date = date('Y-m-d');   
        $dateTimeObject = new DateTime($current_date);
        $end_date = $dateTimeObject->sub(new DateInterval('P'. $period .'D'));
        $start_date = $end_date->format('Y-m-d');
        $end_date = $current_date;
        $distributor = Distributor::where('credit_period','<',2)->get();
        foreach($distributor as $db)
        {
            array_push($alerts,$db->id);
        }
        $data=array(1,2,3);
        $data1=array();
          foreach ($data as $key => $value) 
            {
                $data1[]=$this->distributor_credit_period_exceed_get_alerts_details($value);
            }

    return($data1);
    }
    
    //This function is use to get distributor credit period exceed details for selected distributor Id.
    function distributor_credit_period_exceed_get_alerts_details($id)
    {
    $data=DB::table('distributor')->where('id','=',$id)->select('id','agency_number','agency_name','owner_name','authorised_distributor','credit_period','contact_number')->get();
    if($data)
        return $data[0];
    }

    //This function is use to get invoice amount unpaid details for selected user Id, branch Id and no of days.
    public function invoice_unpaid_get_alerts($user_id, $branch_id, $period)
    {
        $alerts = array();
        $current_date = date('Y-m-d');   
        $dateTimeObject = new DateTime($current_date);
        $end_date = $dateTimeObject->sub(new DateInterval('P'. $period .'D'));
        $start_date = $end_date->format('Y-m-d');
        $end_date = $current_date;
        $branchUsers = UserBranchDetails::where('branch_id',$branch_id)->lists('user_id');
        foreach($branchUsers as $user)
        {
            $myinvoice = Invoice::where('user_id',$user)->where('amount_paid','=',0)->whereBetween('created_at',array($start_date, $end_date))->get();
            foreach($myinvoice as $invoice)
            {
            array_push($alerts,$invoice->id);
            }
        }
        $data=array(1,2,3);
        $data1=array();
          foreach ($data as $key => $value) 
            {
                $data1[]=$this->invoice_partial_paid_get_alerts_details($value);
            }

    return($data1);
    }

    //This function is use to get invoice amount partially paid details for selected user Id, branch Id and no of days.
    public function invoice_partial_paid_get_alerts($user_id, $branch_id, $period)
    {
        $alerts = array();
        $current_date = date('Y-m-d');   
        $dateTimeObject = new DateTime($current_date);
        $end_date = $dateTimeObject->sub(new DateInterval('P'. $period .'D'));
        $start_date = $end_date->format('Y-m-d');
        $end_date = $current_date;
        $branchUsers = UserBranchDetails::where('branch_id',$branch_id)->lists('user_id');

        foreach ($branchUsers as $key => $value) 
        {
            $myinvoice = Invoice::where('user_id',$value)
                ->where('total_amount','>','amount_paid')
                ->whereBetween('created_at',array($start_date, $end_date))
                ->lists('id');
        }
        return $myinvoice;
            $data=array();
          foreach ($myinvoice as $key => $value) 
            {
                $data[]=$this->invoice_partial_paid_get_alerts_details($value);
            }

    return($data);
    }

    //This function is use to get invoice amount partially paid details for selected invoice Id.
    function invoice_partial_paid_get_alerts_details($id)
    {
           return DB::table('invoice')
            ->join('invoice_items', 'invoice.id', '=', 'invoice_items.invoice_id')
            ->where('invoice.id', '=', $id)
            ->select('invoice.id','invoice.created_at','invoice.invoice_date','invoice.payment_type',DB::raw('sum(invoice_items.mrp) AS totalMrp'))
            ->get()[0];
            if($data)
                return $data[0];
    }

    public function order_return_get_alerts($user_id, $branch_id)
    {
    }

    public function sales_return_get_alerts($user_id, $branch_id)
    {
    }

    //This function is use to get available product expires details for selected user Id, branch Id and no of days.
    public function stock_product_expires_get_alerts($user_id, $branch_id, $period)
    {
        $alerts = array();
        $current_date = date('Y-m-d');   
        $dateTimeObject = new DateTime($current_date);
        $end_date = $dateTimeObject->sub(new DateInterval('P'. $period .'D'));
        $start_date = $end_date->format('Y-m-d');
        $end_date = $current_date;
        $stockDetails = Stock::select('product_id')->where('branch_id',$branch_id)->distinct('product_id')->lists('product_id');
        foreach($stockDetails as $stock)
        {
            $productExpire = InvoiceItems::select('product_id')->where('product_id',$stock)->whereBetween('expiry_date',array($start_date, $end_date))->distinct('product_id')->get();
            foreach($productExpire as $productExpireId)
            {
              array_push($alerts,$productExpireId->product_id);
            }
        }
        $data=array();
        foreach ($alerts as $key => $value) 
        {
            array_push($data,$this->stock_product_expires_get_alerts_details($value));
        }
        return($data);
    }

    //This function is use to get available product expires details for selected product Id.
    function stock_product_expires_get_alerts_details($alerts)
    {
           return DB::table('stock')
            ->join('product', 'stock.product_id', '=', 'product.id')
            ->join('invoice_items', 'stock.product_id', '=', 'invoice_items.product_id')
            ->where('stock.product_id', '=', $alerts)
            ->distinct('invoice_items.product_id')
            ->select('product.id','product.created_at', 'product.name', 'product.manufacturer','product.dosage','product.low_quantity','invoice_items.mfg_date', 'invoice_items.expiry_date', 'stock.quantity')
            ->get()[0];
            if($data)
                return $data[0];
    }

    //This function is use to get product stock details when product quantity is not available in stock for selected user Id, branch Id and no of days.
    public function stock_product_out_of_stock_get_alerts($user_id, $branch_id, $period)
    {
        $alerts = array();
        $current_date = date('Y-m-d');   
        $dateTimeObject = new DateTime($current_date);
        $end_date = $dateTimeObject->add(new DateInterval('P'. $period .'D'));
        $start_date = $current_date;
        $end_date = $end_date->format('Y-m-d');
        $stockDetails = Stock::select('product_id')->where('quantity','<=',0)->where('branch_id',$branch_id)->distinct('product_id')->get();
        foreach($stockDetails as $sd)
        {
            array_push($alerts,$sd->product_id);
        }
        $data=array();
        foreach ($alerts as $key => $value) 
        {
            array_push($data,$this->stock_product_out_of_stock_get_alerts_details($value));
        }
        return($data);
    }

    //This function is use to get product stock details when product quantity is not available in stock for selected product Id.
    function stock_product_out_of_stock_get_alerts_details($alerts)
    {
           return DB::table('stock')
            ->join('product', 'stock.product_id', '=', 'product.id')
            ->where('stock.product_id', '=', $alerts)
            ->select('product.id','product.created_at','product.name','product.manufacturer','product.dosage','product.low_quantity','stock.quantity')
            ->get()[0];
            if($data)
                return $data[0];
    }

    //This function is use to get product stock details when product quantity is too low for selected user Id, branch Id and no of days.
    public function stock_product_low_quantity_get_alerts($user_id, $branch_id, $period)
    {
        $alerts = array();
        $current_date = date('Y-m-d');   
        $dateTimeObject = new DateTime($current_date);
        $end_date = $dateTimeObject->add(new DateInterval('P'. $period .'D'));
        $start_date = $current_date;
        $end_date = $end_date->format('Y-m-d');
        $lowQuantity = Product::lists('low_quantity');
        $stockDetails = Stock::select('product_id')->where('quantity', '<', $lowQuantity)->distinct('product_id')->where('branch_id',$branch_id)->get();
        foreach($stockDetails as $sd)
        {
            array_push($alerts, $sd->product_id);
        }
        $data=array();
        foreach ($alerts as $key => $value) 
        {
            array_push($data,$this->stock_product_low_quantity_get_alerts_details($value));
        }
        return($data);
    }

    //This function is use to get product stock details when product quantity is too low for selected product Id. 
    function stock_product_low_quantity_get_alerts_details($id)
    {
           return DB::table('stock')
            ->join('product', 'stock.product_id', '=', 'product.id')
            ->where('stock.product_id', '=', $id)
            ->distinct('product_id')
            ->select('product.id','product.created_at','product.name','product.manufacturer','product.dosage','product.low_quantity','stock.quantity')
            ->get()[0];
            if($data)
                return $data[0];
    }

    public function user_birthday_get_alerts($user_id, $branch_id)
    {
    }
}
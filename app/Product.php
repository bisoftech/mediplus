<?php
namespace App;

use Illuminate\Contracts\Support\JsonableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\MediPlus\Transformers\ProductTransformer as ProductTransformer;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\MediPlus\Pagination\Paginations;
use Input;
use DB;
use Auth;
use App\OperationLog;
use App\User;
use App\Stock;
use App\PurchaseOrderItems;

class Product extends Model
{
    use SoftDeletes;
    protected $table = 'product';      
   
    public function PurchaseOrderItems()
    {
        return $this->hasMany('App\PurchaseOrderItems');
    }
   
    public $reportColumnTitle = array('Sr.No', 'Name', 'Manufacturer', 'Formula', 'Dosage');
   
    //This function is use to assign heading for standard reports.
    public function reportheading(){
       
        return array('Sr.No', 'Created Date', 'Updated Date', 'Name', 'Manufacturer', 'Formula', 'Dosage', 'Deleted Date');
    }
    
    //This function is use to assign heading for standard reports.  
    public function add()
    {      
       $resp = array();
       try
       {
            $resp['success'] = true;
            $product         = (new ProductTransformer)->reverseTransform(Input::all());
            $productId       = Product::insertGetId($product);
            $resp['data']    = (new ProductTransformer)->transform(Product::find($productId));
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => $productId,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table product with id '.$productId.' successfully');
        (new OperationLog)->add($operationLog);
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Product Details Added Failure";
            $resp['ex']      = $e->getMessage();
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => 0,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table product failure');
        (new OperationLog)->add($operationLog);
       }
        return $resp;
    }
  
    //This function is use to get all product details.
    public function getAll()
    {
       $resp = array();
       try
       {
            $limit   = Input::get('limit')?:10;
            $results = Product::orderBy('id', 'DESC')->paginate($limit);
            $productTransformer  = new ProductTransformer;
            $resp['success']     = true;
            $resp['data']        =  $productTransformer->transformCollection($results,false);
            $resp['pagination']  = (new Paginations)->results($results);          
       }
       catch(\Exception $e)
       {   
            $resp['success']    = false;
            $resp['msg']        = $e->getMessage().' '."Product Details Not Available";
            $resp['ex']         = $e->getMessage();
       }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all product details from table product successfully');
            (new OperationLog)->add($operationLog);
            
       return $resp;
    }

    //This function is use to get product details by Id.
    public function getById($id)
    {
       $resp = array();
       try
       {
            $product            = product::findOrFail($id);
            $productTransformer = new ProductTransformer;
            $resp['success']    = true;
            $resp['data']       = $productTransformer->transform($product);
            $operationLog = array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'success',
                                "customMessage"    => 'user id '.Auth::User()->id.' view product details in table product with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
       }
       catch(\Exception $e)
       {
            $resp['success']    = false;
            $resp['msg']        = $e->getMessage().' '."Product Detail Not Available";
            $resp['ex']         = $e->getMessage();
            $operationLog=array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'fail',
                                "customMessage"    => 'user id '.Auth::User()->id.' view product details in table product with id '.$id.' failure');
            (new OperationLog)->add($operationLog);
       }
           
       return $resp;
    }

    //This function is use for update product details by Id.
    public function updateById($id)
    {
       $resp = array();
       try
       {
            $product         = (new ProductTransformer)->reverseTransform(Input::all());
            Product::where('id',$id)->update($product);
            Product::findOrFail($id)->save();
            $resp['success'] = true;
            $resp['data']    = (new ProductTransformer)->transform(Product::findOrFail($id));
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Product Details Not Available";
            $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }

    //Funtion is use for delete the records.
    public function deleteById($id)
    {
       $resp = array();
       try
       {
            $product = product::findOrFail($id);
            $product->delete();
            $limit   = Input::get('limit')?:10;
            $results = $product::paginate($limit);
            $productTransformer = new ProductTransformer;
            $resp['success']    = true;
            $resp['data']       = $productTransformer->transform($product);
            $resp['pagination'] = (new Paginations)->results($results);
       }
       catch(\Exception $e)
       {
            $resp['success']    = false;
            $resp['msg']        = $e->getMessage().' '."Product Details Not Available";
            $resp['ex']         = $e->getMessage();
       }
       return $resp;
    }
   
   //Funtion is use to get the product name.
    public function getProductName($id)
    {
       $product = Product::find($id);
       
       if($product)
            return $product->name;
       else
            return 'Product Deleted';
    }
   
   //FUntion is use for auto select by product name.manufacturer,formula
    public function autocomplete($data)
    {
       $resp = array();
       try
       {
            $limit           = Input::get('limit')?:10;
            $resp['success'] = true;
            $dataProduct     = Product::where('name', 'like', '%'.$data.'%')
                                        ->orWhere('manufacturer', 'like', '%'.$data.'%')
                                        ->orWhere('formula', 'like', '%'.$data.'%')
                                        ->paginate($limit);  
            $resp['data']       = (new ProductTransformer)->transformCollection($dataProduct);
            $resp['pagination'] = (new Paginations)->results($dataProduct);
            $resp['msg']        = "Product Details Information";         
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Product Details Not Available";
            $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }
   
   //FUntion is use for auto select product name.
    public function autocompleteByName($data)
    {
       $resp = array();
       try
       {
            $limit       = Input::get('limit')?:10;
            $dataProduct = Product::where('name', 'like', '%'.$data.'%')->get();
            $arr=array();
            foreach($dataProduct as $value)
            {
                $dataProduct = InvoiceItems::where('product_id', '=', $value->id)->distinct()->lists('mrp');
                $arr[]=$value;
            }
            $resp['success'] = true; 
            $resp['data']    = (new ProductTransformer)->transformCollectionPlain($arr);
            $resp['msg']     = "Product Details Information";
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Product Details Not Available";
            $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }
   
   //function is use to get the mrp for product.
    public function prising($id)
    {
        $data =array();
        $stockData = Stock::select('id','invoice_id','quantity')->where('product_id','=',$id)->where('quantity', '>', 0)->get();

        foreach($stockData as $key)
        {
          $mrp = InvoiceItems::where('invoice_items.invoice_id', '=', $key['invoice_id'])
                               ->where('invoice_items.product_id', '=', $id)->distinct()->lists('mrp');
          
          if((sizeof($mrp) != 0))
          {
            $key['mrp'] = $mrp[0];
          }  
          else
          {
            $key['mrp'] = 0;
          }
          array_push($data, array('mrp'=>$key['mrp'],'qty'=>$key['quantity'],'stockId'=>$key['id']));
        } 

        return $data;
    }
    
    //function is use to get the expiry date for product.
    public function expireDate($id)
    {
        $data =array();
        $stockData = Stock::where('id','=',$id)->lists('invoice_id');
        foreach($stockData as $value)
        {
            $productId = Stock::where('invoice_id','=',$value)->lists('product_id');
            $expiryDate = InvoiceItems::where('invoice_items.invoice_id', '=', $value)
                                            ->orWhere('product_id','=',$productId)
                                            ->distinct()
                                            ->select('expiry_date')->get();                             
            if((sizeof($expiryDate) > 0))
            {
              $data = date("d-m-Y", strtotime($expiryDate[0]->expiry_date));  
            }           
        }
        return $data;
    }
 
    //function is use to get products details by product name.
    public function autocompleteByProductName($data)
    {
       $resp = array();
       try
       {
            $limit         = Input::get('limit')?:10;
            $dataProduct   = Product::where('name', 'like', $data.'%')->orderBy('name','asc')->take($limit)->get();
            $resp['success'] = true;
            $resp['data']    = (new ProductTransformer)->transformCollectionPlain($dataProduct);
            $resp['msg']     = "Product Details Information";
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Product Details Not Available"; 
            $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }
   
   //function is use to get product details by using product id. 
    public function autocompleteById($id)
    {
       $resp=array();    
       try
       {
            $limit   = Input::get('limit')?:10;
            $results = $this::paginate($limit);
            $productData = Product::where('id', '=', $id)->paginate($limit);
            $resp['success']    = true;
            $resp['data']       = (new ProductTransformer)->transformCollection($productData);
            $resp['pagination'] = (new Paginations)->results($productData);
            $resp['msg']        = "Product Details Information";
       }
       catch(\Exception $e)
       {
            $resp['success']    = false;
            $resp['msg']        = $e->getMessage().' '."Product Details Not Available";
            $resp['ex']         = $e->getMessage();
       }
       return $resp;
    }
   
    //function is use to search product details.
    public function search($data)
    {
       $productTransformer = new ProductTransformer;
       $products = Product::where('name', 'like', '%'.$data.'%')->get();   
       return $productTransformer->transformCollection($products); 
    }

    //function is use to get product details in beetween of two dates.
    public function history()
    {
       $resp     = array();
       $limit    = Input::get('limit')?:10;
       $fromDate = Input::get('fromDate');
       $toDate   = Input::get('toDate');
       $from     = date("Y-m-d H:i:s", strtotime($fromDate));
       $to       = date("Y-m-d H:i:s", strtotime($toDate));
       $products = Product::whereBetween('created_at',array($from, $to))->paginate($limit);
       if($fromDate!='' && $toDate!='')
       {
            $productTransformer = new ProductTransformer;
            $resp['success']    = true;
            $resp['msg']        = "Product Details Available From search date";
            $resp['data']       = $productTransformer->transformCollection($products);
            $resp['pagination'] = (new Paginations)->results($products);
       }
       else
       {
            $resp['success'] = false;
            $resp['msg']     = "Please Select date";
       }
       return $resp;
    }
   
    //function is use to search product details.
    public function getDataForReports($fromDate, $toDate, $myRequestObject)
    {
       $table       = Product::whereBetween('created_at',array($fromDate, $toDate))->get();
       $reportName  = new Product;
       $productData = array();
       foreach($table as $row)
       {           
           array_push($productData, array($row['id'], $row['name'], $row['manufacturer'], $row['formula'], $row['dosage']));
       }
       return $productData;
    }
}
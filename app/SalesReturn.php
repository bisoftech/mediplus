<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Response;
use App\MediPlus\Transformers\SalesReturnTransformer as SalesReturnTransformer;
use App\MediPlus\Pagination\Paginations;
use Input;
use DB;
use Auth;
use App\OperationLog;
use App\User;

class SalesReturn extends Model
{
    protected $table = 'sales_return';
    use SoftDeletes;
    
    //This function is use for show all sales return details.
    public function getAll()
    {
        $resp = array();
        try
        {
            $limit   = Input::get('limit')?:10;
            $results = $this::orderBy('id', 'DESC')->paginate($limit);
            $resp['success']     = true;
            $resp['data']        = (new SalesReturnTransformer)->transformCollection($results);
            $resp['pagination']  = (new Paginations)->results($results);  
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Sales Return Details Not Available";
            $resp['ex']      = $ex->getMessage();
        }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all sales return details from table sales_return successfully');
            (new OperationLog)->add($operationLog);
        return $resp;
    }
    
    //This function is use for show sales return details by Id.
    public function getById($id)
    {
        $resp = array();
        try
        {
            $resp['success'] = true;
            $resp['data']    = (new SalesReturnTransformer)->transform(SalesReturn::findOrFail($id));     
            $operationLog=array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'success',
                                "customMessage"    => 'user id '.Auth::User()->id.' view sales return details in table sales_return with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Sales Return Details Not Available";
            $resp['ex']      = $ex->getMessage();
            $operationLog=array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'fail',
                                "customMessage"    => 'user id '.Auth::User()->id.' view sales return details in table sales_return with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
        }
        return $resp;
    }

    //This function is use to add sales return details in sales_return table.
    public function add()
    {
        $resp = array();
        try
        {
            $resp['success'] = true;
            $salesReturn     = (new SalesReturnTransformer)->reverseTransform(Input::all());
            $salesReturnId   = SalesReturn::insertGetId($salesReturn);
            $resp['data']    = (new SalesReturnTransformer)->transform(SalesReturn::find($salesReturnId));
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => $salesReturnId,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table sales_return with id '.$salesReturnId.' successfully');
            (new OperationLog)->add($operationLog);
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Sales Return Details Added Failure";
            $resp['ex']      = $ex->getMessage();
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => 0,
                                  "operationStatus"  => 'fail',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table sales_return failure');
            (new OperationLog)->add($operationLog);
        }
        return $resp;
    }
    
    //This function is use for update sales return details by Id.
    public function updateById($id)
    {
        $resp = array();
        try
        {
            $arr = (new SalesReturnTransformer)->reverseTransform(Input::all());
            SalesReturn::where('id',$id)->update($arr);
            SalesReturn::findOrFail($id)->save();
            $resp['success'] = true;
            $resp['data']    = (new SalesReturnTransformer)->transform(SalesReturn::findOrFail($id));  
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Sales Return Details Not Available";
            $resp['ex']      = $ex->getMessage();
        }
        return $resp;   
    }
    
    //This function is use for delete sales return details by Id.
    public function deleteById($id)
    {    
        $resp = array();
        try
        {
            $salesReturn = SalesReturn::findOrFail($id);
            $salesReturn->delete();
            $limit   = Input::get('limit')?:10;
            $results = $salesReturn::paginate($limit);
            $salesReturnTransformer = new salesReturnTransformer;
            $resp['success']    = true;
            $resp['data']       = $salesReturnTransformer->transform($salesReturn);
            $resp['pagination'] = (new Paginations)->results($results);      
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Sales Return Details Not Available";
            $resp['ex']      = $ex->getMessage();
        }
        return $resp;
    }
    
    public function editedit($id)
    {
        $data=(new SalesReturnTransformer)->reverseTransform(Input::all(),(SalesReturn::find($id)));
        return $this->getById($data->id);    
    }
    
    //This function is use to display purchase order details for search any keyword.
    public function autocomplete($data)
    {
       $resp = array();
       try
       {
            $limit              = Input::get('limit')?:10;
            $resp['success']    = true;
            $dataSalesReturn    = SalesReturn::where('reason', 'like', '%'.$data.'%')->paginate($limit);
            $resp['data']       = (new SalesReturnTransformer)->transformCollection($dataSalesReturn);
            $resp['pagination'] = (new Paginations)->results($dataSalesReturn);
            $resp['msg']        = "Sales Return Details Information";     
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Sales Return Details Not Available"; 
            $resp['ex']      = $ex->getMessage();
       }
       return $resp;
    }
    
    //This function is use to get all sales return details in-beetween of two dates.
    public function history()
    {
        $resp        = array();
        $limit       = Input::get('limit')?:10;
        $fromDate    = Input::get('fromDate');
        $toDate      = Input::get('toDate');
        $from        = date("Y-m-d H:i:s", strtotime($fromDate));
        $to          = date("Y-m-d H:i:s", strtotime($toDate)); 
        $salesReturn = SalesReturn::whereBetween('created_at',array($from, $to))->paginate($limit);
        if($fromDate!='' && $toDate!='')
        {
            $salesReturnTransformer = new SalesReturnTransformer;
            $resp['success']        = "true";
            $resp['msg']            = "Sales Return Details Available From search date";
            $resp['data']           = $salesReturnTransformer->transformCollection($salesReturn); 
            $resp['pagination']     = (new Paginations)->results($salesReturn);
        }
        else
        {
            $resp['success'] = "false";
            $resp['msg']     = "Please Select date";
        }
        return $resp;
    }
}
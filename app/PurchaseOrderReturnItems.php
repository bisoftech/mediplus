<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Response;
use App\MediPlus\Transformers\OrderReturnTransformer;
use App\MediPlus\Pagination\Paginations;
use Input;
use DB;
use Auth;
use App\OperationLog;
use App\User;
use App\InvoiceItems;
use Session;
use Log;
use App\MediPlus\Transformers\PurchaseOrderReturnItemsTransformer;

class PurchaseOrderReturnItems extends Model
{
    protected $table = 'purchase_order_return_items';
    use SoftDeletes;
    
    public $reportColumnTitle = array('Sr.No', 'Created Date', 'Updated Date', 'Product Name', 'Product Dosage', 'Product Location', 'Quantity','Return Reason', 'Extra Notes','Return Id');


    public function purchaseOrderReturn()
    {
        return $this->belongsTo('App\PurchaseOrderReturn','id');
    }

    public function invoiceItems()
    {
        return $this->belongsTo('App\InvoiceItems','invoice_item_id','id');
    }

    //This function is use to update return items by Id
        public function updateById($id)
    {
        $resp = array();
        try
        {
            $arr= PurchaseOrderReturnItems::findOrFail($id);
            $arr             = (new PurchaseOrderReturnItemsTransformer())->reverseTransform(Input::all(),$arr);
//            PurchaseOrderReturnItems::where('id',$id)->update($arr);
            $arr ->update();
            $resp['success'] = true;
            $resp['data']    = (new PurchaseOrderReturnItemsTransformer)->transform(PurchaseOrderReturnItems::findOrFail($id));
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Order Return Details Not Available";
            $resp['ex']      = $ex->getMessage();
        }
        return $resp;
    }

    //This function is use to show return items details and decrement the return item (product) quantity from stock table. 
    public function returnItem($item,$purchaseOrderReturnId){
        $resp = array();
        try {
            $resp['success'] = true;
            $item['purchaseOrderReturnId']=$purchaseOrderReturnId;
            $item['branchId']  = Session::get('branch_id');
            $purchaseOrderReturnItem=((new PurchaseOrderReturnItemsTransformer())->reverseTransform($item,(new PurchaseOrderReturnItems())));
            $purchaseOrderReturnItem->save();
            $stockId=$item['stockId'];
                DB::table('stock')->where('id', '=', $stockId)->Orwhere('product_id','=', $item['productId'])->decrement('quantity',$item['quantity']);


            $resp['success'] = true;
            $resp['data']    = (new PurchaseOrderReturnItemsTransformer())->transform($purchaseOrderReturnItem);
            $operationLog = array("userId"           => Auth::User()->id,
                "operation"        => 'insert',
                "referenceTableId" => $purchaseOrderReturnItem->id,
                "operationStatus"  => 'success',
                "customMessage"    => 'user id '.Auth::User()->id.' inserted in table purchase_order_return with id ' .$purchaseOrderReturnItem->id.' successfully');
            (new OperationLog)->add($operationLog);


        } catch(\Exception $ex) {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Purchase Order Return Items Details Added Failure";
            $resp['ex']      = $ex->getMessage().' '.$ex->getFile().' '.$ex->getLine();
            $resp['exs']      = $ex->getTraceAsString();
            $operationLog = array("userId"           => Auth::User()->id,
                "operation"        => 'insert',
                "referenceTableId" => 0,
                "operationStatus"  => 'fail',
                "customMessage"    => 'user id '.Auth::User()->id.' inserted in table purchase_order_return_items failure');
            (new OperationLog)->add($operationLog);
        }
        return $resp;

    }
    
//    public function getAll()
//    {
//        $resp = array();
//        try
//        {
//            $limit     = Input::get('limit')?:10;
//            $branchId  = Session::get('branch_id');
//            $results   = OrderReturn::where('branch_id', '=' , $branchId)->orderBy('id', 'DESC')->paginate($limit);
//
//            /*$results = OrderReturn::join('invoice_items', 'order_return.invoice_item_id', '=', 'invoice_items.id')
//                              ->join('invoice', 'invoice_items.invoice_id', '=', 'invoice.id')
//                              ->select('order_return.*')
//                              ->orderBy('id', 'desc')
//                              ->whereIn('invoice.user_id',UserBranchDetails::where('branch_id','=',$branchId)->lists('invoice.user_id'))
//                              ->paginate($limit);*/
//            //return $results;
//
//            $resp['success']     = true;
//            //$resp['data']        = $results;
//            $resp['data']        = (new OrderReturnTransformer)->transformCollection($results);
//            $resp['pagination']  = (new Paginations)->results($results);
//        }
//        catch(\Exception $ex)
//        {
//            $resp['success'] = false;
//            $resp['msg']     = $ex->getMessage().' '."Order Return Details Not Available";
//            $resp['ex']      = $ex->getMessage();
//        }
//            $operationLog = array("userId"           => Auth::User()->id,
//                                  "operation"        => 'Show All',
//                                  "referenceTableId" => $results,
//                                  "operationStatus"  => 'success',
//                                  "customMessage"    => 'user id '.Auth::User()->id.' show all order return details from table order_return successfully');
//            (new OperationLog)->add($operationLog);
//        return $resp;
//    }
//
//    public function getById($id)
//    {
//        $resp = array();
//        try
//        {
//            $resp['success'] = true;
//            $branchId               = Session::get('branch_id');
//            $orderReturn   = OrderReturn::where('branch_id', '=' , $branchId)->findOrFail($id);
//            //$resp['data']    = (new OrderReturnTransformer)->transform(OrderReturn::findOrFail($id));
//            $orderReturnTransformer = new OrderReturnTransformer;
//            $resp['data']    = $orderReturnTransformer->transform($orderReturn);
//            $operationLog=array("userId"           => Auth::User()->id,
//                                "operation"        => 'View',
//                                "referenceTableId" => $id,
//                                "operationStatus"  => 'success',
//                                "customMessage"    => 'user id '.Auth::User()->id.' view order return details in table order_return with id '.$id.' successfully');
//            (new OperationLog)->add($operationLog);
//        }
//        catch(\Exception $ex)
//        {
//            $resp['success'] = false;
//            $resp['msg']     = $ex->getMessage().' '."Order Return Details Not Available";
//            $resp['ex']      = $ex->getMessage();
//            $operationLog=array("userId"           => Auth::User()->id,
//                                "operation"        => 'View',
//                                "referenceTableId" => $id,
//                                "operationStatus"  => 'fail',
//                                "customMessage"    => 'user id '.Auth::User()->id.' view order return details in table order_return with id '.$id.' failure');
//            (new OperationLog)->add($operationLog);
//        }
//        return $resp;
//    }
//
//
//    public function add()
//    {
//        $resp = array();
//        try
//        {
//            $resp['success'] = true;
//            foreach (Input::all() as $key => $value)
//            {
//                $value["branchId"] = Session::get('branch_id');
//                $orderReturn     = (new OrderReturnTransformer)->reverseTransform($value);
//                $orderReturnId   = OrderReturn::insertGetId($orderReturn);
//                $stockId=$value['stockId'];
//                DB::table('stock')->where('id', '=', $stockId)->Orwhere('product_id','=', $value['productId'])->decrement('quantity',$value['quantity']);
//            }
//                $resp['success'] = true;
//                $resp['data']    = (new OrderReturnTransformer)->transform(OrderReturn::find($orderReturnId));
//                $operationLog = array("userId"           => Auth::User()->id,
//                                      "operation"        => 'insert',
//                                      "referenceTableId" => $orderReturnId,
//                                      "operationStatus"  => 'success',
//                                      "customMessage"    => 'user id '.Auth::User()->id.' inserted in table order_return with id ' .$orderReturnId.' successfully');
//                (new OperationLog)->add($operationLog);
//            //DB::table('stock')->where('product_id','=', $value['productId'])->decrement('quantity',$value['quantity']);
//        }
//        catch(\Exception $ex)
//        {
//            $resp['success'] = false;
//            $resp['msg']     = $ex->getMessage().' '.$ex->getLine().' '.$ex->getFile().' '."Order Return Details Added Failure";
//            $resp['ex']      = $ex->getMessage();
//            $operationLog = array("userId"           => Auth::User()->id,
//                                  "operation"        => 'insert',
//                                  "referenceTableId" => 0,
//                                  "operationStatus"  => 'fail',
//                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table order_return failure');
//            (new OperationLog)->add($operationLog);
//        }
//        return $resp;
//    }
//

//
//    public function deleteById($id)
//    {
//        $resp = array();
//        try
//        {
//            $orderReturn = OrderReturn::findOrFail($id);
//            $orderReturn->delete();
//            $limit   = Input::get('limit')?:10;
//            $results = $orderReturn::paginate($limit);
//            $orderReturnTransformer  = new OrderReturnTransformer;
//            $resp['success']    = true;
//            $resp['data']       = $orderReturnTransformer->transform($orderReturn);
//            $resp['pagination'] = (new Paginations)->results($results);
//        }
//        catch(\Exception $ex)
//        {
//            $resp['success'] = false;
//            $resp['msg']     = $ex->getMessage().' '."Order Return Details Not Available";
//            $resp['ex']      = $ex->getMessage();
//        }
//        return $resp;
//    }
//
//    public function autocomplete($data)
//    {
//       $resp = array();
//       try
//       {
//            $limit           = Input::get('limit')?:10;
//            $resp['success'] = true;
//            $dataOrderReturn = OrderReturn::where('extra_notes', 'like', '%'.$data.'%')
//                                            ->orWhere('return_reason', 'like', '%'.$data.'%')
//                                            ->orWhere('quantity', 'like', '%'.$data.'%')
//                                            ->paginate($limit);
//            $resp['data']       = (new OrderReturnTransformer)->transformCollection($dataOrderReturn);
//            $resp['pagination'] = (new Paginations)->results($dataOrderReturn);
//            $resp['msg']        = "Order Return Details Information";
//       }
//       catch(\Exception $e)
//       {
//            $resp['success'] = false;
//            $resp['msg']     = $ex->getMessage().' '."Order Return Details Not Available";
//            $resp['ex']      = $ex->getMessage();
//       }
//       return $resp;
//    }
//
//    public function history()
//    {
//        $resp        = array();
//        $limit       = Input::get('limit')?:10;
//        $fromDate    = "2015-08-11";
//        $toDate      = "2015-08-16";
//        $from        = date("Y-m-d H:i:s", strtotime($fromDate));
//        $to          = date("Y-m-d H:i:s", strtotime($toDate));
//        $orderReturn = OrderReturn::whereBetween('created_at',array($from, $to))->paginate($limit);
//        if($fromDate!='' && $toDate!='')
//        {
//            $orderReturnTransformer = new OrderReturnTransformer;
//            $resp['success']        = "true";
//            $resp['msg']            = "Order Return Details Available From search date";
//            $resp['data']           = $orderReturnTransformer->transformCollection($orderReturn);
//            $resp['pagination']     = (new Paginations)->results($orderReturn);
//        }
//        else
//        {
//            $resp['success'] = "false";
//            $resp['msg']     = "Please Select date";
//        }
//        return $resp;
//    }
//
//    function autocompleteByName($data)
//    {
//        $resp = array();
//        try
//       {
//            $limit= Input::get('limit')?:10;
//            $resp['success'] = true;
//            $dataOrderReturn = OrderReturn::where('extra_notes', 'like', '%'.$data.'%')
//                                            ->orWhere('id', 'like', '%'.$data.'%')
//                                            ->orWhere('return_reason', 'like', '%'.$data.'%')
//                                            ->orWhere('quantity', 'like', '%'.$data.'%')
//                                            ->take($limit)
//                                            ->get();
//
//            $resp['data']= (new OrderReturnTransformer)->transformCollectionPlain($dataOrderReturn);
//            $resp['msg']= "Order Return Details Information";
//       }
//       catch(\Exception $e)
//       {
//            $resp['success'] = false;
//            $resp['msg']     = $ex->getMessage().' '."Order Return Details Not Available";
//            $resp['ex']      = $ex->getMessage();
//       }
//       return $resp;
//    }
//
//    public function getDataForReports($fromDate, $toDate, $myRequestObject)
//    {
//        $branchId  = Session::get('branch_id');
//        $orderReturnData = array();
//        $table = OrderReturn::join('invoice_items', 'order_return.invoice_item_id', '=', 'invoice_items.id')
//                              ->join('invoice', 'invoice_items.invoice_id', '=', 'invoice.id')
//                              ->join('product', 'invoice_items.product_id', '=', 'product.id')
//                              ->select('order_return.*', 'product.name', 'product.dosage', 'product.product_location')
//                              ->orderBy('id', 'asc')
//                             ->whereIn('invoice.user_id',UserBranchDetails::where('branch_id','=',$branchId)->lists('invoice.user_id'))
//                              ->whereBetween('order_return.created_at',array($fromDate, $toDate))->get();
//        $i = 0;
//        foreach($table as $row)
//        {
//            $i++;
//            array_push($orderReturnData, array($i, $row['created_at']->format('d-m-Y'), $row['updated_at']->format('d-m-Y'), $row['name'], $row['dosage'], $row['product_location'], $row['quantity'], $row['return_reason'], $row['extra_notes']));
//        }
//        return $orderReturnData;
//    }
}
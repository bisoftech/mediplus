<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MediPlus\Transformers\CustomerTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\MediPlus\Pagination\Paginations;
use Input;
use DB;
use Auth;
use App\OperationLog;
use App\User;
use App\Billing;
use Session;

class Customer extends Model
{
    use SoftDeletes;
    protected $table = 'customer';
    
    public function billing() 
    {
        return $this->hasMany('App\Billing','customer_id','id');
    }
    
    //This function is use to assign heading for standard reports.
    public $reportColumnTitle = array('Sr.No', 'First Name', 'Middle Name', 'Last Name', 'City', 'District', 'State', 'Country', 'Street Address', 'Pin Code', 'Email Id', 'DOB', 'Contact Number', 'Credit Limit', 'Customer Type');
    
    //This function is use for display branch name.
    public function getCustomerName($id)
    {
        $customer = Customer::find($id);
             
        if($customer)
            return $customer['first_name'].' '.$customer['middle_name'].' '.$customer['last_name'];
        else 
            return 'Customer Deleted';  
    }
    
    //This function is use to get all customer data.
    public function getAll()
    {
       $resp = array();
       try
       {
            $limit   = Input::get('limit')?:10;
            $branchId               = Session::get('branch_id');
            $results = Customer::where('branch_id', '=' , $branchId)->orderBy('id', 'DESC')->paginate($limit);
            $customerTransformer = new CustomerTransformer;
            $resp['success']     = true;
            $resp['data']        = $customerTransformer->transformCollection($results); 
            $resp['pagination']  = (new Paginations)->results($results);     
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Customer Details Not Available";
            $resp['ex']      = $e->getMessage();
       }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all customer details from table customer successfully');
            (new OperationLog)->add($operationLog);
       return $resp;
    }

    //This function is use to get customer details by Id.
    public function getById($id)
    {        
       $resp = array();
       try
       {
            $branchId               = Session::get('branch_id');
            $customer               = Customer::where('branch_id', '=' , $branchId)->findOrFail($id);
            $customerTransformer    = new CustomerTransformer;
            $resp['success']        = true;
            $resp['data']           = $customerTransformer->transform($customer);
            $operationLog=array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'success',
                                "customMessage"    => 'user id '.Auth::User()->id.' view customer details in table customer with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Customer Detail Not Available";
            $resp['ex']      = $e->getMessage();
            $operationLog=array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'fail',
                                "customMessage"    => 'user id '.Auth::User()->id.' view customer details in table customer with id '.$id.' failure');
            (new OperationLog)->add($operationLog);
       }
       return $resp;
    }
    
    //This function is use to add customer details.
    public function add()
    {
       $resp = array();
       try
       {
            $customerData = Input::all();
            $customerData["branchId"] = Session::get('branch_id');
            $data             = (new CustomerTransformer)->reverseTransform($customerData);
            $customerId       = Customer::insertGetId($data);
            $resp['success']  = true;
            $resp['data']     = (new CustomerTransformer)->transform(Customer::find($customerId));
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => $customerId,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table customer with id '.$customerId.' successfully');
            (new OperationLog)->add($operationLog);
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Customer Details Added Failure";
            $resp['ex']      = $e->getMessage();
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => 0,
                                  "operationStatus"  => 'fail',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table customer failure');
            (new OperationLog)->add($operationLog);
       }
       return $resp;
    }
    
    //This function is use to update customer details by Id.
    public function updateById($id)
    {
       $resp = array();
       try
       {
            $customerData = Input::all();
            $customerData["branchId"] = Session::get('branch_id');
            $data               = (new CustomerTransformer)->reverseTransform($customerData);
            Customer::where('id',$id)->update($data);
            Customer::findOrFail($id)->save();
            $resp['success']    = true;
            $resp['data']       = (new CustomerTransformer)->transform(Customer::findOrFail($id));
       }
       catch(\Exception $e)
       {
            $resp['success']    = false;
            $resp['msg']        = $e->getMessage().' '."Customer Details Not Available";
            $resp['ex']         = $e->getMessage();
       }
       return $resp;
    }
    
    //This function is use to delete customer details by Id.
    public function deleteById($id)
    {
       $resp = array();
       try
       {
            $customer = Customer::findOrFail($id);
            $customer->delete();
            $limit   = Input::get('limit')?:10;
            $results = $customer::paginate($limit);
            $customerTransformer = new CustomerTransformer;
            $resp['success']     = true;
            $resp['data']        = $customerTransformer->transform($customer);
            $resp['pagination']  = (new Paginations)->results($results);      
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Customer Details Not Available";
            $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }

    //This function is use to get customer credited amount by customer Id.
    public function getCustomerCreditAmount($id)
    {
    	$amount = 0;
    	try
    	{
    	    $customerBills = Billing::where('customer_id',$id)->whereIn('status',['unpaid', 'partial_paid'])->get();
    	    foreach($customerBills as $bill)
    	    {
    		$amount = $amount + ($bill->total_amount - $bill->amount_paid);
    	    }
    	}
    	catch(Exception $ex)
    	{
    	    $amount = -1;
    	}
    	return $amount;
    }

    //This function is use to show customer id and amount for those customer exceed credited amount.
    public function isCreditAmountExceeded($id, $amount)
    {
    	$resp = null;
    	try
    	{
    	    $customer = Customer::find($id);
    	    if($customer->credit_limit < $amount)
    		$resp = true;
    	    else
    		$resp = false;
    	}
    	catch(Exception $ex)
    	{
    	    $resp = null;
    	}
    	return($resp);
    }
    
    //This function is use to search customer details for any keyword.
    public function autocomplete($data)
    {
       $resp = array();
       try
       {
            $limit           = Input::get('limit')?:10;
            $resp['success'] = true;
            $dataCustomer    = Customer::where('first_name', 'like', '%'.$data.'%')
                                         ->orWhere('middle_name', 'like', '%'.$data.'%')
                                         ->orWhere('last_name', 'like', '%'.$data.'%')
                                         ->orWhere('city', 'like', '%'.$data.'%')
                                         ->orWhere('district', 'like', '%'.$data.'%')
                                         ->orWhere('state', 'like', '%'.$data.'%')
                                         ->orWhere('country', 'like', '%'.$data.'%')
                                         ->orWhere('street_address', 'like', '%'.$data.'%')
                                         ->orWhere('pin_code', 'like', '%'.$data.'%')
                                         ->orWhere('email_id', 'like', '%'.$data.'%')
                                         ->orWhere('dob', 'like', '%'.$data.'%')
                                         ->orWhere('contact_number', 'like', '%'.$data.'%')
                                         ->orWhere('credit_limit', 'like', '%'.$data.'%')
                                         ->orWhere('customer_type', 'like', '%'.$data.'%')
                                         ->paginate($limit);
            $resp['data']       = (new CustomerTransformer)->transformCollection($dataCustomer);
            $resp['pagination'] = (new Paginations)->results($dataCustomer);
            $resp['msg']        = "Customer Details Information";     
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Customer Details Not Available";  
            $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }
    
    //This function is use to get all details in-beetween of two dates.
    public function history()
    {
        $resp        = array();
        $limit       = Input::get('limit')?:10;
        $fromDate    = Input::get('fromDate');
        $toDate      = Input::get('toDate');
        $from        = date("Y-m-d H:i:s", strtotime($fromDate));
        $to          = date("Y-m-d H:i:s", strtotime($toDate)); 
        $customer    = Customer::whereBetween('created_at',array($from, $to))->paginate($limit);
        if($fromDate!='' && $toDate!='')
        {
            $customerTransformer    = new CustomerTransformer;
            $resp['success']        = "true";
            $resp['msg']            = "Customer Details Available From search date";
            $resp['data']           = $customerTransformer->transformCollection($customer); 
            $resp['pagination']     = (new Paginations)->results($customer);
        }
        else
        {
            $resp['success'] = "false";
            $resp['msg']     = "Please Select date";
        }
        return $resp;
    }
    
    //This function is use to search customer details using customer name.
    public function autocompleteByName($data)
    {
       $resp = array();
       try
       {
            $limit           = Input::get('limit')?:10;
            $resp['success'] = true;

            $dataCustomer    = Customer::where('first_name', 'like',$data.'%')
                                        ->orWhere('last_name', 'like', '%'.$data.'%')
                                        ->orWhere('contact_number', 'like', '%'.$data.'%')
                                        ->orderBy('first_name','asc')
                                        ->take($limit)->get();
            $resp['data']    = (new CustomerTransformer)->transformCollectionPlain($dataCustomer);
            $resp['msg']     = "Customer Details Information";
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Customer Details Not Available";
            $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }
    
    //This function is use to get all details.
    //This function is use for standard reports.
    public function getDataForReports($fromDate, $toDate, $myRequestObject)
    {
       $table       = Customer::whereBetween('created_at',array($fromDate, $toDate))->get();
       $reportName  = new Customer;
       $customerData = array();
       foreach($table as $row) 
       {            
            array_push($customerData, array($row['id'], $row['first_name'], $row['middle_name'], $row['last_name'], $row['city'],                                                     $row['district'], $row['state'], $row['country'], $row['street_address'], $row['pin_code'],                                               $row['email_id'], $row['dob'], $row['contact_number'], $row['credit_limit'],                                                               $row['customer_type']));
       }
       return $customerData;
    }
    
    //This function is use to get bill details for selected customers.
    public function CustomerBillDetails($id)
    {  
       $resp = array();
       try
       {
            $limit           = Input::get('limit')?:10;
            $resp['success'] = true;
            $customerBillData= Customer::where('customer.id', 'like', '%'.$id.'%')->get();
            
            foreach($customerBillData as $customerBill)
            {
               $customerBill->billing;
               foreach($customerBill->billing as $customerBillDetails)
               {
                   $customerBillDetails->user;
                   $customerBillDetails->billingItems;
                   foreach($customerBillDetails->billingItems as $customerProductBill)
                   {
                       $customerProductBill->product;
                   }
               }
            }
    
            $resp['data'] = $customerBillData; 
            $resp['msg']  = "Customer Bill Details";
       }
       catch(\Exception $e)
       {
            $resp['success']     = false;
            $resp['msg']         = $e->getMessage();
       }
       return $resp;
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\MediPlus\Transformers\TaxTransformer as TaxTransformer;
use App\Http\Requests;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\MediPlus\Pagination\Paginations;
use Input;
use DB;
use Auth;
use App\Branch;
use App\Shop;
use App\User;
use App\UserBranchDetails;
use App\OperationLog;
use Session;

class Tax extends Model
{
    use SoftDeletes;
    protected $table = 'tax';
    
    //This function is use to assign heading for standard reports.
    public $reportColumnTitle = array('Sr.No', 'VAT Tax', 'CST Tax', 'LBT Tax', 'Service Tax', 'Branch Name');
    
    //This function is use for show all tax details.
    public function getAll()
    {
        $resp = array();
        try 
        {
            $limit     = Input::get('limit')?:10;
            $branchId  = Session::get('branch_id');
            $results   = $this::where('branch_id', '=' , $branchId)->orderBy('id', 'DESC')->paginate($limit);
            $taxTransformer     = new TaxTransformer;
            $tax                = $taxTransformer->transformCollection($results);
            $resp['success']    = true;
            $resp['data']       = $tax;
            $resp['pagination'] = (new Paginations)->results($results);    
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Branch Tax Details Not Available";
            $resp['ex']      = $e->getMessage();
        }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all tax details from table tax successfully');
            (new OperationLog)->add($operationLog);
        return $resp;
    }
    
    //This function is use for show tax details by selected tax Id.
    public function getById($id)
    {    
       $resp = array();
       try
       {
            $branchId           = Session::get('branch_id');
            $tax                = Tax::where('branch_id', '=' , $branchId)->findOrFail($id);
            $taxTransformer     = new TaxTransformer;
            $resp['success']    = true;
            $resp['data']       = $taxTransformer->transform($tax);
            $operationLog=array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'success',
                                "customMessage"    => 'user id '.Auth::User()->id.' view tax details in table tax with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Branch Tax Detail Not Available";
            $resp['ex']      = $e->getMessage();
            $operationLog=array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'fail',
                                "customMessage"    => 'user id '.Auth::User()->id.' view tax details in table tax with id '.$id.' failure');
            (new OperationLog)->add($operationLog);
       }
       return $resp;
    }
    
    //This function is use to add tax details in tax table.
    public function add()
    {
       $resp = array();
       try
       {
            $arr             = (new TaxTransformer)->reverseTransform(Input::all());
            $taxId           = Tax::insertGetId($arr); 
            $resp['success'] = true;            
            $resp['data']    = (new TaxTransformer)->transform(Tax::find($taxId));
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => $taxId,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table tax with id '.$taxId.' successfully');
            (new OperationLog)->add($operationLog);
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Branch Tax Details Added Failure";
            $resp['ex']      = $e->getMessage();
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => 0,
                                  "operationStatus"  => 'fail',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table stock failure');
            (new OperationLog)->add($operationLog);
       }
       return $resp;
    }
    
    //This function is use for update tax details by selected Id. 
    public function updateById($id)
    {
       $resp = array();
       try
       {
            $arr             = (new TaxTransformer)->reverseTransform(Input::all());
            Tax::where('id',$id)->update($arr);
            Tax::findOrFail($id)->save();
            $resp['success'] = true;
            $resp['data']    = (new TaxTransformer)->transform(Tax::findOrFail($id));
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Branch Tax Details Not Available";
            $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }
    
    //This function is use for delete tax details by selected Id.
    public function deleteById($id)
    {
       $resp = array();
       try
       {
            $tax = Tax::findOrFail($id);
            $tax->delete();
            $limit   = Input::get('limit')?:10;
            $results = $tax::paginate($limit);
            $taxTransformer      = new TaxTransformer;
            $resp['success']     = true;
            $resp['data']        = $taxTransformer->transform($tax);
            $resp['pagination']  = (new Paginations)->results($results);      
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Branch Tax Details Not Available"; 
            $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }
    
    //This function is use for show tax details by selected branch Id.
    public function getByBranchId($branchId)
    {
        $resp = array();
        try 
        {
            $limit   = Input::get('limit')?:10;
            $results = Tax::where('branch_id', '=' , $branchId)->paginate($limit);
            $taxTransformer     = new TaxTransformer;
            $tax                = $taxTransformer->transformCollection($results);
            $resp['success']    = true;
            $resp['data']       = $tax;
            $resp['pagination'] = (new Paginations)->results($results);    
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Branch Tax Details Not Available";
            $resp['ex']      = $e->getMessage();
        }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all tax details from table tax successfully');
            (new OperationLog)->add($operationLog);
        return $resp;
    } 
    
    //This function is use to display tax details for search any keyword.
    public function autocomplete($data)
    {
       $resp = array();
       try
       {
            $limit           = Input::get('limit')?:10;
            $resp['success'] = true;
            $dataTax         = Tax::where('vat_tax', 'like', '%'.$data.'%')
                                    ->orWhere('cst_tax', 'like', '%'.$data.'%')
                                    ->orWhere('lbt_tax', 'like', '%'.$data.'%')
                                    ->orWhere('service_tax', 'like', '%'.$data.'%')
                                    ->paginate($limit);
            $resp['data']    = (new TaxTransformer)->transformCollection($dataTax);
            $resp['pagination'] = (new Paginations)->results($dataTax);
            $resp['msg']        = "Tax Details Information";     
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Tax Details Not Available"; 
            $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }
    
    //This function is use to get all details in-beetween of two dates.
    public function history()
    {
        $resp        = array();
        $limit       = Input::get('limit')?:10;
        $fromDate    = Input::get('fromDate');
        $toDate      = Input::get('toDate');
        $from        = date("Y-m-d H:i:s", strtotime($fromDate));
        $to          = date("Y-m-d H:i:s", strtotime($toDate)); 
        $tax         = Tax::whereBetween('created_at',array($from, $to))->paginate($limit);
        if($fromDate!='' && $toDate!='')
        {
            $taxTransformer         = new TaxTransformer;
            $resp['success']        = "true";
            $resp['msg']            = "Tax Details Available From search date";
            $resp['data']           = $taxTransformer->transformCollection($tax); 
            $resp['pagination']     = (new Paginations)->results($tax);
        }
        else
        {
            $resp['success'] = "false";
            $resp['msg']     = "Please Select date";
        }
        return $resp;
    }
    
    //This function is use to get all tax details.
    //This function is use for standard reports.
    public function getDataForReports($fromDate, $toDate, $myRequestObject)
    {
        $table = Tax::join('branch', 'tax.branch_id', '=', 'branch.id')
                                    ->select('tax.*', 'branch.branch_name')
                                    ->whereBetween('tax.created_at',array($fromDate, $toDate))->get();
        $reportName  = new Tax;
        $taxData = array();
        foreach($table as $row) 
        {            
            array_push($taxData, array($row['id'], $row['vat_tax'], $row['cst_tax'], $row['lbt_tax'], $row['service_tax'],                                                        $row['branch_name']));
        }
        return $taxData;
    }
}
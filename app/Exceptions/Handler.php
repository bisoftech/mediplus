<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Log;
use Auth;
use App\DAWErrorLog;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    // protected $dontReport = [
    //     HttpException::class,
    // ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {

        try{
                $errorLog=new DAWErrorLog;
                if(Auth::check())
                $errorLog->user_id=Auth::user()->id;
                $errorLog->id=time();
                $errorLog->stack_tace=serialize($e);
                $errorLog->short_message=$e->getMessage();
                $errorLog->line_no=$e->getLine();
                $errorLog->file=$e->getFile();
                $errorLog->error_type='PHP';
                $errorLog->save();
        } catch (Exception $ex){


        }
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        return parent::render($request, $e);
    }
}

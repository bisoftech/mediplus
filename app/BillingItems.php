<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class BillingItems extends Model
{
    protected $table = 'billing_items';

    public function BillingItems()
    {
        return $this->hasMany('App\BillingItems', 'foreign_key', 'local_key');
    }
    
    public function product() 
    {
        return $this->belongsTo('App\Product');
    }
}

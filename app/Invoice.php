<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Response;
use App\MediPlus\Transformers\InvoiceTransformer as InvoiceTransformer;
use App\MediPlus\Transformers\InvoiceItemsTransformer as InvoiceItemsTransformer;
use App\MediPlus\Pagination\Paginations;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use Input;
use App\User;
use App\PurchaseOrder;
use App\PurchaseOrderItems;
use App\Product;
use App\Invoice;
use App\InvoiceItems;
use App\OperationLog;
use App\UserBranchDetails;
use App\Distributor;
use Log;
use Session;

class Invoice extends Model
{
    protected $table = 'invoice';
    use SoftDeletes;
  
    public function invoiceItems()
    {
        return $this->hasMany('App\InvoiceItems','invoice_id','id');
    }
    
    //This function is use to assign heading for standard reports.  
    public $reportColumnTitle = array('Sr.No', 'Created Date', 'Updated Date', 'Order Date', 'Invoice Date', 'Parcel Number', 'User Name', 'Distributor Name', 'Total Amount', 'Paid Amount', 'Payment Type');
  
    //This function is use for add product details in invoice, invoice_items and stock table.
    public function add()
    {   
        $resp = array();
        try
        {
            $invoiceData=Input::all();
            $invoiceData["branchId"] = Session::get('branch_id');
            $data =(new InvoiceTransformer)->reverseTransform($invoiceData);  
            $id = DB::table('invoice')->insertGetId($data);
            $invoiceItemsData=$invoiceData['invoiceItems'];
            foreach ($invoiceItemsData as $value) 
            {
                $iit=(new InvoiceItemsTransformer)->reverseTransform($value,$id);
                DB::table('invoice_items')->insert($iit);
                DB::table('stock')->insert(['created_at' => date('Y-m-d H:i:s'),
                                            'product_id' => $value['productId'], 
                                            'quantity' => $value['quantity'], 
                                            'branch_id' => Session::get('branch_id'),
                                            'invoice_id' => $id]);
                if($invoiceData['poStatus'] == 1)
                {
                  $date = date('Y-m-d H:i:s');
                  DB::table('purchase_order_items')->where('purchase_order_id', '=', $invoiceData['purchaseOrderId'])
                                                   ->where('product_id', '=',  $value['productId'])
                                                   ->update(['deleted_at' => $date]);
                }
            }

            if($invoiceData['poStatus'] == 1)
            {
              DB::table('purchase_order')->where('id', '=',  $invoiceData['purchaseOrderId'])->update(['status' => 'Inactive']);
            }

            $resp['success'] = true;
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => $id,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table invoice with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Invoice Details Added Failure";
            $resp['ex']      = $e->getMessage();
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => 0,
                                  "operationStatus"  => 'fail',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table invoice failure');
            (new OperationLog)->add($operationLog);
        }
          
        return $resp;
    }
 
    //This function is use for show all invoice details.
    public function getAll()
    {
        $resp = array();
        try
        {
            $limit     = Input::get('limit')?:10;
            $branchId  = Session::get('branch_id');
            $results  = Invoice::where('branch_id', '=' , $branchId)->orderBy('id', 'DESC')->paginate($limit);
            $invoiceTransformer      = new InvoiceTransformer;
            $invoiceItemsTransformer = new InvoiceItemsTransformer;
            $invoice         = $invoiceTransformer->transformCollection($results,true);
            $resp['success'] = true;
            $resp['data']    = $invoice;
            $resp['pagination']  = (new Paginations)->results($results);   
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Invoice Details Not Available";
            $resp['ex']      = $e->getMessage();
        }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all invoice details from table invoice successfully');
            (new OperationLog)->add($operationLog);
        return $resp;
    }
    
    //This function is use for show invoice details by invoice Id.
    public function getById($id)
    { 
       $resp = array();
       try
       {
           $branchId  = Session::get('branch_id');
           $invoice   = Invoice::where('branch_id', '=' , $branchId)->findOrFail($id);
           $invoiceTransformer      = new InvoiceTransformer;
           $invoiceItemsTransformer = new InvoiceItemsTransformer;
           $resp['success'] = true;
           $resp['data']    = $invoiceTransformer->transform($invoice);
           $operationLog=array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'success',
                                "customMessage"    => 'user id '.Auth::User()->id.' view invoice details in table invoice with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
       }
       catch(\Exception $e)
       {
           $resp['success'] = false;
           $resp['msg']     = $e->getMessage().' '."Invoice Detail Not Available";
           $resp['ex']      = $e->getMessage();
           $operationLog=array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'fail',
                                "customMessage"    => 'user id '.Auth::User()->id.' view invoice details in table invoice with id '.$id.' failure');
            (new OperationLog)->add($operationLog);
       }
       return $resp;
    }
  
    //This function is use to search invoice details for any keyword.
    public function autocomplete($data)
    {
       $resp=array();   
       try
       {
           $branchId    = Session::get('branch_id');
           $limit       = Input::get('limit')?:10;
           $results     = $this::paginate($limit);     
           $invoiceData = Invoice::whereIn('user_id', User::Where('user_name', 'like', '%'.$data.'%')->lists('id'))  
                                   ->orWhere('parcel_number', 'like', '%'.$data.'%')
                                   ->orWhere('invoice_date', 'like', '%'.$data.'%')
                                   ->whereIn('user_id',UserBranchDetails::where('branch_id','=',$branchId)
                                   ->lists('user_id'))
                                   ->paginate($limit);
           $resp['success']     = true;
           $resp['data']        = (new InvoiceTransformer)->transformCollection($invoiceData,true);
           $resp['pagination']  = (new Paginations)->results($invoiceData);
           $resp['msg']         = "Invoice Details Information";
       }
       catch(\Exception $e)
       {
           $resp['success']     = false;
           $resp['msg']         = $e->getMessage().' '."Invoice Details Not Available";
           $resp['ex']          = $e->getMessage();
       }
       return $resp;
    }
    
    //This function is use to search invoice details using invoice Id.
    public function autocompleteById($id)
    {
       $resp=array();   
       try
       {
           $branchId = Session::get('branch_id');
           $limit    = Input::get('limit')?:10;
           $results  = $this::paginate($limit);
           $invoiceId = Invoice::where('branch_id', '=', $branchId)->lists('id');
           $resp['success']    = true;
           $resp['data']       = ["id"=>$invoiceId];
           $resp['msg']        = "Invoice Details Information";
       }
       catch(\Exception $e)
       {
           $resp['success']    = false;
           $resp['msg']        = $e->getMessage().' '."Invoice Details Not Available";
           $resp['ex']         = $e->getMessage();
       }
       return $resp;
    }

    //This function is use to search distributor details by any keyword.
    public function autocompleteByName($data)
    {
       $resp=array();   
       try
       {
           $limit   = Input::get('limit')?:10;
           $invoiceData = Invoice::where('id', 'like', '%'.$data.'%')
                                   ->orWhere('parcel_number', 'like', '%'.$data.'%')
                                   ->orWhere('payment_type', 'like', '%'.$data.'%')
                                   ->orWhere('cheque_number', 'like', '%'.$data.'%')
                                   ->orWhere('free_items', 'like', '%'.$data.'%')
                                   ->take($limit)
                                   ->get();
           $resp['success']    = true;
           $resp['data']       = (new InvoiceTransformer)->transformCollectionPlain($invoiceData);
           $resp['msg']        = "Invoice Details Information";
       }
       catch(\Exception $e)
       {
           $resp['success']    = false;
           $resp['msg']        = $e->getMessage().' '."Invoice Details Not Available";
           $resp['ex']         = $e->getMessage();
       }
       return $resp;
    }

    //This function is use to get all details in-beetween of two dates.
    public function history()
    {
       $resp = array();
       try
       {
           $limit    = Input::get('limit')?:10;
           $fromDate = Input::get('fromDate');
           $toDate   = Input::get('toDate');
           $from     = date("Y-m-d H:i:s", strtotime($fromDate));
           $to       = date("Y-m-d H:i:s", strtotime($toDate));
           $productData = Product::whereBetween('created_at',array($from, $to))->paginate($limit);        
           foreach($productData as $value)
           {
                foreach(InvoiceItems::where('product_id', '=', $value->id)->whereBetween('created_at',array($from, $to))->lists('invoice_id') as $invoiceData)
                {
                    $resp['data'][]     = (new InvoiceTransformer)->transform(Invoice::find($invoiceData));
                    $resp['pagination'] = (new Paginations)->results($productData);
                }
           }
       }
       catch(\Exception $e)
       {
           $resp['success'] = false;
           $resp['msg']     = $e->getMessage().' '."Please Select Date";
           $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }
  
    //This function is use to show product invoice details for selecting branch Id. 
    public function getByBranchId($branchId)
    {
        $resp = array();
        try
        {
           $limit   = Input::get('limit')?:10;
           $results = Invoice::whereIn('user_id',UserBranchDetails::where('branch_id','=',$branchId)->lists('user_id'))->paginate($limit);
           $invoiceTransformer      = new InvoiceTransformer;
           $invoiceItemsTransformer = new InvoiceItemsTransformer;
           $invoice         = $invoiceTransformer->transformCollection($results);
           $resp['success'] = true;
           $resp['data']    = $invoice;
           $resp['pagination']  = (new Paginations)->results($results);   
        }
        catch(\Exception $e)
        {
           $resp['success'] = false;
           $resp['msg']     = $e->getMessage().' '."Invoice Details Not Available";
           $resp['ex']      = $e->getMessage();  
        }
           $operationLog = array("userId"           => Auth::User()->id,
                                 "operation"        => 'Show All',
                                 "referenceTableId" => $results,
                                 "operationStatus"  => 'success',
                                 "customMessage"    => 'user id '.Auth::User()->id.' show all invoice details from table invoice successfully');
           (new OperationLog)->add($operationLog);
        return $resp;
    }
  
    //This function is use to display mrp for selected product Id.
    public function priceByProductId($id)
    {
        return InvoiceItems::where('product_id', '=', $id)->lists('mrp');
    }
    
    //This function is use to get all invoice details.
    //This function is use for standard reports.
    public function getDataForReports($fromDate, $toDate, $myRequestObject)
    { 
        $branchId  = Session::get('branch_id');
        $table = Invoice::join('users', 'invoice.user_id', '=', 'users.id')
                        ->join('purchase_order', 'invoice.purchase_order_id', '=', 'purchase_order.id')
                        ->join('distributor', 'purchase_order.distributor_id', '=', 'distributor.id')
                        ->join('user_branch_details', 'invoice.user_id' , '=', 'user_branch_details.user_id')
                        ->join('branch', 'branch.id' , '=', 'user_branch_details.branch_id')
                        ->select('invoice.*', 'purchase_order.order_date', 'users.user_name', 'distributor.owner_name')
                        ->orderBy('id', 'asc')
                        ->where('user_branch_details.branch_id','=',$branchId)
                        ->whereBetween('invoice.created_at',array($fromDate, $toDate))->get();
        $reportName  = new Invoice;
        $invoiceData = array();
        $i = 0;
        foreach($table as $row)
        {    
            $i++;       
            array_push($invoiceData, array($i,$row['created_at']->format('d-m-Y'),$row['updated_at']->format('d-m-Y'), $row['order_date'], $row['invoice_date'], $row['parcel_number'], $row['user_name'], $row['owner_name'], $row['total_amount'], $row['amount_paid'], $row['payment_type']));
        }
        return $invoiceData;
    }
}
<?php

namespace App;

use App\MediPlus\Transformers\PurchaseOrderReturnTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Response;
use App\MediPlus\Pagination\Paginations;
use Input;
use DB;
use Auth;
use App\OperationLog;
use App\User;
use App\InvoiceItems;
use Session;
use Log;
use App\PurchaseOrderReturnItems;

class PurchaseOrderReturn extends Model
{
    protected $table = 'purchase_order_return';
    use SoftDeletes;


    public function purchaseOrderReturnItems()
    {
        return $this->hasMany('App\PurchaseOrderReturnItems','purchase_order_return_id','id');
    }
    
    //This function is use to assign heading for standard reports.
    public $reportColumnTitle = array('Sr.No','Return Id', 'Invoice Id','No Of Items','Status','Total Amount','Refund Loss', 'Created Date', 'Updated Date');
    
    //This function is use for add purchase order return details in table purchase_order_return, purchase_order_return_items.
    public function add()
    {
        DB::beginTransaction();
        $resp = array();
        try
        {
            $resp['success'] = true;

            $purchaseOrderReturn=new PurchaseOrderReturn();
            $purchaseOrderReturn->invoice_id=Input::get('invoiceId');
            $purchaseOrderReturn->save();
            $quantityFlag=0;
            $productName='';
            foreach (Input::get('purchaseOrderItems') as $purchaseOrderReturnItem) {

                $stockId=$purchaseOrderReturnItem['stockId'];
                $productId = $purchaseOrderReturnItem['productId'];
                $availableQuantity = Stock::select('quantity')->where('id', '=', $stockId)->where('product_id', '=', $productId)->get()[0]->quantity;
                if($availableQuantity>=$purchaseOrderReturnItem['quantity']) {
                    if($purchaseOrderReturnItem['quantity']>0){
                        $itemResp = (new PurchaseOrderReturnItems())->returnItem($purchaseOrderReturnItem, $purchaseOrderReturn->id);
                        if($itemResp['success']==false)
                            break;
                    }
                }
                else {
                    $productName=$purchaseOrderReturnItem['product']['name'];
                    $quantityFlag = 1;
                    break;
                }
            }


            if($quantityFlag){
                $resp['success'] = false;
                $resp['msg']='Quantity in stock for '.$productName.' is lower than entered quantity';
                DB::rollback();
                $operationLog = array("userId"           => Auth::User()->id,
                    "operation"        => 'insert',
                    "referenceTableId" => 0,
                    "operationStatus"  => 'fail',
                    "customMessage"    => 'user id '.Auth::User()->id.' inserted in table order_return failure');
                DB::rollback();
                return $resp;
            }
            if($itemResp['success']==false) {

                $operationLog = array("userId"           => Auth::User()->id,
                    "operation"        => 'insert',
                    "referenceTableId" => 0,
                    "operationStatus"  => 'fail',
                    "customMessage"    => 'user id '.Auth::User()->id.' inserted in table order_return failure');
                DB::rollback();

                return $itemResp;
            }
            $resp['success'] = true;
            $resp['data'] = (new PurchaseOrderReturnTransformer())->transform($purchaseOrderReturn);
            $operationLog = array("userId" => Auth::User()->id,
                "operation" => 'insert',
                "referenceTableId" => $purchaseOrderReturn->id,
                "operationStatus" => 'success',
                "customMessage" => 'user id ' . Auth::User()->id . ' inserted in table purchase_order_return with id ' . $purchaseOrderReturn->id . ' successfully');
            (new OperationLog)->add($operationLog);

            //DB::table('stock')->where('product_id','=', $value['productId'])->decrement('quantity',$value['quantity']);  
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            $resp['success'] = false;
            $resp['msg']     = $ex->getTraceAsString(). " purchase Order Return Details Added Failure";
            $resp['ex']      = $ex->getTraceAsString();
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => 0,
                                  "operationStatus"  => 'fail',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table order_return failure');
            (new OperationLog)->add($operationLog);
            return $resp;
        }
        DB::commit();
        return $resp;
    }

    //This function is use for show all purchase order return details
    public function getAll()
    {
        $resp = array();
        try
        {
            $branchId  = Session::get('branch_id');
            $limit     = Input::get('limit')?:10;
            $results   = PurchaseOrderReturn::join('purchase_order_return_items','purchase_order_return.id','=','purchase_order_return_items.purchase_order_return_id')
                ->join('invoice','invoice.id','=','purchase_order_return.invoice_id')
                ->where('invoice.branch_id',$branchId)
                ->select('purchase_order_return.*')
                ->groupBy('purchase_order_return.id')
                ->orderBy('purchase_order_return.id', 'DESC')
                ->paginate($limit);
            foreach($results as $purchaseOrderReturn){

                $purchaseOrderReturn['status']=$this->getPurchaseOrderReturnStatus($purchaseOrderReturn['id']);
            }

            $resp['success']     = true;
            //$resp['data']        = $results;
            $resp['data']        = (new PurchaseOrderReturnTransformer())->transformCollection($results);
            $resp['pagination']  = (new Paginations)->results($results);
            $operationLog = array("userId"           => Auth::User()->id,
                "operation"        => 'Show All',
                "referenceTableId" => $results,
                "operationStatus"  => 'success',
                "customMessage"    => 'user id '.Auth::User()->id.' show all order return details from table order_return successfully');
            (new OperationLog)->add($operationLog);
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Order Return Details Not Available";
            $resp['ex']      = $ex->getMessage();
        }

        return $resp;
    }

    //This function is use for show purchase order return details by purchase order return Id.
    public function getById($id){
        $resp = array();
        try
        {
            $result   = PurchaseOrderReturn::find($id);
            $resp['data']=(new PurchaseOrderReturnTransformer())->transform($result);

            $resp['success']     = true;
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Order Return Details Not Available";
            $resp['ex']      = $ex->getMessage();
        }
        return $resp;
    }

    //This function display return order status.
    public function getPurchaseOrderReturnStatus($purchaseOrderId){
        return 'inProgress';
    }

    //This function is use to display purchase order return details for search any keyword.
    public function autocomplete($data)
    {
        $resp = array();
        try
        {
            $limit           = Input::get('limit')?:10;
            $resp['success'] = true;
            $dataOrderReturn = PurchaseOrderReturn::where('id', 'like', '%'.$data.'%')
                ->orWhere('invoice_id', 'like', '%'.$data.'%')
//                ->orWhere('quantity', 'like', '%'.$data.'%')
                ->paginate($limit);
            $resp['data']       = (new PurchaseOrderReturnTransformer())->transformCollection($dataOrderReturn);
            $resp['pagination'] = (new Paginations)->results($dataOrderReturn);
            $resp['msg']        = "Order Return Details Information";
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Order Return Details Not Available";
            $resp['ex']      = $ex->getMessage();
        }
        return $resp;
    }

    //This function is use to get all purchase order return details.
    //This function is use for standard reports.
        public function getDataForReports($fromDate, $toDate, $myRequestObject)
    {
        $branchId  = Session::get('branch_id');
        $orderReturnData = array();
        $results   = PurchaseOrderReturn::join('purchase_order_return_items','purchase_order_return.id','=','purchase_order_return_items.purchase_order_return_id')
            ->join('invoice','invoice.id','=','purchase_order_return.invoice_id')
            ->where('invoice.branch_id',$branchId)
            ->select('purchase_order_return.*')
            ->where('purchase_order_return.created_at','>=',$fromDate)
            ->where('purchase_order_return.created_at','<=',$toDate)
            ->groupBy('purchase_order_return.id')
            ->orderBy('purchase_order_return.id', 'DESC')
        ->get();

        foreach($results as $purchaseOrderReturn){

            $purchaseOrderReturn['status']=$this->getPurchaseOrderReturnStatus($purchaseOrderReturn['id']);
        }
        $returnOrders=(new PurchaseOrderReturnTransformer())->transformCollection($results);
        $i = 0;
        foreach($returnOrders as $row)
        {
            $i++;
            array_push($orderReturnData, array($i,$row['id'], $row['invoiceId'], $row['noOfItems'], $row['status'], $row['totalAmount'],$row['refundOrLossAmt'],$row['createdAt']->format('d-m-Y'), $row['updatedAt']->format('d-m-Y')));
        }
        return $orderReturnData;
    }



}
<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Response;
use Input;
use App\MediPlus\Pagination\Paginations;
use App\MediPlus\Transformers\DistributorTransformer as DistributorTransformer;
use App\OperationLog;
use App\User;
use App\PurchaseOrder;
use Session;

class Distributor extends Model
{
    use SoftDeletes;
    protected $table = 'distributor';
    
    public function PurchaseOrder() 
    {
        return $this->hasMany('App\PurchaseOrder');
    }
    
    //This function is use to assign heading for standard reports.
    public $reportColumnTitle = array('Sr.No', 'Agency Number', 'Owner Name', 'Authorised Distributor', 'Tin Number', 'Cst Number', 'Dl Number', 'Credit Period', 'Contact Number');
    
    //This function is use to assign heading for standard reports.
    public function reportheading()
    {       
        return array('Sr.No', 'Created Date', 'Updated Date', 'Agency Number', 'Owner Name', 'Authorised Distributor', 'Tin Number', 'Cst Number', 'Dl Number', 'Credit Period', 'Contact Number', 'Deleted Date');
    }
    
    //This function is use for add distributor details.    
    public function add()
    {
        $resp = array();
        try
        {
             $data = Input::all();
             $data['branchId']  = Session::get('branch_id');
             $distributor       = (new DistributorTransformer)->reverseTransform($data);
             $distributorId     = Distributor::insertGetId($distributor);
             $resp['success']   = true;
             $resp['data']      = (new DistributorTransformer)->transform(Distributor::find($distributorId));
             $operationLog = array("userId"           => Auth::User()->id,
                                   "operation"        => 'insert',
                                   "referenceTableId" => $distributorId,
                                   "operationStatus"  => 'success',
                                   "customMessage"    => 'user id '.Auth::User()->id.' inserted in table distributor with id '.$distributorId.' successfully');
            (new OperationLog)->add($operationLog);
       }
       catch(\Exception $e)
       {
             $resp['success']   = false;
             $resp['msg']       = $e->getMessage().' '."Distributor Details Added Failure";
             $resp['ex']        = $e->getMessage();
             $operationLog = array("userId"           => Auth::User()->id,
                                   "operation"        => 'insert',
                                   "referenceTableId" => 0,
                                   "operationStatus"  => 'fail',
                                   "customMessage"    => 'user id '.Auth::User()->id.' inserted in table distributor failure');
            (new OperationLog)->add($operationLog);
       }
       return $resp;
    }
    
    //This function is use for display distributor details.
    public function getAll()
    {
        $resp = array();
        try
        {
             $limit   = Input::get('limit')?:10;
             $branchId = Session::get('branch_id');
             $results = Distributor::where('branch_id', '=', $branchId)->orderBy('id', 'DESC')->paginate($limit);
             $distributorTransformer    = new DistributorTransformer;
             $resp['success']           = true;
             $resp['data']              = $distributorTransformer->transformCollection($results);
             $resp['pagination']        = (new Paginations)->results($results);
        }
        catch(\Exception $e)
        {
             $resp['success'] = false;
             $resp['msg']     = $e->getMessage().' '."Distributor Details Not Available";
             $resp['ex']      = $e->getMessage();
        }
             $operationLog = array("userId"           => Auth::User()->id,
                                   "operation"        => 'Show All',
                                   "referenceTableId" => $results,
                                   "operationStatus"  => 'success',
                                   "customMessage"    => 'user id '.Auth::User()->id.' show all distributor details from table distributor successfully');
            (new OperationLog)->add($operationLog);
        return $resp;    
    }
    
    //This function is use for show distributor details by distributor Id.
    public function getById($id)
    { 
        $resp = array();
        try
        {
            $distributor                = Distributor::findOrFail($id);
            $distributorTransformer     = new DistributorTransformer;
            $resp['success']            = true;
            $resp['data']               = $distributorTransformer->transform($distributor);
            $operationLog=array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'success',
                                "customMessage"    => 'user id '.Auth::User()->id.' view distributor details in table distributor with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Distributor Detail Not Available";
            $resp['ex']      = $e->getMessage();
            $operationLog=array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'fail',
                                "customMessage"    => 'user id '.Auth::User()->id.' view distributor details in table distributor with id '.$id.' failure');
            (new OperationLog)->add($operationLog);
        }     
        return $resp;
    }
    
    //This function is use to update distributor details by Id.
    public function updateById($id)
    {
        $resp = array();
        try
        {
            $distributor     = (new DistributorTransformer)->reverseTransform(Input::all());
            Distributor::where('id',$id)->update($distributor);
            Distributor::findOrFail($id)->save();
            $resp['success'] = true;
            $resp['data']    = (new DistributorTransformer)->transform(Distributor::findOrFail($id));
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Distributor Details Not Available";
            $resp['ex']      = $e->getMessage();
        }
        return $resp;
    }
    
    //This function is use to delete distributor details by Id.
    public function deleteById($id)
    {
        $resp = array();
        try
        {
            $distributor = Distributor::findOrFail($id);
            $distributor->delete();
            $limit   = Input::get('limit')?:10;
            $results = $distributor::paginate($limit);
            $distributorTransformer = new DistributorTransformer;
            $resp['success']        = true;
            $resp['data']           = $distributorTransformer->transform($distributor);
            $resp['pagination']     = (new Paginations)->results($results);      
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."Distributor Details Not Available";  
            $resp['ex']      = $e->getMessage();
        }
       return $resp;
    }
    
    //This function is use to display all distributor name (display deleted distributor name also).
    public function getDistributorName($id)
    {
       $distributor = Distributor::withTrashed()->find($id);
        
       if($distributor)
            return $distributor->owner_name;
       else 
            return 'Distributor Deleted';
    }
    
    //This function is use to search distributor details for any keyword.
    public function autocomplete($data)
    {
       $resp = array();
       try
       {
            $limit   = Input::get('limit')?:10;
            $branchId = Session::get('branch_id');
            $resp['success']    = true;
            $dataDistributor = Distributor::where('agency_number', 'like', '%'.$data.'%')
                                            ->orWhere('agency_name', 'like', '%'.$data.'%')
                                            ->orWhere('owner_name', 'like', '%'.$data.'%')
                                            ->orWhere('authorised_distributor', 'like', '%'.$data.'%')
                                            ->orWhere('contact_number', 'like', '%'.$data.'%')
                                            ->paginate($limit);
            $resp['data'] = (new DistributorTransformer)->transformCollection($dataDistributor);
            $resp['pagination']=(new Paginations)->results($dataDistributor);
            $resp['msg']="Distributor Details Information";
       }
       catch(\Exception $e)
       {
            $resp['success']     = false;
            $resp['msg']         = $e->getMessage().' '."Product Details Not Available";
            $resp['ex']          = $e->getMessage();
       }
       return $resp;
    }
    
    //This function is use to search distributor details using distributor name.
    function autocompleteByName($data)
    {
       $resp = array();
       try
       {
            $limit           = Input::get('limit')?:10;
            $resp['success'] = true;
            $dataDistributor = Distributor::where('agency_number', 'like', $data.'%')
                ->orWhere('owner_name', 'like', $data.'%')
                ->orWhere('agency_name', 'like', $data.'%')
                ->orWhere('contact_number', 'like', $data.'%')
                ->orderBy('agency_name','asc')
                ->take($limit)
                ->get();  
            $resp['data'] =(new DistributorTransformer)->transformCollectionPlain($dataDistributor);
            $resp['msg']="Distributor Details Information";
       }
       catch(\Exception $e)
       {
            $resp['success']     = false;
            $resp['msg']         = $e->getMessage().' '."Product Details Not Available";  
            $resp['ex']          = $e->getMessage();   
       }
       return $resp;
    }
    
    //This function is use to get all details in-beetween of two dates.
    public function history()
    {
        $resp         = array();
        $limit        = Input::get('limit')?:10;
        $fromDate     = Input::get('fromDate');
        $toDate       = Input::get('toDate');
        $from         = date("Y-m-d H:i:s", strtotime($fromDate));
        $to           = date("Y-m-d H:i:s", strtotime($toDate)); 
        $distributors = Distributor::whereBetween('created_at',array($from, $to))->paginate($limit);
        if($fromDate!='' && $toDate!='')
        {
            $distributorTransformer = new DistributorTransformer;
            $resp['success']        = "true";
            $resp['msg']            = "Distributor Details Available From search date";
            $resp['data']           = $distributorTransformer->transformCollection($distributors); 
            $resp['pagination']     = (new Paginations)->results($distributors);
        }
        else
        {
            $resp['success'] = "false";
            $resp['msg']     = "Please Select date";
        }
        return $resp;
    } 
    
    //This function is use to get all distributor details.
    //This function is use for standard reports.
    public function getDataForReports($fromDate, $toDate, $myRequestObject)
    {
        $table           = Distributor::whereBetween('created_at',array($fromDate, $toDate))->get();
        $reportName      = new Product;
        $distributorData = array();
        foreach($table as $row) 
        {            
            array_push($distributorData, array($row['id'], $row['agency_number'], $row['owner_name'], $row['authorised_distributor'], $row['tin_number'], $row['cst_number'], $row['dl_number'], $row['credit_period'], $row['contact_number']));
        }
        return $distributorData;
    }
}
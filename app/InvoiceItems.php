<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceItems extends Model
{
    protected $table = 'invoice_items';

    public function invoice()
    {
    return $this->belongsTo('App\Invoice','id');
    }



    public function purchaseOrderReturnItems()
    {
        return $this->has('App\PurchaseOrderReturnItems','invoice_item_id','id');
    }

}


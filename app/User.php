<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\MediPlus\Transformers\UserTransformer as UserTransformer;
use App\MediPlus\Transformers\UserBranchDetailsTransformer as UserBranchDetailsTransformer;
use App\MediPlus\Transformers\BranchTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\MediPlus\Pagination\Paginations;
use Input;
use DB;
use Auth;
use App\UserBranchDetails;
use App\Branch;
use App\OperationLog;
use App\PurchaseOrder;
use Hash;
use Session;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;
    use SoftDeletes;
    
    //This function is use to assign heading for standard reports.
    public $reportColumnTitle = array('Sr.No', 'First Name', 'Middle Name', 'Last Name', 'Email Id', 'City', 'District', 'State', 'Street Address', 'Mobile Number', 'Pin Code', 'Aadhar Number', 'Pan Number', 'Role', 'User Name', 'Branch Name');
    
    protected $table = 'users';
        
    public function PurchaseOrder() 
    {
        return $this->hasMany('App\PurchaseOrder');
    }
    
    protected $fillable = ['name', 'email', 'password'];

    //This function is use for display user name by user Id.
    public function getUserName($userId)
    {
        $user=User::find($userId);
        
        if($user)
            return $user->user_name;
        else 
            return 'User Deleted';   
    }
    
    public function userBranchDetails()
    {
        return $this->hasMany('App\UserBranchDetails','user_id','id');
    }
    
    //This function is use for show all user details.
    public function getAll()
    { 
        $arr = array();
        try
        {
            $userTransformer = new UserTransformer;
            $userBranchDetailsTransformer = new UserBranchDetailsTransformer;
            $limit     = Input::get('limit')?:10;
            $branchId  = Session::get('branch_id');
            $results   = User::whereIn('id',UserBranchDetails::where('branch_id','=',$branchId)->lists('user_id'))->paginate($limit);
            $user = $userTransformer->transformCollection($results, false);
            $resp['success']    = true;
            $resp['data']       = $user;
            $resp['pagination'] = (new Paginations)->results($results);    
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."User Details Not Available";
            $resp['ex']      = $e->getMessage();
        }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all user details from table users successfully');
            (new OperationLog)->add($operationLog);
        return $resp;
    }
    
    //This function is use for show user details by user Id.
    public function getById($id)
    {
       $resp = array();
       try
       {
           $branchId       = Session::get('branch_id');
           $user           = User::whereIn('id',UserBranchDetails::where('branch_id','=',$branchId)
                                                                   ->lists('user_id'))->findOrFail($id);
           $userTransformer = new UserTransformer;
           $userBranchDetailsTransformer = new UserBranchDetailsTransformer;
           $resp['success'] = true;
           $resp['data']    = $userTransformer->transform($user);
           $operationLog=array("userId"           => Auth::User()->id,
                               "operation"        => 'View',
                               "referenceTableId" => $id,
                               "operationStatus"  => 'success',
                               "customMessage"    => 'user id '.Auth::User()->id.' view user details in table users with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
       }
       catch(\Exception $e)
       {
           $resp['success'] = false;
           $resp['msg']     = $e->getMessage().' '."User Detail Not Available";
           $resp['ex']      = $e->getMessage();
           $operationLog=array("userId"            => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'fail',
                                "customMessage"    => 'user id '.Auth::User()->id.' view user details in table users with id '.$id.' failure');
            (new OperationLog)->add($operationLog);
       }
       return $resp; 
    }
    
    //This function is use for display user branch name by user Id.
    public function userBranchData($id)
    {
        $arr = array();
        $userBranch = User::find($id)->userBranchDetails;
        foreach($userBranch as $userBranchData)
        {
            $arr+=array($userBranchData->id=>array('branchID'=>$userBranchData->branch_id,
                    'branchName'=>(new Branch)->getBranchName($userBranchData->branch_id)));
        }
        return $arr;      
    }
    
    //This function is use to add user details in user and user_branch_details table.
    public function add()
    {               
        $resp = array();
        try
        {  
            $user             = (Input::all());
            $user["active"]   = 1;
            $user["password"] = "secret";
            $userData         = (new UserTransformer)->reverseTransform($user,(new User));
            (new UserBranchDetailsTransformer)->reverseTransform($user['branchId'],(new UserBranchDetails),$userData->id);
            $resp['success']  = true;
            $resp['data']     = (new UserTransformer)->transform(User::find($userData->id));
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => User::find($userData->id),
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table users with id '.User::find($userData->id).' successfully');
            (new OperationLog)->add($operationLog);
        }
        catch(\Exception $e)
        {
            $resp['success']  = false;
            $resp['msg']      = $e->getMessage().' '."User Details Added Failure";
            $resp['ex']       = $e->getMessage();
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => 0,
                                  "operationStatus"  => 'fail',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table users failure');
            (new OperationLog)->add($operationLog);
        }
        return $resp;      
    }
    
    //This function is use to update user details by selected user Id.
    public function updateById($id)
    {
       $resp = array();
       try
       {
           $user         = (new UserTransformer)->reverseTransform(Input::all());
           $resp['success'] = true;
           $resp['user']= User::where('id',$id)->update($user);
           $user['id']=$id;
           $resp['data']    = (new UserTransformer)->transform($user,false); 
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."User Details Not Available";
            $resp['ex']      = $e->getMessage();
       }
       return $resp;   
    }

    //This function is use to delete user details by selected user Id.
    public function deleteById($id)
    {
        $resp = array();
        try
        {
            $user = User::findOrFail($id);
            $user->delete();
            $limit   = Input::get('limit')?:10;
            $results = $user::paginate($limit);
            $userTransformer     = new UserTransformer;
            $resp['success']     = true;
            $resp['data']        = $userTransformer->transform($user); 
            $resp['pagination']  = (new Paginations)->results($results);      
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."User Details Not Available";  
            $resp['ex']      = $e->getMessage();
        }       
        return $resp;
    }
    
    //This function is use to get user details by search only user role and user name.
    public function search($search_type,$data)
    {
        $users = Users::where('role', '=',$search_type)
                        ->where('user_name', 'like', '%'.$data.'%')
                        ->get();
        $userTransformer = new UserTransformer;
        return $userTransformer->transformCollection($users);
    }
    
    //This function is use to get all user details in-beetween of two dates.
    public function history()
    {
        $resp     = array();
        $limit    = Input::get('limit')?:10;
        $fromDate = Input::get('fromDate');
        $toDate   = Input::get('toDate');
        $from     = date("Y-m-d H:i:s", strtotime($fromDate));
        $to       = date("Y-m-d H:i:s", strtotime($toDate)); 
        $users    = User::whereBetween('created_at',array($from, $to))->paginate($limit);
        if($fromDate!='' && $toDate!='')
        {
            $userTransformer = new UserTransformer;
            $resp['success'] = "true";
            $resp['msg']     = "Product Details Available From search date";
            $resp['data']    = $userTransformer->transformCollection($users); 
            $resp['pagination'] = (new Paginations)->results($users);
        }
        else
        {
            $resp['success'] = "false";
            $resp['msg']     = "Please Select date";
        }
        return $resp;
    }
    
    //This function is use to get branch id for selected user (because one user assign to multiple branches).
    public function userBranch($id)
    {        
        $branchId = UserBranchDetails::where('user_id','=',$id)->lists('branch_id');
        $branch   = Branch::whereIn('id',UserBranchDetails::where('user_id','=',$id)->lists('branch_id'))->get();
        return (new BranchTransformer)->transformCollection($branch,false);
    }
    
    //This function is use to display user details for search any keyword.
    public function autocomplete($data)
    {
       $resp=array();     
       try
       {
            $limit    = Input::get('limit')?:10;
            $results  = $this::paginate($limit);
            $dataUser = User::where('first_name', 'like', '%'.$data.'%')
                              ->orWhere('middle_name', 'like', '%'.$data.'%')
                              ->orWhere('last_name', 'like', '%'.$data.'%')
                              ->orWhere('email_id', 'like', '%'.$data.'%')
                              ->orWhere('city', 'like', '%'.$data.'%')
                              ->orWhere('mobile_number', 'like', '%'.$data.'%')
                              ->orWhere('pin_code', 'like', '%'.$data.'%')
                              ->orWhere('aadhar_number', 'like', '%'.$data.'%')
                              ->orWhere('pan_number', 'like', '%'.$data.'%')
                              ->orWhere('user_name', 'like', '%'.$data.'%')
                              ->orWhere('role', 'like', '%'.$data.'%')
                              ->orWhere('state', 'like', '%'.$data.'%')
                              ->paginate($limit);
            $resp['success']     = true;
            $resp['data']        = (new UserTransformer)->transformCollection($dataUser);
            $resp['pagination']  = (new Paginations)->results($dataUser);
            $resp['msg']         = "User Detail Information";
       }
       catch(\Exception $e)
       {
            $resp['success']     = false;
            $resp['msg']         = $e->getMessage().' '."User Details Not Available";  
            $resp['ex']          = $e->getMessage();
       }
       return $resp;
    }
    
    //This function is use to display user details for search user_name only.
    public function autocompleteByName($data)
    {
       $resp = array();
       try
       {
            $limit           = Input::get('limit')?:10;
            $dataUser        = User::where('user_name', 'like', $data.'%')
                                     ->orderBy('user_name','asc')
                                     ->take($limit)->get();
            $resp['success'] = true;
            $resp['data']    = (new UserTransformer)->transformCollectionPlain($dataUser);
            $resp['msg']     = "User Details Information";
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."User Details Not Available";  
            $resp['ex']      = $e->getMessage();
       }
       return $resp;
    }
    
    //This function is use to display user details for selected branch Id.
    public function getByBranchId($branchId)
    { 
        $arr = array();
        try
        {
            $userTransformer = new UserTransformer;
            $userBranchDetailsTransformer = new UserBranchDetailsTransformer;
            $limit   = Input::get('limit')?:10;
            $results = User::whereIn('id',UserBranchDetails::where('branch_id','=',$branchId)->lists('user_id'))->paginate($limit);
            $user    = $userTransformer->transformCollection($results, false);
            $resp['success']    = true;
            $resp['data']       = $user;
            $resp['pagination'] = (new Paginations)->results($results);    
            return $resp;
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = $e->getMessage().' '."User Details Not Available";
            $resp['ex']      = $e->getMessage();
        }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all user details from table users successfully');
            (new OperationLog)->add($operationLog);
        return $resp;
    }
    
    //This function is use to get all user details.
    //This function is use for standard reports.
    public function getDataForReports($fromDate, $toDate, $myRequestObject)
    {
        $table = User::join('user_branch_details', 'users.id', '=', 'user_branch_details.user_id')
                       ->join('branch', 'user_branch_details.branch_id', '=', 'branch.id')
                       ->select('users.*', 'branch.branch_name')
                       ->whereBetween('users.created_at',array($fromDate, $toDate))->get();
        $reportName  = new User;
        $userData = array();
        $i = 0;
        foreach($table as $row) 
        {   
            $i++;
            array_push($userData, array($i, $row['first_name'], $row['middle_name'], $row['last_name'], $row['email_id'], $row['city'],                                           $row['district'], $row['state'], $row['street_address'], $row['mobile_number'], $row['pin_code'],                                         $row['aadhar_number'], $row['pan_number'], $row['role'], $row['user_name'], $row['branch_name']));
        }
        return $userData;
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\UserBranchDetails;
use App\MediPlus\Transformers\UserTransformer as UserTransformer;
use App\MediPlus\Transformers\UserBranchDetailsTransformer as UserBranchDetailsTransformer;
use App\Branch;
use App\OperationLog;

class UserBranchDetails extends Model
{
    protected $table = 'user_branch_details';
    
    public function user()
    {
    return $this->belongsTo('App\User','id');
    }   
}
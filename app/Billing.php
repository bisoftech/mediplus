<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MediPlus\Transformers\BillingTransformer as BillingTransformer;
use App\MediPlus\Transformers\BillingItemsTransformer as BillingItemsTransformer;
use App\MediPlus\Pagination\Paginations;
use Input;
use DB;
use App\Product;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Billing;
use App\BillingItems;
use App\Customer;
use App\OperationLog;
use App\User;
use Auth;
use App\Stock;
use Session;

class Billing extends Model
{
    use SoftDeletes;
    protected $table = 'billing';
    
    //This function is use to assign heading for standard reports. 
    public $reportColumnTitle = array('Bill No', 'Created Date', 'Updated Date', 'Employee Name', 'Customer Name', 'Product Name', 'Product Dosage', 'Product Location', 'Quantity', 'Total Amount', 'Paid Amount', 'Payment Type', 'Discount', 'Status');
    
    public function billingItems()
    {
        return $this->hasMany('App\BillingItems', 'billing_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer','id');
    }
    
    public function user() 
    {
        return $this->belongsTo('App\User');
    }
    
    //This function is use to get all billing details for selected branch
    public function getAll()
    {
        $resp = array();
        try 
        {
            $limit     = Input::get('limit')?:10;
            $branchId  = Session::get('branch_id');
            $results = Billing::where('branch_id', '=' , $branchId)->orderBy('id', 'DESC')->paginate($limit);
            $resp['success']    = true;
            $resp['data']       = (new BillingTransformer)->transformCollection($results);
            $resp['pagination'] = (new Paginations)->results($results);
        }
        catch(\Exception $e)
        {
            $resp['success'] = $e;
            $resp['msg']     = "Billing Details Not Available";
        }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all billing details from table billing successfully');
            (new OperationLog)->add($operationLog);
        return $resp;
    }

    //This function is use to get particular billing details by billing id for selected branch
    public function getById($id)
    {   
        $resp = array();
        try
        {
            $branchId = Session::get('branch_id');
            $billing   = Billing::where('branch_id', '=' , $branchId)->findOrFail($id);
            $billingTransformer      = new BillingTransformer;
            $billingItemsTransformer = new BillingItemsTransformer;
            $resp['success']         = true;
            $resp['data']            = $billingTransformer->transform($billing, true);
            
            $operationLog=array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'success',
                                "customMessage"    => 'user id '.Auth::User()->id.' view billing details in table billing with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
            
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = "Bill Detail Not Available";
            
            $operationLog=array("userId"           => Auth::User()->id,
                                "operation"        => 'View',
                                "referenceTableId" => $id,
                                "operationStatus"  => 'fail',
                                "customMessage"    => 'user id '.Auth::User()->id.' view billing details in table billing with id '.$id.' failure');
            (new OperationLog)->add($operationLog);
        }
            
        return $resp; 
    }

    //This function is use to check item is exist or not
    public function checkIfItemExistInArray($itemArray,$item){
      foreach ($itemArray as $itemKey => $itemValue) {
        if($item['stockId']==$itemValue['stockId'] && $item['productId']==$itemValue['productId'])
          return $itemKey;
      }
      return -1;
    }

    //This function is use for add billing details in billing table.
    public function add()
    {
        $resp = array();
        try
        {
            $billing     = (Input::all());
            $billing["branchId"] = Session::get('branch_id');
            $invalidQunatity=false;
            $itemArray=array();
            $bi = Input::get('billingItems');
            foreach($bi as $value) 
            {   
              $stockId=$value['stockId'];
              $productId = $value['productId'];
              $itemExistsKey=$this->checkIfItemExistInArray($itemArray,$value);
              if($itemExistsKey==-1){
                $availableQuantity = Stock::select('quantity')->where('id', '=', $stockId)->where('product_id', '=', $productId)->get()[0]->quantity;
                 if($availableQuantity < $value['quantity'])
                 {
                     $invalidQunatity=true;
                 } else {
                   $currentItem=['stockId'=>$stockId,'productId'=>$productId,'availableQuantity'=>$availableQuantity-$value['quantity']];
                   array_push($itemArray,$currentItem);
                }
              } else {
                  $availableQuantity=$itemArray[$itemExistsKey]['availableQuantity'];
                  if($availableQuantity < $value['quantity'])
                 {
                     $invalidQunatity=true;
                 } else {
                   $itemArray[$itemExistsKey]['availableQuantity']=$availableQuantity-$value['quantity'];
                }
              }
            }

            if(!$invalidQunatity){
            $billingData = (new BillingTransformer)->reverseTransform($billing,(new Billing));
        
      
            foreach($bi as $value) 
            {   
             
                $stockId=$value['stockId'];
                $billingItemsData = (new BillingItemsTransformer)->reverseTransform($value,(new BillingItems),$billingData->id);
                DB::table('stock')->where('id', '=', $stockId)->decrement('quantity',$value['quantity']);
              
            }
            $resp['success'] = true;
            $resp['data']    = (new BillingTransformer)->transform(Billing::find($billingData->id)); 

            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => $billingData->id,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table billing with id '.$billingData->id.' successfully');
            (new OperationLog)->add($operationLog);
          } else {
              $resp['success'] = false;
              $resp['msg']    = 'Sorry! Your enetered quantity is more than available quantity'; 
          }
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = "Bill Details Added Failure".$e;
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => 0,
                                  "operationStatus"  => 'fail',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table billing failure');
            (new OperationLog)->add($operationLog);
        }
  
        return $resp;
    }
    
    //This function is use for delete billing details by using billing Id.
    public function deleteById($id)
    {
        $resp=array();
        try
        {
            $billing = Billing::findOrFail($id);
            $billing->delete();
            $limit   = Input::get('limit')?:10;
            $results = $billing::paginate($limit);
            $billingTransformer = new BillingTransformer;
            $resp['success']    = true;
            $resp['data']       = $billingTransformer->transform($billing);
            $resp['pagination'] = (new Paginations)->results($results);      
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = "Billing Details Not Available";  
        }
       return $resp;
    }
   
    //This function is use for get billing details for selected keywoords.
    public function autocomplete($data)
    {
       $resp=array();     
       try
       {
           $branchId  = Session::get('branch_id');
           $limit     = Input::get('limit')?:10;
           $billingData = Billing::whereIn('user_id',User::Where('user_name', 'like', '%'.$data.'%')->lists('id'))
                                   ->orWhereIn('customer_id',Customer::Where('first_name', 'like', '%'.$data.'%')
                                                                    ->orWhere('middle_name', 'like', '%'.$data.'%')
                                                                    ->orWhere('last_name', 'like', '%'.$data.'%')->lists('id'))
                                   ->orWhere('amount_paid', 'like', '%'.$data.'%')
                                   ->orWhere('status', 'like', '%'.$data.'%')
                                   ->orWhere('payment_type', 'like', '%'.$data.'%')
                                   ->whereIn('user_id',UserBranchDetails::where('branch_id','=',$branchId)->lists('user_id'))
                                   ->paginate($limit);
           $resp['success']    = true;
           $resp['data']       = (new BillingTransformer)->transformCollection($billingData);
           $resp['pagination'] = (new Paginations)->results($billingData);  
           $resp['msg']        = "Billing Details Information";
       }
       catch(\Exception $e)
       {
           $resp['success']     = false;
           $resp['msg']         = "Billing Details Not Available";  
       }
       return $resp;   
    }
    
    public function paymentType($search_type,$data)
    {    
        return $this->search('payment_type',$data);
    }

    //This function is use to get all billing details in-beetween of two dates.
    public function history()
    {
       $resp = array();
       try
       {
           $limit    = Input::get('limit')?:10;
           $fromDate = Input::get('fromDate');
           $toDate   = Input::get('toDate');
           $from     = date("Y-m-d H:i:s", strtotime($fromDate));
           $to       = date("Y-m-d H:i:s", strtotime($toDate)); 
           $productData = Product::whereBetween('created_at',array($from, $to))->paginate($limit);          
           foreach($productData as $value)
            {
                foreach(BillingItems::where('product_id', '=', $value->id)->whereBetween('created_at',array($from, $to))->lists('billing_id') as $billData)
                {
                    $resp['data'][] = (new BillingTransformer)->transform(Billing::find($billData));
                    $resp['pagination'] = (new Paginations)->results($productData);
                }
            }
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = "Please Select Date";
       }
       return $resp;
    }
    
    //This function is use to get billing details by using billing Id for selected branch.
    public function getByBranchId($branchId)
    {
        $resp = array();
        try 
        {
            $limit   = Input::get('limit')?:10;
            $results = Billing::whereIn('user_id',UserBranchDetails::where('branch_id','=',$branchId)->lists('user_id'))->paginate($limit);
            $resp['success']    = true;
            $resp['data']       = (new BillingTransformer)->transformCollection($results);
            $resp['pagination'] = (new Paginations)->results($results);
        }
        catch(\Exception $e)
        {
            $resp['success'] = $e;
            $resp['msg']     = "Billing Details Not Available";
        }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all billing details from table billing successfully');
            (new OperationLog)->add($operationLog);
        return $resp;
    }
    
    //This function is use to get all billing related details
    //This function is use for standard reports.
    public function getDataForReports($fromDate, $toDate, $myRequestObject)
    {   
        $branchId  = Session::get('branch_id');
        $table = Billing::join('users', 'billing.user_id', '=', 'users.id')
                          ->join('customer', 'billing.customer_id', '=', 'customer.id')
                          ->join('billing_items', 'billing.id', '=', 'billing_items.billing_id')
                          ->join('product', 'billing_items.product_id', '=', 'product.id')
                          ->join('user_branch_details', 'billing.user_id' , '=', 'user_branch_details.user_id')
                          ->join('branch', 'branch.id' , '=', 'user_branch_details.branch_id')
                          ->select('billing.*', 'users.user_name', 'customer.first_name', 'product.name', 'product.dosage', 'product.product_location', 'billing_items.quantity')
                          ->orderBy('id', 'asc')
                          ->where('user_branch_details.branch_id','=',$branchId)
                          ->whereBetween('billing.created_at',array($fromDate, $toDate))->get();
        $billingData = array();
        $i = 0;
        foreach($table as $row) 
        {      
            $i++;
            array_push($billingData, array($i, $row['created_at']->format('d-m-Y'), $row['updated_at']->format('d-m-Y'), $row['user_name'], $row['first_name'], $row['name'], $row['dosage'], $row['product_location'], $row['quantity'], $row['total_amount'], $row['amount_paid'], $row['payment_type'], 0, $row['status']));
        }
        return $billingData;
    }
    
    //This function is use to get all billing details for selected branch.
    public function getCustOnCreditDetailsById($id)
    {   
        $resp = array();
        try
        {
            $customerDetails  = Billing::where('customer_id', '=', $id)->where('payment_type','=','onCredit')->get();
            $billingTransformer      = new BillingTransformer;
            $resp['success']         = true;
            $resp['data']            = $customerDetails; 
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = "Bill Detail Not Available";
        }
        return $resp; 
    }
    
    //This function is use to get all billing details for selected branch.
    public function getCustUnpaidAmtById($id)
    {   
        $resp = array();
        try
        {
            $customerDetails  = Billing::select('id','total_amount', 'amount_paid')
                                        ->whereIn('status',['unpaid','partial_paid'])
                                        ->where('customer_id', '=', $id)
                                        ->get();
            $totalAmount    = Billing::whereIn('status',['unpaid','partial_paid'])->where('customer_id', '=', $id)->sum('total_amount');
            $amountPaid    = Billing::whereIn('status',['unpaid','partial_paid'])->where('customer_id', '=', $id)->sum('amount_paid');
            $unpaidAmount = $totalAmount - $amountPaid;
            $resp['success']         = true;
            $resp['data']            = $customerDetails; 
            $resp['total']=array("totalAmount"=>$totalAmount,"amountPaid"=>$amountPaid,"unpaidAmount"=>$unpaidAmount);
        }
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = "Amount Not Available";
        }
        return $resp; 
    }

    //This function is use to get unpaid and partially paid billing details for particular customer.
    public function nextAndPrevoius($id,$unpaid)
    {   
        $resp = array();
        try
        {
            $branchId = Session::get('branch_id');
            $billing   = Billing::where('branch_id', '=' , $branchId)->findOrFail($id);
            $billingTransformer      = new BillingTransformer;
            $billingItemsTransformer = new BillingItemsTransformer;
            $custId   = Billing::select('customer_id')->where('branch_id', '=' , $branchId)->where('id','=',$id)->get();
            if((sizeof($custId) > 0))
            {
              $totalAmount  = Billing::where('customer_id', '=', $custId[0]->customer_id)->sum('total_amount');
              $amountPaid   = Billing::where('customer_id', '=', $custId[0]->customer_id)->sum('amount_paid');
              $unpaidAmount = $totalAmount - $amountPaid;
            
            if($unpaid=='true')
            {
              $previousBillDetails  = Billing::whereIn('status',['unpaid','partial_paid'])->where('id','<',$id)->where('customer_id', '=', $custId[0]->customer_id)->where('branch_id', '=', $branchId)->limit(1)->orderBy('id', 'desc')->lists('id');

              $nextBillDetails  = Billing::whereIn('status',['unpaid','partial_paid'])->where('id','>',$id)->where('customer_id', '=', $custId[0]->customer_id)->where('branch_id', '=', $branchId)->limit(1)->lists('id');
            }
            else
            {
              $previousBillDetails  = Billing::where('id','<',$id)->where('customer_id', '=', $custId[0]->customer_id)->where('branch_id', '=', $branchId)->limit(1)->orderBy('id', 'desc')->lists('id');

              $nextBillDetails  = Billing::where('id','>',$id)->where('customer_id', '=', $custId[0]->customer_id)->where('branch_id', '=', $branchId)->limit(1)->lists('id');
            } 
          }
            $resp['success']   = true;
            $resp['data']      = $billingTransformer->transform($billing, true); 
            $resp['total']     = array("totalAmount"=>$totalAmount,"amountPaid"=>$amountPaid,"unpaidAmount"=>$unpaidAmount);
            $resp['previousBillDetails'] = $previousBillDetails;
            $resp['nextBillDetails'] = $nextBillDetails;
        } 
        catch(\Exception $e)
        {
            $resp['success'] = false;
            $resp['msg']     = "Bill Detail Not Available";
        }          
        return $resp; 
    }
}
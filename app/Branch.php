<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MediPlus\Transformers\BranchTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\MediPlus\Pagination\Paginations;
use Input;
use App\OperationLog;
use App\User;
use Auth;
use App\PurchaseOrder;
use Session;

class Branch extends Model
{
    protected $table = 'branch';
    use SoftDeletes;
    
    public function PurchaseOrder() 
    {
        return $this->hasMany('App\PurchaseOrder');
    }
   
    //This function is use to assign heading for standard reports.  
    public $reportColumnTitle = array('Sr.No', 'Branch Name', 'Registration Number', 'Registration Validity', 'Shop Act Number', 'Shop Act Validity', 'VAT Number', 'CST Number', 'LBT Number', 'Pan Card', 'District', 'City', 'Pin Code', 'State', 'Address', 'User Drug License Number', 'Pharmacist Id', 'Tin Number', 'Contact Number', 'Fax Number', 'Alternate Contact', 'Shop Name');
    
    //This function is use for display branch name.
    public function getBranchName($id)
    {
        $branch = Branch::find($id);
        
        if($branch)
            return $branch->branch_name;
        else 
            return 'Branch Deleted';
    }
    
    //This function is use to get all branch data.
    public function getAll()
    {
        $resp = array();
        try
        {
            $limit   = Input::get('limit')?:10;
            $branchId  = Session::get('branch_id');
            $results = Branch::Where('id', '=', $branchId)->orderBy('id', 'DESC')->paginate($limit);
            $resp['success']    = true;
            $resp['data']       = (new BranchTransformer)->transformCollection($results,false); 
            $resp['pagination'] = (new Paginations)->results($results);
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Branch Details Not Available";
            $resp['ex']      = $ex->getMessage();
        }
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'Show All',
                                  "referenceTableId" => $results,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' show all branch details from table branch successfully');
            (new OperationLog)->add($operationLog);
        return $resp;
    }
    
    //This function is use to get branch details by Id.
    public function getById($id)
    {
        $resp = array();
        try
        {
            $resp['success'] = true;
            $branchId        = Session::get('branch_id');
            $resp['data']    = (new BranchTransformer)->transform(Branch::Where('id', '=', $branchId)
                                                                          ->findOrFail($id));  
            $operationLog=array("userId"           => Auth::User()->id,
                                 "operation"        => 'View',
                                 "referenceTableId" => $id,
                                 "operationStatus"  => 'success',
                                 "customMessage"    => 'user id '.Auth::User()->id.' view branch details in table branch with id '.$id.' successfully');
            (new OperationLog)->add($operationLog);
        }
        catch(\Exception $ex)
        {
             $resp['success'] = false;
             $resp['msg']     = $ex->getMessage().' '."Branch Details Not Available";
             $resp['ex']      = $ex->getMessage();
             $operationLog=array("userId"           => Auth::User()->id,
                                 "operation"        => 'View',
                                 "referenceTableId" => $id,
                                 "operationStatus"  => 'fail',
                                 "customMessage"    => 'user id '.Auth::User()->id.' view branch details in table branch with id '.$id.' failure');
            (new OperationLog)->add($operationLog);
        }
        return $resp;
    }
    
    //This function is use to add branch details.
    public function add()
    {
        $resp = array();
        try
        {
            $resp['success'] = true;
            $branch          = (new BranchTransformer)->reverseTransform(Input::all());
            $branchId        = Branch::insertGetId($branch);
            $resp['data']    = (new BranchTransformer)->transform(Branch::find($branchId));
            
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => $branchId,
                                  "operationStatus"  => 'success',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table branch with id '.$branchId.' successfully');
            (new OperationLog)->add($operationLog);
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Branch Details Added Failure";
            $resp['ex']      = $ex->getMessage();
            $operationLog = array("userId"           => Auth::User()->id,
                                  "operation"        => 'insert',
                                  "referenceTableId" => 0,
                                  "operationStatus"  => 'fail',
                                  "customMessage"    => 'user id '.Auth::User()->id.' inserted in table branch failure');
            (new OperationLog)->add($operationLog);
        }
        return $resp;
    }
    
    //This function is use to update branch details by Id.
    public function updateById($id)
    {
        $resp = array();
        try
        {
            $resp['success'] = true;
            $branch          = (new BranchTransformer)->reverseTransform(Input::all());
            Branch::where('id',$id)->update($branch);
            Branch::findOrFail($id)->save();
            $resp['data']    = (new BranchTransformer)->transform(Branch::findOrFail($id));
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Branch Details Not Available";
            $resp['ex']      = $ex->getMessage(); 
        }
        return $resp;
    }
    
    //This function is use to delete branch details by Id.
    public function deleteById($id)
    {
        $resp = array();
        try
        {
            $branch = Branch::findOrFail($id);
            $branch->delete();
            $limit   = Input::get('limit')?:10;
            $results = $branch::paginate($limit);
            $branchTransformer  = new BranchTransformer;
            $resp['success']    = true;
            $resp['data']       = $branchTransformer->transform($branch);
            $resp['pagination'] = (new Paginations)->results($results);      
        }
        catch(\Exception $ex)
        {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Branch Details Not Available";
            $resp['ex']      = $ex->getMessage();  
        }
        return $resp;
    }
    
    //This function is use to search branch details for any keyword.
    public function autocomplete($data)
    {
       $resp = array();
       try
       {
            $limit   = Input::get('limit')?:10;
            $resp['success']    = true;
            $dataBranch = Branch::where('branch_name', 'like', '%'.$data.'%')
                ->orWhere('registration_number', 'like', '%'.$data.'%')
                ->orWhere('registration_validity', 'like', '%'.$data.'%')
                ->orWhere('shop_act_number', 'like', '%'.$data.'%')
                ->orWhere('shop_act_validity', 'like', '%'.$data.'%')
                ->paginate($limit);
            $resp['data'] =(new BranchTransformer)->transformCollection($dataBranch);
            $resp['pagination']=(new Paginations)->results($dataBranch);
            $resp['msg']="Branch Details Information";
           
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Product Details Not Available";  
            $resp['ex']      = $ex->getMessage(); 
       }
       return $resp;
    }
    
    //This function is use to get all branch details in-beetween of two dates.
    public function history()
    {
        $resp        = array();
        $limit       = Input::get('limit')?:10;
        $fromDate    = Input::get('fromDate');
        $toDate      = Input::get('toDate');
        $from        = date("Y-m-d H:i:s", strtotime($fromDate));
        $to          = date("Y-m-d H:i:s", strtotime($toDate)); 
        $branch      = Branch::whereBetween('created_at',array($from, $to))->paginate($limit);
        if($fromDate!='' && $toDate!='')
        {
            $branchTransformer      = new BranchTransformer;
            $resp['success']        = "true";
            $resp['msg']            = "Branch Details Available From search date";
            $resp['data']           = $branchTransformer->transformCollection($branch); 
            $resp['pagination']     = (new Paginations)->results($branch);
        }
        else
        {
            $resp['success'] = "false";
            $resp['msg']     = "Please Select date";
        }
        return $resp;
    }
    
    //This function is use to get branch details for searchin branch name.
    public function autocompleteByName($data)
    {
       $resp = array();
       try
       {
            $limit           = Input::get('limit')?:10;
            $dataBranch      = Branch::where('branch_name', 'like', $data.'%')
                                       ->orderBy('branch_name','asc')
                                       ->take($limit)->get();
            $resp['success'] = true;
            $resp['data']    = (new BranchTransformer)->transformCollectionPlain($dataBranch);
            $resp['msg']     = "Branch Details Information";
       }
       catch(\Exception $e)
       {
            $resp['success'] = false;
            $resp['msg']     = $ex->getMessage().' '."Branch Details Not Available";
            $resp['ex']      = $ex->getMessage();
       }
       return $resp;
    }
    
    //This function is use to get all branch related details
    //This function is use for standard reports.
    public function getDataForReports($fromDate, $toDate, $myRequestObject)
    {
        $table = Branch::join('shop', 'branch.shop_id', '=', 'shop.id')
                       ->select('branch.*', 'shop.brand_name')
                       ->whereBetween('branch.created_at',array($fromDate, $toDate))->get();
        $reportName  = new Branch;
        $branchData = array();
        foreach($table as $row) 
        {            
            array_push($branchData, array($row['id'], $row['branch_name'], $row['registration_number'],                                                         $row['registration_validity'], $row['shop_act_number'], $row['shop_act_validity'],                                                         $row['vat_number'], $row['cst_number'], $row['lbt_number'], $row['pan_card'], $row['district'],                                           $row['city'], $row['pin_code'], $row['state'], $row['address'],$row['user_drug_license_number'],                                           $row['pharmacist_id'], $row['tin_number'], $row['contact_number'], $row['fax_number'],                                                     $row['alternate_contact'], $row['brand_name']));
        }
        return $branchData;
    }
}

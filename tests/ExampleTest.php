<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * 
     *
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->visit('/')
             ->see('Laravel 5');
             //$this->assertTrue(true);
    }

    public function testNewlogin()
    {
          $credentials = [
            'username' => 'mediplus',
            'password' => 'mediplus',
        ];
        $this->post('/login', $credentials)
             ->seeJsonEquals([
                 'created' => true,
             ]);
        $this->assertTrue(true);
    }
}

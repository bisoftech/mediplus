(function () {
    'use strict';

    angular
    .module('mediplus')
    .controller('products', products);

    products.$inject = ['$scope', '$state','apiUtility','userService','toastr','ngDialog','hotkeys','$rootScope'];
    function products($scope,$state,apiUtility,userService,toastr,ngDialog,hotkeys,$rootScope) {
    $scope.add=add;
    $scope.menu=1;
    $scope.toggleSetting=toggleSetting;
    $scope.toggle={
        flag:true,
        type:"mg"
    };
    $scope.changeSearch=changeSearch;
    $scope.products = {};
    $scope.str="";
    $scope.newProducts={
        lowQuantity:10,
        dosage:'',
        productLocation:'',
        formula:'',
        manufacturer:'',
        name:''
    };
    $scope.currentView='display';
    $scope.editRecordId=null;
    $scope.changeView=changeView;
    $scope.addNewProducts=addNewProducts;
    $scope.deleteProducts=deleteProducts;
    $scope.changeEditMode=changeEditMode;
    $scope.saveProduct=saveProduct;
    $scope.setPage=setPage;
    $scope.getProducts=getProducts;
    $scope.validate=validate;
    $scope.currentPageNo=1;
    $scope.saveValidate=saveValidate;
    $scope.pagination={};
    $scope.pagination.perPage=20;
    $scope.paginate=['10','20','30','40','50','60','70','80'];
    $scope.search=search;
    $scope.str="";
    $scope.newReport={};
    $scope.getData={};
    $scope.reportCreate=reportCreate;
    $scope.items={
                    name:"",
                    manufacturer:"",
                    formula:"",
                    dosage:""
                }

    $scope.editProduct={
                    name:'',
                    formula:'',
                    dosage:'',
                    id:'',
                    manufacturer:''
                }
    $scope.$watch(function($scope){
    return $scope.getData;
    },
            
    function(nv,ov)
    {
        if(nv !== ov)
        console.log('123');
    });

    function init()
    {
        hotkeys.add({combo: 'a',description: 'Description goes here',callback: function() { add(); }});
        hotkeys.add({combo: 's',description: 'Description goes here',callback: function() {document.getElementById("search").focus();}});
    }
        init();
  
    function reportCreate()
    {
        $scope.newReport.reportName="product";
        apiUtility.executeApi('POST','product/reporttoExcel',$scope.newReport)
        .then(function(response){
            $scope.getData=response;
            $scope.newProducts={};
            $scope.newProducts.formula="";
        },
        function(errorObject){
            $scope.errorMessage = errorObject.errorData.error.message;
        });
    }

    function toggleSetting()
    {
        if($scope.toggle.flag==false)
            $scope.toggle.type="mg";
        else
            $scope.toggle.type="ml";
            $scope.toggle.flag=!$scope.toggle.flag;
    }
                    
    function changeSearch(str)
    {
        if(str=="")
            getProducts();
    }
     
    function add()
    {
         $scope.newProducts={
        lowQuantity:10,
        dosage:'',
        productLocation:'',
        formula:'',
        manufacturer:'',
        name:''
    };
        ngDialog.open({ template: '/scripts/app/features/products/addProduct.html',
        scope:$scope 
        });
    }

    function saveValidate()
    {
        console.log('valid:'+( !($scope.editProduct.name==undefined || $scope.editProduct.name=='' || $scope.editProduct.name==null)
        && !($scope.editProduct.manufacturer==undefined || $scope.editProduct.manufacturer=='' || $scope.editProduct.manufacturer==null)
        && !($scope.editProduct.formula==undefined || $scope.editProduct.formula=='' || $scope.editProduct.formula==null)
        && !($scope.editProduct.dosage==undefined || $scope.editProduct.dosage=='' || $scope.editProduct.dosage==null)))
        return  ( !($scope.editProduct.name==undefined || $scope.editProduct.name=='' || $scope.editProduct.name==null)
        && !($scope.editProduct.manufacturer==undefined || $scope.editProduct.manufacturer=='' || $scope.editProduct.manufacturer==null)
        && !($scope.editProduct.formula==undefined || $scope.editProduct.formula=='' || $scope.editProduct.formula==null)
        && !($scope.editProduct.dosage==undefined || $scope.editProduct.dosage=='' || $scope.editProduct.dosage==null));
    }

    function setPage(pageNo)
    {
        $scope.currentPageNo=pageNo;
        getProducts();
    }

    function search(str)
    {
        $scope.currentPageNo=1;
        if(str)
        {
            apiUtility.executeApi('GET','search/product/'+ str +'/?page='+$scope.currentPageNo+'&limit='+$scope.pagination.perPage)
            .then(function(response){
                    $scope.products=response.data;
                    $scope.pagination=response.pagination;
                },

            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;

            });
        }
        else
            getProducts();
    }
        
    function getProducts()
    { 
        apiUtility.executeApi('GET','api/v1/product?page='+$scope.currentPageNo+'&limit='+$scope.pagination.perPage)
            .then(function(response){
                $scope.products=response.data;
                $scope.pagination=response.pagination;
                if($scope.pagination.perPage==20 && $scope.pagination.total<20)
                    $scope.pagination.perPage=10;
            },
            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
    }
        
    function changeView(viewName)
    {
        $scope.currentView=viewName;
    }

    function addNewProducts()
    {
        $scope.newProducts.dosage+="  ";
        $scope.newProducts.dosage+=$scope.toggle.type;
        apiUtility.executeApi('POST','api/v1/product',$scope.newProducts)
        .then(function(response){
            toastr.success('Record Added Successfully!');
            ngDialog.closeAll();
            getProducts();
        });
    }

    function deleteProducts(productId)
    {
        apiUtility.executeApi('DELETE','api/v1/product/' +productId)
        .then(function(response)
        {
            $scope.status="deleted successfully";
            delete $scope.products[productId];
            toastr.success('Record Deleted Successfully!');
        });
    }
    
    function changeEditMode(productId)
    {
        if(productId == $scope.editRecordId)
        {
            $scope.editRecordId=null;
            saveProduct(productId);
        }
        else
        {
            $scope.editRecordId = productId;
            angular.copy($scope.products[productId], $scope.editProduct)
        }
    }

    function saveProduct(productId)
    {
        var editedProduct = $scope.editProduct;
        apiUtility.executeApi('PUT','api/v1/product/' + editedProduct.id, editedProduct)
        .then(function(response)
        {
            if(response.success==true){
                angular.copy($scope.editProduct, $scope.products[productId]);
            }
            toastr.info(' Record Updated !');
        });
    }

    function validate()
    {

    }
}

})();
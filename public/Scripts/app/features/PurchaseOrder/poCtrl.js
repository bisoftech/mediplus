(function () {
  'use strict';

  angular
  .module('mediplus')
  .controller('po', po);

  po.$inject = ['$scope', '$state','apiUtility','userService','$stateParams','toastr','ngDialog','$rootScope','hotkeys'];
  function po($scope,$state,apiUtility,userService,$stateParams,toastr,ngDialog,$rootScope,hotkeys) {
    
    $scope.editDistributor=editDistributor;
    $scope.changeSearch=changeSearch;
    $scope.showDistributor=false;
    $scope.addDistributor=addDistributor;
    $scope.addProduct=addProduct;
    $scope.showDetailInNew=showDetailInNew;
    $scope.getPurchaseOrders=getPurchaseOrders;
    $scope.getPoDetail=getPoDetail;
    $scope.addPo=addPo;
    $scope.addPoItems=addPoItems;
    $scope.deleteItems=deleteItems;
    $scope.poDetail={};
    $scope.poDetail.PurchaseOrderItem=[];
    $scope.editRecordId = '';
    $scope.changeEditMode=changeEditMode;
    $scope.poSubmit=poSubmit;
    $scope.addPoValidated=addPoValidated;
    $scope.setPage=setPage;
    $scope.currentPageNo=1;
    $scope.paginate=['10','20','30','40','50','60','70','80'];
    $scope.pagination={};
    $scope.pagination.perPage=20;
    $scope.getAlert={};
    $scope.newReport={};
    $scope.reportCreate=reportCreate;
    $scope.downloadDisable=downloadDisable;
    $scope.getPurchaseOrders=getPurchaseOrders;
    $scope.validate=validate;
    $scope.search=search;
    $scope.str="";
    $scope.editable=true;
    $scope.isLogin=isLogin;
    $scope.currentUser={};
    $scope.currentUserBranch=userService.getSelectedBranch();
    $scope.poDetail={
      user:userService.getUser(),
      branch:$scope.currentUserBranch,
      distributor:{
        id:""
      },
      PurchaseOrderItem:
      [
      ]
    }
    $scope.newPo={
      product:"",
      quantity:""
    }
    function init(){
    hotkeys.add({combo: 'a',description: 'blah blah',callback: function() {addPo();}});
    hotkeys.add({combo: 's',description: 'Description goes here',callback: function() {document.getElementById("search").focus();}});
    }
    init();
    
    function isLogin()
    {
      apiUtility.executeApi('GET','isLogin')
      .then(function(response){
        $scope.currentUser=response.data.user;
      },
      function(errorObject){
        $scope.errorMessage = errorObject.errorData.error.message;
      });
    }

    function editDistributor()
    {
      $scope.poDetail.distributor={};
    }

    function reportCreate()
    {
      $scope.newReport.reportName="PurchaseOrder";
      apiUtility.executeApi('POST','product/reporttoExcel',$scope.newReport)
      .then(function(response){
        $scope.getData=response;
      },
      function(errorObject){
        $scope.errorMessage = errorObject.errorData.error.message;
      });
    }

    function changeSearch(str)
    {
      if(str=="")
      getPurchaseOrders();
    }

    $scope.$watch("newPo.product", function(newValue, oldValue) {
      addPoValidated();
    });

    function downloadDisable()
    {
      window.location.assign("/product/reporttoExcel?fromDate="+$scope.newReport.fromDate+"&toDate="+$scope.newReport.toDate+"&reportName=PurchaseOrder")
    }

    function addDistributor()
    {
      ngDialog.open({ template: '/scripts/app/features/distributors/addDistributors.html',
      controller: 'distributors' });
    }

    function addProduct()
    {
      ngDialog.open({ template: '/scripts/app/features/products/addProduct.html',
      controller: 'products' });
    }

    $scope.$watch("poDetail.distributor", function(newValue, oldValue) {
    if(newValue.originalObject!=undefined){
      $scope.showDistributor=true;
    }
  });

    function changeEditMode(poId)
    {
      if(poId == $scope.editRecordId)
      {
        $scope.editRecordId = null;
      }
      else
      {
       $scope.editRecordId = poId;
      }
    }

    function search(str)
    {
      $scope.currentPageNo=1;
      if(str)
      {
        apiUtility.executeApi('GET','search/purchaseOrder/'+ str +'/?page='+$scope.currentPageNo+'&limit='+$scope.pagination.perPage)
        .then(function(response){
         $scope.po=response.data;
         $scope.pagination=response.pagination;
       },
       function(errorObject){
        $scope.errorMessage = errorObject.errorData.error.message;
        });
      }
      else 
        getPurchaseOrders();
    }

    function addPoValidated()
    {
      return (!($scope.newPo.quantity==undefined || $scope.newPo.quantity=='' || $scope.newPo.quantity==null) 
      && !($scope.newPo.product==undefined || $scope.newPo.product=='' || $scope.newPo.product==null));
    }

    $scope.poId=$stateParams.poId;

    function setPage(pageNo)
    {
      $scope.currentPageNo=pageNo;
      getPurchaseOrders();
    }

    function getPurchaseOrders()
    {
      apiUtility.executeApi('GET','api/v1/purchaseOrder?page='+$scope.currentPageNo+'&limit='+$scope.pagination.perPage)
 .then(function(response){
      $scope.po=response.data;
      $scope.pagination=response.pagination;
      if($scope.pagination.perPage==20 && $scope.pagination.total<20)
                    $scope.pagination.perPage=10;
      },

      function(errorObject)
      {
        $scope.errorMessage = errorObject.errorData.error.message;
      });
    }  

    function OpenInNewTab(url) {

    }  

    function showDetailInNew(poId)
    {
      window.open('#/admin/purchaseOrder/print/'+poId );  
    }

    function addPo()
    {
      $state.go('admin.po.addPo');
    }

    function getPoDetail()
    {
      if($stateParams.poId)
      {
        $scope.isReadOnly=true;
        apiUtility.executeApi('GET','api/v1/purchaseOrder/' + $stateParams.poId)
        .then(function(response){
          $scope.poDetail = response.data;
        });
      } 
      else 
      {
        $scope.isReadOnly=false;
      }
    }

    function addPoItems()
    {
      if(($scope.newPo.product.originalObject!=undefined) && !($scope.newPo.product.originalObject.id==undefined || $scope.newPo.product.originalObject.id=='' || $scope.newPo.product.originalObject.id==null) && $scope.newPo.quantity ) {
        $scope.newPoItems = {
          product: $scope.newPo.product.originalObject,
          productId: $scope.newPo.product.originalObject.id,
          quantity: $scope.newPo.quantity,
        };

        $rootScope.$broadcast('changeText',{});
        $scope.poDetail.PurchaseOrderItem.push($scope.newPoItems)
        $scope.newPo = {
          product:""
        };
      } else {
        alert('product  not selected properly');
      }
    }

    function poSubmit()
    {
      $scope.poSubmit=
      {
        purchaseOrder :{ 
          userId:$scope.poDetail.user.id,
          distributorId: $scope.poDetail.distributor.originalObject.id,
          branchId:$scope.poDetail.branch.id
        },
        purchaseOrderItems:$scope.poDetail.PurchaseOrderItem
      }  
      apiUtility.executeApi('POST','api/v1/purchaseOrder/',$scope.poSubmit)
      .then(function(response){
        if(response.msg=="record not sufficient")
        {
          toastr.warning('Please fill all details!', 'Warning');
          $state.go('admin.po');
        }
        if(response.msg=="Purchase Order Added Successfully")
        {
          toastr.success('Form Submitted Successfully!');
          $state.go('admin.po')
        }
      });
    }

    function deleteItems(index)
    {
      $scope.poDetail.PurchaseOrderItem.splice(index,1);
    }

    function validate()
    {
      $scope.pn=itemDValue.productName;
      $scope.q=itemDValue.quantity;
    }

    $scope.fromDate = {
      maxDate: 'y',
      dateFormat: 'dd-mm-yy',
    };

    $scope.toDate = {
      maxDate: 'y',
      dateFormat: 'dd-mm-yy',
    };
  }
})();
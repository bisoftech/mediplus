(function () {
    'use strict';

    angular
        .module('mediplus')
        .controller('poDetail', poDetail);

    poDetail.$inject = ['$scope', '$state','$stateParams','apiUtility','userService'];
    function poDetail($scope,$state,$stateParams,apiUtility,userService) 
    {
        getPoDetail();
        function getPoDetail()
        {
            apiUtility.executeApi('GET','api/v1/purchaseOrder/' + $stateParams.poId)
            .then(function(response){
            $scope.poDetail = response.data;
            });
        }
   }
})();
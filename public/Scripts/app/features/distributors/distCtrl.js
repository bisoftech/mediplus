(function () {
    'use strict';

    angular
    .module('mediplus')
    .controller('distributors', distributors);

    distributors.$inject = ['$scope', '$state','apiUtility','userService','toastr','ngDialog','hotkeys'];
    
    function distributors($scope,$state,apiUtility,userService,toastr,ngDialog,hotkeys) {
        
        $scope.distributors = {};
        $scope.temp="";
        $scope.newDistributor = 
        {
            agencyName:"",
            agencyNumber:"",
            authorisedDistributor:"",
            ownerName:"",
            dlNumber:"",
            contactNumber:"",
            TIN:"",
            CST:"",
            vatNumber:"",
            expiryDuration:"",
            creditPeriod:"",
            otherDetails:""
        };
        $scope.currentView = 'display';
        $scope.editRecordId = '';
        $scope.changeView = changeView;
        $scope.addNewDistributor = addNewDistributor;
        $scope.deleteDistributor = deleteDistributor;
        $scope.changeEditMode = changeEditMode;
        $scope.saveDistributor = saveDistributor;
        $scope.getDistributors=getDistributors;
        $scope.add=add;
        $scope.validated=validated;
        $scope.paginate=['10','20','30','40','50','60','70','80']; 
        $scope.alphanumeric=userService.alphanumeric;
        $scope.alphabet=userService.alphabet;
        $scope.number=userService.number;
        $scope.isLoginBranch={};
        $scope.isLogin=isLogin;
        $scope.isNumberTest=isNumberTest;
        $scope.editDistributor=editDistributor;
        $scope.validate = validate;
        $scope.toast=toast;
        $scope.setPage=setPage;
        $scope.currentPageNo=1;
        $scope.pagination={};
        $scope.pagination.perPage=20;
        $scope.search=search;
        $scope.str="";
        $scope.changeSearch=changeSearch;
        $scope.isLoginData={};
        $scope.getData={};
        $scope.reportCreate=reportCreate;
        
        function init()
        {
            hotkeys.add({combo: 'a',description: 'blah blah',callback: function() {add();}});
            hotkeys.add({combo: 's',description: 'Description goes here',callback: function() {document.getElementById("search").focus();}});
        }
        init();
        function isLogin()
        {
            apiUtility.executeApi('GET','isLogin')
            .then(function(response){
                $scope.isLoginData=response.data;
            },
            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }

        function isNumberTest(event)
        {
            var charCode = (event.which) ? event.which : event.keyCode;
            if (charCode > 31 && (charCode > 48 || charCode <= 57))
                return false;
            return true;
        }

        function reportCreate()
        {
            $scope.newReport.reportName="distributor";
            apiUtility.executeApi('POST','product/reporttoExcel',$scope.newReport)
            .then(function(response){    
                $scope.getData=response;        
            },
            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }
        
        function changeSearch()
        {
            if($scope.str=="")
            getDistributors();
        }

        function add()
        {
            ngDialog.open({ template: '/scripts/app/features/distributors/addDistributors.html',
            scope:$scope
            });
        }

        function search()
        {
            $scope.currentPageNo=1;
            if($scope.str)
            {
                apiUtility.executeApi('GET','search/distributor/'+ $scope.str +'/?page='+$scope.currentPageNo+'&limit='+$scope.pagination.perPage)
                .then(function(response){
                      $scope.distributors=response.data;
                      $scope.pagination=response.pagination;
                  },
                  function(errorObject)
                  {
                    $scope.errorMessage = errorObject.errorData.error.message;
                  });
            }
            else
                getDistributors();
        }

        function setPage(pageNo)
        {
            $scope.currentPageNo=pageNo;
            getDistributors();
        }

        function getDistributors()
        {  
            apiUtility.executeApi('GET','api/v1/distributor?page='+$scope.currentPageNo+'&limit='+$scope.pagination.perPage)
            .then(function(response)
            {
                $scope.distributors=response.data;
                $scope.pagination=response.pagination;
                if($scope.pagination.perPage==20 && $scope.pagination.total<20)
                    $scope.pagination.perPage=10;
            },
            function(errorObject)
            {
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }    

        function changeView(viewName)
        {
            $scope.currentView = viewName;
        }

        function addNewDistributor()
        {
            apiUtility.executeApi('POST','api/v1/distributor',$scope.newDistributor)
            .then(function(response){
                $scope.distributors[response.data.id]=response.data;
                $scope.changeView('display');
                $scope.newDistributor={};
                getDistributors();
                toastr.success('Record Added Successfully!');
                ngDialog.closeAll();
            });    
        }

        function deleteDistributor(distributorId)
        {
            apiUtility.executeApi('DELETE','api/v1/distributor/' + distributorId)
            .then(function(response)
            {
                toastr.success('Record Deleted Successfully!');
                delete $scope.distributors[distributorId];
            });
        }

        function editDistributor(id)
        {
            $scope.temp=id;
            $scope.tempData=angular.copy($scope.distributors[$scope.temp]);
            ngDialog.open({ template: '/scripts/app/features/distributors/editDistributor.html',
            scope: $scope
            });
        }

        function changeEditMode(distributorId)
        {
            if(distributorId == $scope.editRecordId)
            {
                $scope.editRecordId = null;
                saveDistributor(distributorId);
            }
            else
            {
                $scope.editRecordId = distributorId;
            }    
        }

        function saveDistributor()
        {
            var id=$scope.temp;
            var temp=$scope.tempData;

            apiUtility.executeApi('PUT','api/v1/distributor/' + id,temp)
            .then(function(response)
            {
                $scope.distributors[id]=response.data;
                toastr.info('Record Updated!');
                ngDialog.closeAll();
            });
        }

        function validate()
        {
            $scope.newDistributor.an = newDistributor.agencyNumber;
            $scope.newDistributor.on = newDistributor.ownerName;
            $scope.newDistributor.ad = newDistributor.authorisedDistributor;
            $scope.newDistributor.tin = newDistributor.TIN;
            $scope.newDistributor.cst= newDistributor.CST;
            $scope.newDistributor.dl = newDistributor.dlNumber;
            $scope.newDistributor.cp = newDistributor.creditPeriod;
            $scope.newDistributor.cn = newDistributor.contactNumber;
        }

        function toast()
        {

        }

        function validated()
        {
            return (!($scope.newDistributor.agencyNumber==undefined || $scope.newDistributor.agencyNumber=='' || $scope.newDistributor.agencyNumber==null)
                   && !($scope.newDistributor.ownerName==undefined || $scope.newDistributor.ownerName=='' || $scope.newDistributor.ownerName==null)
                   && !($scope.newDistributor.authorisedDistributor==undefined || $scope.newDistributor.authorisedDistributor=='' || $scope.newDistributor.authorisedDistributor==null)
                   && !($scope.newDistributor.TIN==undefined || $scope.newDistributor.TIN=='' || $scope.newDistributor.TIN==null)
                   && !($scope.newDistributor.CST==undefined || $scope.newDistributor.CST=='' || $scope.newDistributor.CST==null)
                   && !($scope.newDistributor.dlNumber==undefined || $scope.newDistributor.dlNumber=='' || $scope.newDistributor.dlNumber==null)
                   && !($scope.newDistributor.contactNumber==undefined || $scope.newDistributor.contactNumber=='' || $scope.newDistributor.contactNumber==null)
                   && !($scope.newDistributor.creditPeriod==undefined || $scope.newDistributor.creditPeriod=='' || $scope.newDistributor.creditPeriod==null));
        }
    }

})();
(function () {
    'use strict';
    angular
        .module('mediplus')
        .controller('report', report);
    report.$inject = ['$scope', '$state','apiUtility','userService','toastr','ngDialog'];
    function report($scope,$state,apiUtility,userService,toastr,ngDialog) {

        $scope.data={};
        $scope.example2model = [];
        $scope.valdata={t1:1};
        $scope.printDiv=printDiv;
        $scope.btext=btext;
        $scope.check_select=check_select;
        $scope.data1={
            totalSalesReport:{},
            totalProductDetailsReport:{},
            totalBillReport:{}
        };
    $scope.chartConfig = {
        options: {
            chart: { 
                type:  'spline',
            }
        },
        series: [{
            data: [10, 15, 12, 8, 7, 1, 1, 19, 15, 10],name:'A'
        }],
        title: {
            text: 'Hello'
        },
        xAxis: {
              categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        loading: false
    };
      
        function check_select()
        {
            return 1;
        }

        function btext(bnew)
        {
              if(/^[a-zA-Z()]+$/.test(bnew))
               {
               $scope.data.t1;
               }
                else
                {
                $scope.data.t1.substring(1, $scope.data.t1.length-1);
                }    
        }

        $scope.show={
            "productDetails":{
                "data":{}
            },
            "productData":{},
            "customerDetails":{},
            "stockDetails":{},
            "purchaseOrderDetails":{}
        };
        $scope.newData={
            select:[],
            condition:{}
        };
        $scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
        $scope.data = [300, 500, 100];
        $scope.getPrint=0;
        $scope.getToster=getToster;
        $scope.newInvoice={};
        $scope.newInvoice.field=[];
        $scope.newInvoice.condition=[];
        $scope.temp=temp;
        $scope.newProduct={};
        $scope.newProduct.field=[];
        $scope.newProduct.condition=[];
        $scope.btnPrint=btnPrint;
        $scope.btnPrintPo = btnPrintPo;
        $scope.btnPrintCust = btnPrintCust;
        $scope.user = userService.getUser()
        $scope.displayMain=displayMain;
        $scope.displaySub=displaySub;
        $scope.del=del;
        $scope.tableName=["alert_settings","billing","billing_items","branch","branch_bank_details","customer","daw_error_log","distributor","invoice","invoice_items","mapping","migrations","operation_log","order_return","password_resets","product","purchase_order","purchase_order_items","sales_return","shop","stock","tax","user_branch_details","users"];
        $scope.example1model = []; 
        $scope.example1data = [ {id: 1, label: "David"}, {id: 2, label: "Jhon"}, {id: 3, label: "Danny"}];
        $scope.newReport={};
        $scope.reportCreate=reportCreate;
        $scope.getData={head:[]};
        $scope.header=["test1","test2","test3"];
        $scope.selectedModel=[];
        $scope.productReport=productReport;
        $scope.customerReport=customerReport;
        $scope.purchaseOrderReport=purchaseOrderReport;
        $scope.stockReport=stockReport;
        $scope.report=report;
        $scope.multipleDemo={};
        $scope.multipleDemo.colors=["sdafsd","sdafsdf","sdfdf"];
        $scope.availableColors=["a","s","d","f"];
        $scope.customerData={};
        $scope.billReport={};
        $scope.totalBillReport=[];
        $scope.totalProductSalesReport=[];
        $scope.totalSalesReport=[];
        $scope.totalCustomerDetailsReport=[];
        $scope.totalProductDetailsReport=[];
        $scope.getDate=getDate;

        apiUtility.executeApi('GET','totalRevenueReport')
            .then(function(response){
                        $scope.chartConfig= {
                            title: {text: 'Yearly Chart ', x: -20 }, 
                            subtitle: {text: '', x: -20 }, 
                            xAxis: {categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'] }, 
                            yAxis: {title: {text: 'Quantity'}, 
                            plotLines: [{value: 0, width: 1, color: '#808080'}] }, 
                            tooltip: {valueSuffix: '°C'}, 
                            legend: {layout: 'vertical', align: 'right', verticalAlign: 'middle', borderWidth: 0 }, 
                            series:response.series,                        
                        };

             });

        function temp()
        {
            var asd={"select":["productName","productManufacturar","productFormula","productDosage","invoiceItemsQuantity","invoiceItemsExpiryDate","productDetails"],"condition":{"productDosageOP":"<","productDosage":"45","invoiceItemsQuantityOp":"<","invoiceItemsQuantity":"10000","invoiceItemsExpiryDate":"Tuesday, 29 December, 2015"},"type":"productDetails"};
                   apiUtility.executeApi('POST','product/productReporttoExcel',asd)
            .then(function(response){
             });
        }
        function printDiv() 
        {
            $scope.getPrint=1;
            window.print();
            $scope.getPrint=0;
        } 

        function getDate()
        {
            var fd = new Date();
            var td = new Date();
            $scope.billReport.toDate=td.getMonth()+'/'+td.getDate()+'/'+td.getFullYear();
            fd.setMonth(td.getMonth() -6);
            $scope.billReport.fromDate=fd.getMonth()+'/'+fd.getDate()+'/'+fd.getFullYear();
            return $scope.billReport;
        }
        function report()
        {
            apiUtility.executeApi('GET','totalProductSalesReport')
            .then(function(response){
                  $scope.totalProductSalesReport=response;
             });

            apiUtility.executeApi('POST','totalBillReport',getDate())
            .then(function(response){
                $scope.totalBillReport=response; 
             });
            apiUtility.executeApi('POST','totalSalesReport',getDate())
            .then(function(response){
                $scope.totalSalesReport=response;
             });
            apiUtility.executeApi('POST','totalCustomerDetailsReport',getDate())
            .then(function(response){
                $scope.totalCustomerDetailsReport=response;
             });
            apiUtility.executeApi('POST','totalProductDetailsReport',getDate())
            .then(function(response){
                $scope.totalProductDetailsReport=response;
             });
        }

        function stockReport()
        {
            
        }

        function purchaseOrderReport()
        {
            $scope.newData.select[0] = ($scope.example3model);
            $scope.newData.type="purchaseOrderDetails";
            apiUtility.executeApi('POST','product/productReporttoExcel',$scope.newData)
            .then(function(response){
                  $scope.getData=response;
             });
        }

        function productReport()
        {
            $scope.newData.select[0]=($scope.example2model);
            $scope.newData.type="productDetails";
            apiUtility.executeApi('POST','product/productReporttoExcel',$scope.newData)
            .then(function(response){
                var z=0;
                  $scope.getData={data:response.data,head:[]};
                  var x=response.data[0];
                  angular.forEach(x,function(value,key){
                    $scope.getData.head.push({key:key,value:z++});
                  });
             });
        }
    
        function customerReport()
        {
            $scope.newData.select[0] = ($scope.example1model);
            $scope.newData.type="customerDetails";
            apiUtility.executeApi('POST','product/productReporttoExcel',$scope.newData)
            .then(function(response){
                    $scope.getData=response;
            });
        }
        function getToster()
        {
              toaster.pop({
                type: 'error',
                title: 'Title text',
                body: 'Body text',
                showCloseButton: true
            });
        }
 
        function displayMain(data)
        {
            $scope.show.displayMain=data;
            $scope.show.displaySub='';
        }

        function displaySub(data)
        {
            $scope.show.displaySub=data;  
            $scope.getData ={head:[]};
        }

        function del(index)
        {   
            $scope.tableName.splice(index, 1);  
        }

        function reportCreate()
        {
            $state.go('admin.report.print');
        }
        
        $scope.dateOptions = {
            dateFormat: 'dd-mm-yy',
        };

        $scope.example1model = [];
	    $scope.example1data =[
            {id:"customerName",label:"Name"},
            {id:"customerAddress",label:"Address"},
            {id:"customerEmail",label:"Email"},
            {id:"customerContactNumber",label:"ContactNumber"},
            {id:"customerCreditlimit",label:"Credit Limit"},
            {id:"customerCustomerType",label:"Customer Type"},
            {id:"billingTotalAmount",label:"Total Amount"},
            {id:"billingStatus",label:"Status"},
            {id:"billingPaymentType",label:"Payment Type"}];
	
       $scope.example2data = [
            {id:"productName",label:"Product Name"},
            {id:"productManufacturar",label:"Product Manufacturar"},
            {id:"productFormula",label:"Product Formula"},
            {id:"productDosage",label:"Product Dosage"},
            {id:"invoiceItemsQuantity",label:"Invoice Items Quantity"},
            {id:"invoiceItemsExpiryDate",label:"Invoice Items Expiry Date"}];
      $scope.example3model = [];
      $scope.example3data = [
    		{id:"poUser", label: "User Name"},
    		{id:"poBranch", label: "Branch Name"},
    		{id:"poPlacedDate", label: "Purchase Order Date"},
            {id:"poDistrubutor", label: "Distrubutor Name"}];    
    }

    function btnPrint()
    {
            var divContents = $("#dvContainer").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="styles/thirdParty/bootstrap.min.css"/><link href="styles/thirdParty/stickyheader.css" rel="stylesheet" type="text/css" ><title>DIV Contents</title>');
            printWindow.document.write('</head><body width=1000px>');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();

    }

    function btnPrintPo()
    {
            var divContents = $("#dvContainerPo").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="styles/thirdParty/bootstrap.min.css"/><link href="styles/thirdParty/stickyheader.css" rel="stylesheet" type="text/css" ><title>DIV Contents</title>');
            printWindow.document.write('</head><body width=1000px>');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();

    }

    function btnPrintCust()
    {
            var divContents = $("#dvContainerCust").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="styles/thirdParty/bootstrap.min.css"/><link href="styles/thirdParty/stickyheader.css" rel="stylesheet" type="text/css" ><title>DIV Contents</title>');
            printWindow.document.write('</head><body width=1000px>');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();

    }

})();
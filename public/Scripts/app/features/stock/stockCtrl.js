(function(){
  'use strict';
  angular
        .module('mediplus')
        .controller('stock',stock);
                        
stock.$inject = ['$scope', '$state','apiUtility','userService','$stateParams','toastr','ngDialog','$rootScope'];
    function stock($scope,$state,apiUtility,userService,$stateParams,toastr,ngDialog,$rootScope){

    	$scope.showAll=showAll;
		$scope.pagination={};
		$scope.currentPageNo=1;
        $scope.pagination.perPage=20;
        $scope.paginate=['10','20','30','40','50','60','70','80'];
    	$scope.data={addDb:[]};
    	$scope.addDb=addDb;
    	$scope.setPage=setPage;
    	$scope.showAdd=showAdd;
    	$scope.invoiceId=invoiceId;
        $scope.setFormScope=function(scope)
        {
           this.formScope = scope;
        }
    	$scope.tempAdd=tempAdd;
    	$scope.tempDelete=tempDelete;
        $scope.changeSearch=changeSearch;
        $scope.search=search;
        function search(str)
        {
            apiUtility.executeApi('GET','search/stock/'+str+'?page='+$scope.currentPageNo+'&limit='+$scope.pagination.perPage)
            .then(function(response){
                $scope.data.showAll=response.data;
                $scope.data.pagination=response.pagination;
            },
            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });            
        }
        
        function changeSearch(str)
        {
            if(str=="")
            showAll();
        }

        $scope.$watch('pagination.perPage',function(bnew,bold){
            showAll();
        });
    	
        function tempDelete(index)
    	{    		
    		$scope.data.addDb.splice(index,1);
    	}
    	
        function tempAdd(d)
    	{
    		var temp={};
    		temp.productId=d.product.originalObject.id;
    		temp.product=d.product.originalObject.name;
    		temp.quantity=d.quantity;
    		temp.productLocation=d.productLocation
    		temp.invoiceId=d.invoiceId;
            temp.dosage=d.product.originalObject.dosage;
            temp.productLocation=d.product.originalObject.productLocation;
    		$scope.data.addDb.push(temp);
    		$scope.data.temp={};
            this.formScope.stock_form.$setPristine();
            this.formScope.stock_form.$setUntouched();
    	}

    	function invoiceId()
    	{
		   apiUtility.executeApi('GET','autocomplete/invoice/1')
            .then(function(response){
            	$scope.data.invoiceId=response.data.id;   	
            },

            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
    	}

    	function setPage(pageNo)
        {	
            $scope.currentPageNo=pageNo;
            showAll();
        }
    	
        function showAll()
    	{
    		apiUtility.executeApi('GET','api/v1/stock?page='+$scope.currentPageNo+'&limit='+$scope.pagination.perPage)
            .then(function(response){    	
            	$scope.data.showAll=response.data;
            	$scope.data.pagination=response.pagination;
                if($scope.data.pagination.perPage==20 && $scope.data.pagination.total<20)
                    $scope.data.pagination.perPage=10;
            },
            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
    	}

    	function addDb()
    	{
    		apiUtility.executeApi('POST','api/v1/stock',$scope.data.addDb)
            .then(function(response){
            	toastr.info('Stock Added Successfully');
            	$state.go('admin.stock');
            },
            function(errorObject){

                $scope.errorMessage = errorObject.errorData.error.message;

            });
    	}
    	
        function showAdd()
    	{
    		$state.go('admin.addStock');
    	}
    }
})();
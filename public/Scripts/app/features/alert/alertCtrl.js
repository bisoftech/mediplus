    (function () {
    'use strict';

    angular
        .module('mediplus')
        .controller('alert', alert);
    
    alert.$inject = ['$scope', '$state','apiUtility','userService','$stateParams','toastr','ngDialog','$rootScope','hotkeys'];
    function alert($scope,$state,apiUtility,userService,$stateParams,toastr,ngDialog,$rootScope,hotkeys) {
        $scope.getAlert=getAlert;
        $scope.show=show;
        function getAlert()
        {
            $scope.show=0;
            apiUtility.executeApi('GET','/api/v1/getAlerts')
            .then(function(response){
                    $scope.alert=response.alerts;
                toastr.info('You have Alerts!','You have Alerts!');
            },
            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }
        function show()
        {
        }
}
})();

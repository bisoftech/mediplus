(function () {
    'use strict';

    angular
        .module('mediplus')
        .controller('mainController', mainController)

    mainController.$inject = ['$scope', '$state','apiUtility','userService','toastr','userSv','ngDialog','$rootScope'];

    function mainController($scope,$state,apiUtility,userService,toastr,userSv,ngDialog,$rootScope) {  
    
    	$scope.min=function(arrayVar){

        	return Math.min.apply(Math,arrayVar);
        }
    $scope.data={};
    $scope.show=show;
    $scope.view='Menu1';
    $scope.newAdmin={};
    $scope.validataKeySubmit=validataKeySubmit;
    $scope.logout = logout;
    $scope.updatePassword=updatePassword;
    $scope.changePassword=changePassword;
    $scope.validateKey=validateKey;
    $scope.onlineRegistration=onlineRegistration;
    $scope.user =localStorage.getItem('userData');
    $scope.alertSet = alertSet;

    function show(data)
    {
        $scope.view=data;
    }

    function validataKeySubmit()
    {
        apiUtility.executeApi('POST','mediplusRegistration',$scope.data)
        .then(function(response){
            ngDialog.close();
            toastr.info('Registered Successfully ');
        },
        function(errorObject){
            $scope.errorMessage = errorObject.errorData.error.message;
        });
    }
        
    function onlineRegistration()
    {
        apiUtility.executeApi('GET','/registration/onlineRegistration')
        .then(function(response)
        {
        });
    }
        
    function changePassword()
    {
        ngDialog.open({ template: '/scripts/app/features/admin/changePassword.html',
            scope:$scope  });
    }

    function validateKey()
    {
        apiUtility.executeApi('GET','registration/isValid')
        .then(function(response){
            if(response.is_valid)
            {
                $scope.data=response.data;
                ngDialog.open({ template: 'scripts/app/features/home/validateKey2.html',
                    scope:$scope
                });
            }
            else
            {
                ngDialog.open({ template: '/scripts/app/features/home/validateKey.html',
                   scope:$scope
               });
            }          
        },
        function(errorObject){
            $scope.errorMessage = errorObject.errorData.error.message;
        });
    }
        /*function validateKey()
        {
            
                apiUtility.executeApi('GET','registration/isValid')
            .then(function(response){
                  $scope.data=response.data;
                if(response.is_valid)
                {
                  
                    console.log(response.is_valid);
                    ngDialog.open({ template: 'scripts/app/features/home/validateKey2.html',
                    scope:$scope
                    });
                }
                else
                {
                    console.log(response.is_valid);
                    ngDialog.open({ template: '/scripts/app/features/home/validateKey.html',
                     scope:$scope
                    });
                }
        }*/

    function logout()
    {
        apiUtility.executeApi('GET','/logout')
        .then(function(response)
        {
            toastr.info('TATA.. :P ');
            $state.go('login');
            toastr.pop({
                type: 'info',
                body: '<a href="#">home</a>',
                bodyOutputType: 'directive'
            });
        });
    }
        
    function alertSet()
    {
        $state.go('admin.alert');
    }    
            
    function updatePassword()
    {
        if(angular.equals($scope.newAdmin.newField1, $scope.newAdmin.newField2))
        {
            apiUtility.executeApi('POST','users/changePassword?newUserInputPassword='+$scope.newAdmin.newField2+'&oldUserInputPassword='+$scope.newAdmin.current)
            .then(function(response){
                toastr.success('Password changed successfully!');
                logout();
                ngDialog.close();
            },

            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }
        else
        {
         toastr.info('Password do not match!');  
     }
    }
    }
})();

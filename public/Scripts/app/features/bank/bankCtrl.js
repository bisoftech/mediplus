(function () {
    'use strict';

    angular
        .module('mediplus')
        .controller('bank', bank);

    bank.$inject = ['$scope', '$state','apiUtility','userService','$stateParams','toastr','ngDialog','hotkeys'];
    function bank($scope,$state,apiUtility,userService,$stateParams,toastr,ngDialog,hotkeys){
        
        $scope.newBank={};
        $scope.temp={};
        $scope.editedBranch={};
        $scope.moveBankDetails=moveBankDetails;
        $scope.addBank=addBank;
        $scope.bank = {};
        $scope.searchField={
            str:""
        }
        $scope.editRecordId='';
        $scope.branchValue={}
        $scope.newBank = {};
        $scope.currentView='display';
        $scope.editRecordId=null;
        $scope.changeView=changeView;
        $scope.addNewBank=addNewBank;
        $scope.deleteBank=deleteBank;
        $scope.changeEditMode=changeEditMode;
        $scope.saveBank=saveBank;
        $scope.addDialog=addDialog;
        $scope.validate=validate;
        $scope.addBank=addBank;
        $scope.pagination={};
        $scope.Login=isLogin;
        $scope.isLoginData={};
        $scope.paginate=['10','20','30','40','50','60','70','80'];
        $scope.pagination.perPage=20;
        $scope.currentPageNo=1;
        $scope.getBankDetail=getBankDetail;
        $scope.bankSubmit=bankSubmit;
        $scope.showDetail=showDetail;
        $scope.shopId=shopId;
        $scope.setPage=setPage;
        $scope.changeSearch=changeSearch;
        $scope.search=search;
        $scope.str="";
        $scope.reportCreate=reportCreate;
        $scope.newReport={};
        $scope.getData={};

        function init()
        {
            getBank();
            isLogin();
            hotkeys.bindTo($scope).add({combo: 'a',description: 'blah blah',callback: function() {addDialog();}});
            hotkeys.add({combo: 's',description: 'Description goes here',callback: function() {document.getElementById("search").focus();}});
        }
        
        init();

        function reportCreate()
        {
              $scope.newReport.reportName="BranchBankDetails";
            apiUtility.executeApi('POST','product/reporttoExcel',$scope.newReport)
            .then(function(response){
                $scope.getData=response;
            },
        
            function(errorObject)
            {
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }

        function moveBankDetails()
        {
            $state.go('admin.bank.detail');
        }

        function addBank()
        {   
            $scope.newBank.branchId=$scope.isLoginData.selectBranchId;
            $scope.newBank.shopId=$scope.isLoginData.shopId;         
              apiUtility.executeApi('POST','api/v1/branchBankDetails',$scope.newBank)
                .then(function(response){
                    $scope.bank[response.data.id]=response.data;    
                    toastr.success('Record Added Successfully!');                
                    ngDialog.close(); 
                    $state.go('admin.bank');
                }); 
        }

        function changeSearch(str)
        {
            if(str=="")
                getBank();
        }
        
        function isLogin()
        {    
            apiUtility.executeApi('GET','isLogin')
            .then(function(response){
            $scope.isLoginData=response.data.user;
            },
            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }
 
        function setPage(pageNo)
        {
            $scope.currentPageNo=pageNo;
            getBank();    
        }

        function search(str)
        {
            $scope.currentPageNo=1;
            if(str)
            {
            apiUtility.executeApi('GET','search/branchBankDetails/'+ str +'/?page='+$scope.currentPageNo+'&limit='+$scope.pagination.perPage)
            .then(function(response){
                $scope.bank=response.data;
                $scope.pagination=response.pagination;
            },
            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
            }
            else 
                 getBank();
        }

        function addDialog()
        {
            $state.go('admin.addBank');
        }

        function getBank()
        {
            if($stateParams.branchId)
            {
                $scope.isReadOnly=true;
                apiUtility.executeApi('GET','api/v1/branchBankDetails/'+ $stateParams.branchId)
                .then(function(response){
                
                           $scope.bank=response.data;
                    });
            } 
            else 
            {
                $scope.isReadOnly = false;
            }
                
            if(!$stateParams.branchId)   
            apiUtility.executeApi('GET','api/v1/branchBankDetails?page='+$scope.currentPageNo+'&limit='+$scope.pagination.perPage)
            .then(function(response){
                $scope.bank=response.data;
                $scope.pagination=response.pagination;
                if($scope.pagination.perPage==20 && $scope.pagination.total<20)
                    $scope.pagination.perPage=10;
            },
            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }

        function changeView(viewName)
        {
            $scope.currentView=viewName;
        }

        function addNewBank()
        {
            apiUtility.executeApi('POST','api/v1/branchBankDetails',$scope.newBank)
                .then(function(response){
                    $scope.bank[response.data.id]=response.data;
                    $scope.changeView('display');
                });
        }

        function deleteBank(branchId)
        {
            apiUtility.executeApi('DELETE','api/v1/branchBankDetails/' +branchId)
                .then(function(response)
                {
                    delete $scope.bank[branchId];
                    toastr.success('Record Deleted Successfully!');
                });
        }

        function changeEditMode(branchId,data)
        {
            $scope.temp=angular.copy(data);

                if(branchId == $scope.editRecordId)
                {
                    $scope.editRecordId =null;
                    saveBank(branchId,data);

                }

                else
                {
                    $scope.editRecordId = branchId;
                }
        }

        function saveBank(branchId,data)
        {
            apiUtility.executeApi('PUT','api/v1/branchBankDetails/' + branchId,data)
                .then(function(response)
                {
                    toastr.success('Record Updated!');
                    $scope.editRecordId='';
                });
        }
            
        function getBankDetail()
        {
            if($stateParams.branchId){
                             $scope.isReadOnly=true;
                    apiUtility.executeApi('GET','api/v1/branch/'+ $stateParams.branchId)
                    .then(function(response){
                        $scope.branch=response;  
                    });
                } else {
                    $scope.isReadOnly = false;
                }
        }

        function validate()
        {
            $scope.name= $scope.newBank.name;
            $scope.ifsc=$scope.newBank.ifscCode;
            $scope.an=$scope.newBank.accountNumber;
            $scope.ahn=$scope.newBank.accountHolderName;
            $scope.cnn=$scope.newBank.contactNumber;
            $scope.email=$scope.newBank.emailId;
        }

        function bankSubmit()
        {

        }

        function showDetail(branchId)
        {
            $state.go('admin.bank.detail',{branchId : branchId});
        }

        function shopId()
        {
            apiUtility.executeApi('GET','api/v1/shop')
            .then(function(response){
            $scope.shop=response.data;
            });
        }
    }
})();
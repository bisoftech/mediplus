(function(){
  'use strict';
  angular
        .module('mediplus')
        .controller('customer',customer);
                        
customer.$inject = ['$scope', '$state','apiUtility','userService','$stateParams','toastr','ngDialog','$rootScope'];
    function customer($scope,$state,apiUtility,userService,$stateParams,toastr,ngDialog,$rootScope){
          /*add by db*/
          $scope.temp="";
          $scope.customerData={index:""};
          $scope.newCustomer={
            firstName:"",
            middleName:"",
            lastName:"",
            city:"",
            district:"",
            state:"",
            country:"",
            streetAddress:"",
            pinCode:"",
            emailId:"",
            contactNumber:"",
            creditLimit:"",
            creditPeriod:"",
            customerType:"Individual",
            dob:""
          };
          $scope.dataTemp={};
          $scope.currentPageNo=1;
          $scope.pagination={};
          $scope.pagination.perPage=15;
          $scope.allCustomer=allCustomer;
          $scope.setPage=setPage;
          $scope.add=add;
          $scope.search=search;
          $scope.deleteCustomer=deleteCustomer;
          $scope.customerAdd=customerAdd;
          $scope.editCustomer=editCustomer;
          $scope.editSave=editSave;
          $scope.changeSearch=changeSearch;
          $scope.paginate=['10','20','30','40','50','60','70','80'];
          $scope.reportCreate=reportCreate;
          $scope.newReport={};
          $scope.getData={};
          $scope.custType=['Individual','Hospital'];
          $scope.dateOptions = {
          changeYear: true,
          changeMonth: true,
          yearRange: '1900:-0',
          dateFormat: 'dd-mm-yy',    
    };

    function init()
    {
      allCustomer();
    }
    init();
         
    function reportCreate()
    {
        $scope.newReport.reportName="Customer";
        apiUtility.executeApi('POST','product/reporttoExcel',$scope.newReport)
            .then(function(response){
                $scope.getData=response;
            },
            function(errorObject){

                $scope.errorMessage = errorObject.errorData.error.message;

            });

    }
        
    function changeSearch()
    {
        if($scope.str=="")
        allCustomer();
    }
          
    function editSave()
    {
        var id=$scope.temp;
        var newData=$scope.dataTemp;    
        apiUtility.executeApi('PUT','/api/v1/customer/'+id,newData)
            .then(function(response){   
              toastr.success('Record Updated Successfully!');
              $scope.customerData[id]=response.data;
            },
          function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
            ngDialog.close();
          }
          
          function editCustomer(index)
          {
            $scope.temp=index;
            $scope.dataTemp=angular.copy($scope.customerData[$scope.temp]);
            ngDialog.open({ template: '/scripts/app/features/customer/editCustomer.html',
            scope: $scope
            });
          }

          function customerAdd()
          {
            var bstate=localStorage.getItem("state");
            apiUtility.executeApi('POST','api/v1/customer',$scope.newCustomer)
            .then(function(response){
                $scope.newCustomer='';
                toastr.success('Record Added Successfully!');
                if(bstate=='home.addBill')
                {
                  var bData=response.data;
                  localStorage.setItem("customerData",JSON.stringify(bData));
                  $state.go(bstate);
                }
                else
                $state.go('admin.customer');
            },
           
            function(errorObject){

                $scope.errorMessage = errorObject.errorData.error.message;

            });
          }
          
          function deleteCustomer(id)
          {
            apiUtility.executeApi('DELETE','api/v1/customer/'+id)
            .then(function(response){
              delete $scope.customerData[id];
              allCustomer();
              toastr.success('Record Deleted Successfully!');
            },
            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
          }
          
          function add()
          {
            $state.go('admin.addCustomer');
          }

          function search()
          {
            $scope.currentPageNo=1;
            if($scope.str)
            {
            apiUtility.executeApi('GET','search/customer/'+ $scope.str +'/?page='+$scope.currentPageNo+'&limit='+$scope.pagination.perPage)
            .then(function(response){
            
                $scope.customerData=response.data;
                $scope.pagination=response.pagination;
              
            },
            function(errorObject){

                $scope.errorMessage = errorObject.errorData.error.message;

            });
            }
            else
                 allCustomer();
          }
          
          function setPage(pageNo)
          {
            $scope.currentPageNo=pageNo;
            allCustomer();
          }

          function allCustomer()
          {
            apiUtility.executeApi('GET','api/v1/customer?page='+$scope.currentPageNo+'&limit='+$scope.pagination.perPage)
            .then(function(response){
            $scope.customerData=response.data; 
            $scope.pagination=response.pagination;   
            if($scope.pagination.perPage==20 && $scope.pagination.total<20)
                    $scope.pagination.perPage=10;        
            },
          
            function(errorObject){
            $scope.errorMessage = errorObject.errorData.error.message;
            });
          }
    }    
})();
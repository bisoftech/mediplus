(function () {
    'use strict';

    angular
        .module('mediplus')
        .controller('userBranchController', userBranchController)

    userBranchController.$inject = ['$scope', '$state','apiUtility','userService','toastr','userSv','ngDialog'];

    function userBranchController($scope,$state,apiUtility,userService,toastr,userSv,ngDialog) {
        $scope.user=userService.getUser();
        $scope.add=add;
        $scope.newBranch = {};
        $scope.addNewBranch=addNewBranch;
        $scope.userSelectedBranch=userService.getSelectedBranch();  
        $scope.selectBranch= function (branch) {
            apiUtility.executeApi('GET','/api/v1/setBranchInSession/'+branch.id)
                .then(function (response) {
                    if(response.success){
                        userService.selectBrnach(branch);
                        $state.go('home.addBill');
                    }
                })
        }
        
        function add()
        {
           ngDialog.open({ template: '/scripts/app/features/userBranch/addBranch.html',
           controller: 'userBranchController' });   
        }
        
        function addNewBranch()
        {
            apiUtility.executeApi('POST','api/v1/branch',$scope.newBranch)
            .then(function(response){
                toastr.success('Record Added Successfully!');
                ngDialog.closeAll();
                $scope.branch[response.data.id]=$scope.newBranch;
            });
        }
    }
})();
(function () {
    'use strict';

    angular
        .module('mediplus')
        .controller('returnn', returnn);

    returnn.$inject = ['$scope', '$state','apiUtility','userService','$stateParams','toastr','ngDialog','returnService'];

    function returnn($scope,$state,apiUtility,userService,$stateParams,toastr,ngDialog,returnService) {
        $scope.invoiceItemDtl=invoiceItemDtl;
        $scope.showDetails=showDetails;
        $scope.newReturnn={}
        $scope.add=add;
        $scope.getReturn=getReturn;
        $scope.getAdd=getAdd;
        $scope.pagination={};
        $scope.pagination.perPage=20;
        $scope.setPage=setPage;
        $scope.currentPageNo=1;
        $scope.limit=10;
        $scope.search=search;
        $scope.returnId='';
        $scope.str="";
        $scope.paginate=['10','20','30','40','50','60','70','80'];
        $scope.deleteReturn=deleteReturn;
        $scope.changeSearch=changeSearch;
        $scope.addDialog=addDialog;
        $scope.reportCreate=reportCreate;
        $scope.newReport={};
        $scope.getData={};
        $scope.returnData={};
        $scope.invoiceId=invoiceId;
        $scope.editReturnItem=editReturnItem;
        $scope.getSelectedItem=getSelectedItem;
        $scope.currentItem={}
        $scope.returnStatus=['In Progress','cancel','cashback','replace'];
        $scope.updateReturnItem=updateReturnItem;
        $scope.newData=
                    {   returnReason:"",
                        invoiceItemId:"",
                        extraNotes:"",
                        status:"",
                        replaceInvoiceId:"",
                        returnItems:[],insert:[]
                        
                    };


/*        $scope.value.extraNotes={};*/
        function invoiceId()
        {
        //autocomplete/invoice/1
           apiUtility.executeApi('GET','autocomplete/invoice/1')
            .then(function(response){
                $scope.getData.invoiceId=response.data.id;
                
            },
            function(errorObject){

                $scope.errorMessage = errorObject.errorData.error.message;

            });
        }


        function getSelectedItem(){
            $scope.currentItem=returnService.getSelectedItem();
        }

        $scope.newInvoice={
            id:""
        }
        $scope.deleteItems=deleteItems;
        $scope.downloadDisable=downloadDisable;
        $scope.reason=['Defected','Expire'];
        $scope.statu=['Cancel','Cashback','Relace','Inprogress'];

        $scope.requestInvoiceId=0;
      
        

        function invoiceItemDtl(id)
        {
            var idData=id.replace(/\s/g,'');
            $scope.requestInvoiceId=idData;
            apiUtility.executeApi('GET','api/v1/getInvoiceDetailWithStockUpdated/'+idData)
            .then(function(response){
                $scope.newData.invoiceData=response.data;
                angular.forEach($scope.newData.invoiceData.invoiceItems,function(value,key){
                    value.invoiceItemId=value.id;
                })
            },
            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
            /**/
        }
        function downloadDisable()
          {
            window.location.assign("/product/reporttoExcel?fromDate="+$scope.newReport.fromDate+"&toDate="+$scope.newReport.toDate+"&reportName=OrderReturn")
          }


         function reportCreate()
            {
          //http://localhost:8000/product/reporttoExcel?reportName=Customer&fromDate=07/13/2015&toDate=10/27/2015
              $scope.newReport.reportName="OrderReturn";

            apiUtility.executeApi('POST','product/reporttoExcel',$scope.newReport)
            .then(function(response){
            
                $scope.getData=response;

                
            },
            function(errorObject){

                $scope.errorMessage = errorObject.errorData.error.message;

            });

            }
          $scope.$watch("newInvoice", function(newValue, oldValue) {
        
 
              if(newValue!=undefined){
                  $scope.id=newValue.originalObject;

              }
          });
          function add()
          {

              if($scope.newData.invoiceData.invoiceItems.length) {
                  /*
                   extraNotes
                   returnReason
                   quantity
                   invoiceItemId
                   productId
                   */
                  /*console.log($scope.newData.returnItems);*/

                  var requestData = {
                      invoiceId: $scope.requestInvoiceId,
                      purchaseOrderItems: $scope.newData.invoiceData.invoiceItems
                  }
                  apiUtility.executeApi('POST', 'api/v1/orderReturn', requestData)
                      .then(function (response) {
                          toastr.success('Record Added Successfully!');
                          $state.go('admin.returnn');
                          ngDialog.close();
                      },
                      function (errorObject) {

                          $scope.errorMessage = errorObject.errorData.error.message;

                      });
              } else {
                  toastr.error('There are no items to return');
              }


          }
         function addDialog()
        {
            $state.go('admin.returnn.returnAdd');
         /*  ngDialog.open({ template: 'scripts/app/features/Return/returnAdd.html',
            controller: 'returnn' });*/
        }

          function showDetails(id)
          {
    
            $state.go('admin.returnn.returnDetails',{id:id});
          }

        function changeSearch(str)
        {
            if(str=="")
             getReturn();
        }
       function setPage(pageNo){
            $scope.currentPageNo=pageNo;
            getReturn();

        }
        function deleteReturn(returnId)
        {
  
            apiUtility.executeApi('DELETE','api/v1/orderReturn/' + returnId)
            .then(function(response)
            {
               delete  $scope.returnData[returnId];
                toastr.success('Record Deleted Successfully!');


            });

        }





        function search(str)
        {


            $scope.currentPageNo=1;
            if(str)
            {
            apiUtility.executeApi('GET','/search/orderReturn/'+ str +'/?page='+$scope.currentPageNo+'&limit='+$scope.limit)
            .then(function(response){
                
                  $scope.returnData=response.data;
                $scope.pagination=response.pagination;
            },
            function(errorObject){

                $scope.errorMessage = errorObject.errorData.error.message;

            });
            }
            else
                 getReturn();

        }
        function deleteItems(item)
        {

            var index = $scope.newData.invoiceData.invoiceItems.indexOf(item);
            $scope.newData.invoiceData.invoiceItems.splice(index, 1);
        }
   function getReturn()
        {
           if($stateParams.id)
           {
            $scope.returnId=($stateParams.id);
            apiUtility.executeApi('GET','api/v1/orderReturn/'+$stateParams.id)
            .then(function(response){
                $scope.returnData=response.data;
                $scope.pagination=response.pagination;
            },
        function(errorObject){
            $scope.errorMessage = errorObject.errorData.error.message;
            });
           }
            if(!$stateParams.id)
            {
            apiUtility.executeApi('GET','api/v1/orderReturn/?page='+$scope.currentPageNo+'&limit='+$scope.pagination.perPage)
            .then(function(response){

                $scope.returnData=response.data;
                $scope.pagination=response.pagination;
   
            },
        function(errorObject){
            $scope.errorMessage = errorObject.errorData.error.message;
            });
        }
        }

        function getAdd()
        {
            $state.go('admin.returnn.returnAdd');  
        }


        function editReturnItem(items,$index){
            var localItem={};
            angular.copy(items,localItem);
            returnService.selectItem(localItem);
            returnService.selectItemIndex($index);
            ngDialog.open({ template: '/scripts/app/features/Return/editReturn.html',
                scope:$scope
            });
        }

        function updateReturnItem(){
            apiUtility.executeApi('POST','/api/v1/updateReturnItem/'+$scope.currentItem.id,$scope.currentItem)
                .then(function (resp) {
                   if(resp.success){
                       angular.copy($scope.currentItem,$scope.returnData.purchaseOrderItems[returnService.getSelectedItemIndex()]);
                       ngDialog.close();
                       toastr.success('Order Return #'+$scope.currentItem.purchaseOrderReturnId+' updated successfully');
                   } else {
                       toastr.error(resp.msg);
                   }
                });
        }


        $scope.dateOptions = {
            dateFormat: 'dd-mm-yy'
        };
        
        $scope.toDate = {
            maxDate: 'y',
            dateFormat: 'dd-mm-yy'
        };

        $scope.fromDate = {
            maxDate: 'y',
            dateFormat: 'dd-mm-yy'
        };
    }
})();
(function () {
    'use strict';

    angular

    .module('mediplus')

    .controller('home', home);
    home.$inject = ['$scope', '$state', 'apiUtility', 'userService', '$stateParams', 'toastr', 'ngDialog', '$rootScope'];
    function home($scope, $state, apiUtility, userService, $stateParams, toastr, ngDialog, $rootScope) 
    {
        $scope.addCustomer=addCustomer;
        $scope.customerShow=0;
        $scope.changeBillValue=changeBillValue;
        $scope.editCustomer=editCustomer;
        $scope.top="";
        $scope.head="";
        $scope.product={originalObject:{prising:[]}};
        $scope.changeSearch=changeSearch;
        $scope.deleteItems=deleteItems;
        $scope.amountPaidStatus=amountPaidStatus;
        $scope.discountApply=discountApply;
        $scope.paymentTypeStatus=paymentTypeStatus;
        $scope.selectedProduct={};
        /*add by dhananjay*/
        $scope.f = { flag: 1, showHide: { show1: 1, show2: 0 } };
        $scope.validKey = validKey;
        $scope.data = {};
        $scope.bmrp = 0;
        $scope.stockTemp = stockTemp;
        $scope.print = print;
        $scope.printData = {};
        $scope.price = price;
        $scope.billDtl = billDtl;
        $scope.billing = billing;
        $scope.bi = [];
        $scope.biv = { quantity: 0, product: { originalObject: { prising: [] } } };
        $scope.billValue = { totalAmount: 0, discount: 0, paymentType: "Cash", customerId:{originalObject:{id:''}}};
        $scope.billValue.customerId.originalObject=JSON.parse(localStorage.getItem('customerData'));
        $scope.billValue.billingItems=[];
        $scope.currentPageNo=1;
        $scope.pagination={};
        $scope.pagination.perPage=20;
        $scope.setUserId=setUserId;    
        $scope.getAllBillingDetails=getAllBillingDetails;
        $scope.bilingDataAvailable=bilingDataAvailable;
        $scope.user = userService.getUser();    
        $scope.setFormScope= function(scope)
        {
           this.formScope = scope;
        }

       localStorage.setItem("userData",JSON.stringify($scope.user));
       $scope.showAddBill=showAddBill;
       $scope.addBillItems=addBillItems;
       $scope.getMrp=getMrp;
       $scope.generateBill=generateBill;
       $scope.showById=showById;
       $scope.setPage=setPage;
       $scope.logout = logout;
       $scope.search = search;
       $scope.alertSet = alertSet;
       $scope.total = 0;
       $scope.hideAddBill = hideAddBill;
       $scope.alert = {};
       $scope.getAlert = getAlert;
       $scope.newReport = {};
       $scope.getData = {};
       $scope.reportCreate = reportCreate;
       $scope.btnPrint = btnPrint;
       $scope.detailView = false;
       $scope.addView = false;
       $scope.mrpCal = mrpCal;
       $scope.isLogin = isLogin;
       $scope.isLoginData = {};
       $scope.goBackHome = goBackHome;
       $scope.downloadDisable = downloadDisable;
       $scope.myFunction = myFunction;
       $scope.pay = ['Online',
        'Cheque',
        'Debit',
        'Credit',
        'Cash'
        ];
       $scope.dateOptions = {
            dateFormat: 'dd-mm-yy',
        };

       $scope.fromDate = {
            maxDate: 'y',
            dateFormat: 'dd-mm-yy',
        };

       $scope.toDate = {
            maxDate: 'y',
            dateFormat: 'dd-mm-yy',
        };

       $scope.registrationDate = {
            dateFormat: 'dd-mm-yy',
        };

       $scope.getCustUnpaidAmtDetails = getCustUnpaidAmtDetails;
       $scope.getCustUnpaidAmtDetailsData = {};

       function init() 
       {
            if ($state.current.url == '/home')
                $scope.addView = false;
            if ($state.current.url == '/addBill')
                $scope.addView = true;
            if ($state.current.url == '/print/:id')
                $scope.detailView = true;
        }
        init();

        $scope.$watch('billValue.customerId', function (bnew, bold) {

            if(bnew!=undefined && bnew.originalObject!=undefined && bnew.originalObject.id!=undefined){
                getCustUnpaidAmtDetails(bnew.originalObject.id);
                $scope.customerShow=1;
            }
        });

        function editCustomer()
        {
            $scope.customerShow=0;
            var userId=$scope.billValue.userId;
            var billingItems=$scope.billValue.billingItems;
            $scope.billValue={totalAmount: 0,discount:0,paymentType:"Cash",customerId:{originalObject:{}}};
            $scope.getCustUnpaidAmtDetailsData={};
            $scope.billValue.userId=userId;
            $scope.billValue.billingItems=billingItems;
        }

        $scope.$watch("biv.product",function(newValue,oldValue){
          chackStockQty();
        });

        function chackStockQty()
        {
            var bLength=$scope.biv.product.originalObject.prising.length;
            var billingItemsLength=$scope.billValue.billingItems.length;
            for(var i=0;i<bLength;i++)
            for(var j=0;j<billingItemsLength;j++)
            {
                var aValue=$scope.biv.product.originalObject.prising[i];
                var bValue=$scope.billValue.billingItems[j];
                if($scope.biv.product.originalObject.prising[i].stockId==$scope.billValue.billingItems[j].stockId)
                {
                   $scope.biv.product.originalObject.prising[i].qty-=$scope.billValue.billingItems[j].quantity;
               }
           }
       }

       function changeBillValue(billValue) 
       {
            if(billValue.quantity < billValue.stockQty)
                billValue.mrp = billValue.perUnit * billValue.quantity;
            else
                toastr.warning('Available quantity is less than required quantity');
            var billTotalAmount = 0;
            angular.forEach($scope.billValue.billingItems, function (value, key) {
                billTotalAmount += value.mrp;
            });
            $scope.billValue.totalAmount = billTotalAmount;
            $scope.billValue.amountPaid = billTotalAmount;
        }

        function validKey() 
        {
            apiUtility.executeApi('GET', 'registration/isValid')
            .then(function (response) {
            },
            function (errorObject) {
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }

        function stockTemp(product, selectedProductJson) 
        {
            $scope.selectedProduct = JSON.parse(selectedProductJson);;
            product.stockId = $scope.selectedProduct.stockId;
            product.stockQty = $scope.selectedProduct.qty;
        }

        function getCustUnpaidAmtDetails(id) 
        {
            apiUtility.executeApi('GET', '/api/v1/getCustUnpaidAmtDetails/' + id)
            .then(function (response) {
               $scope.getCustUnpaidAmtDetailsData = response;
               if ($scope.billValue.customerId.originalObject.name) {
                       $scope.showHide = { show1: 0, show2: 1 };
                   }
               },
               function (errorObject) {
                   $scope.errorMessage = errorObject.errorData.error.message;
               });
        }
        
        function isLogin() 
        {
            apiUtility.executeApi('GET', 'isLogin')
            .then(function (response) {
                $scope.isLoginData = response.data.user;
            },

            function (errorObject) {
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }

        function myFunction() 
        {
            document.getElementById("mySelect").disabled = true;
        }

        function downloadDisable() 
        {
            window.location.assign("/product/reporttoExcel?fromDate=" + $scope.newReport.fromDate + "&toDate=" + $scope.newReport.toDate + "&reportName=Billing")
        }
        
        function goBackHome() 
        {
            $state.go('home.addBill');
        }
        
        function btnPrint() 
        {
            var divContents = $("#dvContainer").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="styles/thirdParty/bootstrap.min.css"/><link href="styles/thirdParty/stickyheader.css" rel="stylesheet" type="text/css" ><title>DIV Contents</title>');
            printWindow.document.write('</head><body width=1000px>');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        }

        $scope.status = ['Paid', 'UnPaid', 'Partially paid'];
        $scope.paginate = ['10', '20', '30', '40', '50', '60', '70', '80'];

        function print() {
            billDtl();
        }

        function showDetailInNew()
        {
        }
        
        function validateKey() 
        {
            apiUtility.executeApi('GET', 'registration/isValid')
            .then(function (response) {
                if (response.is_valid == true) {
                    ngDialog.open({
                        template: '/scripts/app/features/home/validateKey2.html',
                        scope: $scope
                    });
                }
            else {
                ngDialog.open({
                    template: '/scripts/app/features/home/validateKey.html',
                    scope: $scope
                });
            }

            },
        
            function (errorObject) {
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }

        function mrpCal(selectedProductJson, qty) 
        {
            if (selectedProductJson != undefined) {
                $scope.selectedProduct = JSON.parse(selectedProductJson);
                        $scope.bmrp = $scope.selectedProduct.mrp * qty;
                        if (qty > $scope.selectedProduct.qty) {
                            toastr.info('Insufficient Quantity');
                            $scope.f.flag = 1;
                        }
                        else {
                        }
                    }
        }

        function reportCreate() 
        {
            $scope.newReport.reportName = "Billing";
            apiUtility.executeApi('POST', 'product/reporttoExcel', $scope.newReport)
            .then(function (response) {
                $scope.getData = response;
            },
            function (errorObject) {
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }

        function getAlert() 
        {
            var isAlertDisplayed = localStorage.getItem(' ') == "0" ? false : true;
            if (!isAlertDisplayed && userService.isLogin()) {
                $scope.show = 0;
                apiUtility.executeApi('GET', '/api/v1/getAlerts')
                .then(function (response) {
                    $scope.alert = response.alerts;

                    if ($scope.alert.length != 0) {
                        localStorage.setItem("isAlertDisplayed", 1);
                        ngDialog.open({
                            template: 'scripts/app/features/alert/alertNotification.html',
                            controller: 'alert',
                            scope: $scope
                        });
                    }
                },
                function (errorObject) {
                    $scope.errorMessage = errorObject.errorData.error.message;
                });
            }
        }
        
        function changeSearch(str) 
        {
            if (str == "")
                getAllBillingDetails();
        }
        
        function price(d) {
        }

        function discountApply(data)
        {
            if(data.discount!=undefined && (data.totalAmount >= data.discount))
                data.amountPaid=data.totalAmount-data.discount;
            else
                data.amountPaid=data.totalAmount;
        }
        
        function amountPaidStatus(data) 
        {
            if (data.totalAmount == data.amountPaid)
                $scope.billValue.status = 'Paid';
            if (0 == data.amountPaid)
                $scope.billValue.status = 'UnPaid';
            if (data.totalAmount > data.amountPaid && data.amountPaid != 0)
                $scope.billValue.status = 'Partially Paid';
            if (!data.amountPaid)
                $scope.billValue.status = "UnPaid";
        }
        
        if ($scope.user == null) {
            $state.go('login');
        }
        
        function alertSet() 
        {
            $state.go('admin.alert');
        }
        
        function search(str) 
        {
            $scope.currentPageNo = 1;
            if (str) {
                apiUtility.executeApi('GET', '/search/billing/' + str + '/?page=1' + '&limit=' + $scope.pagination.perPage)
                .then(function (response) {
                    $scope.home = response.data;
                    $scope.pagination = response.pagination;
                },
                function (errorObject) {
                    $scope.errorMessage = errorObject.errorData.error.message;
                });
            }
            else
                getAllBillingDetails();
        }
        
        function setPage(pageNo) 
        {
            $scope.currentPageNo = pageNo;
            getAllBillingDetails();
        }
        
        function showById(key) 
        {
            $scope.detailView = false;
            $scope.addView = false;
            window.open('/#/home/print/'+key.id ); 
        }
        
        function bilingDataAvailable(id)
        {
        }

        function billDtl() 
        {
            isLogin();
            $scope.data={billDetails:{}};
            apiUtility.executeApi('GET', 'api/v1/showNextPrevious/' + $state.params.id)

            .then(function (response) {

              $scope.data = response;
                  var customerId = $scope.data.customer.id;
            },
        
            function (errorObject) {
              $scope.errorMessage = errorObject.errorData.error.message;
          });
        }

        function showAddBill()
        {
             $scope.customerShow=0;
             var userId=$scope.billValue.userId;
             $scope.billValue={discount:0,paymentType:"Cash",customerId:{originalObject:{}}};
             $scope.getCustUnpaidAmtDetailsData={};
             $scope.billValue.userId=userId;
             $scope.billValue.billingItems=[];
             $scope.addView = true;
             $scope.detailView=false;
             $state.go('home.addBill');
        }
     
         function hideAddBill()
         {
            $scope.customerShow=0;
            var userId=$scope.billValue.userId;
            $scope.billValue={discount:0,paymentType:"Cash",customerId:{originalObject:{}}};
            $scope.getCustUnpaidAmtDetailsData={};
            $scope.billValue.userId=userId;
            $scope.billValue.billingItems=[];
            $scope.addView = false;
            $scope.detailView=true;
            $state.go('home');
        }

        function setUserId(x)
        {
            $scope.billValue.userId=x;
        }

        function deleteItems(index,billValue)
        {
            $scope.billValue.totalAmount-=billValue.mrp;
            $scope.billValue.amountPaid=$scope.billValue.totalAmount;
            $scope.billValue.billingItems.splice(index,1);
        }

        function addCustomer()
        {
            $state.go('admin.addCustomer');
        }
    
        function generateBill(print) 
        {
            if($scope.billValue.customerId ==null)
            {     
                toastr.error('Select Customer name');
                return; 
            }
            var btotal = $scope.billValue.totalAmount;
                    
            if(!$scope.billValue.billingItems.length > 0)
            {
             toastr.error('enter Billi Items');   
             return;
            }
            var creditLimit = $scope.billValue.customerId.originalObject.creditLimit;
            var unpaidAmount=$scope.getCustUnpaidAmtDetailsData.total.unpaidAmount;
            var bamountPaid = $scope.billValue.amountPaid;
            var bstatus = $scope.billValue.status;
            if ($scope.billValue.status == 'Partially Paid')
                $scope.billValue.status = 'partial_paid';
            if ($scope.billValue.status == 'Paid')
                $scope.billValue.status = 'paid';
            $scope.data.billValue = angular.copy($scope.billValue);
            $scope.billValue.customerId = $scope.billValue.customerId.originalObject.id;
            
            if ($scope.billValue.paymentType != 'Cheque')
                $scope.billValue.chequeNumber = '';

            if(($scope.billValue.status == 'UnPaid') && (unpaidAmount >= creditLimit) 
                || ($scope.billValue.status=='partial_paid') && ($scope.billValue.amountPaid > (creditLimit-unpaidAmount)))

            {
                toastr.error('Credit Limit is low,can not generate bill');
                return;
            }
            else
            {
                $scope.billValue.userId=$scope.user.id;
                apiUtility.executeApi('POST', 'api/v1/billing', $scope.billValue)
                .then(function (response) {

                    if(print)
                    {
                        $scope.billValue = { totalAmount: 0, discount: 0, paymentType: "Cash", customerId:{originalObject:{}},billingItems:[]};
                        $scope.getCustUnpaidAmtDetailsData={};
                        $scope.customerShow=0;
                        $scope.customerShow=0;
                        $state.go('home.addBill');
                    }
                    else{
                           hideAddBill();
                    $state.go('home.print', { id: response.data.id });
                  }
                  toastr.info('Bill added successful');
                },

            function (errorObject) {
              $scope.errorMessage = errorObject.errorData.error.message;
            });
                $scope.billValue = { discount: 0, paymentType: "Cash", customerId: { originalObject: {} } };
                localStorage.removeItem('customerData');

            }
            $scope.billValue=$scope.data.billValue;
        }

        function getMrp(id) 
        {
            apiUtility.executeApi('GET', '/product/productPrice/' + id)
            .then(function (response) {
               $scope.mrp = response.data.MRP[0];
           },
           function (errorObject) {
               $scope.errorMessage = errorObject.errorData.error.message;
           });
            return $scope.mrp;
        }

        function addBillItems(billValue) 
        {
            if(isNaN($scope.billValue.totalAmount))
            {
                $scope.billValue.totalAmount=0;
            }
            if ($scope.f.flag == 1) {
                $scope.biv.productId = $scope.biv.product.originalObject.id;
                $scope.biv.stockId = $scope.biv.product.stockId;
                $scope.biv.stockQty = $scope.biv.product.stockQty;
                $scope.biv.name = $scope.biv.product.originalObject.name;
                $scope.biv.manufacturer = $scope.biv.product.originalObject.manufacturer;
                $scope.biv.formula = $scope.biv.product.originalObject.formula;
                $scope.biv.dosage = $scope.biv.product.originalObject.dosage;
                $scope.biv.perUnit = $scope.selectedProduct.mrp;
                $scope.billValue.totalAmount += ($scope.biv.mrp = $scope.selectedProduct.mrp * $scope.biv.quantity);
                $scope.selectedProduct = {};
                $scope.billValue.billingItems.push($scope.biv);

                var tempTotal = $scope.billValue.totalAmount;
                var bamountPaid = $scope.billValue.amountPaid;
                $scope.billValue.amountPaid = tempTotal;

                amountPaidStatus({ totalAmount: tempTotal, amountPaid: $scope.billValue.amountPaid });
                $scope.biv={quantity:0,product:{originalObject:{prising:[]}}};
                $scope.bmrp = 0;
                $rootScope.$broadcast('changeText', {});
            } else {
                toastr.info('Insufficient Quantity');
                $scope.f.flag = 1;
            }
            $scope.product = { originalObject: { prising: [] } };
            this.formScope.addBill_form.$setPristine();
            this.formScope.addBill_form.$setUntouched();
        }

        function billing() 
        {
            apiUtility.executeApi('GET', 'api/v1/billing/10')
            .then(function (response) {

                $scope.home = response.BillingItems.product;

            },

            function (errorObject) {
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }

        function paymentTypeStatus(data) 
        {
            if (data == "cheque")
                return true;
            else {

                $scope.billValue.chequeNumber = "";
                return false;
            }
        }

        function getAllBillingDetails() 
        {
            apiUtility.executeApi('GET', 'api/v1/billing?page=' + $scope.currentPageNo + '&limit=' + $scope.pagination.perPage)
            .then(function (response) {
                $scope.home = response.data;
                $scope.pagination = response.pagination;
                if($scope.pagination.perPage==20 && $scope.pagination.total<20)
                    $scope.pagination.perPage=10;
            },
            function (errorObject) {
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }

        function logout() 
        {
            apiUtility.executeApi('GET', '/logout')
            .then(function (response) {
                toastr.info('TATA.. :P ');
                $state.go('login');
            });
        }

    }})();
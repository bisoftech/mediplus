(function () {
    'use strict';

    angular
    .module('mediplus')
    .controller('user', user)
    .service("userSv",function(){
        var newUser={};
        return {
            set:function(data){
                newUser.data=data;
            },
            get:function(){
                return newUser;
            }
        };
    });
    user.$inject = ['$scope','$state','apiUtility','userService','$stateParams','toastr','ngDialog'];
    function user($scope,$state,apiUtility,userService,$stateParams,toastr,ngDialog){
        /*add by DB*/
        $scope.isLoginData={};
        $scope.mainData={user:{},shop:{},branch:{}};
        $scope.mainDataSubmit=mainDataSubmit;
        $scope.user = {};
        $scope.data = {};
        $scope.newUser = {role:"admin"};
        $scope.branch={};
        $scope.login = login;
        $scope.progressBar=progressBar;
        $scope.gettUser=gettUser;
        $scope.deleteUser=deleteUser;
        $scope.addUser=addUser;
        $scope.userAdd=userAdd;
        $scope.userAdd=userAdd;
        $scope.getBranch=getBranch;
        $scope.addDialog=addDialog;
        $scope.forgotPassword=forgotPassword;
        $scope.addForgotDialog=addForgotDialog;
        $scope.send=send;
        $scope.brole=['Admin','Employee'];
        $scope.role2=['Employee'];
        $scope.counter=0;
        $scope.editRecordId='';
        $scope.search=search;
        $scope.str="";
        $scope.setPage=setPage;
        $scope.currentPageNo=1;
        $scope.limit=20;
        $scope.userEdit=userEdit;
        $scope.saveUser=saveUser;
        $scope.update=update;
        $scope.paginate=['10','20','30','40','50','60','70','80'];
        $scope.pagination={};
        $scope.pagination.perPage=20;
        $scope.changeSearch=changeSearch;
        $scope.reportCreate=reportCreate;
        $scope.newReport={};
        $scope.getData={};
        isLogin();
        $scope.data={};
        $scope.emptyTable=emptyTable;
        $scope.validateKey=validateKey;
        $scope.onlineRegistration=onlineRegistration;
        function validateKey()
        {
            apiUtility.executeApi('GET','registration/isValid')
            .then(function(response){
                if(response.is_valid==true)
                {
                    ngDialog.open({ template: '/scripts/app/features/home/validateKey2.html',
                       scope:$scope
                   });
                }
                else
                {
                    ngDialog.open({ template: '/scripts/app/features/home/validateKey.html',
                       scope:$scope
                   });
                }
            },
            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }
        function onlineRegistration()
        {
            apiUtility.executeApi('GET','/generateKeys')    
            .then(function(response){
                apiUtility.executeApi('POST','generateRegistrationKey',response)    
                .then(function(response){
                    apiUtility.executeApi('POST','completeRegistration',response)    
                    .then(function(response){
                    },
                    function(errorObject){
                    });  
                },
                function(errorObject){
                });
apiUtility.executeApi('GET','http://demo.promedic.in/generateRegistrationKey?key1='+response.key1+'&key2='+response.key2)    
            .then(function(response){
            apiUtility.executeApi('POST','completeRegistration',response)    
            .then(function(response){
                    toastr.success(response.msg);
                    ngDialog.close();

            },
            function(errorObject){
                    toastr.error(response.msg); 
            });
         
            },
            function(errorObject){

            });
            },
            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }

        function mainDataSubmit()
        {
            apiUtility.executeApi('POST','addSuperAdmin',$scope.mainData)
            .then(function(response){  
            },
            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }

        function emptyTable()
        {
        }
        
        function send()
        {
          apiUtility.executeApi('GET','sendResetPasswordEmail?email_id='+$scope.getData.email_id)
          .then(function(response){
            if(response.success == false) 
            {
                toastr.info('Invalid Email! Please enter correct email');
            }

            else
            {
                toastr.success('Password Send Successfully to your email');
            }
            ngDialog.close(); 
            $state.go('login');
        });    
      }

      function reportCreate()
      {
          $scope.newReport.reportName="User";
          apiUtility.executeApi('POST','product/reporttoExcel',$scope.newReport)
          .then(function(response){            
            $scope.getData=response;
        },
        function(errorObject){
            $scope.errorMessage = errorObject.errorData.error.message;
        });

      }

      function changeSearch(str)
      {
        if(str=="")
           gettUser();
    }

   function forgotPassword()
   {
      $state.go('admin.forgotPassword');
  }

  function addForgotDialog()
  {
     ngDialog.open({ template: '/scripts/app/features/user/forgotPassword.html',
        controller: 'user' });
 }

 function isLogin()
 {
    apiUtility.executeApi('GET','isLogin')
    .then(function(response){
        if(response.success)
            $scope.isLoginData=response.data.user;
    },
    function(errorObject){
        $scope.errorMessage = errorObject.errorData.error.message;
    });
}

function update()
{
    $scope.data.active=1;
    apiUtility.executeApi('PUT','/api/v1/users/'+$scope.data.id,$scope.data)
    .then(function(response){
        toastr.success('Record Updated Successfully!');  
        $scope.user[$scope.data.id]=response.data;
    },
    function(errorObject){
        $scope.errorMessage = errorObject.errorData.error.message;
    });
    ngDialog.close();
}
function userEdit(userData)
{
    $scope.data=angular.copy(userData);
    ngDialog.open({ template: '/scripts/app/features/user/userEdit.html',
        scope: $scope  
    });
}

function saveUser(userId)
{

}

function search()
{
    $scope.currentPageNo=1;
    if($scope.str)
    {
        apiUtility.executeApi('GET','search/user/'+ $scope.str +'/?page=1'+'&limit='+$scope.pagination.perPage)
        .then(function(response){
          $scope.user=response.data;
          $scope.pagination=response.pagination;
      },
      function(errorObject){
        $scope.errorMessage = errorObject.errorData.error.message;
    });
    }
    else 
        gettUser(); 
}
/* user all function */
function setPage(pageNo){
    $scope.currentPageNo=pageNo;
    gettUser();
}

function getBranch()
{
   apiUtility.executeApi('GET','api/v1/branch')
   .then(function(response){
    $scope.branch=response.data;
},
function(errorObject){
    $scope.errorMessage = errorObject.errorData.error.message;
});
}

function addUser()
{
    $state.go('admin.userAdd');
}

function userAdd()
{
   $scope.newUser.branchId=$scope.isLoginData.selectBranchId;
   $scope.newUser.shopId=$scope.isLoginData.shopId;  
   apiUtility.executeApi('POST','api/v1/users',$scope.newUser)   
   .then(function(response){
    $scope.newUser='',
    toastr.success('Record Added Successfully!');  
    $state.go('admin.user');
    ngDialog.close();
}); 
   gettUser();
}
function addDialog()
{
    ngDialog.open({ template: '/scripts/app/features/user/userEdit.html',
        controller: 'user'});
}

function gettUser()
{
    apiUtility.executeApi('GET','api/v1/users?page='+$scope.currentPageNo+'&limit='+$scope.pagination.perPage)
    .then(function(response){
        $scope.user=response.data;
        $scope.pagination=response.pagination;
        if($scope.pagination.perPage==20 && $scope.pagination.total<20)
            $scope.pagination.perPage=10;
    },
    function(errorObject){
        $scope.errorMessage = errorObject.errorData.error.message;
    });
}

function deleteUser(userId)
{ 
    apiUtility.executeApi('DELETE','/api/v1/users/' + userId)
    .then(function(response)
    {
        delete $scope.user[userId];
        toastr.success('Record Deleted Successfully!');
    });
}

function login()
{
    var branch={};
    localStorage.setItem("alert", "1");
    apiUtility.executeApi('POST','login',{'username' : $scope.user.username, 'password' : $scope.user.password})
    .then(function(result){
        var data=result;
        localStorage.setItem("userData",JSON.stringify(result.user));
        userService.setUser(result.user);
        angular.forEach(result.user.UserBranchDetails, function(value, key) {
          $scope.counter++;
          branch=value;
      });
        if($scope.counter!=1){
            $state.go('userBranch');
        }
        else{
            userService.selectBrnach(branch);

            apiUtility.executeApi('GET','/api/v1/setBranchInSession/'+branch.id)
            .then(function (response) {
                if(response.success){
                    userService.selectBrnach(branch);
                    $state.go('home.addBill');
                }
            });
            $state.go('home.addBill');
        }       
    },
    function(errorObject){
        $scope.errorMessage = errorObject.errorData.error.message;
    });
}

function progressBar()
{
    $scope.progress = ngProgress.createInstance();
    $scope.progress.start();
}
}
})();
(function () {
    'use strict';

    angular
        .module('mediplus')
        .controller('admin', admin);

    admin.$inject = ['$scope', '$state','apiUtility','userService','toastr','ngDialog'];

    function admin($scope,$state,apiUtility,userService,toastr,ngDialog) {
        $scope.newAdmin={};
        $scope.user = userService.getUser();
        $scope.alertSet=alertSet;
        $scope.profileInfo=profileInfo;
        $scope.toggleSetting=toggleSetting;
        $scope.addAlert=addAlert;
        $scope.getAlert=getAlert;
        $scope.setValue=setValue;
        $scope.dig=dig;
        $scope.tempData={};
        $scope.alert={};
        $scope.alertDetails=0;
        $scope.settingsModel={
            settings:{
                app_setting1:true
            }
        };
 
        function init()
        {
           if(!$scope.user)
            apiUtility.executeApi('GET','/isLogin')
                    .then(function(response){

                       $scope.isLogin=response.data.user.selectBranchId;

                    }); 
        }      
                
        init();      

        function dig(data)
        {

        }

        function getAlert()
        {
           apiUtility.executeApi('GET','api/v1/alertDetails')
            .then(function(response){
                $scope.alert=response;
            },
            function(errorObject){
                $scope.errorMessage = errorObject.errorData.error.message;
            });
        }
        
        function setValue()
        {   
            $scope.alert[$scope.tempData.key]=true;
            $scope.alert[$scope.tempData.value]=$scope.tempData.newData;    
            apiUtility.executeApi('POST','/api/v1/updateAlertSettings/',$scope.alert)
            .then(function(response){
                toastr.success('Record Added Successfully!');
                    ngDialog.closeAll();
            },
            function(errorObject){
            });
        }

        function addAlert(newkey,newValue,data)
        {
            if(data==1)
            {
            $scope.tempData.key=newkey;
            $scope.tempData.value=newValue;

            $scope.tempData.newData=$scope.alert[$scope.tempData.value];
            ngDialog.open({ template: 'scripts/app/features/admin/alertData.html',
                scope:$scope 
            });    
            }
            else
            {
            $scope.alert[$scope.tempData.key]=false;
            $scope.alert[$scope.tempData.value]=0;
            apiUtility.executeApi('POST','/api/v1/updateAlertSettings/',$scope.alert)
            .then(function(response){
                toastr.success('Record Added Successfully!');
                    ngDialog.closeAll();
            },
            function(errorObject){

            });

            }
        }
        
        function toggleSetting(setting_name)
        {
            $scope.alert[setting_name]=!$scope.alert[setting_name];
        }

        function alertSet()
        {
          $state.go('admin.alert');
        }

        function profileInfo()
        {
            $state.go('admin.profile')
        }
}

})();
(function (){
  'use strict';

  angular
  .module('mediplus')
  .controller('pi', pi);

  pi.$inject = ['$scope', '$state','apiUtility','userService','$stateParams','toastr','ngDialog','$rootScope','hotkeys'];
  function pi($scope,$state,apiUtility,userService,$stateParams,toastr,ngDialog,$rootScope,hotkeys)
  {
    $scope.dataPi={
      invoiceItems:[],
      totalMrp:0,
      alertDate:new Date(),
      invoiceDate:new Date(),
      mrp:0,
      poStatus:1
    };
        $scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
        $scope.data = [300, 500, 100];
        $scope.toggle=1;
        $scope.toggleSet=toggleSet;
        $scope.bindex=0;        
        $scope.addTemp=addTemp;
        $scope.getProduct=getProduct;
        $scope.user=userService.getUser();
        $scope.paymentTypeStatus=paymentTypeStatus;
        $scope.getTotalAmount=getTotalAmount;
        $scope.dataPiI={
            batchNumber:0,
            expiryDate:"",
            mfgDate:"",
            mrp:0,
            product:{},
            purchasePrice:0,
            quantity:0,
            rsPu:0,
        temp:function(a){ 

          if(a.purchasePrice <= a.mrp)
          {
            a.rsPu=parseInt(a.quantity)*parseInt(a.purchasePrice);
          }
          else
          {
            toastr.info('Purchase Price should be less than MRP');
          }
        }
      };
      $scope.pay=['Online','Cheque','Debit','Credit'];
      $scope.paginate=['10','20','30','40','50','60','70','80'];        
      $scope.invoiceItemValue=[];
      $scope.deleteItems=deleteItems;
      $scope.showDetailInNew=showDetailInNew;
      $scope.getPurchaseInvoice=getPurchaseInvoice;
      $scope.getPiDetail=getPiDetail;
      $scope.editPurchaseOrderNo=editPurchaseOrderNo;
      $scope.editPurchaseOrderNoShow='show';
      $scope.addPi=addPi;
      $scope.addPiItems=addPiItems;
      $scope.piSubmit=piSubmit;
      $scope.validate=validate;
      $scope.search=search;
      $scope.str="";
      $scope.addPiValidated=addPiValidated;
      $scope.setPage=setPage;
      $scope.currentPageNo=1;
      $scope.pagination={};
      $scope.pagination.perPage=20;
      $scope.invoiceItemValue={};
      $scope.changeSearch=changeSearch;
      $scope.reportCreate=reportCreate;
      $scope.downloadDisable=downloadDisable;
      $scope.newReport={};
      $scope.getData={};
      $scope.fromDate = {
        maxDate: 'y',
        dateFormat: 'dd-mm-yy',
      };
      
      $scope.toDate = {
        maxDate: 'y',
        dateFormat: 'dd-mm-yy',
      };
      
      $scope.mfgDate = {
        maxDate: 'y',
        dateFormat: 'dd-mm-yy',
      };
      
      $scope.expiryDate = {
        changeYear: true,
        changeMonth: true,
        yearRange: '1900:+9',
        minDate: '+1d',
             dateFormat: 'dd-mm-yy',
           }; 
        $scope.dateOptions = {
          changeYear: true,
          changeMonth: true,
          yearRange: '1900:-0',
          dateFormat: 'dd-mm-yy',    
        };

        $scope.newPi={
          batchNumber:"",
          quantity:"",
          expiryDate:"",
          mfgDate:"",
          purchasePrice:"",
          sellingPrice:"",
          MRP:"",
          sellDiscount:"",
          purchaseDiscount:"",
          margin:"",
          totalAmount:"",
          amountPaid:"",
          paymentType:"",
          chequeNumber:"",
          freeItem:"",
          productId:"",
          invoiceId:""
        }
        $scope.piDetail={
          user:userService.getUser(),
          branch:{
            id:""
          },
          distributor:{
            id:""
          },
          InvoiceItems:
          [{
            batchNumber:"",
            quantity:"",
            expiryDate:"",
            mfgDate:"",
            purchasePrice:"",
            sellingPrice:"",
            MRP:"",
            sellDiscount:"",
            purchaseDiscount:"",
            margin:"",
            totalAmount:"",
            amountPaid:"",
            paymentType:"",
            chequeNumber:"",
            freeItem:"",
            productId:"",
            invoiceId:"",
          }],
        }
        $scope.$watch('dataPi',function(bnew,bold){
        });
        
        function toggleSet(data)
        {
          $scope.toggle=data;
          $scope.dataPi.poStatus=data;
        }
        
        function editPurchaseOrderNo()
        {
          $scope.dataPi={
          invoiceItems:[],
          totalMrp:0,
          alertDate:new Date(),
          invoiceDate:new Date(),
          mrp:0,
          totalAmount:0
          };
        }
        
        function init()
        {
          hotkeys.add({combo: 'a',description: 'blah blah',callback: function() {addPi();}});
          hotkeys.add({combo: 's',description: 'Description goes here',callback: function() {document.getElementById("search").focus();}});
        }
        init();
        
        function reportCreate()
        {
          $scope.newReport.reportName="Invoice";
          apiUtility.executeApi('POST','product/reporttoExcel',$scope.newReport)
          .then(function(response){
            $scope.getData=response;
          },
          function(errorObject){
            $scope.errorMessage = errorObject.errorData.error.message;
          }); 
        }

        function downloadDisable()
        {
          window.location.assign("/product/reporttoExcel?fromDate="+$scope.newReport.fromDate+"&toDate="+$scope.newReport.toDate+"&reportName=Invoice")
        }

        function getProduct(id)
        {
          apiUtility.executeApi('GET','/api/v1/product/'+id)
          .then(function(response){
            $scope.piDetail.invoiceItems.product=response.data.dosage;
          },
          function(errorObject)
          {
            $scope.errorMessage = errorObject.errorData.error.message;
          });
        }

        function addTemp(data)
        {
          $scope.dataPi.userId=$scope.user.id;
          $scope.dataPi.purchaseOrderId= $scope.dataPi.po.originalObject.id;  
          $scope.dataPi.invoiceItems.push(data);
        }
        
        function paymentTypeStatus(data)
        {
          if(data=="cheque")
            return true;
          else
          {
            $scope.dataPi.chequeNumber="";
            return false;
          }
        }
        
        function changeSearch(str)
        {
          if(str=="")
            getPurchaseInvoice();
        }
        $scope.$watch("newPi.product", function(newValue, oldValue) {

          addPiValidated();
        });
        $scope.$watch("dataPi.po", function(newValue, oldValue) {

          if (newValue!=undefined) 
          {
            $scope.showPi=true;
          };  
        });
        
        function getTotalAmount()
        {
          var total=0;
          angular.forEach($scope.dataPi.invoiceItems,function(value) {           
            total+=Number(value.rsPu)?Number(value.rsPu):0;
            $scope.dataPi.totalAmount=total;
          }, this);
          return total;
        }
        
        function search(str)
        { 
          $scope.currentPageNo=1;
          if(str)
          {
            apiUtility.executeApi('GET','search/invoice/'+ str +'/?page='+$scope.currentPageNo+'&limit='+$scope.pagination.perPage)
            .then(function(response){   
              $scope.pi=response.data;
              $scope.pagination=response.pagination;
            },
            function(errorObject){
              $scope.errorMessage = errorObject.errorData.error.message;
            });
          }
          else 
            getPurchaseInvoice();
        }

        function setPage(pageNo)
        {
          $scope.currentPageNo=pageNo;
          getPurchaseInvoice();
        }

        function getPurchaseInvoice()
        {
          if($state.$current.toString()=='admin.pi')
           apiUtility.executeApi('GET','/api/v1/invoice?page='+$scope.currentPageNo+'&limit='+$scope.pagination.perPage)
         .then(function(response){
          $scope.pi=response.data;
          $scope.pagination=response.pagination;
          if($scope.pagination.perPage==20 && $scope.pagination.total<20)
                    $scope.pagination.perPage=10;
        },
        
        function(errorObject)
        {
          $scope.errorMessage = errorObject.errorData.error.message;
        });
       }

       function showDetailInNew(piId)
       {
          window.open('#/admin/purchaseInvoice/detail/'+piId);
        }

        function addPi()
        {
          $state.go('admin.pi.addPi');
        }

        function getPiDetail()
        {
          if($stateParams.piId){
            $scope.isReadOnly=true;
            apiUtility.executeApi('GET','api/v1/invoice/'+ $stateParams.piId)
            .then(function(response){
            $scope.piDetail= response.data;
            $scope.dataPi=response.data;
            $scope.piDetail.id=$stateParams.piId;
            });
          }  
          else 
          {
            $scope.isReadOnly=false;
          }
        }

        function addPiItems()
        {          
          if($scope.dataPi.batchNumber > 0 )
          {
            toastr.error('enter batchNumber'); 
            return; 
          }

          if($scope.dataPiI.expiryDate == "")
          {
            toastr.error('enter expiryDate'); 
            return; 
          }
          
          if($scope.dataPiI.mfgDate =="" )
          {
            toastr.error('enter mfgDate'); 
            return;  
          }
          
          if($scope.dataPiI.mrp == 0  )
          {
            toastr.error('enter mrp'); 
            return; 
          }
          
          if($scope.dataPiI.purchasePrice == 0 )
          {
            toastr.error('enter purchasePrice'); 
            return; 
          }
                    
          if($scope.dataPiI.quantity == 0 )
          {
            toastr.error('enter quantity'); 
            return; 
          }
          $scope.dataPi.userId=$scope.user.id;
          if($scope.dataPiI.product.originalObject.id)
            $scope.dataPiI.productId=$scope.dataPiI.product.originalObject.id;
          if($scope.dataPi.po.originalObject.id)
            $scope.dataPi.purchaseOrderId= $scope.dataPi.po.originalObject.id;
            $scope.dataPiI.productName=$scope.dataPiI.product.originalObject.name;
            $scope.dataPi.invoiceItems.push($scope.dataPiI);
        $scope.dataPiI={
            batchNumber:0,
            expiryDate:"",
            mfgDate:"",
            mrp:0,
            product:{},
            purchasePrice:0,
            quantity:0,
            rsPu:0,
        temp:function(a){ 

          if(a.purchasePrice <= a.mrp)
          {
            a.rsPu=parseInt(a.quantity)*parseInt(a.purchasePrice);
          }
          else
          {
            toastr.info('Purchase Price should be less than MRP');
          }
        }
      };
       $rootScope.$broadcast('changeText',{});
    }
                       
    function deleteItems(index)
    {               
     if($scope.dataPi.po.originalObject.purchaseOrderItem)
      $scope.dataPi.po.originalObject.purchaseOrderItem.splice(index,1);
    $scope.dataPi.invoiceItems.splice(index,1);

    }
    function piSubmit()
    {
      if($scope.dataPi.parcelNumber == undefined)
      {
        toastr.error('Enter Parcel Number');
        return; 
      }
      if($scope.dataPi.invoiceItems.length < 0)
      {
        toastr.error('Enter Invoice Items');
        return; 
      }
      apiUtility.executeApi('POST','api/v1/invoice/',$scope.dataPi)
      .then(function(response){
        if(response.msg=="record not sufficient")
        {
         $state.go('admin.pi');
         toastr.warning('Please fill all details!', 'Warning');
       }
       else
       {
         $scope.piDetail= response.data;
         $state.go('admin.pi');
         toastr.success('Form Submitted Successfully!');
       } 
     });
    }
    
    function validate()
    {
               
    }
              
    function addPiValidated()
    {
      return (!($scope.newPi.batchNumber==undefined || $scope.newPi.batchNumber=='' || $scope.newPi.batchNumber==null) 
        && !($scope.newPi.quantity==undefined || $scope.newPi.quantity=='' || $scope.newPi.quantity==null)
        && !($scope.newReport.fromDate==undefined || $scope.newReport.fromDate=='' || $scope.newReport.fromDate==null)
        && !($scope.newReport.toDate==undefined || $scope.newReport.toDate=='' || $scope.newReport.toDate==null)
        && !($scope.newPi.expiryDate==undefined || $scope.newPi.expiryDate=='' || $scope.newPi.expiryDate==null)
        && !($scope.newPi.mfgDate==undefined || $scope.newPi.mfgDate=='' || $scope.newPi.mfgDate==null)
        && !($scope.newPi.purchasePrice==undefined || $scope.newPi.purchasePrice=='' || $scope.newPi.purchasePrice==null)
        && !($scope.newPi.MRP==undefined || $scope.newPi.MRP=='' || $scope.newPi.MRP==null)
        && !($scope.newPi.sellDiscount==undefined || $scope.newPi.sellDiscount=='' || $scope.newPi.sellDiscount==null)
        && !($scope.newPi.purchaseDiscount==undefined || $scope.newPi.purchaseDiscount=='' || $scope.newPi.purchaseDiscount==null)
        && !($scope.newPi.sellingPrice==undefined || $scope.newPi.sellingPrice=='' || $scope.newPi.sellingPrice==null)
        && !($scope.newPi.margin==undefined || $scope.newPi.margin=='' || $scope.newPi.margin==null));

    }
  }
})();
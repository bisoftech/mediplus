  (function(){
     initialSetup();
    function initialSetup()
    {
       var initInjector = angular.injector(["ng"]);
       var $http = initInjector.get("$http");
       var $state = initInjector.get["$state"];
       //make api calls using $hhtp
       //store results in local storage
       //bootstrap angular manually
       
         $http.get('/user/emptyTable')
              .then(function(response){
              
                if(response.data.success==false)
                {
                  localStorage.setItem("isEmptyTable",'false');
                  angular.element(document).ready(function() {
                    angular.bootstrap(document, ["mediplus"]);
                 });     
                }
                else 
                {
                  localStorage.setItem("isEmptyTable",'true');
                  $http.get('isLogin')
                  .then(function(result){
                    if(result.data.success){
                     localStorage.setItem("userData",JSON.stringify(result.data.data));
                   } else {
                     localStorage.setItem("userData",JSON.stringify({}));
                   }
                   angular.element(document).ready(function() {
                    angular.bootstrap(document, ["mediplus"]);
                  });
                 });
                }
              });
       //In run method, access data from session storage and then supply it to suitable services (eg. user service)   
    }
     
    angular.module('mediplus',[
          'focus-if',  
          'ui.router',
          'angucomplete',
          'ngAnimate',
          'angular-spinner',
          'toastr',
          'ui.bootstrap',
          'ngDialog',
          'ui.date',
          'ngSanitize', 
          'ngCsv',
          'angularjs-dropdown-multiselect',
          'ui.select',
          'highcharts-ng',
          'cfp.hotkeys',
    ])
  .config(function(toastrConfig) {
          angular.extend(toastrConfig, {
              allowHtml: true,
              autoDismiss: false,
              closeButton: false,
              closeHtml: '<button>&times;</button>',
              containerId: 'toast-container',
              extendedTimeOut: 1000,
              iconClasses: {
                  error: 'toast-error',
                  info: 'toast-info',
                  success: 'toast-success',
                  warning: 'toast-warning'
              },
              maxOpened: 0,
              messageClass: 'toast-message',
              newestOnTop: true,
              onHidden: null,
              onShown: null,
              positionClass: 'toast-bottom-right',
              preventDuplicates: false,
              preventOpenDuplicates: false,
              progressBar: false,
              tapToDismiss: true,
              target: 'body',
              templates: {
                  toast: 'directives/toast/toast.html',
                  progressbar: 'directives/progressbar/progressbar.html'
              },
              timeOut: 5000,
              titleClass: 'toast-title',
              toastClass: 'toast'
          });
      })
  .filter('propsFilter', function() {
          return function(items, props) {
            var out = [];
            if (angular.isArray(items)) {
              items.forEach(function(item) {
                var itemMatches = false;
                var keys = Object.keys(props);
                for (var i = 0; i < keys.length; i++) {
                  var prop = keys[i];
                  var text = props[prop].toLowerCase();
                  if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                    itemMatches = true;
                    break;
                  }
                }
                if (itemMatches) {
                  out.push(item);
                }
              });
            } else {
              // Let the output be the input untouched
              out = items;
            }
            return out;
          };
        }).config(['$provide', '$stateProvider', '$urlRouterProvider','apiUtilityProvider',
            function ($provide, $stateProvider, $urlRouterProvider,apiUtilityProvider) {
              $provide.value('apiRoot',document.getElementById("baseUrl").href);
              $provide.value('token',''); 
              var apiUtility = apiUtilityProvider.$get();
              apiUtility.executeApi('GET','token')
              .then(function(result){
                  $provide.value('token',result);
              });
              $urlRouterProvider.when("", "home/addBill");
              $urlRouterProvider.when("/", "home/addBill");
              $urlRouterProvider.otherwise('/error');
              $stateProvider.state('login', {
                    url: '/login',
                    templateUrl: 'scripts/app/features/user/login.html',
                    controller: 'user'
                })
                  .state('admin.alert',{
                  url: '/alert',
                  templateUrl: 'scripts/app/features/admin/alert.html',
                  controller:'admin',
                  menu:'F1'
                })
              .state('admin.alertNotification',{
                  url: '/alertNotification',
                  templateUrl: 'scripts/app/features/alert/alertNotification.html',
                  controller:'alert',
                  menu:'F1'
                })
                .state('profile',{
                  url: '/profileInfo',
                  templateUrl: 'scripts/app/features/admin/profile.html',
                  controller: 'admin'
                })
                .state('error',{
                    url: '/error',
                    templateUrl: 'scripts/app/features/user/Error.html'
                })
                  .state('userBranch',{
                      url:'/userBranch',
                      templateUrl:'scripts/app/features/userBranch/branch.html',
                      controller:'userBranchController'
                  })
                .state('home', {
                    url: '/home',
                    templateUrl: 'scripts/app/features/home/home.html',
                    controller: 'home'
                })  
                .state('home.addBill', {
                    url: '/addBill',
                    templateUrl: 'scripts/app/features/home/addBill.html',
                })   
              .state('home.print',{
                    url: '/print/:id',
                    templateUrl: 'scripts/app/features/home/print.html',
                }) 
                .state('admin', {
                    url: '/admin',
                    templateUrl: 'scripts/app/features/admin/admin.html',
                    controller: 'admin',
                    menu:'F2'
                })
                .state('admin.products', {
                    url: '/products',
                    templateUrl: 'scripts/app/features/products/products.html',
                    controller: 'products',
                    menu:'1'
                })
                .state('admin.distributors', {
                    url: '/distributors',
                    templateUrl: 'scripts/app/features/distributors/distributors.html',
                    controller: 'distributors',
                      parent:'admin',
                      menu:'2'
                })
                .state('admin.editDistributor',{
                    url:'/editDistributor',
                    templateUrl:'/scripts/app/features/distributors/editDistributor.html',
                    controller: 'customer',
                  menu:'2'
                })   
                .state('admin.po',{
                    url:'/purchaseOrder',
                    templateUrl:'scripts/app/features/PurchaseOrder/po.html',
                    controller:'po',
                    menu:'3'
                })  
                .state('admin.po.detail',{
                    url:'/detail/:poId',
                    templateUrl:'scripts/app/features/PurchaseOrder/poDetail.html',
                    controller:'po',
                    menu:'3'
                })
                .state('admin.po.poPrint',{
                    url:'/print/:poId',
                    templateUrl:'scripts/app/features/PurchaseOrder/poPrint.html',
                    controller:'po',
                    menu:'3'
                })
                .state('admin.po.addPo',{
                    url:'/addPo',
                    templateUrl:'scripts/app/features/PurchaseOrder/poDetail.html',
                    controller:'po',
                    menu:'3'
                })
                .state('admin.pi',{
                    url:'/purchaseInvoice',
                    templateUrl:'scripts/app/features/PurchaseInvoice/pi.html',
                    controller:'pi',
                    menu:'4'
                })
                .state('admin.pi.detail',{
                  url:'/detail/:piId',
                  templateUrl:'scripts/app/features/PurchaseInvoice/piDetail.html',
                  controller: 'pi',
                    menu:'4'                    
                }) 
                .state('admin.pi.addPi',{
                  url:'/addPi',
                  templateUrl: 'scripts/app/features/PurchaseInvoice/piDetail.html',
                  controller:'pi',
                    menu:'4'
                })
                .state('admin.bank',{
                  url:'/bank',
                  templateUrl: 'scripts/app/features/bank/Bank.html',
                  controller: 'bank',
                    menu:'5'
                })
                .state('admin.addBank',{
                  url:'/addBank',
                  templateUrl:'scripts/app/features/bank/addBank.html',
                  controller: 'bank',
                    menu:'5'
                }) 
              .state('admin.user',{
                  url:'/user',
                  templateUrl:'/scripts/app/features/user/user.html',
                  controller: 'user',
                    menu:'6'
                })
                .state('admin.forgotPassword',{
                  url: '/forgotPassword',
                  templateUrl:'/scripts/app/features/user/forgotPassword.html',
                  controller: 'user'
              })
              .state('admin.userEdit',{
                  url:'/userEdit',
                  templateUrl:'/scripts/app/features/user/userEdit.html',
                  controller: 'user',
                    menu:'6'
                })
                .state('admin.userAdd',{
                  url:'/userAdd',
                  templateUrl:'/scripts/app/features/user/userAdd.html',
                  controller: 'user',
                    menu:'6'
                })
                .state('admin.superAdminAdd',{
                  url:'/superAdminAdd',
                  templateUrl:'/scripts/app/features/user/superAdminAdd.html',
                  controller: 'user'
                }) 
              .state('admin.customer',{
                    url:'/customer',
                    templateUrl:'/scripts/app/features/customer/customer.html',
                    controller: 'customer',
                    menu:'8'
                })
              .state('admin.report',{
                    url:'/report',
                    templateUrl:'/scripts/app/features/report/report.html',
                    controller: 'report',
                    menu:'9'
                })
              .state('admin.report.print',{
                    url:'/reportPrint',
                    templateUrl:'/scripts/app/features/report/reportPrint.html',
                    menu:'9'
                })
                .state('admin.addCustomer',{ 
                    url:'/addCustomer',
                    templateUrl:'/scripts/app/features/customer/addCustomer.html',
                    controller: 'customer',
                    menu:'8'
                })           
                .state('admin.editCustomer',{ 
                    url:'/editCustomer',
                    templateUrl:'/scripts/app/features/customer/editCustomer.html',
                    controller: 'customer',
                    menu:'8'
                }) 
                .state('admin.returnn',{
                    url:'/return',
                    templateUrl:'/scripts/app/features/Return/return.html',
                    controller: 'returnn',
                    menu:'7'
                })   
                .state('admin.stock', {
                    url: '/stock',
                    templateUrl: '/scripts/app/features/stock/stock.html',
                    controller: 'stock',
                    menu:'0'
                })
                .state('admin.addStock', {
                    url: '/addStock',
                    templateUrl: '/scripts/app/features/stock/addStock.html',
                    controller: 'stock',
                    menu:'0'
                })  
                .state('admin.returnn.returnAdd',{
                  url:'/returnAdd/:id',
                  templateUrl:'/scripts/app/features/Return/returnAdd.html',
                  controller: 'returnn',
                    menu:'7'
                })
                .state('admin.returnn.returnDetails',{
                      url:'/returnDetails/:id',
                      templateUrl:'/scripts/app/features/Return/returnDetails.html',
                      controller: 'returnn',
                      menu:'7'
                });
              
          function open(size) {
  
              var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                size: size,
                resolve: {
                  items: function () {
                  return $scope.items;
                }
              }
            });
  
          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
          }, function () {
            $log.info('Modal dismissed at: ' + new Date());
          });
        };
      }
      ])
  .run(['apiUtility','$state','userService','$rootScope',
          function(apiUtility,$state,userService,$rootScope){
        $rootScope.$on('$stateChangeSuccess', function(event, to, toParams, from, fromParams) {
              localStorage.setItem("state", from.name);
              localStorage.setItem("currentState", to.name);
              localStorage.setItem("isAlertDisplayed", 0);
              if(localStorage.getItem("isEmptyTable") == 'false' && to.name!='admin.superAdminAdd' ){
                $state.go('admin.superAdminAdd');
              }
              else 
              {
                if(((angular.equals("{}", localStorage.getItem("userData"))) && to.name!='login') &&  !( localStorage.getItem("isEmptyTable") == 'false' && to.name=='admin.superAdminAdd') ) 
                {
                  $state.go('login');
                }
              }
          });
      
                if(localStorage.getItem("isEmptyTable") == 'false')
                      $state.go('admin.superAdminAdd');
                else 
                {
                    if(!(angular.equals("{}", localStorage.getItem("userData")))) {
                        userData = JSON.parse(localStorage.getItem("userData"));
                        userService.setUser(userData.user);
                        var selectedBranch=null;
                        angular.forEach(userData.selectedBranch, function(value, key) {
                          selectedBranch=value;
                        });
      
                        if(selectedBranch){
                          userService.selectBrnach(selectedBranch);
                        }
                        else
                            $state.go('userBranch');                          
                        $rootScope.userServiceRef = userService;
                        $rootScope.$on('$stateChangeStart', 
                        function(event, toState, toParams, fromState, fromParams){ 
                            if(!selectedBranch)
                            $state.go('userBranch');
                            if(!userService.hasAccess(toState.name,'stateTranstion')){
                                event.preventDefault();                
                            }
                        });
                    }
                }
          }
      ])
  .filter('range', function() {
    return function(input, total) {
      total = parseInt(total);
      for (var i=0; i<total; i++)
        input.push(i);
      return input;
    };
  })
  .filter('orderObjectBy', function() {
    return function(items, field, reverse) {
      var filtered = [];
      angular.forEach(items, function(item) {
        filtered.push(item);
      });
      filtered.sort(function (a, b) {
        return (a!==null && b!=null && a[field]!=undefined && b[field]!=undefined)? (a[field] > b[field] ? 1 :-1):-1;
      });
      if(reverse) filtered.reverse();
      return filtered;
    };
  });
  Object.size = function(obj) {
      var size = 0, key;
      for (key in obj) {
          if (obj.hasOwnProperty(key)) size++;
      }
      return size;
  };
  })();

(function(){
    'use strict';

    angular
        .module('mediplus')
        .service('returnService', returnService);

    returnService.$inject = [];

    function returnService() {
        this.returnPurchase = {
            selectedItem: {},
            selectedItemIndex: ''
        };
        this.selectItem = selectItem;
        this.selectItemIndex = selectItemIndex;
        this.getSelectedItem = getSelectedItem;
        this.getSelectedItemIndex = getSelectedItemIndex;


        function selectItem(item) {
            this.returnPurchase.selectedItem = item;
        }

        function selectItemIndex(index) {
            this.returnPurchase.selectedItemIndex = index;
        }

        function getSelectedItem() {
            return this.returnPurchase.selectedItem;
        }

        function getSelectedItemIndex() {
            return this.returnPurchase.selectedItemIndex
        }
    }

})();

(function(){
    'use strict';
//DB settings for spinner 
     angular
        .module('mediplus')
        .service('apiUtility', apiUtility);

        apiUtility.$inject = ['$q','$http','apiRoot','token','toastr','spinner','$state'];
        function apiUtility($q, $http,apiRoot,token,toastr,spinner,$state)
        {
            var cache = {};

            this.executeApi = executeApi;

            function executeApi(method,url,requestData,cacheKey,headersObject)
            {

//spinner show
spinner.start('mainBody');
                if(method == 'POST')
                {
                    var headersObject = {};
                    headersObject['X-CSRF-TOKEN'] = token;
                }

                var deferred = $q.defer();

                if(cacheKey && cache[cacheKey])
                {
                    deferred.resolve(cache[cacheKey]);
                }
                else
                {
                    $http({
                    method: method,
                    url: url,
                    headers: headersObject, // it takes object as value
                    data: requestData,
                    })
                    .success(function(data){
//spinner.hide
 spinner.stop('mainBody');
                        if (cacheKey) 
                            cache[cacheKey] = data;
                        
                        deferred.resolve(data);

                    })
                    .error(function(data, status) {
    spinner.stop('mainBody');
                        switch (status) {
                    
                            case 400:
                           if(data.error!=undefined && data.error.message!=undefined && data.error.message!='')   
                            toastr.error(data.error.message)
                        else
                            toastr.error('The web page cannot be found', 'Error');
                            break;

                            case 401:
                           if(data.error!=undefined && data.error.message!=undefined && data.error.message!='')   
                            toastr.error(data.error.message)
                        else
                          toastr.error('Authorization Required', 'Error');
                            break;

                            case 403:
                             if(data.error!=undefined && data.error.message!=undefined && data.error.message!='')   
                            toastr.error(data.error.message)
                        else
                            toastr.error('You do not have permission to access', 'Error');
                            
                            break;

                             case 498:
                             if(data.error!=undefined && data.error.message!=undefined && data.error.message!='')   
                            toastr.error(data.error.message)
                        else
                            toastr.error('Token Mismatch', 'Error');
                            break;

                            case 404:
                             if(data.error!=undefined && data.error.message!=undefined && data.error.message!='')   
                            toastr.error(data.error.message)
                        else
                            toastr.error('Login again to access the features of Mediplus','Session Expired');
                                $state.go('login');
                                break;

                            default:
                         if(data.error!=undefined && data.error.message!=undefined && data.error.message!='')   
                            toastr.error(data.error.message)
                        else
                             toastr.error('HTTP Error 500 Internal Server Error' + status.message, 'Error');


                        }
                         deferred.reject({errorData : data, errorCode : status});

                    });
                }

                return deferred.promise;
            }

        }

})();



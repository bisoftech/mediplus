(function(){
    'use strict';

     angular
        .module('mediplus')
        .service('userService', userService);

        userService.$inject = ['$q','$http','hotkeys','$state','ngDialog'];

        function userService($q, $http,hotkeys,$state,ngDialog)
        {
            this.user ={
                menu:''
            };
            this.flag = 1;
            this.sgFlag=sgFlag;
            this.setMenu=setMenu;
            this.getMenu=getMenu;
            this.userBranches=null;
            this.selectedBranch=null;
            this.setUser = setUser;
            this.getUser = getUser;
            this.selectBrnach = selectBrnach;
            this.getSelectedBranch = getSelectedBranch;
            this.getAllBranches=getAllBranches;
            this.hasAccess = hasAccess;
            this.alphanumeric=alphanumeric;
            this.alphabet=alphabet;
            this.number=number;
            this.isLogin=isLogin;

            function isLogin(){
                if(this.user=={} || this.user==undefined)
                    return false;
                return true;
            }
            function alphanumeric(n,data)

                {
                //var re= new RegExp("^[a-zA-Z0-9]{"+n+"}$");
                var re= new RegExp("^[a-zA-Z][a-zA-Z\\s]+$");
                var num=0;
                if(re.test(data))
                {
                num =data.length;
                return num;
                }
                else
                {
                    return num;
                }
                }
            function alphabet(data)
                {
                var re= new RegExp("^[a-zA-Z][a-zA-Z\\s]+$");
                return re.test(data);
                }
            function number(n,data){
                var re = new RegExp("^[0-9]{"+n+"}$");
                return re.test(data);
                }
            function sgFlag(value,sg)
            {
                if(sg==0)
                    this.flag==1;
                if(sg==1)
                    return this.flag;
            }
            function setMenu(id)
            {
                this.user.menu=id;
            }
            function getMenu(id)
            {
                return this.user.menu;
                
            }
            function setUser(user){
                this.user = user;
                if(this.user.menu==undefined || this.user.menu==null)
                    this.user.menu='F2';
                if(Object.size(user.UserBranchDetails)==1){
                        this.selectBrnach(user.UserBranchDetails[Object.keys(user.UserBranchDetails)]);
                }
            }
                    hotkeys.add({
                    combo: '1',
                    description: 'This one goes to 11',
                    callback: function() {
                          $state.go('admin.products');
                    }
                  });
                   hotkeys.add({
                    combo: '2',
                    description: 'This one goes to 11',
                    callback: function() {
                          $state.go('admin.distributors');
                    }
                  });
                   hotkeys.add({
                    combo: '3',
                    description: 'This one goes to 11',
                    callback: function() {
                          $state.go('admin.po.addPo');
                    }
                  });
                    hotkeys.add({
                    combo: '4',
                    description: 'This one goes to 11',
                    callback: function() {
                          $state.go('admin.pi.addPi');
                    }
                  });
                hotkeys.add({
                    combo: '5',
                    description: 'This one goes to 11',
                    callback: function() {
                          $state.go('admin.addBank');
                    }
                  });
               hotkeys.add({
                    combo: '6',
                    description: 'This one goes to 11',
                    callback: function() {
                          $state.go('admin.userAdd');
                    }
                  });
                     hotkeys.add({
                    combo: '7',
                    description: 'This one goes to 11',
                    callback: function() {
                          $state.go('admin.returnn');
                    }
                  });
                 hotkeys.add({
                    combo: '8',
                    description: 'This one goes to 11',
                    callback: function() {
                          $state.go('admin.addCustomer');
                    }
                  });
                hotkeys.add({
                    combo: '9',
                    description: 'This one goes to 11',
                    callback: function() {
                          $state.go('admin.report');
                    }
                  });
             hotkeys.add({
                    combo: '0',
                    description: 'This one goes to 11',
                    callback: function() {
                          $state.go('admin.stock');
                    }
                  });
            hotkeys.add({
                    combo: 'F1',
                    description: 'This one goes to 11',
                    callback: function() {
                          $state.go('admin.alertNotification');
                    }
                  });

            hotkeys.add({
                    combo: 'F2',
                    description: 'This one goes to 11',
                    callback: function() {
                          $state.go('admin');
                    }
                  });
            function getUser(){

                return this.user;
            }

            function getAllBranches(){
                return this.userBranches;
            }

            function getSelectedBranch(){
                return this.selectedBranch;
            }


            function selectBrnach(branch){
                this.selectedBranch=branch;
            }

            function hasAccess(bObject,action)
            {

                if(this.user == null)
                {
                    return false;
                }

                if(action == 'stateTranstion')
                {
                    var states = bObject.split('.');
                    if(states[0] == 'home' || states[0] == 'login' || states[0] == 'admin' )
                    return true;
                }

                switch(this.user.role){
                        case 'super_admin':
                            return true;
                            break;

                        case 'admin':
                            return evaluateAccessForAdmin(bObject,action)
                            break;

                        case 'pharmacist':
                            return evaluateAccessForPharmacist(bObject,action)
                            break;

                        case 'employee':
                            return evaluateAccessForEmployee(bObject,action)
                            break;

                        default:
                            return false;
                            break;
                }
            }

            function evaluateAccessForAdmin(bObject,action)
            {
                 
                    switch(action)
                    {
                        case 'stateTranstion':
                            var states = bObject.split('.');
                            if(states[0] == 'admin')
                               return true;
                            break;

                        default:
                            if(
                            'products' == bObject 
                            || 'distributors' == bObject
                            || 'po' == bObject
                            || 'pi' == bObject
                            || 'customer' == bObject)
                                return true;
                            else
                                return false;
                        
                    }

            }
            /*
products
distributors
po
pi
admin
user
returnn
customer*/
            function evaluateAccessForEmployee(bObject,action)
            {
                    
                   switch(action)
                    {
                        case 'stateTranstion':
                           var states = bObject.split('.');
                            if(states[0] == 'admin')
                               return false;
                            if(states[0] == 'userBranch')
                               return true;
                            break;

                        default:
                            if('customer' == bObject)
                                return true;
                            else
                            return false;
                    }
                    
             
            }
           
        }

})();

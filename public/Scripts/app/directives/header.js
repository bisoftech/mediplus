(function () {
    'use strict';

    angular
        .module('mediplus')
        .directive('header', header);

   header.$inject = ['$state','userService','$interval'];

    function header($state,userService,$interval) {
           var directive = {};
   directive.restrict = 'E';
   
   directive.templateUrl = "scripts/app/directives/header.html";

    directive.compile = function(element, attributes) { 

      var linkFunction = function($scope,element, attributes) {
        $scope.CurrentDate= new Date();
          $interval(function(){$scope.CurrentDate= new Date();},1000);
          $scope.setMenu=setMenu;
          $scope.headerModule={
              isHomeSection:false,
              isAdminSection:false,
              isBranchSection:false,
              isProfileSection:false,
              user:{}
          }
          function getParentList(state) {
              var parentList = [];
              var state = state.parent;
              while(state) {
                  parentList.push(state.toString());
                  state = state.parent;
              }
              return parentList;
          }

          function setMenu(id){
            var currentMenu=userService.getMenu()
            if(currentMenu==id){
              $state.reload();
            } else {
              userService.setMenu(id);
            }
          }

          var parents = getParentList($state.$current);

          if(parents.indexOf('home')>=0 || ($state.current.name=="home"))
              $scope.headerModule.isHomeSection=true;

          if(parents.indexOf('admin')>=0 || ($state.current.name=="admin"))
              $scope.headerModule.isAdminSection=true;

          if(parents.indexOf('profile')>=0 || ($state.current.name=="profile"))
              $scope.headerModule.isProfileSection=true;


          if(parents.indexOf('userBranch')>=0 || ($state.current.name=="userBranch"))
              $scope.headerModule.isBranchSection=true;

          $scope.headerModule.user=userService.getUser();
          $scope.headerModule.user.menu=userService.getMenu();

          if($scope.headerModule.user.menu==''  && $state.current.menu==undefined)
            $scope.headerModule.user.menu='F2';
          if( $state.current.menu!=undefined)
             $scope.headerModule.user.menu=$state.current.menu;

           userService.setMenu($scope.headerModule.user.menu);

          $scope.branch=userService.getSelectedBranch();  
      }
      return linkFunction;
   }
   return directive;
}

})();
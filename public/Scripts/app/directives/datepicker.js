(function () {
    'use strict';

    angular
        .module('mediplus')
        .directive('datepicker', datepicker);

   datepicker.$inject = [ '$state','userService'];

    function datepicker($state,userService) {

           var directive = {};
   
   //restrict = E, signifies that directive is Element directive
   directive.restrict = 'E';
   
   directive.templateUrl = "scripts/app/directives/datepicker.html";

    directive.compile = function(element, attributes) {

      var linkFunction = function($scope,element, attributes) {

          function getParentList(state) {
              var parentList = [];
              var state = state.parent;
              while(state) {
                  parentList.push(state.toString());
                  state = state.parent;
              }
              return parentList;
          }
      }
      return linkFunction;
   }
   return directive;
}
})();
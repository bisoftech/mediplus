<!DOCTYPE html>
<html>
    <head>
        <!-- Third Party --> 
        
        <script type="text/javascript" src="scripts/third-party/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="scripts/third-party/angular.js"></script>
        <script type="text/javascript" src="scripts/third-party/angular-ui-router.js"></script>
        <script type="text/javascript" src="scripts/third-party/xeditable.js"></script>
        <script type="text/javascript" src="scripts/third-party/xeditable.min.js"></script>
        <script type="text/javascript" src="scripts/third-party/spin.min.js"></script>
        <script type="text/javascript" src="scripts/third-party/angular-spinner.js"></script>
        <script type="text/javascript" src="scripts/third-party/ui-bootstrap-tpls-0.13.3.min.js"></script>
        <script type="text/javascript" src="scripts/third-party/angular-animate.min.js"></script>
        <script type="text/javascript" src="scripts/third-party/bootstrap-switch.min.js"></script>
        <script type="text/javascript" src="scripts/third-party/jquery-ui.js"></script>
        <script type="text/javascript" src="scripts/third-party/select.min.js"></script>
        <script type="text/javascript" src="scripts/third-party/focusif.js"></script>

        <!-- an chart start-->
        <script type="text/javascript" src="scripts/third-party/highstock.js"></script>
        <script type="text/javascript" src="scripts/third-party/highcharts-ng.min.js"></script>
        <!-- an chart end -->

        <!-- chart   -->
        <script type="text/javascript" src="scripts/third-party/angular-chart.js"></script>

        <!-- hotkeys -->
        <script type="text/javascript" src="scripts/third-party/hotkeys.min.js"></script>
        <!-- hotkeys -->
        
        <!-- chart   -->
        <script type="text/javascript" src="scripts/third-party/bootstrap.min.js"></script>

        <!-- ng-csv -->
        <script type="text/javascript" src="scripts/third-party/ng-csv.min.js"></script>
        <script type="text/javascript" src="scripts/third-party/angular-sanitize.min.js"></script>
        <!-- ng-csv -->

        <script type="text/javascript" src="scripts/third-party/isteven-multi-select.js"></script>
        <!-- metisMenu.min -->
      
        <link href="styles/thirdParty/metisMenu.min.css" rel="stylesheet" type="text/css">
        <!-- metisMenu.min -->
        
        <!-- Modules -->
        <script type="text/javascript" src="scripts/app/module.app.js"></script>
    
        <!--service -->
        <script type="text/javascript" src="scripts/app/services/apiUtility.js"></script>
        <script type="text/javascript" src="scripts/app/services/userService.js"></script>
        <script type="text/javascript" src="scripts/app/services/returnService.js"></script>
        <script type="text/javascript" src="scripts/third-party/angucomplete.js"></script>
        <script type="text/javascript" src="scripts/third-party/angular-toastr.tpls.min.js"></script>
        <script type="text/javascript" src="scripts/third-party/multiple.js"></script>
        <script type="text/javascript" src="scripts/third-party/nprogress.js"></script>
        <script type="text/javascript" src="scripts/third-party/toastr.js"></script>
        <script type="text/javascript" src="scripts/third-party/toastr.tpl.js"></script>
        <script type="text/javascript" src="scripts/third-party/angular-permission.js"></script>           
        <script type="text/javascript" src="scripts/third-party/ngDialog.min.js"></script> 
        <script type="text/javascript" src="scripts/third-party/date.js"></script> 
        <script type="text/javascript" src="scripts/third-party/bootstrap-select.min.js"></script>
        <script type="text/javascript" src="scripts/third-party/jquery.selectBox.js"></script>
        <script type="text/javascript" src="scripts/third-party/angular-growl-notifications.min.js"></script>

        <script type="text/javascript" src="scripts/third-party/jquery.stickyheader.js"></script>

        <!-- Features -->
        <script type="text/javascript" src="scripts/app/features/user/userCtrl.js"></script>
        
        <script type="text/javascript" src="scripts/app/features/customer/customerCtrl.js"></script>

        <script type="text/javascript" src="scripts/app/features/home/homeCtrl.js"></script>    
        <script type="text/javascript" src="scripts/app/features/admin/adminCtrl.js"></script>    
        <script type="text/javascript" src="scripts/app/features/products/prodCtrl.js"></script> 
        <script type="text/javascript" src="scripts/app/features/distributors/distCtrl.js"></script>   
        <script type="text/javascript" src="scripts/app/features/PurchaseOrder/poCtrl.js"></script>
        <script type="text/javascript" src="scripts/app/features/report/reportCtrl.js"></script> 
        <script type="text/javascript" src="scripts/app/features/PurchaseInvoice/piCtrl.js"></script>
        <script type="text/javascript" src="scripts/app/features/stock/stockCtrl.js"></script>
        <!-- aert.js  -->
        <script type="text/javascript" src="scripts/app/features/alert/alertCtrl.js"></script>

        <script type="text/javascript" src="scripts/app/features/bank/bankCtrl.js"></script>
        <script type="text/javascript" src="scripts/app/features/Return/returnCtrl.js"></script>
        <script type="text/javascript" src="scripts/app/directives/header.js"></script>
        <script type="text/javascript" src="scripts/app/features/userBranch/userBranchController.js"></script>
        <script type="text/javascript" src="scripts/app/features/MainCtrl.js"></script>

        <link rel="stylesheet" type="text/css" href="styles/thirdParty/bootstrap.min.css"/>
        <link href="styles/thirdParty/xeditable.css" rel="stylesheet" type="text/css">
        <link href="styles/thirdParty/angucomplete.css" rel="stylesheet" type="text/css">
        <link href="styles/thirdParty/angular-toastr.min.css" rel="stylesheet" type="text/css">
        <link href="styles/thirdParty/ngDialog.min.css" rel="stylesheet" type="text/css">
        <link href="styles/thirdParty/ngDialog-theme-default.css" rel="stylesheet" type="text/css">
        <link href="styles/thirdParty/angular-datepicker.min.css" rel="stylesheet" type="text/css">
        <link href="styles/thirdParty/search.css" rel="stylesheet" type="text/css">
        <link href="styles/thirdParty/bootstrap-multiselect.css" rel="stylesheet" type="text/css">
        <link href="styles/thirdParty/bootstrap-select.min.css" rel="stylesheet" type="text/css" >
        <link rel="stylesheet" type="text/css" href="styles/thirdParty/jquery.selectBox.css">       
        <link href="styles/thirdParty/bootstrap-switch.min.css" rel="stylesheet" type="text/css">
        <link href="styles/thirdParty/main.css" rel="stylesheet" type="text/css" >
        <link href="styles/thirdParty/stickyheader.css" rel="stylesheet" type="text/css" >
        <link href="styles/thirdParty/jquery-ui.css" rel="stylesheet" type="text/css" >
        <link href="styles/main.css" rel="stylesheet" type="text/css" >
        <!--  multiple-select -->
        <script type="text/javascript" src="scripts/third-party/bootstrap-multiselect.js"></script>
        <script type="text/javascript" src="scripts/third-party/bootstrap-multiselect-collapsible-groups.js"></script>
        
        <link href="styles/thirdParty/bootstrap-multiselect.css" rel="stylesheet" type="text/css">
        <!--  multiple-select -->
        <link rel="stylesheet" type="text/css" href="styles/thirdParty/select.min.css">
        <link rel="stylesheet" type="text/css" href="styles/thirdParty/select.css">
        <link rel="stylesheet" type="text/css" href="styles/thirdParty/selectize.default.css">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/lodash.js/2.4.1/lodash.min.js"></script> 
        <script type="text/javascript" src="scripts/third-party/angularjs-dropdown-multiselect.js"></script>
        <link href="styles/thirdParty/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="scripts/third-party/html2canvas.js"></script>
        <script type="text/javascript" src="scripts/third-party/tableExport.js"></script>
        <script type="text/javascript" src="scripts/third-party/jquery.base64.js"></script>
        <script type="text/javascript" src="scripts/third-party/jspdf/libs/sprintf.js"></script>
        <script type="text/javascript" src="scripts/third-party/jspdf/jspdf.js"></script>
        <script type="text/javascript" src="scripts/third-party/jspdf/libs/base64.js"></script>

        <!-- report -->
        <script type="text/javascript" src="scripts/third-party/d3.min.js"></script>
        <!-- report -->    
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        
        <link id="baseUrl" href="http://<?=$_SERVER['SERVER_NAME']?>:<?=$_SERVER['SERVER_PORT']?>" rel="text"/>

        <style type="text/css">
          .css-form input.ng-invalid.ng-touched {
            border-color: #FA787E;
          }

          .css-form input.ng-valid.ng-touched {
            border-color: #31bc86;
          }
          .opaque {
          opacity: 0.1 !important; }

        .standalone {
          position: relative;
          padding-top: 40px; }
        </style>

        <script>
            $(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip();   
            });
        </script>
    
    <script type="text/javascript" src="scripts/third-party/d3.min.js"></script>
<!-- report -->

         <link id="baseUrl" href="http://<?=$_SERVER['SERVER_NAME']?>:<?=$_SERVER['SERVER_PORT']?>" rel="text"/>

         <style type="text/css">
  .css-form input.ng-invalid.ng-touched {
    border-color: #FA787E;
  }

  .css-form input.ng-valid.ng-touched {
    border-color: #31bc86;
  }
  .opaque {
  opacity: 0.1 !important; }

.standalone {
  position: relative;
  padding-top: 40px; }
</style>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

    </script>
    </head>
    <body spinner spinner-name="mainBody">
        <div ui-view ng-controller="mainController">
        </div>
    </body>

</html>

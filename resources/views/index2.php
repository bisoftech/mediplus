<!DOCTYPE html>
<html>
    <head>
   
   
    <script type="text/javascript" src="scripts/build/mediplus.min.js"></script>

    <link href="tmp/absolute.min.css" rel="stylesheet" type="text/css">
<!-- report -->

            

        
         <link id="baseUrl" href="http://<?=$_SERVER['SERVER_NAME']?>:<?=$_SERVER['SERVER_PORT']?>" rel="text"/>

         <style type="text/css">
  .css-form input.ng-invalid.ng-touched {
    border-color: #FA787E;
  }

  .css-form input.ng-valid.ng-touched {
    border-color: #31bc86;
  }
  .opaque {
  opacity: 0.1 !important; }

.standalone {
  position: relative;
  padding-top: 40px; }
</style>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

    </script>
    </head>
    <body ng-app="mediplus" spinner spinner-name="mainBody">

        <div ui-view ng-controller="mainController">
        </div>

    </body>
</html>
